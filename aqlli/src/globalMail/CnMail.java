package globalMail;

public class CnMail {
	//activate account 
	protected final static String welcome          = "欢迎";
	protected final static String obj              = welcome + " | " + "激活您的帐号Aqlli";
	protected final static String activateMsg      = "点击下面的按钮来激活您的帐号Aqlli。";
	protected final static String btnActivate      = "激活";
	protected static final String settingsUrlTxt   = "账户设置";
	protected static final String followUsOn	   = "跟随我们：";
	protected final static String reasonMailReg    = "您会收到这封电子邮件是因为您订阅Aqlli。";
	//password lost
	protected final static String greetingHello    = "你好";
	protected final static String objRecoverPass   = "忘记密码 | Aqlli";
	protected final static String recoverPassMsg   = "您请求您Aqlli设置一个新密码点击按钮波纹管确认复位。";
    protected final static String btnRecoverPass   = "重置";
    protected final static String recoverPassNotRequested="如果请求不是来自你那么有人可能试图模仿你<BR/>您可以放心地忽略此电子邮件，我们建议：";
    protected final static String reasonMailRecoPass   ="你收到这封电子邮件，因为你申请一个新的密码。";
    protected final static String recoverPassPwdAdvice ="有一个强大和独特的密码不要与任何人分享同样，我们绝不会要求这些信息。";
    protected final static String recoverPassSdMail    ="设置辅助电子邮件地址，将允许您，如果您有您的主要地址的访问检索您的帐户信息。";
    //invite
    protected final static String objInvite = "您被邀请加入Aqlli。";
    protected final static String inviteMessage = "嗨，我邀请你加入Aqlli。 <BR> Aqlli是一种新的社会化媒体，我觉得interresting。 <BR>见！";
    protected final static String btnJoinHere = "加入";
    protected final static String reasonInvite = "你收到这封电子邮件是因为有人邀请您加入Aqlli。如果您不想再接收来自Aqlli.com邀请您可以unsuscribe从这个邮件列表。";
    //password update
    protected final static String objPassUpd   = "密码更新 | Aqlli";
    protected final static String passUpdMsg = "您的密码已成功更新。";
    protected final static String reasonPassUpd = "你收到这封电子邮件，因为你已经更新通过Aqlli.com您的密码。<BR/>如果你不知道我们在说什么，我们劝告立即更改您的密码。";
    //deleted user
    protected final static String reasonDelUsr = "您收到这封电子邮件是因为你已经删除了自己Aqlli帐户。";
    protected final static String objDelUsr = "帐户删除 | Aqlli";
    protected final static String msgDelUsr = "您好<br/>您的帐户已成功删除。我们很遗憾地看到你去，并希望尽快见到你。";
}
