package globalMail;

public class FrMail {
	protected final static String welcome = "Bienvenue";
	protected final static String obj = welcome
			+ " | Activez votre compte Aqlli";
	protected final static String activateMsg = "Activez votre compte Aqlli en cliquant sur le bouton ci-dessous";
	protected final static String btnActivate = "Activer";
	protected static final String settingsUrlTxt = "Page de réglages";
	protected static final String followUsOn = "Suivez nous:";
	protected final static String reasonMailReg = "Vous recevez ce courriel suite à votre inscripton à Aqlli";
	protected final static String greetingHello = "Bonjour";
	protected final static String objRecoverPass = "Mot de passe oublié | Aqlli";
	protected final static String recoverPassMsg = "Vous avez demandé la réinitialisation du mot de passe de votre compte Aqlli.<br/> Cliquez sur le bouton ci-dessous pour le changer.";
	protected final static String btnRecoverPass = "Nouveau mot de passe";
	protected final static String recoverPassNotRequested = "Si vous n'avez pas demander cette réinitialisation, il est fort probable qu'une personne essaie de se faire passer pour vous.<br/> Vous pouvez ignorer ce message en toute sécurité. Nous vous recommandons toutefois de :";
	protected final static String reasonMailRecoPass = "Vous recevez ce message car vous avez demandé la réinitialisation de votre mot de passe.";
	protected final static String recoverPassPwdAdvice = "Choissir un mot de passe fort et ne le partagez avec personne. Nous ne vous demanderons jamais cette information.";
	protected final static String recoverPassSdMail = "Configurer une adresse de messagerie secondaire. Elle vous permettra de récupérer vos informations de compte au cas où vous n'avez plus accès à l'adresse actuelle.";
	protected final static String objInvite = "Vous êtes invité à rejoindre Aqlli";
	protected final static String inviteMessage = "Salut, Je t'invite à rejoindre Aqlli. <br> Aqlli est un nouveau média social que je trouve captivant.";
	protected final static String btnJoinHere = "Rejoindre";
	protected final static String reasonInvite = "Vous recevez ce message car vous avez été invité à rejoindre Aqlli par une personne. Si vous ne souhaitez plus recevoir de messages d'invitation, vous pouvez vous désinscrire de cette liste de diffusion.";
	//password update
    protected final static String objPassUpd    = "Mise à jour de mot de passe | Aqlli";
    protected final static String passUpdMsg    = "Votre mot de passe à bien été mis à jour.";
    protected final static String reasonPassUpd = "Vous recevez cet email car vous avez mis à jour votre mot de passe via Aqlli.com. <br/>Si vous ne savez pas de quoi nous parlons, nous conseillons de changer votre mot de passe maintenant.";
    //deleted user
    protected final static String reasonDelUsr = "Vous recevez cet email car vous avez supprimé votre compte Aqlli.";
    protected final static String objDelUsr = "Compte supprimé | Aqlli";
    protected final static String msgDelUsr = "Bonjour <br/> Votre compte Aqlli a bien été supprimé. Nous sommes navré de vous voir partir et espérons vous revoir bientôt.";
}
