package globalMail;

public class EnMail {
	//activate account
	protected static final String welcome 	 	     = "Welcome";
	protected static final String obj 			     = welcome + " | " + "Activate your Aqlli account";
	protected static final String activateMsg        = "Click the button below to activate your Aqlli account.";
	protected static final String btnActivate        = "Activate";
	protected static final String settingsUrlTxt     = "Account settings";
	protected static final String followUsOn	     = "Follow us on:";
	protected final static String reasonMailReg      = "You receive this email because you subscribed to Aqlli";
	//password lost
	protected final static String greetingHello      = "Hello";
	protected final static String objRecoverPass     = "Forgotten Password | Aqlli";
	protected final static String recoverPassMsg     = "You have requested a new password for your Aqlli account. Click the button bellow to confirm your reset.";
	protected final static String btnRecoverPass     = "Reset";
	protected final static String recoverPassNotRequested    = "If the request does not come from you then someone may be trying to impersonate you.<br/> You can safely ignore this email and we advice to:";
	protected final static String reasonMailRecoPass     	 = "You receive this email because you requested a new password.";
	protected final static String recoverPassPwdAdvice   	 = "Have a strong and unique password. Do not share it with anyone. Also, we will never ask for this information.";
	protected final static String recoverPassSdMail      	 = "Set up a secondary email address. It will allow you to retrieve your account information if you have no access to your primary address.";
	//invite
	protected final static String objInvite     		 	 = "You are invited to join Aqlli";
	protected final static String inviteMessage     	 	 = "Hi, I Invite you to join Aqlli. <br> Aqlli is a new Social Media that I find interresting. <br>See you there!";
	protected final static String btnJoinHere   	 		 = "Join";
	protected final static String reasonInvite      	 	 = "You receive this email because someone invited you to join Aqlli. If you do not want to receive anymore invites from Aqlli.com you can unsubscribe from this mailing list.";
	//password update
    protected final static String objPassUpd   = "Password update | Aqlli";
    protected final static String passUpdMsg = "Your password has been successfully updated.";
    protected final static String reasonPassUpd = "You receive this email because you have updated your password via Aqlli.com. <br/> If you do not know what we are talking about, we advice to change your password now.";
  //deleted user
    protected static final String reasonDelUsr = "You are receiving this email because you've deleted your account Aqlli.";
    protected static final String objDelUsr = "Account Deleted | Aqlli";
    protected static final String msgDelUsr = "Hello <br/> Your account has been successfully deleted. We are sorry to see you go and hope to see you soon.";
}
