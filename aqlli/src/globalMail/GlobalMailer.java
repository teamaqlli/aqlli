package globalMail;

import java.util.ArrayList;
import java.util.List;

public class GlobalMailer extends AGlobalMailer{
    List<String> msg;
	@Override
	protected List<String> mEng() {
		msg = new ArrayList<String>();
		msg.add( EnMail.obj);  /*0*/
		msg.add( EnMail.welcome);  /*1*/
		msg.add( EnMail.activateMsg);  /*2*/
		msg.add( EnMail.btnActivate);  /*3*/
		msg.add( EnMail.settingsUrlTxt); /*4*/
		msg.add( EnMail.followUsOn); /*5*/
		msg.add( EnMail.reasonMailReg); /*6*/
		msg.add( EnMail.greetingHello); /*7*/
		msg.add( EnMail.objRecoverPass); /*8*/
		msg.add( EnMail.recoverPassMsg); /*9*/
		msg.add( EnMail.btnRecoverPass); /*10*/
		msg.add( EnMail.recoverPassNotRequested); /*11*/
		msg.add( EnMail.reasonMailRecoPass); /*12*/
		msg.add( EnMail.recoverPassPwdAdvice); /*13*/
		msg.add( EnMail.recoverPassSdMail); /*14*/
		msg.add( EnMail.objInvite); /*15*/
		msg.add( EnMail.inviteMessage); /*16*/
		msg.add( EnMail.btnJoinHere); /*17*/
		msg.add( EnMail.reasonInvite); /*18*/
		msg.add( EnMail.objPassUpd); /*19*/
		msg.add( EnMail.passUpdMsg); /*20*/
		msg.add( EnMail.reasonPassUpd); /*21*/
		msg.add( EnMail.reasonDelUsr);/*22*/
		msg.add( EnMail.objDelUsr);/*23*/
		msg.add( EnMail.msgDelUsr);/*24*/
		return msg;
	}

	@Override
	protected List<String> mFr() {
		msg = new ArrayList<String>();
		msg.add( FrMail.obj);
		msg.add( FrMail.welcome);
		msg.add( FrMail.activateMsg);
		msg.add( FrMail.btnActivate);
		msg.add( FrMail.settingsUrlTxt);
		msg.add( FrMail.followUsOn);
		msg.add( FrMail.reasonMailReg);
		msg.add( FrMail.greetingHello);
		msg.add( FrMail.objRecoverPass);
		msg.add( FrMail.recoverPassMsg);
		msg.add( FrMail.btnRecoverPass);
		msg.add( FrMail.recoverPassNotRequested);
		msg.add( FrMail.reasonMailRecoPass);
		msg.add( FrMail.recoverPassPwdAdvice);
		msg.add( FrMail.recoverPassSdMail);
		msg.add( FrMail.objInvite);
		msg.add( FrMail.inviteMessage);
		msg.add( FrMail.btnJoinHere);
		msg.add( FrMail.reasonInvite);
		msg.add( FrMail.objPassUpd); 
		msg.add( FrMail.passUpdMsg); 
		msg.add( FrMail.reasonPassUpd);
		msg.add( FrMail.reasonDelUsr);
		msg.add( FrMail.objDelUsr);
		msg.add( FrMail.msgDelUsr);
		return msg;
	}

	@Override
	protected List<String> mCn() {
		msg = new ArrayList<String>();
		msg.add( CnMail.obj);
		msg.add( CnMail.welcome);
		msg.add( CnMail.activateMsg);
		msg.add( CnMail.btnActivate);
		msg.add( CnMail.settingsUrlTxt);
		msg.add( CnMail.followUsOn);
		msg.add( CnMail.reasonMailReg);
		msg.add( CnMail.greetingHello);
		msg.add( CnMail.objRecoverPass);
		msg.add( CnMail.recoverPassMsg);
		msg.add( CnMail.btnRecoverPass);
		msg.add( CnMail.recoverPassNotRequested);
		msg.add( CnMail.reasonMailRecoPass);
		msg.add( CnMail.recoverPassPwdAdvice);
		msg.add( CnMail.recoverPassSdMail);
		msg.add( CnMail.objInvite);
		msg.add( CnMail.inviteMessage);
		msg.add( CnMail.btnJoinHere);
		msg.add( CnMail.reasonInvite);
		msg.add( CnMail.objPassUpd); 
		msg.add( CnMail.passUpdMsg); 
		msg.add( CnMail.reasonPassUpd);
		msg.add( CnMail.reasonDelUsr);
		msg.add( CnMail.objDelUsr);
		msg.add( CnMail.msgDelUsr);
		return msg;
	}
	
	public List<String> getInternationalizedContent( String lang){
		List<String> res = new ArrayList<String>();
		final String regex ="([_*])\\w+"; //toute chaine formée de la sorte xxx_YYY, le _YYY est retiré
		lang = lang.replaceAll(regex, "");
		switch( lang){
		case "fr" :
			res = mFr();
			break;
		case "en" :
			res = mEng();
			break;
		case "cn" : 
			res = mCn();
		default:
			res = mEng();
		}
		return res;
	}
}
