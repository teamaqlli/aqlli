package globalMail;

import java.util.List;

public abstract class AGlobalMailer {
  protected abstract List< String> mEng();
  protected abstract List<String>  mFr();
  protected abstract List<String>  mCn();
}
