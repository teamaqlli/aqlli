package com.aqlli.main.client.controller;

import java.text.ParseException;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.aqlli.main.client.model.UserDetails;
import com.aqlli.main.client.model.UserInfosAndSettings;
import com.aqlli.main.client.model.VerificationToken;
import com.aqlli.main.repositories.VerificationTokenRepository;
import com.aqlli.main.service.AqlliOfyService;
import com.aqlli.main.service.UserService;
import com.aqlli.main.shared.Endpoints;
import com.aqlli.main.shared.exceptions.EmailExistsException;
import com.aqlli.main.shared.exceptions.EmailInvalideException;
import com.aqlli.main.shared.exceptions.PasswordNotMatchingException;
import com.aqlli.main.shared.exceptions.UserTermsStateException;
import com.aqlli.main.shared.security.ServerSideUserValidator;
import com.aqlli.main.shared.util.GetCookieVal;
/**
 * @author Arix OWAYE / twitter : @arixowaye
 *
 */
@Controller
public class RegistrationController {
	RegistrationListener regLi           = new RegistrationListener();
	VerificationToken verificationToken  = null;
	Logger log                           = Logger.getLogger("global");
	private UserService userService      = new UserService();
	private String appUrl = "";
	private String lang = "";
	private VerificationTokenRepository tokenService = new VerificationTokenRepository();
	ServerSideUserValidator serverSideUserValidator;
	GetCookieVal cookieVal               = new GetCookieVal();
	String GWT_LOCALE                    = "GWT_LOCALE";
	ModelAndView model;
	private boolean passNotMatching 	 = false;
    /**
     * @return the registration view
     */
	@RequestMapping(value = Endpoints.REGISTRATION, method = RequestMethod.GET)
	public ModelAndView showRegistrationForm( @RequestParam(value = "invite", required = false) String inviteToken, HttpServletRequest request, HttpServletResponse response) {
		model = new ModelAndView();
		// check if user is logged in, if yes return to home page controller
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			model.setViewName("redirect:" + Endpoints.HOME);
			return model;
		}
		String isInviteTokenValid = "invalid";
		if (( inviteToken == null) || ( inviteToken.trim() == "")){
			isInviteTokenValid = "invalid";
		}else{
			VerificationToken invitoToUseToken = userService.getInvitationToken(inviteToken.trim());
			if (( invitoToUseToken != null) && (!invitoToUseToken.hasExpired()) || inviteToken.equalsIgnoreCase("jesuslovesyou"))
			{
				isInviteTokenValid = "valid";
			}
		}
		
		UserDetails user = new UserDetails();
		model.addObject("isInviteTokenValid", isInviteTokenValid);
		model.getModelMap().addAttribute("user", user); // binding the user
		model.setViewName("register");
		return model;
	}
	
	@RequestMapping(value =  Endpoints.REGHELP, method = RequestMethod.GET)
	public ModelAndView showRegistrationHelp() {
		model = new ModelAndView();
		model.setViewName("registerHelp");
		return model;
	}

	@RequestMapping(value = Endpoints.REGISTRATION, method = RequestMethod.POST)
	public ModelAndView register(
			@RequestParam(value = "invite", required = false) String inviteToken, @ModelAttribute("user") @Valid UserDetails account,
			BindingResult result, HttpServletRequest request,
			HttpServletResponse response) {
		/* Validation of email, passwords and userTerms fields */
		model = new ModelAndView();
		boolean emailInvalid = false;
		// check if user is logged in, if yes return to login page controller
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			model.setViewName("redirect:" + Endpoints.HOME);
			return model;
		}
		serverSideUserValidator = new ServerSideUserValidator( account);
		try {
			serverSideUserValidator.isEmailValid();
		} catch (EmailInvalideException e1) {
			result.rejectValue("email", "emailInvalid");
			emailInvalid = true;
		}
		try {
			serverSideUserValidator.isPassValid();
		} catch (PasswordNotMatchingException e1) {
			result.rejectValue("matchingPassword", "passwordsMustMatch");
			passNotMatching  = true;
		}
		try {
			serverSideUserValidator.isUseTermsValid();
		} catch (UserTermsStateException e1) {
			result.rejectValue("useTerms", "mustAgreeUseTerms");
		}
		try {
			serverSideUserValidator.isBirthDayValid();
		} catch (ParseException e1) {
			result.rejectValue( "birthDay","birthDayInvalid");
		}
		UserDetails registered = new UserDetails();

		if (!result.hasErrors()) {
			registered = createUserAccount(account, result);
		}

		if (result.hasErrors()) {
			System.out
					.println( result.getErrorCount() + " " + result.toString());
			
			model = new ModelAndView("register", "user", account);
			if ( emailInvalid)
				model.addObject("emailRejected", "invalidMail");
			if( passNotMatching)
			    model.addObject("passNotEq", "passwordsMustMatch");
			return model;
		} else {
			if (registered != null) {
				try{
					userService.SaveRegisteredUser( registered);
					 }catch( Exception e){
						 e.printStackTrace();
						 model = new ModelAndView("register", "user", account );
						 return model;
					}
				try {
					appUrl = request.getScheme() + "://" + request.getServerName();//http(s)://server
					lang = cookieVal.getCookie( GWT_LOCALE, request);//internationalization cookie
					if ( lang.trim() == "") lang = "en"; //fallback to default
					regLi.confirmReg(registered, appUrl, lang);//registration confirmation process
				} catch (Exception e) {
					e.printStackTrace();
					model = new ModelAndView("register", "user", account);
					return model;
				}
			} else {
				result.rejectValue("email", "userExists");
			    model = new ModelAndView("register", "user", account);
				return model;
			}
			//success
			model = new ModelAndView("registrationStatus", "user", account);
			final String registrationSuccess = "registrationSuccess";
			request.setAttribute("userForMail", registered);
			model.addObject(registrationSuccess, "registrationSuccess");
			//We delete invitationToUse
			if (( inviteToken != null) && !inviteToken.trim().equals("")){
			  VerificationToken inviteToUse = userService.getInvitationToken(inviteToken);
			  tokenService.deleteToken( inviteToUse);
			}
			
			return model;
		}
	}
	/**
	 * @param userdto User data Object
	 * @param result 
	 * @param request
	 * @param response
	 * @return registrationStatus model
	 */
	@RequestMapping(value = Endpoints.CONFIRMREGNEWTOKEN, method = RequestMethod.POST)
	public ModelAndView registerNewToken(@ModelAttribute("userForMail") UserDetails userdto,
			BindingResult result, HttpServletRequest request,
			HttpServletResponse response) {
		model = new ModelAndView();
		// checks if user is logged in, if yes return to home page controller
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			model.setViewName("redirect:" + Endpoints.HOME);
			return model;
		}
		model.setViewName("registrationStatus");
		appUrl = request.getScheme() + "://" + request.getServerName();//http(s)://server; en locale rajouter le port
		lang = cookieVal.getCookie( GWT_LOCALE, request);//internationalization cookie
		if ( lang.trim() == "") lang = "en"; //fallback to default
		regLi.sendNewVerificationToken( userdto, appUrl, lang);//registration confirmation process
		return model;
	}
	
	/**
	 * 
	 * @param modelUi
	 * @param token : unik value which has been generated when the user registered
	 * @param expired this is set if the validation token expired
	 * @param request
	 * @param response
	 * @return the modelandview
	 */
	@RequestMapping( value = Endpoints.CONFIRMREG, method = RequestMethod.GET)
	public ModelAndView showRegistrationForm( Model modelUi, @RequestParam(value ="token", required = false) String token, @RequestParam(value = "expired", required = false) String expired, HttpServletRequest request, HttpServletResponse response){
		// check if user is logged in, if yes return to login page controller
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		UserDetails userForMail = new UserDetails();
		model = new ModelAndView();
		//if the user is already connected, redirect
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			model.setViewName("redirect:" + Endpoints.HOME);
			return model;
		}
		//token
		if (token != null) {
		  try{
		    verificationToken = userService.getVerificationToken( token);
		  }catch( Exception e){
		    e.printStackTrace();
		  }
		  if( verificationToken == null){
		    String message = "invalid";
			model.addObject( "nullToken", message);
			model.setViewName( "registrationStatus");
			return model;
		  }
		  UserDetails user = verificationToken.getUser();
		  if( verificationToken.hasExpired()) {
			model.addObject( "mail", user.getEmail());
			model.setViewName( "redirect:" + Endpoints.CONFIRMREGEXPIRED);
		    return model;
		  } 
		  //enabling user and saving the enabled user
		  try{
		    if( user.isEnabled()){
		      model.setViewName( "redirect:" + Endpoints.LOGIN);
		    }else{
		      user.setEnabled( true);
			  userService.SaveRegisteredUser( user);
		    }
		  }catch ( Exception e){
		    e.printStackTrace();
		  }
		  try{
		    //utilisateur validé; suppression du token
		    tokenService.deleteToken(verificationToken);
		    model.setViewName("redirect:" + Endpoints.LOGIN);
			return model;
		  }catch( Exception e){
		    final String message = "unknownToken";
			model.addObject("unknownToken", message);//error
			e.printStackTrace();
			return model;
		  }
		}else if (expired != null) {//expired
		  final String message = "expired";
		  model.addObject("mail", request.getParameter("mail"));
		  model.setViewName( "registrationStatus");
		  model.addObject("confirmTokenExpired", message);
		  request.setAttribute("userForMail", userForMail);
		  return model;
		}else{
		  //no params
		  model.setViewName("redirect:" + Endpoints.LOGIN);
		  return model;
		}
	}
	
	private UserDetails createUserAccount( UserDetails accountDto,
			BindingResult result) {
		UserDetails registered = null;
		try {
			registered = userService.registerNewUserAccount( accountDto);
			if( registered != null){
	 	        UserInfosAndSettings uis = new UserInfosAndSettings();
	 	        uis.setUserId( registered.getUniqueID());
	 	        uis.setAccountState( uis.getAccountStates()[1]);
	 	        uis.setAccountType( uis.getAccountTypes()[0]);
	 	        uis.setNbAccountViews( (long) 0);
	 	        AqlliOfyService.ofy().save().entities( uis).now();
			}
		} catch ( NullPointerException e) {
			return null;
		} catch ( EmailExistsException e) {
			return null;
		}
		return registered;
	}

}
