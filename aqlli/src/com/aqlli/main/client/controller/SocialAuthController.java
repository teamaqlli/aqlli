package com.aqlli.main.client.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.brickred.socialauth.AuthProvider;
import org.brickred.socialauth.Contact;
import org.brickred.socialauth.SocialAuthManager;
import org.brickred.socialauth.spring.bean.SocialAuthTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.aqlli.main.shared.Endpoints;

@Controller
public class SocialAuthController {
	
	@Autowired(required=false)
	private SocialAuthTemplate socialAuthTemplate;
	
	@RequestMapping(value = Endpoints.SOCIALAUTHSUCCESS)
    public ModelAndView getRedirectURL(final HttpServletRequest request)
                    throws Exception {
            ModelAndView mv = new ModelAndView();
            List<Contact> contactsList = new ArrayList<Contact>();
            SocialAuthManager manager = socialAuthTemplate.getSocialAuthManager();
            AuthProvider provider = manager.getCurrentAuthProvider();
            contactsList = provider.getContactList();
            if (contactsList != null && contactsList.size() > 0) {
                    for (Contact p : contactsList) {
                            if (!StringUtils.hasLength(p.getFirstName())
                                            && !StringUtils.hasLength(p.getLastName())) {
                                    p.setFirstName(p.getDisplayName());
                            }
                    }
            }
            mv.addObject("profile", provider.getUserProfile());
            mv.addObject("contacts", contactsList);
            mv.setViewName("inviteFriend");

            return mv;
    }
	@RequestMapping(value = Endpoints.SOCIALAUTHDENIED)
    public ModelAndView authAccessDenied(final HttpServletRequest request)
                    throws Exception {
            ModelAndView mv = new ModelAndView();
            mv.setViewName("socialAuthAccessDenied");
            return mv;
    }


}
