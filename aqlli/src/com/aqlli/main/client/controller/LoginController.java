package com.aqlli.main.client.controller;

import globalMail.GlobalMailer;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.aqlli.main.client.model.UserDetails;
import com.aqlli.main.client.model.VerificationToken;
import com.aqlli.main.repositories.VerificationTokenRepository;
import com.aqlli.main.service.AqlliMailerService;
import com.aqlli.main.service.UserService;
import com.aqlli.main.shared.Endpoints;
import com.aqlli.main.shared.exceptions.UserNotEnabledException;
import com.aqlli.main.shared.exceptions.UserNotFoundException;
import com.aqlli.main.shared.security.PasswordRecoverImpl;
import com.aqlli.main.shared.util.Consts;
import com.aqlli.main.shared.util.EmailValidator;
import com.aqlli.main.shared.util.GetCookieVal;

/**
 * @author Arix OWAYE / twitter : @arixowaye
 *
 */
@Controller
public class LoginController {
        private ModelAndView model;
        private PasswordRecoverImpl recoverPass;
        private UserService userService = new UserService();
        private VerificationTokenRepository tokenRepository = new VerificationTokenRepository();
        private String[] resetPassConsts = {"lostPass", "passwordResetSent", "passwordResetSendError"};
		private AqlliMailerService mailerService = new AqlliMailerService();
        
		@RequestMapping(value = Endpoints.LOGIN, method = RequestMethod.GET)
		public ModelAndView login( HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout, @RequestParam(value = "pwd", required = false) String pwd) {
	 
			model = new ModelAndView();
			/*
			 * */
			//Testing if the user is already authenticated
			//If Yes, he is redirected to the home page
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			if( !(auth instanceof AnonymousAuthenticationToken)){
				model.setViewName("redirect:" + Endpoints.HOME);
				return model;
			}
			
			if (error != null) {
				model.addObject("error", "invalidUsernameAndPassword");
				return model;
			}
	 
			if (logout != null) {
				model.addObject("msg", "LoggedOutSuccessfully");
				return model;
			}
			if (pwd != null) {
				model.addObject("msg", "pwdupdok");
				return model;
			}
			
			model.setViewName("login");
			return model;
	 
		}
		
		@RequestMapping(value = Endpoints.LOGINHELP, method = RequestMethod.GET)
		public ModelAndView showLoginHelp( HttpServletRequest request, HttpServletResponse response) {
			model = new ModelAndView( );
			userService.addUserToRequest( model, request);
			model.setViewName("loginHelp");
			return model;
		}
		
		@RolesAllowed("ROLE_ANONYMOUS")
		@RequestMapping( value = Endpoints.PASSLOST, method = RequestMethod.POST, produces = "application/json", consumes = MediaType.APPLICATION_JSON_VALUE, headers="Accept=application/json")
		@ResponseBody
		public String passwordLostRequest( @RequestBody String jsonEmail, 
				BindingResult result, HttpServletRequest request){
			recoverPass = new PasswordRecoverImpl();
			JSONObject jsonObj = new JSONObject();
			EmailValidator emailValidator = new EmailValidator();
			boolean res = false;
			boolean isEmailValid = false;
			String email = "";
			String appUrl = request.getScheme() + "://" + request.getServerName();//http(s)://server
			String lang = new GetCookieVal().getCookie( "GWT_LOCALE", request);//internationalization cookie
			if ( lang.trim() == "") lang = "en"; //fallback to default
			//validating email
			try {
				JSONObject convertEmail = new JSONObject( jsonEmail);
				email = convertEmail.getString("email");
				isEmailValid = emailValidator.isValid( email);
				if ( !isEmailValid){
				  jsonObj.append("isEmailValid", "false");
				  return jsonObj.toString();
			  }
			} catch (JSONException ex) {
					ex.printStackTrace();
			}	
			try {
				VerificationToken token = recoverPass.lostPasswordChange( email);
				if ( token != null){
					List<String> content = new ArrayList<String>();
					content = new GlobalMailer().getInternationalizedContent( lang);
					String aqlliDefaultUrl  = appUrl;
					String settingsUrlTxt   = content.get( 4);
					String followUsOn	    = content.get( 5);
					String subject          = content.get( 8);
					String welcomeBackUser  = content.get( 7);
					String introPhrase      = content.get( 9);
					String btnChangePass    = content.get( 10);
					String msgNotRequested  = content.get( 11);
					String messageAim       = content.get( 12);
					String advicePwd        = content.get( 13);
					String adviceSecondMail = content.get( 14);
					String changePassUrl = appUrl +  Endpoints.PASSLOST + "?token="
							+ token.getToken();
					String settingsUrl     = appUrl + Endpoints.SETTINGS;
					if( email != "" && email != null && subject != "");
					  res = mailerService.sendRecoverPass(email, subject, aqlliDefaultUrl, changePassUrl, welcomeBackUser, introPhrase, btnChangePass, msgNotRequested, advicePwd, adviceSecondMail, followUsOn, Consts.fb, Consts.tw, Consts.gp, settingsUrl, settingsUrlTxt, messageAim);
				}
			} catch (UserNotFoundException ex) {
				ex.printStackTrace();
			} catch (UserNotEnabledException ex) {
				ex.printStackTrace();
			}
			//returned json object
			try {
			  jsonObj.append("email", email);
			  if( res){
		        jsonObj.append(resetPassConsts[0], resetPassConsts[1]);
		        request.setAttribute(resetPassConsts[0], resetPassConsts[2]);
			  }else{
				request.setAttribute(resetPassConsts[0], resetPassConsts[2]);
				jsonObj.append(resetPassConsts[0], resetPassConsts[2]);
			  }
			} catch (JSONException ex) {
			  ex.printStackTrace();
			}
			return jsonObj.toString();
		}
		
		@RolesAllowed("ROLE_ANONYMOUS")
		@RequestMapping( value = Endpoints.PASSLOST, method = RequestMethod.GET)
		public ModelAndView changePasswordLost( @RequestParam(value = "token", required = false)  String token, HttpServletRequest request){
			model = new ModelAndView();
			model.setViewName("changepasswordform");
			if (( token == null) || ( token.trim() == "")){
				model.addObject("nullToken", "tokenIsNull");
				return model;
			}else{
				VerificationToken vToken = tokenRepository.findByToken( token);
				if (( vToken == null) /*|| vToken.hasExpired()*/){
					model.addObject("nullToken", "tokenIsNull");
					return model;
				}
				model.addObject("token", vToken.getToken());
				model.addObject("mail", vToken.getUser().getEmail());
				model.addObject("nullToken", "tokenNotNull");
			}
			return model;
		}
		
		/**
		 * @param credentialJson credential entered by the user to be validated
		 * @param result
		 * @param request
		 * @return Json response
		 */
		@RolesAllowed("ROLE_ANONYMOUS")
		@RequestMapping( value = Endpoints.CHANGEPASS, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, headers="Accept=application/json")
		public ModelAndView changeLostPassRequest( @RequestBody String credentialJson, 
				BindingResult result, HttpServletRequest request, HttpServletResponse response){
			recoverPass = new PasswordRecoverImpl();
			String p1 = "", p2 = "", email = "", token = "";
			boolean res = false;
			VerificationToken vToken = null;
			model = new ModelAndView();
			//validating email
			try {
				JSONObject credential = new JSONObject( credentialJson);
				p1    = credential.getString("newpassword");
				p2    = credential.getString("repeatnewpassword");
				email = credential.getString("email");
				token = credential.getString("token");
				//verifier que le token peut etre utilisé
				//verifier que le token est bien lié au mail indiqué
				vToken = tokenRepository.findByToken( token);
				if ( !p1.trim().equals( p2.trim()) ){
				  model.addObject("arePassEqual", "false");
				  return model;
			    }
				if ( email.trim().isEmpty() || token.trim().isEmpty() || ( vToken == null) /*|| vToken.hasExpired()*/ || ( ( vToken != null) && !( email.equalsIgnoreCase( vToken.getUser().getEmail())))){
				  model.addObject("isEmailOrTokenValide", "false");
				  model.addObject("nullToken", "tokenIsNull");
				  return model;
				}
			} catch (JSONException ex) {
					ex.printStackTrace();
			}	
			String appUrl = request.getScheme() + "://" + request.getServerName();//http(s)://server
			String lang = new GetCookieVal().getCookie( "GWT_LOCALE", request);//internationalization cookie
			if ( lang.trim() == "") lang = "en"; //fallback to default
			UserDetails user = vToken.getUser();
			res = recoverPass.resetPassword(p1, user);
			if (( user != null) && ( res)){
				tokenRepository.deleteToken(vToken);
				//send email to notify the user
				List<String> content = new ArrayList<String>();
				content = new GlobalMailer().getInternationalizedContent( lang);
				String aqlliDefaultUrl  = appUrl;
				String settingsUrlTxt   = content.get( 4);
				String followUsOn	    = content.get( 5);
				String subject          = content.get( 19);
				String welcomeBackUser  = content.get( 7);
				String introPhrase      = content.get( 20);
				String messageAim       = content.get( 21);
				String settingsUrl     = appUrl + Endpoints.SETTINGS;
				if( email != "" && email != null && subject != "")
				  mailerService.sendPassChangedNotif(user.getEmail(), subject, aqlliDefaultUrl, welcomeBackUser, introPhrase, followUsOn, Consts.fb, Consts.tw, Consts.gp, settingsUrl, settingsUrlTxt, messageAim);
			}
			model.addObject("passUpd", "success");
			return model;
		}
		
		@RolesAllowed("ROLE_USER")
		@RequestMapping( value = Endpoints.SETNEWPASS, method = RequestMethod.POST)
		@ResponseBody
		public String resetPassword( @RequestBody String credentialJson, HttpServletRequest request, HttpServletResponse response){
			boolean res = false;
			recoverPass = new PasswordRecoverImpl();
			JSONObject jsonObj = null;
			JSONObject result = new JSONObject();
			UserDetails user = userService.getUserFromRequest(request);
			try {
			  jsonObj = new JSONObject( credentialJson);
			  String oldPass = jsonObj.getString( "p1"); //old password
			  String newPass = jsonObj.getString( "p2"); //new password
			  
			  if( oldPass.isEmpty() || newPass.isEmpty()){
				  result.append("resPass", "passEmpty");
				  return result.toString();
			  }
			  res = recoverPass.resetPassword( oldPass, newPass, user);
			  if ( res){
			    String appUrl = request.getScheme() + "://" + request.getServerName();//http(s)://server
				String lang = new GetCookieVal().getCookie( "GWT_LOCALE", request);//internationalization cookie
				if ( lang.trim() == "") lang = "en"; //fallback to default
				List<String> content = new ArrayList<String>();
				content = new GlobalMailer().getInternationalizedContent( lang);
				String aqlliDefaultUrl  = appUrl;
				String settingsUrlTxt   = content.get( 4);
				String followUsOn	    = content.get( 5);
				String subject          = content.get( 19);
				String welcomeBackUser  = content.get( 7);
				String introPhrase      = content.get( 20);
				String messageAim       = content.get( 21);
				String settingsUrl      = appUrl + Endpoints.SETTINGS;
				mailerService.sendPassChangedNotif(user.getEmail(), subject, aqlliDefaultUrl, welcomeBackUser, introPhrase, followUsOn, Consts.fb, Consts.tw, Consts.gp, settingsUrl, settingsUrlTxt, messageAim);
				result.append("resPass", "passUpdated");
			  }else{
				result.append("resPass", "passResetError");
			  }
			} catch (JSONException ex) {
				ex.printStackTrace();
			}
			return result.toString();
		}
}
