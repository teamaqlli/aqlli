package com.aqlli.main.client.controller;

import globalMail.GlobalMailer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.HtmlUtils;

import com.aqlli.main.client.model.AqNotification;
import com.aqlli.main.client.model.AqlliPost;
import com.aqlli.main.client.model.ContactUs;
import com.aqlli.main.client.model.ReportABug;
import com.aqlli.main.client.model.UserDetails;
import com.aqlli.main.client.model.UserInfosAndSettings;
import com.aqlli.main.service.AqlliMailerService;
import com.aqlli.main.service.AqlliOfyService;
import com.aqlli.main.service.InviteFriendService;
import com.aqlli.main.service.UserService;
import com.aqlli.main.service.WallService;
import com.aqlli.main.shared.Endpoints;
import com.aqlli.main.shared.exceptions.UserNotFoundException;
import com.aqlli.main.shared.util.AqlliUserStats;
import com.aqlli.main.shared.util.CityInfos;
import com.aqlli.main.shared.util.Consts;
import com.aqlli.main.shared.util.EmailValidator;
import com.aqlli.main.shared.util.GetCookieVal;
import com.aqlli.main.shared.util.StringHashGenerator;
import com.google.gwt.http.client.Response;
@Controller
public class AqlliController {
	UserService userService 	   = new UserService();
	WallService wallService 	   = new WallService();
	AqlliMailerService mailService = new AqlliMailerService();
	AqlliUserStats aqUserStats 	   = new AqlliUserStats();
	InviteFriendService inviteFriendService = new InviteFriendService();
	ModelAndView model;
	
	/**
	 * @param user binded user
	 * @param result
	 * @param request
	 * @param response
	 * @return user home page
	 */
	@RequestMapping(value = { Endpoints.HOME }, method = RequestMethod.GET)
	public ModelAndView welcomePage( @ModelAttribute("user") UserDetails user, 
			BindingResult result, HttpServletRequest request,
			HttpServletResponse response) {
 
		model = new ModelAndView();
		JSONObject citydatajson = null;
		userService.addUserToRequest( model, request);
		
		UserDetails connecteduser = userService.getUserFromRequest( request);
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		if (auth instanceof AnonymousAuthenticationToken) {
		  model.setViewName("redirect:/");
		  return model;
		}
		
		//adding various variables to the view
		if( connecteduser != null){ //the user is authenticated
		  
		  String nbViews = aqUserStats.getFormattedNumberOfPageViews( connecteduser);
		  model.addObject( "nbViews", nbViews);
		
		  String nbFollowers = aqUserStats.getFormattedNumberOfFollowers(connecteduser);
		  model.addObject( "nbFollowers", nbFollowers);
		  
		  String nbCities = aqUserStats.getReadersNumberOfUniqueCities( connecteduser);
		  model.addObject( "nbUniqueCities", nbCities);
		  
		  try {
			citydatajson = new JSONObject( connecteduser.getLocation());
			//map variables
			String data_map_country  = citydatajson.getString( "country");
			String data_map_lat		 = citydatajson.getString( "latitude");
			String data_map_lng	  	 = citydatajson.getString( "longitude");
			String data_map_city     = citydatajson.getString( "name");
			String data_map_show     = (data_map_city == null || data_map_city.trim() == "") ? "false" : "true"; 
			model.addObject( "data_map_show", data_map_show);
			model.addObject( "data_map_city", data_map_city);
			model.addObject( "data_map_country", data_map_country);
			model.addObject( "data_map_lat", data_map_lat);
			model.addObject( "data_map_lng", data_map_lng);
		  } catch (JSONException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		  }
		  String data_map_usrname  = connecteduser.getFullName();
		  model.addObject( "data_map_usrname", data_map_usrname);  
			
		  String  data_map_usrid   = connecteduser.getUniqueID();
		  model.addObject( "data_map_usrid", data_map_usrid);
		}
		model.setViewName( "home");
		return model;
	}
	
	/**
	 * @param userId unik user ID
	 * @param request
	 * @param response
	 * @return  user profile page
	 */
	@RequestMapping(value = { Endpoints.USERS + "/{userId}" }, method = RequestMethod.GET)
	public ModelAndView displayUserPage( @PathVariable String userId, HttpServletRequest request,
			HttpServletResponse response) {
		
		model = new ModelAndView();
		model.setViewName( "perso");
		AqlliUserStats aqStats = new AqlliUserStats();
		UserDetails authUser = null;
		JSONObject citydatajson = null;
		try{
			authUser = userService.addUserToRequest( model, request);
		}catch( Exception e){/*ignored*/}
		//user page from the url
		try {
			if (( userId == null) || ( userId.trim() == "")) throw new NullPointerException();
			UserDetails userFromUrl = userService.getUserByUniqueID( userId);
			UserInfosAndSettings userSettings = AqlliOfyService.ofy().load().type(UserInfosAndSettings.class).id(userId).now();
			if( authUser != null && userFromUrl != null){
				boolean isUserAReader = userService.isUserAReader( authUser, userFromUrl);
				model.addObject( "isUserAReader", isUserAReader);
			}
			if( userSettings != null){
				model.addObject( "userSettings", userSettings);	
			}
			
			if( userFromUrl != null){
				//adding user from url to the view
				model.addObject( "userFromUrl", userFromUrl);
				model.addObject( "data_name", userFromUrl.getFullName());
				model.addObject( "data_user_id", userFromUrl.getUniqueID());
				model.addObject( "data_user_name", userFromUrl.getFullName());
				//adding some variables to the view
				String nbFollowing   =  aqStats.getFormattedNumberOfFollowing( userFromUrl);
				String nbFollowers   =  aqStats.getFormattedNumberOfFollowers( userFromUrl);
				model.addObject( "nbFollowing", nbFollowing);
				model.addObject( "nbFollowers", nbFollowers);
				
				String citydata = userFromUrl.getLocation();
				try {
					citydatajson = new JSONObject( citydata);
					model.addObject("currentlocation", citydatajson.getString("name"));
				} catch (JSONException ex) {
					ex.printStackTrace();
				}
				
			}
			String follow_status = "disable-following";
			
			if( authUser != null && userService.canFollow( authUser.getUniqueID(), userId)){
			  follow_status = "follow";
			  
			}
			if( authUser != null && userService.canUnFollow( authUser.getUniqueID(), userId)){
			  follow_status = "following";
			}
			model.addObject( "followStatus", follow_status);
			model.addObject( "fstatusclass", follow_status);
			//incrementing page view
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			
			if( auth != null && (auth instanceof AnonymousAuthenticationToken) || (authUser != null && !authUser.getEmail().equals( userFromUrl.getEmail()))){
			  userService.incrementPageNumberOfViews( userFromUrl);
			  //if the user is connected and views another page then save this page
			  if( authUser != null && !authUser.getEmail().equals( userFromUrl.getEmail()))
			    userService.savePageVisite( authUser, userFromUrl);
			}
		} catch (UserNotFoundException e) {
			e.printStackTrace();
			model.setViewName( "/error/error");
		}catch( NullPointerException e){
			e.printStackTrace();
			model.setViewName( "/error/error");
		}
		return model;
	}
	
	/**
	 * @return index page
	 */
	@RequestMapping(value = { Endpoints.ROOT }, method = RequestMethod.GET)
	public ModelAndView aqlliPage( HttpServletRequest request,
			HttpServletResponse response) {
		AqlliUserStats funFacts = new AqlliUserStats();
		model = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			model.setViewName("redirect:" + Endpoints.HOME);
		}

		String nbUsers = funFacts.getFormattedNumberOfUser();
		model.getModelMap().addAttribute("nbUsers", nbUsers);
		//binding object contact to the contact form
		ContactUs contactUs = new ContactUs();
		model.getModelMap().addAttribute("contactUs", contactUs);
		model.setViewName("index");
		System.out.print( model.getModelMap().get( "city"));
		return model;
	}
	/**
	 * @return invite page
	 */
	@RequestMapping(value = { Endpoints.INVITE }, method = RequestMethod.GET)
	public ModelAndView aqlliInvite(HttpServletRequest request,
			HttpServletResponse response) {
		model = new ModelAndView();
		userService.addUserToRequest( model, request);
		model.setViewName("inviteFriend");
		return model;
	}
	/**
	 * @return messages page
	 */
	@RequestMapping(value = { Endpoints.MESSAGES }, method = RequestMethod.GET)
	public ModelAndView aqlliMessages(HttpServletRequest request,
			HttpServletResponse response) {
		model = new ModelAndView();
		userService.addUserToRequest( model, request);
		model.setViewName("messages");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth instanceof AnonymousAuthenticationToken) {
			model.setViewName("redirect:/login");
		}
		return model;
	}
	/**
	 * @return legal page
	 */
	@RequestMapping(value = { Endpoints.LEGAL }, method = RequestMethod.GET)
	public ModelAndView aqlliLegalPage(HttpServletRequest request,
			HttpServletResponse response) {
		model = new ModelAndView();
		userService.addUserToRequest( model, request);
		model.setViewName("legal_root");
		return model;
	}
	/**
	 * @return cookies page
	 */
	@RequestMapping(value = { Endpoints.COOKIES }, method = RequestMethod.GET)
	public ModelAndView aqlliCookiesPage(HttpServletRequest request,
			HttpServletResponse response) {
		model = new ModelAndView();
		userService.addUserToRequest( model, request);
		model.setViewName("cookiespolicy");
		return model;
	}
	/**
	 * @return privacy page
	 */
	@RequestMapping(value = { Endpoints.PRIVACY }, method = RequestMethod.GET)
	public ModelAndView aqlliPrivacyPage(HttpServletRequest request,
			HttpServletResponse response) {
		model = new ModelAndView();
		userService.addUserToRequest( model, request);
		model.setViewName("privacypolicy");
		return model;
	}
	/**
	 * @return terms page
	 */
	@RequestMapping(value = { Endpoints.TERMS }, method = RequestMethod.GET)
	public ModelAndView aqlliTermsPage(HttpServletRequest request,
			HttpServletResponse response) {
		model = new ModelAndView();
		userService.addUserToRequest( model, request);
		model.setViewName("termsOfUse");
		return model;
	}
	
	/**
	 * @param user User to delete
	 * @param result
	 * @param request
	 * @param response
	 * @return login page after user account deletion 
	 */
	@RequestMapping(value = { Endpoints.USERDELETE}, method = RequestMethod.POST, produces = "application/json", consumes = MediaType.APPLICATION_JSON_VALUE, headers="Accept=application/json")
	@ResponseBody
	public int deleteAccount(@RequestBody String uid, 
			BindingResult result, HttpServletRequest request,
			HttpServletResponse response) {
		UserDetails user;
		//JSONObject obj = new JSONObject(uid);  security issues
		//String id = obj.getString("uid");
		//user = userService.getUserByUniqueID( id);
		user = userService.getUserFromRequest(request);
		EmailValidator emailValidator =  new EmailValidator();
		if( !emailValidator.isValid( user.getEmail())) return HttpServletResponse.SC_OK;
		String appUrl = request.getScheme() + "://" + request.getServerName();//http(s)://server
		String lang = new GetCookieVal().getCookie( "GWT_LOCALE", request);//internationalization cookie
		if ( lang.trim() == "") lang = "en"; //fallback to default
		List<String> content = new GlobalMailer().getInternationalizedContent( lang);
    	final String userName 		    = user.getFullName();
    	final String followUsOn	        = content.get( 5);
    	final String messageAim         = content.get( 22);
    	final String subject            = content.get( 23);
    	final String sendMessage 		= content.get( 24);
    	
		if ( user != null){
		  //TODO supprimer tous les objets coté clients!
		  userService.deleteUser( user);
		  Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		  if ( auth != null){    
		    new SecurityContextLogoutHandler().logout(request, response, auth);
		    HttpSession session = request.getSession( false);
		    if ( session != null) {
		      session.invalidate();
		    }
		  }
		  SecurityContextHolder.getContext().setAuthentication( null);
		  mailService.sendAccountDeleted(userName, user.getEmail(), sendMessage, appUrl, followUsOn, Consts.fb, Consts.tw, Consts.gp, messageAim, subject);
		}
		return HttpServletResponse.SC_OK;		
	}
	
	@RequestMapping( value={Endpoints.UPDWALL}, method = RequestMethod.GET, produces = "application/json", headers="Accept=application/json")
	@ResponseBody
	public List<AqlliPost> infiniteScrollUserWallPush( @RequestParam("lastID") String lastPId, @RequestParam("uuid") String uuid, HttpServletRequest request, HttpServletResponse response){
		List<AqlliPost> liste = new ArrayList<AqlliPost>();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if( uuid == null || uuid.equals(""))
			return liste;
		UserDetails user;
		try {
			user = userService.getUserByUniqueID(uuid);
			if (!(auth instanceof AnonymousAuthenticationToken)) {
				if (user != null){
					liste = wallService.getNextPosts( user, lastPId, 5);
					return liste;
				}
				return liste;//liste vide
			}
		} catch (UserNotFoundException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}
		
		return null;
	}
	
	@RequestMapping( value={ Endpoints.INITWALL}, method = RequestMethod.GET, produces = "application/json", consumes = "application/json", headers="Accept=application/json")
	@ResponseBody
	public List<String> infiniteScrollUserWallInit( @RequestParam("uuid") String uuid, HttpServletRequest request, HttpServletResponse response){
		List<String> l = new ArrayList<String>();
		if( uuid == null || uuid.equals(""))
			return l;
		
		UserDetails user = null;
		String json = "";
		org.codehaus.jackson.map.ObjectWriter ow = new ObjectMapper().writer();
		ObjectMapper mapper = new ObjectMapper();
		List<AqlliPost> liste =  null;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		try {
			user = userService.getUserByUniqueID(uuid);
			if (!(auth instanceof AnonymousAuthenticationToken)) {
				if (user != null){
					liste = wallService.initWall( user, 5);
					for( AqlliPost aq : liste){
				    	try {
				    		Object jsonObj = mapper.readValue(ow.writeValueAsString( aq), Object.class);
				    		json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonObj);
				    		l.add(json);
						} catch (JsonGenerationException ex) {
							ex.printStackTrace();
						} catch (JsonMappingException ex) {
							ex.printStackTrace();
						} catch (IOException ex) {
							ex.printStackTrace();
						}
				    }
				}
			  }
		} catch (UserNotFoundException ex1) {
			// TODO Auto-generated catch block
			ex1.printStackTrace();
		}		  
	  return l;
	}
	@RequestMapping( value={Endpoints.UPDWALL}, method = RequestMethod.POST)
	@ResponseBody
	public AqlliPost infiniteScrollUserWall(@RequestBody String aqPost,
			HttpServletRequest request, HttpServletResponse response){
		AqlliPost post = new AqlliPost();
		try{
			UserDetails user = userService.getUserFromRequest(request);
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			if (!(auth instanceof AnonymousAuthenticationToken)) {
				if ( user != null){
					post = wallService.addPost(user, aqPost);
				}
			}
		}catch( Exception e){
			e.printStackTrace();
		}
		return post;
	}
	/**
	 * @return the view for a page containing links to all public informative pages
	 */
	@RequestMapping(value = { Endpoints.PAGES }, method = RequestMethod.GET)
	public ModelAndView aqlliPages( HttpServletRequest request,
			HttpServletResponse response) {
		model = new ModelAndView();
		userService.addUserToRequest( model, request);
		model.setViewName("pages_root");
		return model;
	}
	/**
	 * 
	 * @return Aqlli thanks page
	 */
	@RequestMapping(value = { Endpoints.PAGESTHNKS }, method = RequestMethod.GET)
	public ModelAndView aqlliThanksPage( HttpServletRequest request,
			HttpServletResponse response) {
		model = new ModelAndView();
		userService.addUserToRequest( model, request);
		model.setViewName("thanks");
		return model;
	}
	/**
	 * 
	 * @return Aqlli careers page
	 */
	@RequestMapping(value = { Endpoints.PAGESCAREERS }, method = RequestMethod.GET)
	public ModelAndView aqlliCareerPage( HttpServletRequest request,
			HttpServletResponse response) {
		model = new ModelAndView();
		userService.addUserToRequest( model, request);
		model.setViewName("careers");
		return model;
	}
	/**
	 * 
	 * @return Aqlli settings page
	 */
	@RequestMapping(value = { Endpoints.SETTINGS }, method = RequestMethod.GET)
	public ModelAndView aqlliSettingssPage( HttpServletRequest request,
			HttpServletResponse response) {
		model = new ModelAndView();
		UserDetails user = userService.addUserToRequest( model, request);
		UserInfosAndSettings uis = AqlliOfyService.ofy().load().type(UserInfosAndSettings.class).id( user.getUniqueID()).now();
		
		if( uis != null){
		  model.addObject( "accountType", uis.getAccountType());
		}
		model.setViewName("settings");
		return model;
	}
	
	/**
	 * @param ln User's language to set
	 * @param result
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = { Endpoints.UPDLANG}, method = RequestMethod.POST, produces = "application/json", consumes = MediaType.APPLICATION_JSON_VALUE, headers="Accept=application/json")
	@ResponseBody
	public int setLang(@RequestBody String ln, 
			BindingResult result, HttpServletRequest request,
			HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if ( auth != null){
        	UserDetails user = userService.getUserFromRequest( request);
        	if ( user != null){
        	  	try {
					userService.setLang(user, ln);
				} catch (JSONException ex) {
				  ex.printStackTrace();
				}
        	}
        }
       return HttpServletResponse.SC_OK;		
	}
	/**
	 * @param contactUs
	 * @param result
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = Endpoints.CONTACTUS, method = RequestMethod.POST, produces = "application/json", consumes = MediaType.APPLICATION_JSON_VALUE, headers="Accept=application/json")
	@ResponseBody
	public int contactUs(
			@RequestBody String contactUs,
			BindingResult result, HttpServletRequest request,
			HttpServletResponse response) {
		/* Validation of email, name and message fields */
		boolean isEmailValid, isMessageEmpty, isNameEmpty, isSubjectEmpty = false;
		EmailValidator emailValidator = new EmailValidator();
		JSONObject contactUsjson;
		try {
			contactUsjson   = new JSONObject( contactUs);
			String name     = contactUsjson.getString("name").trim();
			String mail     = contactUsjson.getString("email").trim();
			String subject  = contactUsjson.getString("subject").trim();
			String message  = contactUsjson.getString("message").trim();
			
			isEmailValid 	= emailValidator.isValid( mail);
			isMessageEmpty 	= message.isEmpty();
			isNameEmpty		= name.isEmpty();
			isSubjectEmpty 	= subject.isEmpty();
			
			if ( isEmailValid && !isMessageEmpty && !isSubjectEmpty && !isNameEmpty) {
				name    = HtmlUtils.htmlEscape( name);
				subject = HtmlUtils.htmlEscape( subject);
				message = HtmlUtils.htmlEscape( message);
				mail    = HtmlUtils.htmlEscape( mail);
				mailService.contactUs( name, mail, subject, message);
			}
		} catch (JSONException ex) {
			ex.printStackTrace();
		}
		return  HttpServletResponse.SC_OK;
	}
	
	@RequestMapping(value = { Endpoints.SINGLEINVITE}, method = RequestMethod.POST, produces = "application/json", consumes = MediaType.APPLICATION_JSON_VALUE, headers="Accept=application/json")
	@ResponseBody
	public int sendSingleInvite(@RequestBody String email, 
			BindingResult result, HttpServletRequest request,
			HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		if ( auth != null){
			JSONObject mailJson;
			
			EmailValidator emailValidator =  new EmailValidator();
			String appUrl = request.getScheme() + "://" + request.getServerName();//http(s)://server
			String lang = new GetCookieVal().getCookie( "GWT_LOCALE", request);//internationalization cookie
			if ( lang.trim() == "") lang = "en"; //fallback to default
			List<String> content = new GlobalMailer().getInternationalizedContent( lang);
        	UserDetails user = userService.getUserFromRequest( request);
        	String aqlliDefaultUrl 	= appUrl;
        	String joinUrl 			= appUrl + Endpoints.REGISTRATION;
        	final String userPicUrl 		= user.getDefaultUserAvatarUrl();
        	final String senderName 		= user.getFullName();
        	final String settingsUrlTxt     = content.get( 4);
        	final String followUsOn	        = content.get( 5);
        	final String messageAim         = content.get( 18);
        	final String subject            = content.get( 15);
        	final String sendMessage 		= content.get( 16);
        	final String btnJoinHere 		= content.get( 17);
        	final String settingsUrl        = appUrl + Endpoints.SETTINGS;
        	if ( user != null){
        	  	try {
        	  		mailJson  = new JSONObject( email);
        	  		String mail = mailJson.getString("email");
					if ( emailValidator.isValid( mail)){
						final String invToken = StringHashGenerator.generate(4);
						userService.createInvitationToken(user, invToken);
						joinUrl = joinUrl + "?invite=" + invToken;
						mailService.sendSingleInvite(mail, subject, aqlliDefaultUrl, joinUrl, userPicUrl, senderName, sendMessage, btnJoinHere, followUsOn, Consts.fb, Consts.tw, Consts.gp, settingsUrl, settingsUrlTxt, messageAim);
						inviteFriendService.singleInvitation(user.getEmail(), mail); //saves the friends email
					}
				} catch (JSONException ex) {
				  ex.printStackTrace();
				}
        	}
        }
       return HttpServletResponse.SC_OK;		
	}
	
	@RequestMapping(value = Endpoints.UPDINFOS, method = RequestMethod.POST, produces = "application/json", consumes = MediaType.APPLICATION_JSON_VALUE, headers="Accept=application/json")
	@ResponseBody
	public int updateUserInfosAndsettings( @RequestParam(value = "field", required = false) String field,
			@RequestBody String settings,
			BindingResult result, HttpServletRequest request,
			HttpServletResponse response) {
		
		JSONObject settingsUpdJson, bday;
		
		UserDetails user = userService.getUserFromRequest(request);
		//if field or user is empty stop the process
		if(( field == null) || field.trim().equals( "") || (user == null))
			return  HttpServletResponse.SC_OK;
		
		try {
			settingsUpdJson   = new JSONObject( settings);
			if( field.equalsIgnoreCase(Consts.APIUPDSETTINGSGEN)){
				String lastName   = settingsUpdJson.getString( "lastName").trim();
			    String firstName  = settingsUpdJson.getString( "firstName").trim();
			    bday = new JSONObject( settingsUpdJson.getString( "dateOfBirth").trim());
			    String day		  = bday.getString( "day").trim();
			    String month 	  = bday.getString( "month").trim();
			    String year 	  = bday.getString( "year").trim();
			    userService.updateBirthDate(day, month, year, user);
			    userService.updateName(firstName, lastName, user);
			}
			if( field.equalsIgnoreCase(Consts.APIUPDSETTINGSSOC)){
				String twId  = settingsUpdJson.getString("twitterId").trim();
			    String fbId  = settingsUpdJson.getString("facebookId").trim();
			    String gpId	 = settingsUpdJson.getString("googlepId").trim();
			    String webs  = settingsUpdJson.getString("website").trim();
			    userService.updateSocialBtns(twId, fbId, gpId, webs, user);
			}
			if( field.equalsIgnoreCase(Consts.APIUPDSETTINGSDESC)){
				String desc  = settingsUpdJson.getString( "desc").trim();
			    userService.updateDescriptionInfos(desc, user);
			}
			if( field.equalsIgnoreCase(Consts.APIUPDSETTINGSOCCU)){
				String job  	 = settingsUpdJson.getString( "job").trim();
			    String comp  	 = settingsUpdJson.getString( "company").trim();
				String citydata  = settingsUpdJson.getString( "city").trim();
				System.out.println( citydata);
			    userService.updateOccupInfos(job, comp, citydata, user);
			}
			/*if( field.equalsIgnoreCase(Consts.APIUPDSETTINGSHFO)){
				String hfo  = settingsUpdJson.getString( "hfo").trim();
			    userService.updateHForInfos(hfo, user);
			}*/
			if( field.equalsIgnoreCase(Consts.APIUPDSETTINGSNOTIFS)){
				
				String desktopNotifs  = settingsUpdJson.getString( "desktopNotifs");
				String emailNotifs    = settingsUpdJson.getString( "emailNotifs");
				String type = "abort";
				boolean value = false;
				
				if( desktopNotifs == "on"){
					type = "desktopNotifs";
					value = true;
				}else if ( desktopNotifs == "off"){
					type = "desktopNotifs";
					value = false;
				}
				
				if( emailNotifs == "on"){
					type = "emailNotifs";
					value = true;
				}else if( emailNotifs == "off"){
					type = "emailNotifs";
					value = false;
				}
				userService.updateNotifsSettings(type, value, user);
			}
		} catch (JSONException ex) {
			ex.printStackTrace();
		}
		return  HttpServletResponse.SC_OK;
	}
	@RequestMapping(value = { Endpoints.USRFOLLOWORUNFOLLW}, method = RequestMethod.POST, produces = "application/json", consumes = MediaType.APPLICATION_JSON_VALUE, headers="Accept=application/json")
	@ResponseBody
	public String followManager( @RequestParam(value = "screen_name", required = false) String screen_name, @RequestParam(value = "original_referer", required = false) String original_referer, @RequestBody String followInfos, 
			BindingResult result, HttpServletRequest request,
			HttpServletResponse response) {
		
		String data_user_id; //User unique ID
		String data_follow_status; //following or not-following
		JSONObject jsonResp = null;
		//traiter le contenu du json recu TODO
		UserDetails user    = userService.getUserFromRequest(request);
		try {
			jsonResp = new JSONObject(followInfos);
			data_user_id   = jsonResp.getString( "data_user_id");
			data_follow_status  = jsonResp.getString( "data_follow_status");
			if( user == null){
				jsonResp.append( "error", "null principal");
				return jsonResp.toString();
			}
			if( user.getUniqueID().trim().equals( data_user_id.trim())){
				jsonResp.append( "error", "same user");
				return jsonResp.toString();
			}
			//user to follow or unfollow
			UserDetails userToProcess = userService.getUserByUniqueID(data_user_id);
			boolean canProcess 		  = false;
			if( data_follow_status.equalsIgnoreCase( "following")){
			  //Retirer de la liste des personnes suivis
			  canProcess    = userService.canUnFollow(user.getUniqueID(), data_user_id);
			  if( canProcess){
				  userService.processUnFollowing(user, userToProcess);
				  jsonResp.append( "type", "follow");
			  }
			}else if( data_follow_status.equalsIgnoreCase( "follow")){
			  //ajout dans la liste des personnes suivis
			  canProcess    = userService.canFollow(user.getUniqueID(), data_user_id);
			  if( canProcess){
				  userService.processFollowing(user, userToProcess);
				  jsonResp.append( "type", "following");
			  }
			}
		} catch (JSONException ex) {
			ex.printStackTrace();
		} catch (UserNotFoundException ex) {
			ex.printStackTrace();
		}
		return jsonResp.toString();		
	}
	
	@RequestMapping( value={Endpoints.CITIESINFOS}, method = RequestMethod.GET, produces = "application/json", headers="Accept=application/json")
	@ResponseBody
	public List<CityInfos> getReadersCities( @RequestParam("uuid") String authUser, HttpServletRequest request, HttpServletResponse response){
		List<CityInfos> liste = new ArrayList<CityInfos>();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if( authUser == null || authUser.trim().equals("")) return liste;
		UserDetails user;
		try {
			user = userService.getUserByUniqueID( authUser);
			if (!(auth instanceof AnonymousAuthenticationToken)) {
				if (user != null){
					liste = userService.readersCitiesInfos( user);
				}
			}
		} catch (UserNotFoundException ex) {
			ex.printStackTrace();
		}
		
		return liste;
	}
	@RequestMapping( value={Endpoints.POSTLIKEORSHARE}, method = RequestMethod.POST)
	@ResponseBody
	public String likeOrSharePost(@RequestParam String type, @RequestParam String postId,
			HttpServletRequest request, HttpServletResponse response){
		AqlliPost post = null;
		AtomicInteger atl = null;
		JSONObject result = new JSONObject();
		try{
			UserDetails user = userService.getUserFromRequest(request);
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			if (!(auth instanceof AnonymousAuthenticationToken)) {
				if ( (user != null) && ( postId != null) && (!postId.trim().equals(""))){
					Long id = Long.valueOf( postId).longValue();
					post = wallService.findPostByUniqueId( id);
					if( post != null && !post.getUsersWhoLike().contains( user.getUniqueID())){
						int nl = 0;
						try{
							nl = post.getNumberOfLikes();
						}catch( NullPointerException e){
							post.setNumberOfLikes( 0);
						}
						atl = new AtomicInteger( nl);
						Integer n = atl.incrementAndGet();
						post.setNumberOfLikes( n);
						post.getUsersWhoLike().add( user.getUniqueID());
						result.append( "numberOfLikes", n);
						result.append( "postId", postId);
						String postTitle = "";
						if( post.getTitle() != null) postTitle = post.getTitle();
						result.append( "postTitle", postTitle);
						result.append( "likerId", user.getUniqueID());
						wallService.savePost( post);
					}
				}
			}
		}catch( Exception e){
			e.printStackTrace();
		}
		return result.toString();
	}
	@RequestMapping( value={Endpoints.REPORTBUG}, method = RequestMethod.POST)
	@ResponseBody
	public int reportAbug(@RequestBody String reportJson,
			HttpServletRequest request, HttpServletResponse response){
		ReportABug rep = new ReportABug();
		try{
			JSONObject report = new JSONObject( reportJson);
			if( reportJson != null && !reportJson.trim().equals("")){
				UserDetails user = userService.getUserFromRequest(request);
				Authentication auth = SecurityContextHolder.getContext().getAuthentication();
				if (!(auth instanceof AnonymousAuthenticationToken)) {
					if ( user != null){
						rep.setTitle( report.getString( "title"));
						rep.setUrlOrigin( report.getString( "urlOrigin"));
						rep.setDescription( report.getString( "description"));
						rep.setTicket( report.getString( "ticket"));
						rep.setReporterId( user.getUniqueID());
						wallService.saveBugReport( rep);
					}
				}
			}
		}catch( Exception e){
			e.printStackTrace();
		}
		return Response.SC_OK;
	}
	
	@RequestMapping( value={Endpoints.NOTIFY}, method = RequestMethod.POST)
	@ResponseBody
	public int updatenotify(@RequestBody String body,
			HttpServletRequest request, HttpServletResponse response){
		try{
			JSONObject notifyUser = new JSONObject( body);
			if( body != null && !body.trim().equals("")){
				UserDetails user = userService.getUserFromRequest(request);
				Authentication auth = SecurityContextHolder.getContext().getAuthentication();
				if (!(auth instanceof AnonymousAuthenticationToken)) {
					if ( user != null){
						String receiverId  = notifyUser.getString( "userId");
						//TODO check if receiverId exists
						String senderId = user.getUniqueID();
						String title = notifyUser.getString( "title");
						String nbody = notifyUser.getString( "body");
						String iconUrl = user.getDefaultUserAvatarUrl();
						String type  = notifyUser.getString( "type");
						AqNotification notif = new AqNotification();
						notif.setTitle( title);
						notif.setBody( nbody);
						notif.setIconUrl( iconUrl);
						notif.setType( type);
						notif.setSenderId(senderId);
						notif.setReceiverId(receiverId);
						wallService.saveNotification( notif);
					}
				}
			}
		}catch( Exception e){
			e.printStackTrace();
		}
		return Response.SC_OK;
	}
	
	@RequestMapping( value={Endpoints.NOTIFY}, method = RequestMethod.GET)
	@ResponseBody
	public List<AqNotification> notify(@RequestParam String receiverId, @RequestParam String lastNotifId,
			HttpServletRequest request, HttpServletResponse response){
		List<AqNotification> l = new ArrayList<>();
		try{
			if( receiverId != null && !receiverId.trim().equals("")){
				UserDetails user = userService.getUserFromRequest(request);
				Authentication auth = SecurityContextHolder.getContext().getAuthentication();
				if (!(auth instanceof AnonymousAuthenticationToken)) {
					if ( user != null){
						l = wallService.getNotifications( receiverId, new Long( lastNotifId), 5);
					}
				}
			}
		}catch( Exception e){
			e.printStackTrace();
		}
		return l;
	}
	
}
