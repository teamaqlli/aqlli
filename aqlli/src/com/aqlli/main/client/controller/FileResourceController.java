package com.aqlli.main.client.controller;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.aqlli.main.service.AqlliOfyService;
import com.aqlli.main.shared.util.fileUpload.Entity;
import com.aqlli.main.shared.util.fileUpload.FileMeta;
import com.aqlli.main.shared.util.fileUpload.FileUrl;
import com.google.appengine.api.blobstore.BlobInfo;
import com.google.appengine.api.blobstore.BlobInfoFactory;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreFailureException;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.images.ImagesService;
import com.google.appengine.api.images.ImagesServiceFactory;
import com.google.appengine.api.images.ServingUrlOptions;
import com.google.common.collect.Lists;


@Path("/file")
public class FileResourceController {

  private final BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
  private final BlobInfoFactory blobInfoFactory = new BlobInfoFactory();
  @Context private HttpServletResponse response;
  /* step 1. get a unique url */

  @GET
  @Path("/url")
  public Response getCallbackUrl() {
    /* this is /_ah/upload and it redirects to its given path */
	  String url = "";
	try{
		url = blobstoreService.createUploadUrl( "/rest/file");
	}catch( Exception e){
		e.printStackTrace();
	}
	
	setHeaders( response);
	
    return Response.ok( new FileUrl( url)).build();
  }

  /* step 2. post a file */

  @POST
  @Consumes(MediaType.MULTIPART_FORM_DATA)
  public Response post(@Context HttpServletRequest req,
        @Context HttpServletResponse res) throws IOException,
        URISyntaxException {
	  Entity entity = null;
     try{
    	 Map<String, List<BlobKey>> blobs = blobstoreService.getUploads(req);
         List<BlobKey> blobKey = blobs.get("files[]");
         System.out.print( "below files");
         BlobInfo info = blobInfoFactory.loadBlobInfo(blobKey.get( 0));
         String name = info.getFilename();
         long size = info.getSize();
         String url = "/rest/file/" + blobKey.get(0).getKeyString();
         System.out.print( "\n"+url);
         
         ImagesService imagesService = ImagesServiceFactory.getImagesService();
         ServingUrlOptions.Builder.withBlobKey(blobKey.get( 0)).crop(true).imageSize(80);
         int sizePreview = 80;
         String urlPreview = imagesService
                    .getServingUrl(ServingUrlOptions.Builder.withBlobKey(blobKey.get( 0))
                .crop(true).imageSize(sizePreview));

         FileMeta meta = new FileMeta(name, size, url, urlPreview);
         System.out.print( "\n" + meta.getName() + "/" + meta.getSize() + "/" + meta.getUrl() + "/" + meta.getThumbnail_url() + "/" + meta.getDelete_url());
         
         List<FileMeta> metas = Lists.newArrayList(meta);
         entity = new Entity(metas);
 
     }catch(Exception e){
    	 e.printStackTrace();
     }
	 return Response.ok(entity, MediaType.APPLICATION_JSON).build();
  }

  /* step 3. redirected to the meta info */

  @GET
  @Path("/{key}/meta")
    public Response redirect(@PathParam("key") String key) throws IOException {
    	Entity entity = null;
      try{
    	  BlobKey blobKey = new BlobKey(key);
          BlobInfo info = blobInfoFactory.loadBlobInfo(blobKey);

          String name = info.getFilename();
          long size = info.getSize();
          String url = "/rest/file/" + key;

          ImagesService imagesService = ImagesServiceFactory.getImagesService();
          ServingUrlOptions.Builder.withBlobKey(blobKey).crop(true).imageSize(80);
          int sizePreview = 80;
          String urlPreview = imagesService
                    .getServingUrl(ServingUrlOptions.Builder.withBlobKey(blobKey)
                .crop(true).imageSize(sizePreview));

          FileMeta meta = new FileMeta(name, size, url, urlPreview);

          List<FileMeta> metas = Lists.newArrayList(meta);
          entity = new Entity(metas);

      }catch( Exception e){
    	  e.printStackTrace();
      }
      return Response.ok(entity,MediaType.APPLICATION_JSON).build();
    }

  /* step 4. download the file */

  @GET
  @Path("/{key}")
  public Response serve(@PathParam("key") String key, @Context HttpServletResponse response) throws IOException {
    try{
    	BlobKey blobKey = new BlobKey(key);
        final BlobInfo blobInfo = blobInfoFactory.loadBlobInfo(blobKey);
        response.setHeader("Content-Disposition", "attachment; filename=" + blobInfo.getFilename());
        BlobstoreServiceFactory.getBlobstoreService().serve(blobKey, response);
    }catch( Exception e){
    	e.printStackTrace();
    }
	
    return Response.ok().build();
  }

  /* step 5. delete the file */

  @DELETE
  @Path("/{key}")
  public Response delete(@PathParam("key") String key) {
    Status status;
    try {
      blobstoreService.delete(new BlobKey(key));
      status = Status.OK;
    } catch (BlobstoreFailureException bfe) {
      status = Status.NOT_FOUND;
    }
    return Response.status(status).build();
  }
  
  private void setHeaders( HttpServletResponse response){
	  response.setHeader( "Accept", "application/json");
		response.setHeader( "Content-Type", "application/json");
		response.setHeader( "Access-Control-Allow-Origin", "*");
		response.setHeader( "X-Requested-With", "XMLHttpRequest");
		response.setHeader( "X-Ajax-call", "true");
		response.setHeader( "X-Content-Type-Options", "nosniff");
		response.setHeader( "X-Frame-Options", "DENY");
		response.setHeader( "Access-Control-Allow-Methods", "GET, POST, PUT, OPTIONS");
		response.setHeader( "Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
		response.setHeader( "Access-Control-Allow-Headers", "X-Custom-Header");
		response.setHeader( "X-XSS-Protection", "1; mode=block");
		response.setHeader( "Expires", "0");
  }
}
