package com.aqlli.main.client.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aqlli.main.client.model.UserDetails;
import com.aqlli.main.service.SearchService;
import com.aqlli.main.shared.Endpoints;

@Controller
public class SearchController {
	private final SearchService searchService = new SearchService();
	@RequestMapping(value = { Endpoints.SEARCH }, method = RequestMethod.GET, produces = "application/json", headers="Accept=application/json")
	@ResponseBody
	public String searchResult( 
			@RequestParam("query") String query, HttpServletRequest request,
			HttpServletResponse response) {
		
		if( query == null || query.trim().equals("")) return "";
		JSONObject finalRes = new JSONObject();
		JSONObject intermiateRes = new JSONObject();
		List<UserDetails> l = searchService.searchUsersbyQuery(20, query, "lowercaseFullName");
		try{
		  for( UserDetails u : l){
		      //l'utilisateir doit être valable et ajouté seulement une seule fois
			  if( u.isEnabled() && u.isAccountNonLocked()){
		    	intermiateRes.put( "avatar", u.getDefaultUserAvatarUrl());
			    intermiateRes.put( "name"  , u.getFullName());
			    intermiateRes.put( "work"  , u.getJobPosition());
			    intermiateRes.put( "city"  , u.getLocation());
			    intermiateRes.put( "id"    , u.getUniqueID());
			    if( !finalRes.toString().contains( u.getUniqueID()))
			    	finalRes.append( "users", intermiateRes);  
		    }
		  }
		}catch( Exception e){
			e.printStackTrace();
		}
		return finalRes.toString();
	}
	
}
