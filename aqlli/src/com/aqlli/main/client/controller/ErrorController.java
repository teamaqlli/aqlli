package com.aqlli.main.client.controller;

import java.text.MessageFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.aqlli.main.client.model.UserDetails;
import com.aqlli.main.service.UserService;
import com.aqlli.main.shared.Endpoints;
import com.google.appengine.labs.repackaged.com.google.common.base.Throwables;

/**
 * Error Controller. handles the calls for 404, 500 and 401 HTTP Status codes.
 */
@Controller
public class ErrorController {
	UserService userService = new UserService();


    /**
     * Page Not Found.
     *
     * @return Home Page
     */
    /*
    @RequestMapping(value = ERROR_URL)
    public String notFound(HttpServletRequest request, HttpServletResponse response, Model uiModel) {
    	Integer statusCode = (Integer)request.getAttribute("javax.servlet.error.status_code"); //http code
    	Throwable throwable = (Throwable) request.getAttribute("javax.servlet.error.exception"); //exception
    	String exceptionMessage = getExceptionMessage(throwable, statusCode); //message
    	String requestUri = (String)request.getAttribute("javax.servlet.error.request_uri");
    	if( requestUri == null) requestUri = "Unknown";
    	
    	String message = MessageFormat.format("{0} returned for {1} with message {3}",
    			   statusCode, requestUri, exceptionMessage
    			  );
    	
        uiModel.addAttribute("errorMessage", message);
        return "";
    }*/
    
    @RequestMapping(value = Endpoints.ERROR403)
    public ModelAndView forbidden(HttpServletRequest request, HttpServletResponse response, Model uiModel) {
    	ModelAndView model = new ModelAndView();
    	try{
    		Integer statusCode = (Integer)request.getAttribute("javax.servlet.error.status_code"); //http code
        	Throwable throwable = (Throwable) request.getAttribute("javax.servlet.error.exception"); //exception
        	String exceptionMessage = getExceptionMessage(throwable, statusCode); //message
        	String requestUri = (String)request.getAttribute("javax.servlet.error.request_uri");
        	if( requestUri == null) requestUri = "Unknown request uri";
        	
        	String message = MessageFormat.format("{0} returned for {1} with message {3}",
        			   statusCode, requestUri, exceptionMessage
        			  );
        	model.addObject("errorMessage", message);
    	}catch( Exception e){
    		e.printStackTrace();
    	}
    	UserDetails user = userService.getUserFromRequest(request);
        model.addObject("user", user);
        model.setViewName("error/error");
        return model;
    }
    

    /**
     * Error page.
     *
     * @return the model and view
     */
    /*
    @RequestMapping(value = ERROR404)
    public ModelAndView errorPage() {
        ModelAndView model = new ModelAndView();
        model.addObject("message", "The page you requested could not be found. This location may not be current, due to the recent site redesign.");

        return model;
    }*/
    private String getExceptionMessage(Throwable throwable, Integer statusCode) {
    	  if (throwable != null) {
    	   return Throwables.getRootCause(throwable).getMessage();
    	  }
    	  try{
    	    if ( statusCode != null){
    		  HttpStatus httpStatus = HttpStatus.valueOf(statusCode);
        	  return httpStatus.getReasonPhrase();
    		}
    	  }catch( Exception e){
    		  e.printStackTrace();
    	  }
    	    return "";
    	 } 

}