package com.aqlli.main.client.controller;

import globalMail.GlobalMailer;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.aqlli.main.client.model.UserDetails;
import com.aqlli.main.client.model.VerificationToken;
import com.aqlli.main.repositories.VerificationTokenRepository;
import com.aqlli.main.service.AqlliMailerService;
import com.aqlli.main.service.UserService;
import com.aqlli.main.shared.util.EmailValidator;
import com.aqlli.main.shared.util.IdBuilder;
import com.aqlli.main.shared.util.Consts;

@Component
public class RegistrationListener {
	private UserService service   = new UserService();
	private VerificationTokenRepository vTokenRepo = new VerificationTokenRepository();
	private AqlliMailerService mailSender;
	EmailValidator emailValidator = new EmailValidator();
	GlobalMailer gMailer          = new GlobalMailer();
	List<String> content = null;

	public void confirmReg( UserDetails user, String appUrl, String lang) throws com.googlecode.objectify.SaveException{
		mailSender = new AqlliMailerService();
		content = new ArrayList<String>();
		content = gMailer.getInternationalizedContent( lang);
		//activation Token
		String token = IdBuilder.generateId();
		service.createVerificationToken(user, token);
		String recipientAdress = user.getEmail();
		
		String aqlliDefaultUrl = appUrl;
		String subject         = content.get( 0);
		String welcomeUser     = content.get( 1) + " - " + user.getFullName();
		String introPhrase     = content.get( 2);
		String btnActivate     = content.get( 3);
		String settingsUrlTxt  = content.get( 4);
		String followUsOn	   = content.get( 5);
		String messageAim      = content.get( 6);
		String confirmationUrl = appUrl + "/user/confirm?token="
				+ token;
		String settingsUrl     = appUrl + "/settings";
		
		if( recipientAdress != "" && recipientAdress != null && subject != "")
		  mailSender.sendRegistrationMailSendgrid(recipientAdress, subject, aqlliDefaultUrl, introPhrase, confirmationUrl, welcomeUser, btnActivate, followUsOn, Consts.fb, Consts.tw, Consts.gp, settingsUrl, settingsUrlTxt, messageAim);	
	}

	public void sendNewVerificationToken(UserDetails userdto, String appUrl,
			String lang) {
		if( userdto != null){
			mailSender = new AqlliMailerService();
			//delete previous validation token
			VerificationToken vtoken = vTokenRepo.findByUser( userdto);
			if ( vtoken != null){
				vTokenRepo.deleteToken(vtoken);
			}
			//send new validation token
			content = new ArrayList<String>();
			content = gMailer.getInternationalizedContent( lang);
			//send an other activation Token
			String token = IdBuilder.generateId();
			service.createVerificationToken(userdto, token);
			String recipientAdress = userdto.getEmail();
			String aqlliDefaultUrl = appUrl;
			String subject         = content.get( 0);
			String welcomeUser     = content.get( 1) + " - " + userdto.getFullName();
			String introPhrase     = content.get( 2);
			String btnActivate     = content.get( 3);
			String settingsUrlTxt  = content.get( 4);
			String followUsOn	   = content.get( 5);
			String messageAim      = content.get( 6);
			String confirmationUrl = appUrl + "/user/confirm?token="
					+ token;
			String settingsUrl     = appUrl + "/settings";
			
			if( recipientAdress != "" && recipientAdress != null && subject != "")
			  mailSender.sendRegistrationMailSendgrid(recipientAdress, subject, aqlliDefaultUrl, introPhrase, confirmationUrl, welcomeUser, btnActivate, followUsOn, Consts.fb, Consts.tw, Consts.gp, settingsUrl, settingsUrlTxt, messageAim);
		}	
	}

}