package com.aqlli.main.client.model;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
@Cache
public class ImportedContactsInviter{
	
	@Id
    private String invited_user_id;
	

	@NotNull
	@Index
	private boolean type = false; //singleInvite = false / oauth provider invite = true
	
	@Nullable
	@Index
	private String nom;
	
	@Nullable
	@Index
	private String prenom;
	
	@NotNull
	@Index List<String> importedEmails;
	

	public ImportedContactsInviter(){
		importedEmails = new ArrayList<String>();
	}
	

	public boolean isType() {
		return this.type;
	}

	public void setType(boolean type) {
		this.type = type;
	}
	
	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return this.prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public List<String> getImportedEmails() {
		return this.importedEmails;
	}

	public void setImportedEmails(List<String> importedEmails) {
		this.importedEmails = importedEmails;
	}


	public String getInvited_user_id() {
		return this.invited_user_id;
	}


	public void setInvited_user_id(String invited_user_id) {
		this.invited_user_id = invited_user_id;
	}
	
}
