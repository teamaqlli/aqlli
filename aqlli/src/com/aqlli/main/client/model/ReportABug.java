package com.aqlli.main.client.model;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Unindex;

@Entity
public class ReportABug {
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getReporterId() {
		return this.reporterId;
	}

	public void setReporterId(String reporterId) {
		this.reporterId = reporterId;
	}

	public String getUrlOrigin() {
		return this.urlOrigin;
	}

	public void setUrlOrigin(String urlOrigin) {
		this.urlOrigin = urlOrigin;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTicket() {
		return this.ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	@Id
	private Long id;
	
	@Index
	private String reporterId;
	
	@Unindex
	private String urlOrigin;
	
	@Index
	private String title, description, ticket;
	
}
