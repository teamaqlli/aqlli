package com.aqlli.main.client.model;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.aqlli.main.shared.util.Pollable;
import com.google.appengine.api.datastore.Text;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Unindex;

@Entity
@Cache
@Pollable
public class AqlliPost{
		@Id
	    private Long ident; 
	    
	    private Text editableContent;
	    
	    private Text shortContent;
	    
	    @Index
		private String created;
		
	    @Index
		private String user_id;
		
	    @Index
		private int postId;
		
	    @Index
		private String shareType;
	    
	    @Index
	    private Integer numberOfLikes;
	    
	    @Index
	    private Integer numberOfShares;
	    
	    @Unindex
	    private boolean hide;
	    
	    @Index
	    private List<String> usersWhoLike;//contains the Ids of users who like this post
	    
		@NotNull
	    private Text message;
		
	    private String image;
		
	    private String videoUrl;
		
	    @Index
		private String title;
		
	    private String userProfilePic;

		@Index
	    private ArrayList<String> reports;
		
		public List<String> getUsersWhoLike() {
			return this.usersWhoLike;
		}

		public void setUsersWhoLike(List<String> usersWhoLike) {
			this.usersWhoLike = usersWhoLike;
		}

		public void setEditableContent(Text editableContent) {
			this.editableContent = editableContent;
		}

		public void setShortContent(Text shortContent) {
			this.shortContent = shortContent;
		}

		public AqlliPost( ){
	    	this.setMessage( new Text(""));
		    this.editableContent = new Text("");
		    this.shortContent    = new Text("");
		    this.postId 		 = 0;
		    this.numberOfLikes   = 0;
		    this.numberOfShares  = 0;
		    this.usersWhoLike 	 = new ArrayList<String>();
		    this.setHide(false);
		    this.setReports(new ArrayList<String>());
	    }
		
	    public Long getIdent() {
	        return ident;
	    }
	    
	    public void setIdent(Long ident) {
	        this.ident = ident;
	    }

	    public String getEditableContent() {
	        return editableContent.getValue();
	    }

	    public void setEditableContent( String editableContent) {
	        this.editableContent = new Text(editableContent);
	    }

	    public String getShortContent() {
	        return shortContent.getValue();
	    }

	    public void setShortContent( String shortContent) {
	        this.shortContent = new Text(shortContent);
	    }

	    @Override
	    public boolean equals( Object obj) {
	        if ( this == obj)
	            return true;
	        if ( obj == null)
	            return false;
	        if ( getClass() != obj.getClass())
	            return false;
	        AqlliPost other = ( AqlliPost) obj;
	        if ( getUser_id() == null) {
	            if ( other.getUser_id() != null)
	                return false;
	        } else if ( !getUser_id().equals(other.getUser_id()))
	            return false;
	        if ( getMessage() == null) {
	            if ( other.getMessage() != null)
	                return false;
	        } else if ( !getMessage().equals( other.getMessage()))
	            return false;
	        if ( ident == null) {
	            if ( other.ident != null)
	                return false;
	        } else if ( !ident.equals(other.ident))
	            return false;
	        return true;
	    }
		
		@Override
	    public int hashCode() {
	        final int prime = 61;
	        int result = 1;
	        result = prime * result + ((ident == null) ? 0 : ident.hashCode());
	        return result;
	    }
		
		/**
		 * @return the created
		 */
		public String getCreated() {
			return this.created;
		}
		
		/**
		 * @param locale
		 */
		public void setCeated( String millis){
	    	this.created = millis;
	    }
		
		/**
		 * @return the user_id
		 */
		public String getUser_id() {
			return this.user_id;
		}
		/**
		 * @param user_id the user_id to set
		 */
		public void setUser_id(String user_id) {
			this.user_id = user_id;
		}
		
		/**
		 * @return the message
		 */
		public Text getMessage() {
			return this.message;
		}
		
		/**
		 * @param message the message to set
		 */
		public void setMessage(Text message) {
			this.message = message;
		}
		/**
		 * @return the image
		 */
		public String getImage() {
			return this.image;
		}
		
		/**
		 * @param image the image to set
		 */
		public void setImage(String image) {
			this.image = image;
		}
		
		/**
		 * @return the id
		 */
		public int getPostId() {
			return postId;
		}
		/**
		 * @param id the id to set
		 */
		public void setPostId(int postId) {
			this.postId = postId;
		}
		
		/**
		 * @return the shareType
		 */
		public String getShareType() {
			return shareType;
		}
		
		/**
		 * @param shareType the shareType to set
		 */
		public void setShareType(String shareType) {
			this.shareType = shareType;
		}
		/**
		 * @return the videoUrl
		 */
		public String getVideoUrl() {
			return videoUrl;
		}
		
		/**
		 * @param videoUrl the videoUrl to set
		 */
		public void setVideoUrl(String videoUrl) {
			this.videoUrl = videoUrl;
		}
		
		/**
		 * @return the title
		 */
		public String getTitle() {
			return title;
		}
		/**
		 * @param title the title to set
		 */
		public void setTitle(String title) {
			this.title = title;
		}
		
		public String getUserProfilePic() {
			return this.userProfilePic;
		}

		public void setUserProfilePic(String userProfilePic) {
			this.userProfilePic = userProfilePic;
		}
		
	    public Integer getNumberOfLikes() {
			return this.numberOfLikes;
		}

		public void setNumberOfLikes(Integer numberOfLikes) {
			this.numberOfLikes = numberOfLikes;
		}
		
		public Integer getNumberOfShares() {
			return this.numberOfShares;
		}

		public void setNumberOfShares(Integer numberOfShares) {
			this.numberOfShares = numberOfLikes;
		}

		/**
		 * @return the reports
		 */
		public ArrayList<String> getReports() {
			return reports;
		}

		/**
		 * @param reports the reports to set
		 */
		public void setReports(ArrayList<String> reports) {
			this.reports = reports;
		}

		/**
		 * @return the hide
		 */
		public boolean isHide() {
			return hide;
		}

		/**
		 * @param hide the hide to set
		 */
		public void setHide(boolean hide) {
			this.hide = hide;
		}
	
}