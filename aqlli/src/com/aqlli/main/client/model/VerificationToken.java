package com.aqlli.main.client.model;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
/**
 * @author Arix OWAYE / twitter : @arixowaye
 *
 */
@Entity
public class VerificationToken{
private static final int EXPIRE = 60 * 24; //2 days
@Id
@GeneratedValue( strategy = GenerationType.AUTO)
private Long id;
@Index
private String token;

@OneToOne( targetEntity = UserDetails.class, fetch = FetchType.EAGER)
@Index
private UserDetails user;
@Index private String type;

private Date expiryDate;

public VerificationToken(){
  super();
}

public VerificationToken( String token, UserDetails user, String type){
 super();
 this.token = token;
 this.user = user;
 this.expiryDate = calcuteExpiryDate( EXPIRE);
 this.type = type;
}

public Date calcuteExpiryDate( int expirTimeInMinutes){
  Calendar cal = Calendar.getInstance();
  cal.setTime( new Timestamp( cal.getTime().getTime()));
  cal.add( Calendar.MINUTE, expirTimeInMinutes);
  return new Date( cal.getTime().getTime());
}

public Long getId() {
	return id;
}

public void setId(Long id) {
	this.id = id;
}

public String getToken() {
	return token;
}

public void setToken(String token) {
	this.token = token;
}

public UserDetails getUser() {
	return user;
}

public void setUser(UserDetails user) {
	this.user = user;
}

public Date getExpiryDate() {
	return expiryDate;
}

public void setExpiryDate(Date expiryDate) {
	this.expiryDate = expiryDate;
}


public static int getExpire() {
	return EXPIRE;
}
public void updateToken( String token){
	this.token = token;
	this.expiryDate = calcuteExpiryDate( EXPIRE);
}
public boolean hasExpired (){
	Calendar cal = Calendar.getInstance();
	return (getExpiryDate().getTime() - cal.getTime().getTime()) <= 0 ? true : false;
}
@Transient
public Key<VerificationToken> getKey() {
   return Key.create(VerificationToken.class, id);
}

@Override
public boolean equals(Object obj) {
    if (this == obj)
        return true;
    if (obj == null)
        return false;
    if (getClass() != obj.getClass())
        return false;
    VerificationToken other = (VerificationToken) obj;
    if (expiryDate == null) {
        if (other.expiryDate != null)
            return false;
    } else if (!expiryDate.equals(other.expiryDate))
        return false;
    if (token == null) {
        if (other.token != null)
            return false;
    } else if (!token.equals(other.token))
        return false;
    if (user == null) {
        if (other.user != null)
            return false;
    } else if (!user.equals(other.user))
        return false;
    return true;
}

}