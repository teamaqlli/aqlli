package com.aqlli.main.client.model;

public class Roles {
	
	public static final String USER    = "ROLE_USER";
	public static final String ADMIN   = "ROLE_ADMIN";
	public static final String SUPER   = "ROLE_SUPER";//super user
	public static final String COMPANY = "ROLE_COMPANY";
	
	public static String getUser() {
		return USER;
	}
	public static String getAdmin() {
		return ADMIN;
	}
	public static String getSuper() {
		return SUPER;
	}
	public static String getCompany() {
		return COMPANY;
	}
}
