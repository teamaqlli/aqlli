package com.aqlli.main.client.model;

import java.util.Date;

import javax.validation.constraints.NotNull;

public abstract class Publication {
	
	@NotNull
	String permalink, author;

	@NotNull
    Date created;
	String title; 
	Date lastUpdated;
    
    @NotNull
    private String typeOfPost;

	public String getTypeOfPost() {
		return typeOfPost;
	}

	public void setTypeOfPost(String typeOfPost) {
		this.typeOfPost = typeOfPost;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPermalink() {
		return permalink;
	}

	public void setPermalink(String permalink) {
		this.permalink = permalink;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	/*@Transient
	public Key<Publication> getKey() {
	   return Key.create(Publication.class, ident);
	}*/

}
