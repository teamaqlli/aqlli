package com.aqlli.main.client.model;

import java.util.List;
import javax.persistence.Transient;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
@Cache
public class UserInfosAndSettings {
	/**
	 * Starter --> Free
	 * AqlliStar --> Users who participated at Ulule compaign
	 * Advanced --> Paid
	 */
	@Id
	private String userId; //user unique ID, not the email address
	
	private final String[] accountTypes = {"Starter", "AqlliStar", "Advanced"};
	private final String[]  accountStates = {"Life licence", "On", "Off", "Banned", "Deleted"};
	@Index
	private String accountType, accountState, hereFor;
	private Long nbAccountViews;
	private String userDescription;
	private int nbReceivedBadReport;
	private List<String> otherRoles;
	private boolean activateDesktopNotifs = false;
	private boolean activateEmailNotifs = true;
	
	public boolean isActivateDesktopNotifs() {
		return this.activateDesktopNotifs;
	}

	public void setActivateDesktopNotifs(boolean activateDesktopNotifs) {
		this.activateDesktopNotifs = activateDesktopNotifs;
	}

	public boolean isActivateEmailNotifs() {
		return this.activateEmailNotifs;
	}

	public void setActivateEmailNotifs(boolean activateEmailNotifs) {
		this.activateEmailNotifs = activateEmailNotifs;
	}

	public String getUserId() {
		return this.userId;
	}

	public Long getNbAccountViews() {
		return this.nbAccountViews;
	}
	public void setNbAccountViews(Long nbAccountViews) {
		this.nbAccountViews = nbAccountViews;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getAccountType() {
		return this.accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public String getAccountState() {
		return this.accountState;
	}
	public void setAccountState(String accountState) {
		this.accountState = accountState;
	}
	public String getUserDescription() {
		return this.userDescription;
	}
	public void setUserDescription(String userDescription) {
		this.userDescription = userDescription;
	}
	public int getNbReceivedBadReport() {
		return this.nbReceivedBadReport;
	}
	public void setNbReceivedBadReport(int nbReceivedBadReport) {
		this.nbReceivedBadReport = nbReceivedBadReport;
	}
	
	public String getHereFor() {
		return this.hereFor;
	}
	public void setHereFor(String hereFor) {
		this.hereFor = hereFor;
	}
	
	public List<String> getOtherRoles() {
		return this.otherRoles;
	}
	public void setOtherRoles(List<String> otherRoles) {
		this.otherRoles = otherRoles;
	}
	public String[] getAccountTypes() {
		return this.accountTypes;
	}
	public String[] getAccountStates() {
		return this.accountStates;
	}

	@Transient
	public Key<UserInfosAndSettings> getKey() {
	   return Key.create( UserInfosAndSettings.class, userId);
	}
	
}
