package com.aqlli.main.client.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.security.core.GrantedAuthority;

import com.aqlli.main.shared.util.UserPrincipaleInterface;
import com.aqlli.main.shared.util.ValideEmail;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
@SuppressWarnings("serial")
@Entity
@Cache
public class UserDetails implements UserPrincipaleInterface, Serializable, org.springframework.security.core.userdetails.UserDetails{

	public Map<String, Integer> getRecentlyVisited() {
	  return this.recentlyVisited;
	}

	public void setRecentlyVisited(Map<String, Integer> recentlyVisited) {
	  this.recentlyVisited = recentlyVisited;
	}

	@Id
	@ValideEmail
	@Email
	String email;
	
	@Index
	private String uniqueID;
	
	@Index
	private boolean enabled;
	
	@Index
	private boolean tokenExpired;
	
	String role;
	
	@NotNull
	@Size(min=1)
	@NotEmpty
	@Index
	private String lastName, firstName, password, matchingPassword;

	@Index
	private String nickName, lowercaseFullName;
	
	private String fullName;
	
	private long subscriptionTimeStamp;
	
	@NotNull
	@Size(min=1)
	@NotEmpty
	private String birthDay, birthMonth, birthYear;
	private String fullBirthDay;
	
	@NotNull
	@NotEmpty
	@Index
	private String gender;
	
	
	@NotNull
	private String useTerms, defaultLang;
	private String facebookFollowME, twitterFollowMe, googlePlusFollowMe, website;
	private String defaultUserAvatarUrl, defaultUserCoverUrl;
	
	@Index
	private String location, subscriptionPurpose;
	private String jobPosition, company;
	private String secondaryEmail;
	
	@Index
	private List<String> following 		 	= new ArrayList<String>();
	@Index
	private List<String> followers 		 	= new ArrayList<String>();
	@Index
	private Map<String, Integer> recentlyVisited = new LinkedHashMap<String, Integer>();

	/**
	 * @return the list of the users that read the current user
	 */
	public List<String> getFollowers() {
	  return this.followers;
	}

	/**
	 * @param followers the list of users that the current user follows
	 */
	public void setFollowers(List<String> followers) {
	  this.followers = followers;
	}

	public List<String> getFollowing() {
	  return this.following;
	}

	public void setFollowing( List<String> following) {
	  this.following = following;
	}

	public String getWebsite() {
	  return this.website;
	}

	public void setWebsite( String website) {
	  this.website = website;
	}

	public String getLocation() {
	  return this.location;
	}
	/**
	 * @param location json string
	 * locationdatajson='{  "title",  "name",  "geonameId",  "latitude",  "longitude",  "population",  "country",  "admin1Division",  "admin1DivisionCode",  "tzOffsetMinutes",  "uaName",  "uaId",  "uaCityUrl",  "uaSlug"}
	 */
	public void setLocation( String location) {
	  this.location = location;
	}

	public String getJobPosition() {
	  return this.jobPosition;
	}

	public void setJobPosition( String jobPosition) {
	  this.jobPosition = jobPosition;
	}

	public String getCompany() {
	  return this.company;
	}

	public void setCompany( String company) {
	  this.company = company;
	}

	public String getSubscriptionPurpose() {
	  return this.subscriptionPurpose;
	}

	public void setSubscriptionPurpose( String subscriptionPurpose) {
	  this.subscriptionPurpose = subscriptionPurpose;
	}

	public String getDefaultLang() {
	  return defaultLang;
	}

	public void setDefaultLang( String defaultLang) {
	  this.defaultLang = defaultLang;
	}

	public UserDetails(){
		this.enabled 		= false;
		this.tokenExpired 	= false;
		this.useTerms 		= "disagree"; //default value; the user does not have yet agreed to Aqlli terms of use
		this.defaultLang 	= "default";
		this.setDefaultUserCoverUrl( "http://static.aqlli.com/pictures/default.jpg");
		this.setSubscriptionTimeStamp(new Long(0));
	}
	
	public boolean isTokenExpired() {
		return tokenExpired;
	}
	
	public void setTokenExpired( boolean tokenExpired) {
		this.tokenExpired = tokenExpired;
	}
	
	public void intiFullBirthDay(){
		this.fullBirthDay = birthDay + "/" + birthMonth  + "/" + birthYear;
	}
	
	public String getFullBirthDay( ){
		return fullBirthDay;
	}
	
	public String getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(String birthDay) {
		this.birthDay = birthDay;
	}

	public String getBirthMonth() {
		return birthMonth;
	}

	public void setBirthMonth(String birthMonth) {
		this.birthMonth = birthMonth;
	}

	public String getBirthYear() {
		return birthYear;
	}

	public void setBirthYear(String birthYear) {
		this.birthYear = birthYear;
	}

	
	public String getMatchingPassword() {
		return matchingPassword;
	}

	public void setMatchingPassword(String matchingPassword) {
		this.matchingPassword = matchingPassword;
	}

	@Override
	public String getUserId() {
		return email;
	}
	
	public String getEmail(){
		return email;
	}
	public void setEmail( String email){
		this.email = email;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastname) {
		this.lastName = lastname;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstname) {
		this.firstName = firstname;
	}
	
	@Override
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}

	public void setFullName(String fullName) {
		this.fullName 		   = fullName;
	   setLowercaseFullName( fullName.toLowerCase());
	}
	
	public String getFullName() {
	    return this.fullName;
	}
	
	public void setUniqueID(String uniqueID) {
		this.uniqueID = uniqueID;
	}

	@Override
	public String getRole() {
		return this.role;
	}
	public void setRole(String role){
		this.role = role;
	}

	/**
	 * @return the useTerms
	 */
	public String getUseTerms() {
		return useTerms;
	}

	/**
	 * @param useTerms the useTerms to set
	 */
	public void setUseTerms(String useTerms) {
		this.useTerms = useTerms;
	}
	
	public String getFacebookFollowME() {
		return facebookFollowME;
	}

	public void setFacebookFollowME(String facebookFollowME) {
		this.facebookFollowME = facebookFollowME;
	}

	public String getTwitterFollowMe() {
		return twitterFollowMe;
	}

	public void setTwitterFollowMe(String twitterFollowMe) {
		this.twitterFollowMe = twitterFollowMe;
	}

	public String getUniqueID() {
		return uniqueID;
	}
	/**
	 * @return the defaultUserAvatar
	 */
	public String getDefaultUserAvatarUrl() {
		return defaultUserAvatarUrl;
	}

	/**
	 * @param defaultUserAvatar the defaultUserAvatar to set
	 */
	public void setDefaultUserAvatarUrl( String defaultUserAvatarUrl) {
		this.defaultUserAvatarUrl = defaultUserAvatarUrl;
	}
	
	@Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        UserDetails other = (UserDetails) obj;
        if (email == null) {
            if (other.email != null)
                return false;
        } else if (!email.equals(other.email))
            return false;
        if (password == null) {
            if (other.password != null)
                return false;
        } else if (!password.equals(other.password))
            return false;
        if (fullName == null) {
            if (other.fullName != null)
                return false;
        } else if (!fullName.equals(other.fullName))
            return false;
        return true;
    }
	
	@Override
    public int hashCode() {
        final int prime = 61;
        int result = 1;
        result = prime * result + ((email == null) ? 0 : email.hashCode());
        return result;
    }

	/**
	 * @return the defaultUserCoverUrl
	 */
	public String getDefaultUserCoverUrl() {
		return defaultUserCoverUrl;
	}

	/**
	 * @param defaultUserCoverUrl the defaultUserCoverUrl to set
	 */
	public void setDefaultUserCoverUrl(String defaultUserCoverUrl) {
		this.defaultUserCoverUrl = defaultUserCoverUrl;
	}

		public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public void initDefaultProfilePicture(){
	  if( gender == "M"){
	    this.setDefaultUserAvatarUrl( defaultMalePicture());
	  }else if ( gender == "F"){
		this.setDefaultUserAvatarUrl( defaultFemalePicture());
	  }else{
		this.setDefaultUserAvatarUrl( defaultOtherPicture());
	  }
	}
	
	private String defaultFemalePicture(){
	  return "http://static.aqlli.com/pictures/thumbs/female-profile-180-180.png";
	}
	
	private String defaultMalePicture(){
	  return "http://static.aqlli.com/pictures/thumbs/male-profile-180-180.png";
	}
	
	private String defaultOtherPicture(){
	  return "http://static.aqlli.com/pictures/thumbs/other-profile-180-180.jpg";
	}
	
	public String defaultUserCoverUrl(){
	  return "http://static.aqlli.com/pictures/default.jpg";
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
	  return null;
	}

	@Override
	public String getUsername() {
	  return email;
	}

	@Override
	public boolean isAccountNonExpired() {
	  return tokenExpired;
	}

	@Override
	public boolean isAccountNonLocked() {
	  return enabled;
	}

	@Override
	public boolean isCredentialsNonExpired() {
	  return false;
	}

	/**
	 * @return the googlePlus
	 */
	public String getGooglePlusFollowMe() {
	  return googlePlusFollowMe;
	}

	/**
	 * @param googlePlus the googlePlus to set
	 */
	public void setGooglePlusFollowMe(String googlePlusFollowMe) {
	  this.googlePlusFollowMe = googlePlusFollowMe;
	}
	
	@Transient
	public Key<UserDetails> getKey() {
	  return Key.create( UserDetails.class, email);
	}

	/**
	 * @return the nickName
	 */
	public String getNickName() {
	  return nickName;
	}

	/**
	 * @param nickName the nickName to set
	 */
	public void setNickName(String nickName) {
	  this.nickName = nickName;
	}

	/**
	 * @return the secondaryEmail
	 */
	public String getSecondaryEmail() {
	  return secondaryEmail;
	}

	/**
	 * @param secondaryEmail the secondaryEmail to set
	 */
	public void setSecondaryEmail(String secondaryEmail) {
	  this.secondaryEmail = secondaryEmail;
	}
	
	public String getLowercaseFullName() {
	  return this.lowercaseFullName;
	}
	
	public void setLowercaseFullName( String lowercaseFullName) {
	  this.lowercaseFullName = lowercaseFullName;
	}

	/**
	 * @return the subscriptionTimeStamp
	 */
	public long getSubscriptionTimeStamp() {
		return subscriptionTimeStamp;
	}

	/**
	 * @param subscriptionTimeStamp the subscriptionTimeStamp to set
	 */
	public void setSubscriptionTimeStamp(long subscriptionTimeStamp) {
		this.subscriptionTimeStamp = subscriptionTimeStamp;
	}
	
}
