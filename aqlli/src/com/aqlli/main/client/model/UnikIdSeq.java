package com.aqlli.main.client.model;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class UnikIdSeq {
  @Id private Long id;
  @Index private String userUniqueId;
  @Index int lastId;
  
  public int getLastId() {
	return this.lastId;
}
public void setLastId(int lastId) {
	this.lastId = lastId;
}
public Long getId() {
	return this.id;
}
public void setId(Long id) {
	this.id = id;
}
public String getUserUniqueId() {
	return this.userUniqueId;
}
public void setUserUniqueId(String userUniqueId) {
	this.userUniqueId = userUniqueId;
}

}
