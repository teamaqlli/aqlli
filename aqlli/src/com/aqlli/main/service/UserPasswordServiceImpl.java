package com.aqlli.main.service;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.List;

import com.aqlli.main.client.model.UserDetails;
import com.aqlli.main.client.model.VerificationToken;
import com.aqlli.main.shared.util.Consts;
import com.aqlli.main.shared.util.UserPasswordServiceInterface;



public class UserPasswordServiceImpl implements UserPasswordServiceInterface {

	@Override
	public void SavePasswordRecoverToken(VerificationToken token) {
		ofy().save().entity( token).now();
	}

	@Override
	public void deleteRecoverPassToken(VerificationToken token) {
		ofy().delete().entity(token).now();
	}

	@Override
	public VerificationToken getRecoverPassToken(UserDetails user) {
		List<VerificationToken> vt = ofy().load().type( VerificationToken.class).filter("user", user).filter("type", Consts.typeTokenRecoverPassword).limit(1).list();
		return  ( vt.size() > 0) ? vt.get(0) : null;
	}

}
