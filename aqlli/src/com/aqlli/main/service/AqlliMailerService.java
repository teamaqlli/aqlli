package com.aqlli.main.service;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.aqlli.main.shared.util.Consts;
import com.aqlli.main.shared.util.DatesUtil;
import com.sendgrid.SendGrid;
import com.sendgrid.SendGridException;


public class AqlliMailerService {

	private final static String BCC_MAIL   = "registration-bcc@aqlli.com";
	private final static String NO_REPLY   = "no-reply@aqlli.com";
	private final static String CONTACT_AQLLI = "contact@aqlli.com";
	private final SendGrid sendgrid        = new SendGrid( Consts.sendgridApiKey);
	SendGrid.Email email = null;
	final static String cronJob = DatesUtil.getCronJobDate() + "";
	// javamail est deja supporté par app engine; pas besoin de libs
	public boolean sendValidateSignUpMail(String user_mail, String subject,
			String htmlContent) throws UnsupportedEncodingException,
			MessagingException {//send with appengine javamail
		boolean res = false;
		if ("".equals(user_mail) || "".equals(subject)
				|| "".equals(htmlContent))
			;

		try {
			// smtp server properties
			Properties prop = new Properties();

			// session
			Session session = Session.getDefaultInstance(prop, null);
			session.setDebug(true);
			InternetAddress[] internetAdresses = new InternetAddress[2];
			internetAdresses[0] = new InternetAddress(user_mail); // mail du
																	// destinataire
			internetAdresses[1] = new InternetAddress(BCC_MAIL); // mail de la
																	// copie
																	// cach��e
			// the message
			Message message = new MimeMessage(session);
			message.setRecipient(Message.RecipientType.TO, internetAdresses[0]); // client
			message.setRecipient(Message.RecipientType.BCC, internetAdresses[1]); // aqlli
			message.setFrom(new InternetAddress(NO_REPLY)); // no-reply aqlli
			message.setSubject(subject); // Welcome to Aqlli

			MimeMultipart multiPart = new MimeMultipart();
			BodyPart messageBody = new MimeBodyPart();
			messageBody.setContent(htmlContent, "text/html");
			multiPart.addBodyPart(messageBody);

			message.setContent(multiPart);
			message.setSentDate(new Date()); // verifier: jour mois annee heure
			Transport.send(message);
			res = true; // tout s'est bien passé
		} catch (MessagingException me) {
			me.printStackTrace();
		}
		return res;
	}
	public void sendRegistrationMailSendgrid(String recipientAddress, String subject, String aqlliDefaultUrl, String introPhrase, String confirmationUrl, String welcomeUser, String btnActivate, String followUsOn, String fb, String tw, String gp, String settingsUrl, String settingsUrlTxt, String messageAim){
	  email = new SendGrid.Email();
	  email.setTemplateId("37883870-946f-459a-911c-01363a7bbc1d");
	  email.setASMGroupId( 81);
	  email.addTo( recipientAddress).setFrom(NO_REPLY).setFromName("Noreply Aqlli").setSubject(subject);
	  email.setHtml( "<i></i>");//html content is already setup as a SendGrip Template
	  try {
	    email.addSubstitution("%welcomeUser%", new String[]{welcomeUser});
	    email.addSubstitution("%introPhrase%", new String[]{introPhrase});
	    email.addSubstitution("%btnActivate%", new String[]{btnActivate});
	    email.addSubstitution("%followUsOn%", new String[]{followUsOn});
	    email.addSubstitution("%fbAqlliUrl%", new String[]{fb});
	    email.addSubstitution("%twAqlliUrl%", new String[]{tw});
	    email.addSubstitution("%gpAqlliUrl%", new String[]{gp});
	    email.addSubstitution("%reasonEmail%", new String[]{messageAim});
	    email.addSubstitution("%confirmationUrl%", new String[]{confirmationUrl});
	    email.addSubstitution("%settingsUrl%", new String[]{settingsUrl});
	    email.addSubstitution("%accountSettingsTxt%", new String[]{settingsUrlTxt});
	    email.addSubstitution("%urlToAqllicom%", new String[]{ aqlliDefaultUrl});
	    sendgrid.send( email);
	    } catch (SendGridException ex) {
			ex.printStackTrace();
		}
	}
	public boolean sendRecoverPass(String recipientAddress, String subject, String aqlliDefaultUrl, String changePassUrl, String welcomeBackUser, String introPhrase, String btnChangePass, String msgNotRequested, String advicePwd, String adviceSecondMail, String followUsOn, String fb, String tw, String gp, String settingsUrl, String settingsUrlTxt, String messageAim) {
		boolean res = false;
		email = new SendGrid.Email();
		email.setTemplateId("6a608404-afc7-46e4-9d44-146b854b66df");
		email.setASMGroupId( 85); //security group on SendGrid--> 85
		email.addTo( recipientAddress).setFrom(NO_REPLY).setFromName("Noreply Aqlli").setSubject(subject);
		email.setHtml( "<i></i>");//html content is already setup as a SendGrip Template. however we need to add something
		SendGrid.Response response = null;
		try {
		   email.addSubstitution("%welcomeBackUser%", new String[]{welcomeBackUser});
		   email.addSubstitution("%introPhrase%", new String[]{introPhrase});
		   email.addSubstitution("%btnChangePass%", new String[]{btnChangePass});
		   email.addSubstitution("%followUsOn%", new String[]{followUsOn});
		   email.addSubstitution("%fbAqlliUrl%", new String[]{fb});
		   email.addSubstitution("%twAqlliUrl%", new String[]{tw});
		   email.addSubstitution("%gpAqlliUrl%", new String[]{gp});
		   email.addSubstitution("%reasonEmail%", new String[]{messageAim});
		   email.addSubstitution("%changePassUrl%", new String[]{changePassUrl});
		   email.addSubstitution("%settingsUrl%", new String[]{settingsUrl});
		   email.addSubstitution("%accountSettingsTxt%", new String[]{settingsUrlTxt});
		   email.addSubstitution("%urlToAqllicom%", new String[]{ aqlliDefaultUrl});
		   email.addSubstitution("%msgNotRequested%", new String[]{ msgNotRequested});
		   email.addSubstitution("%advicePwd%", new String[]{ advicePwd});
		   email.addSubstitution("%adviceSecondMail%", new String[]{ adviceSecondMail});
		   response = sendgrid.send( email);
		   res = response.getMessage().contains("success");
		} catch (SendGridException ex) {
			ex.printStackTrace();
		}
		return res;
	}
	
	public boolean sendSingleInvite(String recipientAddress, String subject, String aqlliDefaultUrl, String joinUrl, String userPicUrl, String senderName, String sendMessage, String btnJoinHere, String followUsOn, String fb, String tw, String gp, String settingsUrl, String settingsUrlTxt, String messageAim) {
		boolean res = false;
		email = new SendGrid.Email();
		email.setTemplateId("494fe933-eadb-4996-b2ba-e0d9b8604765");
		email.setASMGroupId( 86);
		email.addTo( recipientAddress).setFrom(NO_REPLY).setFromName("Noreply Aqlli").setSubject(subject);
		email.setHtml( "<i></i>");//html content is already setup as a SendGrip Template. however we need to add something
		SendGrid.Response response = null;
		try {
		   email.addSubstitution("%userPicUrl%", new String[]{userPicUrl});
		   email.addSubstitution("%senderName%", new String[]{senderName});
		   email.addSubstitution("%btnJoinHere%", new String[]{btnJoinHere});
		   email.addSubstitution("%sendMessage%", new String[]{sendMessage});
		   email.addSubstitution("%followUsOn%", new String[]{followUsOn});
		   email.addSubstitution("%fbAqlliUrl%", new String[]{fb});
		   email.addSubstitution("%twAqlliUrl%", new String[]{tw});
		   email.addSubstitution("%gpAqlliUrl%", new String[]{gp});
		   email.addSubstitution("%reasonEmail%", new String[]{messageAim});
		   email.addSubstitution("%joinUrl%", new String[]{joinUrl});
		   email.addSubstitution("%settingsUrl%", new String[]{settingsUrl});
		   email.addSubstitution("%accountSettingsTxt%", new String[]{settingsUrlTxt});
		   email.addSubstitution("%urlToAqllicom%", new String[]{ aqlliDefaultUrl});
		   response = sendgrid.send( email);
		   res = response.getMessage().contains("success");
		} catch (SendGridException ex) {
			ex.printStackTrace();
		}
		return res;
	}
	public boolean sendPassChangedNotif(String recipientAddress, String subject, String aqlliDefaultUrl, String welcomeBack, String introPhrase, String followUsOn, String fb, String tw, String gp, String settingsUrl, String settingsUrlTxt, String messageAim) {
		boolean res = false;
		email = new SendGrid.Email();
		email.setTemplateId("43e2f3ed-d794-4d5f-8989-0a45f158b3f6");
		email.setASMGroupId( 86);
		email.addTo( recipientAddress).setFrom(NO_REPLY).setFromName("Noreply Aqlli").setSubject(subject);
		email.setHtml( "<i></i>");//html content is already setup as a SendGrip Template. however we need to add something
		SendGrid.Response response = null;
		try {
		   email.addSubstitution("%welcomeBackUser%", new String[]{welcomeBack});
		   email.addSubstitution("%introPhrase%", new String[]{introPhrase});
		   email.addSubstitution("%followUsOn%", new String[]{followUsOn});
		   email.addSubstitution("%fbAqlliUrl%", new String[]{fb});
		   email.addSubstitution("%twAqlliUrl%", new String[]{tw});
		   email.addSubstitution("%gpAqlliUrl%", new String[]{gp});
		   email.addSubstitution("%reasonEmail%", new String[]{messageAim});
		   email.addSubstitution("%settingsUrl%", new String[]{settingsUrl});
		   email.addSubstitution("%accountSettingsTxt%", new String[]{settingsUrlTxt});
		   email.addSubstitution("%urlToAqllicom%", new String[]{ aqlliDefaultUrl});
		   response = sendgrid.send( email);
		   res = response.getMessage().contains("success");
		} catch (SendGridException ex) {
			ex.printStackTrace();
		}
		return res;
	}
	
	public boolean contactUs( String name, String mail, String subject, String message){
		boolean res = false;
		if ( message.trim().equals("")) return res;
		StringBuilder content = new StringBuilder();
		content.append( "<p>De: "  + name + "</p>");
		content.append( "<p>Email: " + mail + "</p>");
		content.append( "<p>Sujet: " + subject + "</p>");
		content.append( "<p>Message: </p>");
		content.append( "<p>" + message + "</p>");
		email = new SendGrid.Email();
		email.setTemplateId("249e9265-f38a-4d24-abf0-17cc17d47ba1");
		email.addTo( CONTACT_AQLLI).setFrom(CONTACT_AQLLI).setFromName( name + " via Aqlli.com").setSubject( subject);
		email.setHtml( content.toString());
		SendGrid.Response response = null;
		try {
		   response = sendgrid.send( email);
		   res      = response.getMessage().contains( "success");
		} catch (SendGridException ex) {
			ex.printStackTrace();
		}
		return res;
	}
	public boolean sendAccountDeleted(String userName, String recipientAddress, String message, String aqlliDefaultUrl, String followUsOn, String fb, String tw, String gp, String messageAim, String subject) {
		boolean res = false;
		if( recipientAddress.equals("")) return res;
		email = new SendGrid.Email();
		email.setTemplateId("d6a33a74-1530-49ae-a939-7d0cb9c0866d");
		email.setASMGroupId( 435);
		email.addTo( recipientAddress).setFrom(NO_REPLY).setFromName("Noreply Aqlli").setSubject(subject);
		email.setHtml( "<i></i>");//html content is already setup as a SendGrip Template. however we need to add something
		SendGrid.Response response = null;
		try {
		   email.addSubstitution("%senderName%", new String[]{userName});
		   email.addSubstitution("%followUsOn%", new String[]{followUsOn});
		   email.addSubstitution("%sendMessage%", new String[]{message});
		   email.addSubstitution("%fbAqlliUrl%", new String[]{fb});
		   email.addSubstitution("%twAqlliUrl%", new String[]{tw});
		   email.addSubstitution("%gpAqlliUrl%", new String[]{gp});
		   email.addSubstitution("%reasonEmail%", new String[]{messageAim});
		   email.addSubstitution("%urlToAqllicom%", new String[]{ aqlliDefaultUrl});
		   response = sendgrid.send( email);
		   res = response.getMessage().contains("success");
		} catch (SendGridException ex) {
			ex.printStackTrace();
		}
		return res;
	}
	
}
