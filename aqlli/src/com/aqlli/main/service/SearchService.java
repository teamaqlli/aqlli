package com.aqlli.main.service;

import java.util.List;

import com.aqlli.main.client.model.UserDetails;
import com.aqlli.main.shared.util.ISearchService;
import com.google.appengine.api.datastore.Query;

/**
 * @author Arix OWAYE / twitter : @arixowaye
 *
 */
public class SearchService implements ISearchService{
	
	/**
	 * Look up for distinct users whome fullname starts with query
	 */
	@Override
	public List<UserDetails> searchUsersbyQuery(final int limit, final String query, final String propertyToLookUp) {
	  return  AqlliOfyService.ofy().load().type( UserDetails.class).filter(propertyToLookUp + " " + Query.FilterOperator.GREATER_THAN_OR_EQUAL, query.toLowerCase()).filter(propertyToLookUp + " " + Query.FilterOperator.LESS_THAN, query.toLowerCase() + "\ufffd").limit(limit).list();
	}	
}
