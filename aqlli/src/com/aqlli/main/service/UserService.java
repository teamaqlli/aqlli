package com.aqlli.main.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.jdo.annotations.Transactional;
import javax.servlet.http.HttpServletRequest;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.aqlli.main.client.model.UserDetails;
import com.aqlli.main.client.model.UserInfosAndSettings;
import com.aqlli.main.client.model.VerificationToken;
import com.aqlli.main.repositories.UserRepository;
import com.aqlli.main.repositories.VerificationTokenRepository;
import com.aqlli.main.shared.exceptions.EmailExistsException;
import com.aqlli.main.shared.exceptions.UserNotFoundException;
import com.aqlli.main.shared.security.AqlliPasswordEncoderGenerator;
import com.aqlli.main.shared.util.CityInfos;
import com.aqlli.main.shared.util.Consts;
import com.aqlli.main.shared.util.FollowUtil;
import com.aqlli.main.shared.util.IUserService;
import com.aqlli.main.shared.util.IdBuilder;

@Service
public class UserService implements IUserService, FollowUtil, UserDetailsService{
	    
		private static String ROLE_USER = "ROLE_USER";
		private UserRepository repository = new UserRepository();
	    private VerificationTokenRepository tokenRepository = new VerificationTokenRepository();
	    private AqlliPasswordEncoderGenerator passEncoder = new AqlliPasswordEncoderGenerator();
	    
	    @Override
		@Transactional
	    public UserDetails registerNewUserAccount(UserDetails accountDto) throws EmailExistsException {
	        boolean continueRegistration = true;
	        try{
	    		if ( emailExist( accountDto.getEmail())) {   
		        	continueRegistration = false;
		        	return null;
		        	//throw new EmailExistsException("There is an account with that email address:" + accountDto.getEmail(), this);
		        }
	    	}catch( Exception e){
	    		e.getMessage();
	    	}
	        UserDetails user = null; 
	        try{
	        	if ( continueRegistration){
	        		user = new UserDetails();
	        		user.setFirstName( accountDto.getFirstName().trim());
		 	        user.setLastName( accountDto.getLastName().trim());
		 	        user.setFullName(accountDto.getFirstName().trim() + " " + accountDto.getLastName().trim());
		 	        user.setPassword( passEncoder.encodePassword( accountDto.getPassword()));
		 	        user.setMatchingPassword( passEncoder.encodePassword( accountDto.getMatchingPassword()));
		 	        user.setUniqueID( IdBuilder.generateId());
		 	        user.setEmail( accountDto.getEmail().toLowerCase().trim());
		 	        user.setBirthDay( accountDto.getBirthDay().trim());
		 	        user.setBirthMonth( accountDto.getBirthMonth().trim());
		 	        user.setBirthYear( accountDto.getBirthYear().trim());
		 	        user.setUseTerms( accountDto.getUseTerms());
		 	        user.setRole( ROLE_USER);
		 	        user.setGender( accountDto.getGender());
		 	        user.initDefaultProfilePicture();
		 	        //user.setCustomeAuthorities(roles);
		 	        return user;
	        	}
	        }catch( Exception e){
	        	e.printStackTrace();
	        }
	        return user;
	    }   
	    private boolean emailExist(String email) {
	        boolean exist = false;
	    	try{
	    		exist = repository.isUserExists( email);
	        }catch( Exception e){
	        	e.getMessage();
	        }
	    	return exist;
	    }

		@Override
		public UserDetails getUser( String verificationToken) {
			UserDetails user = null;
			try{
				user = tokenRepository.findByToken( verificationToken).getUser();
			}catch ( Exception e){
				System.out.println( e.getMessage());
			}
			return user;
		}

		@Override
		public void createVerificationToken(UserDetails user, String token) throws com.googlecode.objectify.SaveException{
			  VerificationToken newToken = new VerificationToken(token, user, Consts.typeTokenRegistration);
			  AqlliOfyService.ofy().save().entity( newToken);
		}
		
		@Override
		public void createInvitationToken(UserDetails inviter, String token) throws com.googlecode.objectify.SaveException{
			  VerificationToken newToken = new VerificationToken(token, inviter, Consts.typeTokenInvitation);
			  AqlliOfyService.ofy().save().entity( newToken);
		}

		@Override
		public VerificationToken getVerificationToken(String verificationToken) {
			return tokenRepository.findByToken( verificationToken);
		}

		@Override
		public void SaveRegisteredUser(UserDetails user) {
			if ( user != null)
			  AqlliOfyService.ofy().save().entity( user).now();
		}

		@Override
		public void deleteUser(UserDetails user) {
			if ( user != null)
			  AqlliOfyService.ofy().delete().entity( user).now();
		}

		@Override
		public UserDetails getUserPrincipal( String email) {
			return AqlliOfyService.ofy().load().type( UserDetails.class).id(email).now();
		}
		public UserDetails getUserByUniqueID( String uniqueID) throws UserNotFoundException{
			UserDetails user = AqlliOfyService.ofy().load().type( UserDetails.class).filter("uniqueID", uniqueID).first().now();
		    if ( user == null) throw new UserNotFoundException(this,  "No user was found with id: " + uniqueID);
			return user;
		}

		@Override
		public org.springframework.security.core.userdetails.UserDetails loadUserByUsername(
				String username) throws UsernameNotFoundException {
			UserDetails user = AqlliOfyService.ofy().load().type( UserDetails.class).id(username).now();
			if ( user == null) throw new UsernameNotFoundException("user not found");
			return user;
		}

		@Override
		public int setLang(UserDetails userDto, String jsonLang) throws JSONException {
			if ( userDto != null && jsonLang.trim() != ""){
				JSONObject obj = new JSONObject( jsonLang);
				String ln = obj.getString("defaultLang");
				userDto.setDefaultLang( ln);
				return 0;
			}
			return -1;
		}
		public UserDetails addUserToRequest( ModelAndView modelAndView, HttpServletRequest request){
			//current logged in user
			UserDetails current = getUserFromRequest( request);
			if( current != null){
				modelAndView.addObject( "user", current);
			}
			return current;
		}
		/**
		 * @param request the Request
		 * @return the user binded to the request
		 */
		public UserDetails getUserFromRequest( HttpServletRequest request){
			if( request.getUserPrincipal() != null){
				String email = request.getUserPrincipal().getName();
				return getUserPrincipal( email);
			}
			return null;
		}
		@Override
		public UserDetails followUser(UserDetails connectedUser, UserDetails userToFollow) {
			if (( userToFollow == null) || (connectedUser == null)) throw new UsernameNotFoundException( "user not found");
			if( !connectedUser.getFollowing().contains(userToFollow.getEmail())){
				connectedUser.getFollowing().add(userToFollow.getEmail());
				AqlliOfyService.ofy().save().entity(connectedUser).now();
			}
			return userToFollow;
		}
		@Override
		public UserDetails unfollowUser(UserDetails connectedUser, UserDetails userToUnFollow) {
			if (( userToUnFollow == null) || (connectedUser == null)) throw new UsernameNotFoundException("user not found");
			if( connectedUser.getFollowing().contains( userToUnFollow.getEmail())){
				connectedUser.getFollowing().remove( userToUnFollow.getEmail());
				AqlliOfyService.ofy().save().entity(connectedUser).now();
			}
			return userToUnFollow;
		}
		
		@Override
		public boolean isAlreadyFollowed(UserDetails connectedUser,
				UserDetails userToTest) {
			return connectedUser.getFollowing().contains( userToTest);
		}
	
		@Override
		public int getNumberOfUser(){
			return AqlliOfyService.ofy().load().type(UserDetails.class).count();
		}
		@Override
		public Long incrementPageNumberOfViews(UserDetails u) {
			UserInfosAndSettings uInfAndS = AqlliOfyService.ofy().load().type( UserInfosAndSettings.class).id( u.getUniqueID()).now();
			if ( uInfAndS == null){
				saveInfosFirst(uInfAndS, u);
				uInfAndS = AqlliOfyService.ofy().load().type( UserInfosAndSettings.class).id( u.getUniqueID()).now();
			}
			uInfAndS.setNbAccountViews( uInfAndS.getNbAccountViews() + 1);
			AqlliOfyService.ofy().save().entity(uInfAndS);
			return uInfAndS.getNbAccountViews();
		}
		@Override
		public Long getPageNumberOfViews(UserDetails u) {
			UserInfosAndSettings uInfAndS = AqlliOfyService.ofy().load().type( UserInfosAndSettings.class).id(u.getUniqueID()).now();
			return saveInfosFirst( uInfAndS, u);
		}
		private Long saveInfosFirst( UserInfosAndSettings uInfAndS, UserDetails u){
			if ( uInfAndS == null){
				uInfAndS = new UserInfosAndSettings();
				uInfAndS.setUserId( u.getUniqueID());
				uInfAndS.setAccountState( uInfAndS.getAccountStates()[1]);
				uInfAndS.setAccountType( uInfAndS.getAccountTypes()[0]);
				uInfAndS.setNbAccountViews((long) 0);
			}
			AqlliOfyService.ofy().save().entity(uInfAndS).now();
			return uInfAndS.getNbAccountViews();
		}
		@Override
		public VerificationToken getInvitationToken(String invToken) {
			return tokenRepository.findByToken( invToken);
		}
		@Override
		public void updateBirthDate(String day, String month, String year, UserDetails user) {
			user.setBirthDay(day);
			user.setBirthMonth(month); //month number
			user.setBirthYear(year);
			user.intiFullBirthDay();
			AqlliOfyService.ofy().save().entities(user).now();
		}
		@Override
		public void updateName(String firstName, String lastName, UserDetails user) {
			user.setFirstName(firstName);
			user.setLastName(lastName);
			user.setFullName(firstName + " " + lastName);
			AqlliOfyService.ofy().save().entities(user).now();
		}
		@Override
		public void updateSocialBtns(String twitterId, String fbId,
				String gpId, String webs, UserDetails user) {
			String tw = twitterId;
			if( twitterId.startsWith("@")) tw = twitterId.replace("@", "");
			if( user != null){
				user.setTwitterFollowMe( tw);
				user.setFacebookFollowME( fbId);
				user.setGooglePlusFollowMe( gpId);
				user.setWebsite( webs);	
				AqlliOfyService.ofy().save().entities(user).now();
			}
		}
		@Override
		public void updateDescriptionInfos(String description, UserDetails user) {
			if ( user != null){
			  UserInfosAndSettings uIn = AqlliOfyService.ofy().load().type( UserInfosAndSettings.class).id( user.getUniqueID()).now();
			  if( uIn != null){
				  uIn.setUserDescription( description);
				  AqlliOfyService.ofy().save().entities( uIn).now();
			  }
			}
		}
		
		@Override
		public void updateHForInfos(String hFor, UserDetails user) {
			if( user != null){
			  UserInfosAndSettings uIn = AqlliOfyService.ofy().load().type( UserInfosAndSettings.class).id( user.getUniqueID()).now();
			    if( uIn != null){
				  uIn.setHereFor( hFor);
				  AqlliOfyService.ofy().save().entities( uIn).now();
				}
			 }
		}
		
		@Override
		public void updateNotifsSettings(String type, boolean value, UserDetails user) {
			if( user != null){
			  UserInfosAndSettings uIn = AqlliOfyService.ofy().load().type( UserInfosAndSettings.class).id( user.getUniqueID()).now();
			    if( uIn != null){
				  if( type == "desktopNotifs"){
					  uIn.setActivateDesktopNotifs( value);  
				  }
				  if( type == "desktopNotifs"){
					  uIn.setActivateEmailNotifs( value);
				  }
				  AqlliOfyService.ofy().save().entities( uIn).now();
				}
			 }
		}
		
		@Override
		public void updateOccupInfos(String poste, String company,
				String citydata, UserDetails user) {
			if ( user != null){
			  user.setCompany( company);
			  user.setJobPosition(poste);
			  user.setLocation( citydata);//json string
			  AqlliOfyService.ofy().save().entities( user).now();
			}
		}
		@Override
		public boolean canFollow(String currentUserID, String userToFollowID) {
			boolean res = false;
			if( currentUserID.equals(userToFollowID)) return res; //user cannot follow himself
			try {
				UserDetails current  = getUserByUniqueID( currentUserID);
				UserDetails toFollow = getUserByUniqueID( userToFollowID);
				if(( current != null) && ( toFollow != null)){
					res = !current.getFollowing().contains( userToFollowID);
				}
			} catch (UserNotFoundException ex) {
				ex.printStackTrace();
			}
			return res;
		}
		@Override
		public boolean canUnFollow(String currentUserID, String userToUnFollowID) {
			boolean res = false;
			if( currentUserID.equals(userToUnFollowID)) return res; //user cannot follow himself
			try {
				UserDetails current  = getUserByUniqueID( currentUserID);
				UserDetails toFollow = getUserByUniqueID( userToUnFollowID);
				if(( current != null) && ( toFollow != null)){
					res = current.getFollowing().contains( userToUnFollowID);
				}
			} catch (UserNotFoundException ex) {
				ex.printStackTrace();
			}
			return res;
		}
		@Override
		public boolean processUnFollowing(UserDetails currentUser,
				UserDetails userToUnFollow) {
			if( currentUser.getUniqueID().equals( userToUnFollow.getUniqueID())) return false;
			currentUser.getFollowing().remove(userToUnFollow.getUniqueID());
			userToUnFollow.getFollowers().remove( currentUser.getUniqueID());
			AqlliOfyService.ofy().save().entities( currentUser).now();
            AqlliOfyService.ofy().save().entities( userToUnFollow).now();
			return true;
		}
		
		@Override
		public boolean processFollowing(UserDetails currentUser,
				UserDetails userToFollow) {
			if( currentUser.getUniqueID().equals( userToFollow.getUniqueID())) return false;
			currentUser.getFollowing().add(userToFollow.getUniqueID());
			userToFollow.getFollowers().add( currentUser.getUniqueID());
            AqlliOfyService.ofy().save().entities( currentUser).now();
            AqlliOfyService.ofy().save().entities( userToFollow).now();
			return true;
		}

		@Override
		public boolean isUserAReader( final UserDetails authenticatedUser, final UserDetails userToTest){
		  if( ( authenticatedUser == null) || ( userToTest == null) || authenticatedUser.getUniqueID().equals(userToTest.getUniqueID())) return false;
		  return userToTest.getFollowing().contains( authenticatedUser.getUniqueID());
		}
		@Override
		public void savePageVisite(UserDetails user, UserDetails visitedUser) {
			if( user != null){
			  if( user.getRecentlyVisited().containsKey( visitedUser.getUniqueID())){
				  int nbVisite = user.getRecentlyVisited().get( visitedUser.getUniqueID());
				  user.getRecentlyVisited().remove( visitedUser.getUniqueID());
				  user.getRecentlyVisited().put( visitedUser.getUniqueID(), ++nbVisite);
			  }else{
				  user.getRecentlyVisited().put(visitedUser.getUniqueID(), 1);
			  }
			  AqlliOfyService.ofy().save().entities( user);
			}
		}
		
		@Override
		public List<CityInfos> readersCitiesInfos( UserDetails user) {
			
			List<CityInfos> citiesInf 	  = new ArrayList<CityInfos>();
			Map<String[], Integer> tmpMap = new HashMap<String[], Integer>();
			JSONObject citydatajson 	  = null;
			if( user == null) return citiesInf;
			//update cities count
			for( String readers  : user.getFollowers()){
				try {
					UserDetails u  = getUserByUniqueID( readers);
					if( u != null){
						String loc = u.getLocation();
						String   c = null;
						String lat = null;
						String lng = null;
						String country = null;
						try {
							citydatajson = new JSONObject(loc);
							c 			 = citydatajson.getString( "name");
							lat = citydatajson.getString( "latitude");
							lng = citydatajson.getString( "longitude");
							country = citydatajson.getString( "country");
							if( c != null && !c.trim().equals( "")){
								String[] k = {c, lat, lng, country}; //city=0, lat=1, long=2, country=3
								if( tmpMap.containsKey( c)){
									Integer count = tmpMap.get( c);
									tmpMap.remove( k);
									tmpMap.put( k, count++);
								}else{
									tmpMap.put( k, 0);
								}
							}
						} catch (JSONException ex) {
							ex.printStackTrace();
						}
					}
				} catch (UserNotFoundException ex) {
					ex.printStackTrace();
				}
			}
			
			//update cities info objects
			for( Map.Entry<String[], Integer> entry : tmpMap.entrySet()){
				CityInfos cityinf 	= new CityInfos();
				cityinf.setCityName( entry.getKey()[ 0]);
				cityinf.setLatitude( entry.getKey()[ 1]);
				cityinf.setLongitude( entry.getKey()[ 2]);
				cityinf.setCountry( entry.getKey()[ 3]);
				cityinf.setCount( entry.getValue());
				citiesInf.add( cityinf);
			}
			//add a cityinfo for testing - remove this in production mode
			/*for( int i = 0; i < 2; i++){
				CityInfos cityinf 	= new CityInfos();
				String city = "";
				if( i == 0){ city = "Lisboa";
				cityinf.setLatitude("0");
				cityinf.setLongitude("0");
				}
				if( i == 1){ city = "Libreville";
				cityinf.setLatitude("50");
				cityinf.setLongitude("70");
				}
				cityinf.setCityName( city);
				cityinf.setCount( 10);
				citiesInf.add( cityinf);		
			}*/
			return citiesInf;
		}
		
	}