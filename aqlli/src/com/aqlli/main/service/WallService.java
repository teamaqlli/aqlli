package com.aqlli.main.service;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.aqlli.main.client.model.AqNotification;
import com.aqlli.main.client.model.AqlliPost;
import com.aqlli.main.client.model.ReportABug;
import com.aqlli.main.client.model.UserDetails;

import static com.aqlli.main.service.AqlliOfyService.ofy;

import com.aqlli.main.shared.util.IWallService;
import com.aqlli.main.shared.util.PostCounter;
import com.google.appengine.api.datastore.Text;

public class WallService implements IWallService {
	private static final String BTNLINK = "btn-link";
	
	@Override
	public AqlliPost addPost(UserDetails userDto, String data) {
		AqlliPost aqPost = new AqlliPost();;
		Text text;
		if ( userDto == null) return aqPost;
		JSONObject obj;
		try {
			obj 			 = new JSONObject( data);
			String title     = (String) obj.get( "title");
			String message   = (String) obj.get( "message");
			String videoUrl  = (String) obj.get( "videoUrl");
			String user_id 		 = (String) obj.get( "id");
			String shareType = (String) obj.get( "shareType");
			String created   = (String) obj.get( "created");
			shareType 		 = shareType.replace( BTNLINK, "").trim();
			aqPost.setCeated( created);
			aqPost.setTitle( title);
			aqPost.setUser_id( user_id);
			aqPost.setShareType( shareType);
			aqPost.setUserProfilePic( userDto.getDefaultUserAvatarUrl());
			int pId = PostCounter.getNextValue( user_id);
			aqPost.setPostId( pId);
			if ( message.trim() != ""){
			  text 			 = new Text( message);
			  aqPost.setMessage(text);
			}
			aqPost.setVideoUrl(videoUrl);
			ofy().save().entity( aqPost).now();
		} catch (JSONException ex) {
			ex.printStackTrace();
		}
		return aqPost;
	}

	@Override
	public boolean deletePost( Long postId) {
		if ( postId == null) return false;
		AqlliPost post = ofy().load().type( AqlliPost.class).id( postId).now();
		ofy().delete().entity( post);
		return true;
	}
	@Override
	public List<AqlliPost> getNextPosts(UserDetails user, String lastPId, int i) {
		List<AqlliPost> range = new ArrayList<AqlliPost>();
		if ( user == null) return range;
		if ( lastPId.trim() == "") return range;
		if ( i <= 0) i = 5;
		int id = Integer.valueOf( lastPId);
		range = ofy().load().type( AqlliPost.class).filter("user_id", user.getUniqueID()).filter("postId <", id).limit(i).order("-postId").list();
		for( AqlliPost post : range){
			  updateProfilePicture( user, post);
		}
		return range;
	}

	@Override
	public List<AqlliPost> initWall(UserDetails user, int i) {
		List<AqlliPost> range = new ArrayList<AqlliPost>();
		if ( user == null) return range;
		if ( i <= 0) i = 5;
		  range = ofy().load().type( AqlliPost.class).filter("user_id", user.getUniqueID()).limit(i).order("-created").list();
		  for( AqlliPost post : range){
			  updateProfilePicture( user, post);
		  }
		  return range;
	}
	
	private void updateProfilePicture( UserDetails user, AqlliPost aqPost){
		aqPost.setUserProfilePic( user.getDefaultUserAvatarUrl());
	}

	@Override
	public AqlliPost findPostByUniqueId(Long postId) {
		if ( postId == null) return null;
		AqlliPost post = ofy().load().type( AqlliPost.class).id( postId).now();
		return post;
	}

	@Override
	public void savePost(AqlliPost post) {
		if( post != null){
			ofy().save().entities( post).now();
		}
	}

	@Override
	public void saveBugReport(ReportABug rep) {
		if( rep != null){
			AqlliOfyService.ofy().save().entities( rep);
		}
	}

	public void saveNotification(AqNotification notif) {
		if( notif != null)
			AqlliOfyService.ofy().save().entities( notif).now();
	}
	
	@Override
	public void deleteNotification(AqNotification notif) {
		if( notif != null)
			AqlliOfyService.ofy().delete().entities( notif);
	}
	
	@Override
	public AqNotification findNotifById(Long id) {
		if( id == null) throw new NullPointerException();
		return AqlliOfyService.ofy().load().type( AqNotification.class).id( id).now();
	}
	
	@Override
	public List<AqNotification> getNotifications( String receiverId, Long lastId, int limit) {
		List<AqNotification> l = new ArrayList<>();
		UserDetails u = AqlliOfyService.ofy().load().type( UserDetails.class).id( receiverId).now();
		if( lastId != null && limit > 0 && u != null)
			l = AqlliOfyService.ofy().load().type(AqNotification.class).filter("lastId >=", lastId).limit( limit).list();
		return l;
	}

}
