package com.aqlli.main.service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.aqlli.main.client.model.UserDetails;

public class PymkService {
	
	/**
	 * @param userId connected User
	 * @param limit size of the reco list
	 * @return reco list
	 */
	Map<String, Integer> pymk = null;
	public Map<String, Integer> peopleYouMayKnow( UserDetails user, int limit, String tagToFilter){
		//if the users follow the same users then they may have some same interrest
		pymk = new LinkedHashMap<String, Integer>();
		List<String> tempCommon = null;
		//put already visited users
		pymk.putAll( user.getRecentlyVisited());
		//search for other users to add
		for( UserDetails u : getUsers( limit, tagToFilter)){
		  if( !user.getFollowing().contains( u.getUniqueID())){
			  tempCommon = new ArrayList<String>( user.getFollowing());
			  tempCommon.retainAll( u.getFollowing());
			  if( tempCommon.size() > 0){
			     for( String usr : tempCommon){
			    	 if( pymk.containsKey( usr)){
			    	   int i = pymk.get( usr);
			    	   pymk.remove( usr);
			    	   pymk.put( usr, ++i);
			    	 }else{
			    	   pymk.put(usr, 1); 
			    	 }
			     }
			  }
		  }
		}
		
	    return pymk;
	}
	
	public List<UserDetails> lastRegistrationInCity( int limit, String tag){
		if( tag != null)
			return AqlliOfyService.ofy().load().type(UserDetails.class).filter(tag, tag).reverse().limit( limit).list();
		return AqlliOfyService.ofy().load().type(UserDetails.class).reverse().limit( limit).list();
	}
	
	private List<UserDetails> getUsers( int limit, String tag){
		if( tag != null)
		  return AqlliOfyService.ofy().load().type(UserDetails.class).filter(tag, tag).limit( limit).list();
		return AqlliOfyService.ofy().load().type(UserDetails.class).limit( limit).list();
	}

}
