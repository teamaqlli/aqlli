package com.aqlli.main.service;

import com.aqlli.main.client.model.ImportedContactsInviter;
import com.aqlli.main.shared.util.IInviteFriend;
import com.googlecode.objectify.ObjectifyService;

public class InviteFriendService implements IInviteFriend{

	@Override
	public void singleInvitation(final String user_email, final String friends_email) {
		ImportedContactsInviter importedContacts = ObjectifyService.ofy().load().type( ImportedContactsInviter.class).id(user_email).now();
		
		if( importedContacts == null){
			importedContacts = new ImportedContactsInviter();
			importedContacts.setInvited_user_id( user_email);
			importedContacts.setType( false);
		}
		
		importedContacts.getImportedEmails().add( friends_email);
		ObjectifyService.ofy().save().entities( importedContacts);
	}
}
