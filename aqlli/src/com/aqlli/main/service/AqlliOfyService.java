package com.aqlli.main.service;

import com.aqlli.main.client.model.AqNotification;
import com.aqlli.main.client.model.AqlliPost;
import com.aqlli.main.client.model.ImportedContactsInviter;
import com.aqlli.main.client.model.ReportABug;
import com.aqlli.main.client.model.UnikIdSeq;
import com.aqlli.main.client.model.UserDetails;
import com.aqlli.main.client.model.UserInfosAndSettings;
import com.aqlli.main.client.model.VerificationToken;
import com.aqlli.main.repositories.RememberMeToken;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.util.Closeable;

public class AqlliOfyService {
	
	static
	{
		ObjectifyService.factory().register( UserDetails.class);
		ObjectifyService.factory().register( VerificationToken.class);
		ObjectifyService.factory().register( RememberMeToken.class);
		ObjectifyService.factory().register( AqlliPost.class);
		ObjectifyService.factory().register( UnikIdSeq.class);
		ObjectifyService.factory().register( UserInfosAndSettings.class);
		ObjectifyService.factory().register( ImportedContactsInviter.class);
		ObjectifyService.factory().register( ReportABug.class);
		ObjectifyService.factory().register( AqNotification.class);
	}
	
	public static Objectify ofy() {
		ObjectifyService.begin();
        return ObjectifyService.ofy();
    }
	
	public static Closeable clocableOfy(){
		return ObjectifyService.begin();
	}

    public static ObjectifyFactory factory() {
        return ObjectifyService.factory();
    }
}