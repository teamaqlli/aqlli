package com.aqlli.main.server;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
/**
 * Enabling CORS support - Access-Control-Allow-Origin
 * @author Arix OWAYE / twitter : @arixowaye
 */
@Component
public class CORSFilter extends OncePerRequestFilter{ //This class sets access control origin on each request
	private static final Log LOG = LogFactory.getLog(CORSFilter.class);
	@Override
	protected void doFilterInternal(HttpServletRequest request,
			HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		  response.addHeader( "Access-Control-Allow-Origin", "*");//remplacer * par http://www.aqlli.com pour ne limiter l'accès que à cette url
		  if ( request.getHeader( "Access-Control-Request-Method") != null && "OPTIONS".equalsIgnoreCase( request.getMethod())){
			  LOG.trace( "Sending Header: " + request.getHeaderNames());
			  response.addHeader( "Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");//options possibles GET, POST, PUT, DELETE
			  //response.addHeader( "Access-Control-Allow-Headers","Authorization"); authorization: envoie aussi les cookies dans la requete
			  response.addHeader( "Access-Control-Allow-Headers", "Content-Type, x-requested-with");
			  response.addHeader( "Access-Control-Max-Age", "1");
		  }
		filterChain.doFilter(request, response);
	}
}
