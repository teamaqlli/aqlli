package com.aqlli.main.server;

import java.io.IOException;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.mail.BounceNotificationParser;

//source: google
public class BounceHandlerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	public void doPost( HttpServletRequest req, HttpServletResponse resp) throws IOException{
		com.google.appengine.api.mail.BounceNotification bounce;
		try {
			bounce = BounceNotificationParser.parse( req);
			bounce.getNotification().getFrom();
			bounce.getNotification().getTo();
			bounce.getNotification().getSubject();
			bounce.getNotification().getText();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
		
	}
}