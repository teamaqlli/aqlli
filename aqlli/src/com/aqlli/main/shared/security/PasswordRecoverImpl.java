package com.aqlli.main.shared.security;

import org.springframework.util.Assert;

import com.aqlli.main.client.model.UserDetails;
import com.aqlli.main.client.model.VerificationToken;
import com.aqlli.main.repositories.UserRepository;
import com.aqlli.main.service.UserPasswordServiceImpl;
import com.aqlli.main.shared.exceptions.UserNotEnabledException;
import com.aqlli.main.shared.exceptions.UserNotFoundException;
import com.aqlli.main.shared.util.Consts;
import com.aqlli.main.shared.util.IdBuilder;
import com.aqlli.main.shared.util.PasswordRecoverInterface;

public class PasswordRecoverImpl implements PasswordRecoverInterface {
    private UserRepository userRepo = new UserRepository();
    private UserPasswordServiceImpl userPassService = new UserPasswordServiceImpl();
    private AqlliPasswordEncoderGenerator pGen = new AqlliPasswordEncoderGenerator();
    
    @Override
    public boolean resetPassword(String oldPass, String newPass, UserDetails user) {
    	boolean res = false;
    	try{
    		Assert.notNull( user);
    		if( user.isEnabled()){
    			if( pGen.isPasswordMatching(oldPass, user.getPassword())){
    			  
    			  user.setPassword( pGen.encodePassword( newPass));
    			  user.setMatchingPassword( pGen.encodePassword( newPass));
    			  userRepo.save( user);
    			  res = true;
    			}
    		}
    	}catch( IllegalArgumentException ie){
    		ie.printStackTrace();
    	}
		return res;
	}

	@Override
	public VerificationToken lostPasswordChange(String email) throws UserNotFoundException, UserNotEnabledException {
		VerificationToken token = null;
		try{
			Assert.notNull( email);
			UserDetails user = userRepo.loadUser( email);
			if ( user == null) throw new UserNotFoundException(this, " No user with email: " + email);
			//if ( !user.isEnabled()) throw new UserNotEnabledException(this, " User: " + user.getEmail());
			token = userPassService.getRecoverPassToken(user);
			if ( token != null){
			  userPassService.deleteRecoverPassToken( token); 
			}
			token = new VerificationToken(IdBuilder.generateId(), user, Consts.typeTokenRecoverPassword);
			userPassService.SavePasswordRecoverToken( token);
    	}catch( IllegalArgumentException ie){
    		ie.printStackTrace();
    	}
		return token;
	}

	@Override
	public boolean resetPassword(String newPass, UserDetails user) {
		String nPassEncoded = pGen.encodePassword( newPass);
		user.setPassword( nPassEncoded);
		user.setMatchingPassword( nPassEncoded);
		userRepo.save( user);
		return true;
	}

}
