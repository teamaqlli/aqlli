package com.aqlli.main.shared.security;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.aqlli.main.client.model.UserDetails;
import com.aqlli.main.shared.exceptions.EmailInvalideException;
import com.aqlli.main.shared.exceptions.MalformedPasswordException;
import com.aqlli.main.shared.exceptions.PasswordNotMatchingException;
import com.aqlli.main.shared.exceptions.UserTermsStateException;
import com.aqlli.main.shared.util.EmailValidator;
import com.aqlli.main.shared.util.PasswordMatchesValidator;

/**
 * 
 * @author Arix OWAYE / twitter : @arixowaye
 *
 */
public class ServerSideUserValidator {
	  private UserDetails user;
	  private EmailValidator emailValidator;
	  private static final String AGREE = "agree";
	  PasswordMatchesValidator passValidator = new PasswordMatchesValidator();
	  /**
	   * @param user
	   */
	  public ServerSideUserValidator( UserDetails user){
	    this.user = user;
	    emailValidator = new EmailValidator();
	  }

	  /**
	   * @return
	   * @throws EmailInvalideException
	   */
	  public boolean isEmailValid() throws EmailInvalideException {
	    if( !emailValidator.isValid( user.getEmail())) throw new EmailInvalideException( this, "email not valid");
		return emailValidator.isValid( user.getEmail());
	  }
	  /**
	   * @return
	   * @throws PasswordNotMatchingException
	   */
      //use this before the passwords have been encrypted
	  public boolean isPassValid() throws PasswordNotMatchingException {
		if( !user.getPassword().equals( user.getMatchingPassword())) throw new PasswordNotMatchingException( this, "passwords not equals"); 
	    if (( user.getPassword() == "") && ( user.getMatchingPassword() == "")) return true;
		return user.getPassword().equals( user.getMatchingPassword());
	  }

	  /**
	   * @return
	   * @throws UserTermsStateException
	   */
	  public boolean isUseTermsValid( ) throws UserTermsStateException {
	    if( (user.getUseTerms() == null) || !user.getUseTerms().equals( AGREE)) throw new UserTermsStateException( this, "User terms not accepted");
	    return user.getUseTerms().equals( AGREE);
	  }
	  
	  /**
	   * @return
	   * @throws ParseException
	   */
	  public boolean isBirthDayValid() throws ParseException{
		  Date d = new Date();
		  SimpleDateFormat formater = new SimpleDateFormat("dd-MM-yyyy");
		  formater.setLenient( false);//Ne pas déduire de date en cas d'erreur
	      String date = user.getBirthDay() + "-" + user.getBirthMonth() + "-" + user.getBirthYear();
		  d = formater.parse( date);
		  return ( d == null) ? false : true;
	  }
	  /**
	   * @param password password entered by the user in the recover password form
	   * @return boolean is the password valid
	   * @throws MalformedPasswordException
	   */
	  public boolean isPasswordValid( String password) throws MalformedPasswordException{
		 if ( !passValidator.isValid( password)) throw new MalformedPasswordException( this, "this password word does not match policy");
	     return true;
	  }
	}