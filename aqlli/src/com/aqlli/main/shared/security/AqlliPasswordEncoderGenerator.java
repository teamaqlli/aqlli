package com.aqlli.main.shared.security;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
public class AqlliPasswordEncoderGenerator{

	private final BCryptPasswordEncoder passwordEncoder;
	
	public AqlliPasswordEncoderGenerator(){
		passwordEncoder = new BCryptPasswordEncoder();
	}
	
	public String encodePassword( String clearPass){
		return passwordEncoder.encode( clearPass);
	}
	public boolean isPasswordMatching( CharSequence rawPassword, String encodedPassword){
		return passwordEncoder.matches(rawPassword, encodedPassword);
	}
	
	public static void main( String args){
		AqlliPasswordEncoderGenerator enc = new AqlliPasswordEncoderGenerator();
		String pass1 = "12345";
		String encodedPass1 = enc.encodePassword(pass1);
		
		System.out.println( "pass1: " + pass1);
		System.out.println( "encoded pass1: " + encodedPass1);
		
		System.out.println( "is pass1 matching? : " + enc.isPasswordMatching(pass1, encodedPass1));
		AqlliPasswordEncoderGenerator enc2 = new AqlliPasswordEncoderGenerator();
		System.out.println( "is pass1 matching? : " + enc2.isPasswordMatching(pass1, encodedPass1));
	}
}
