package com.aqlli.main.shared.exceptions;

@SuppressWarnings("serial")
public class UserNotFoundException extends Exception{
	
	Object obj;
	public UserNotFoundException( Object obj, String mess){
		super(mess);
	}
	
	public Object objSource(){
		return obj;
	}
}
