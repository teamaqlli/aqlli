package com.aqlli.main.shared.exceptions;

@SuppressWarnings("serial")
public class EmailInvalideException extends Exception{
	
	Object obj;
	public EmailInvalideException(Object obj, String message){
		super( message);
		this.obj = obj;
	}

	public Object getObj(){
		return obj;
	}
}
