package com.aqlli.main.shared.exceptions;

public class EmailExistsException extends Exception {

	private static final long serialVersionUID = 1L;
	
	private Object obj;
	public EmailExistsException(String message, Object obj){
		super( message);
		this.setObj(obj);
	}
	/**
	 * @return the obj
	 */
	public Object getObj() {
		return obj;
	}
	private void setObj(Object obj) {
		this.obj = obj;
	}

}
