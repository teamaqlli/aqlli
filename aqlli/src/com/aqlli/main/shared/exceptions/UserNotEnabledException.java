package com.aqlli.main.shared.exceptions;

@SuppressWarnings("serial")
public class UserNotEnabledException extends Exception {
	
	Object obj;
	public UserNotEnabledException( Object obj, String message){
		super( message);
	    this.obj = obj;
	}
	public Object getObj(){
		return obj;
	}
}
