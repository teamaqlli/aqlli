package com.aqlli.main.shared.exceptions;

@SuppressWarnings("serial")
public class UserTermsStateException extends Exception {
	Object obj;
	
	public UserTermsStateException ( Object obj, String message){
		super( message);
		this.obj = obj;
	}
	
	public Object getObj(){
		return obj;
	}
}
