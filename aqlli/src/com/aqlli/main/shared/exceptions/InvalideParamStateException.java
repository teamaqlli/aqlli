package com.aqlli.main.shared.exceptions;

public class InvalideParamStateException extends Exception {
	private static final long serialVersionUID = 1L;
	
	private Object obj;
	public InvalideParamStateException(String message, Object obj) {
		super(message);
		this.obj = obj;
	}
	public Object getObj(){
		return obj;
	}

}
