package com.aqlli.main.shared.exceptions;

@SuppressWarnings("serial")
public class PasswordNotMatchingException extends Exception {
	Object obj;  
	public PasswordNotMatchingException( Object obj, String message){
		  super( message);
		  this.obj = obj;
	  }
	public Object getObj(){
		return obj;
	}

}
