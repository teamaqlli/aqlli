package com.aqlli.main.shared.exceptions;

@SuppressWarnings("serial")
public class MalformedPasswordException extends Exception {
	Object obj;
	public MalformedPasswordException( Object obj, String message){
	  super( message);
	  this.obj = obj;
	}
	public Object getObj(){
		return obj;
	}
}
