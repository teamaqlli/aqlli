package com.aqlli.main.shared;

public class Endpoints {
	public final static String LOGIN 				= "/login";
	public final static String LOGINHELP 			= "/login/help";
	public final static String PASSLOST 			= "/login/password.lost";  /*request - forgotten password, anonymous user*/
	public final static String SETNEWPASS 			= "/api/login/password.reset"; /*connected user*/
	public final static String CHANGEPASS 			= "/login/password.change"; /*Anonymous user - final form*/
	public final static String HOME 				= "/home";
	public final static String USERS 				= "/users";
	public final static String REGISTRATION 		= "/user/registration";
	public final static String SETTINGS 			= "/settings";
	public final static String REGHELP 				= "/user/registration/help";
	public final static String CONFIRMREG 			= "/user/confirm";
	public final static String CONFIRMREGEXPIRED  	= "/user/confirm?expired";
	public final static String CONFIRMREGNEWTOKEN 	= "/user/confirm/reset";
	public final static String INVITE 				= "/invite";
	public final static String LEGAL 				= "/legal";
	public final static String TERMS 				= "/legal/terms";
	public final static String COOKIES 				= "/legal/cookies";
	public final static String PRIVACY 				= "/legal/privacy";
	public final static String USERDELETE 			= "/user/delete";
	public final static String UPDWALL 				= "/api/updatewall";
	public final static String REPORTBUG			= "/api/reportbug";
	public final static String INITWALL 			= "/api/initwall";
	public final static String CITIESINFOS  		= "/api/map/citiesinfos"; /*connected user*/
	public final static String NOTIFY				= "/api/notify";
	public final static String PAGES 				= "/pages";
	public final static String PAGESTHNKS 			= "/pages/thanks";
	public final static String PAGESCAREERS 		= "/pages/careers";
	public final static String UPDLANG 				= "/api/update.lang"; /*connected user*/
    public static final String ERROR_URL 			= "/error";
    public static final String ERROR404				= "/404";
    public static final String ERROR403 			= "/403";
    public static final String SOCIALAUTH 			= "/socialauth";
    public static final String SOCIALAUTHSUCCESS 	= "/socialauth/authsuccess";
    public static final String SOCIALAUTHDENIED 	= "/socialauth/authdenied";
	public static final String CONTACTUS 			= "/contact/message";
	public static final String SINGLEINVITE	 		= "/user/invite";
	public static final String MESSAGES 			= "/messages";
	public static final String ROOT 				= "/";
	public static final String UPDINFOS 			= "/api/settings";
	public static final String USRFOLLOWORUNFOLLW 	= "/api/follow";
	public static final String POSTLIKEORSHARE	  	= "/api/post"; //used to like or share a post; params--> ?type=like/share; ?postId
	public static final String SEARCH 				= "/search";
    
}
