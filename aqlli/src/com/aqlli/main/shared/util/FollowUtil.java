package com.aqlli.main.shared.util;

import com.aqlli.main.client.model.UserDetails;

public interface FollowUtil {

	
	public boolean canFollow( final String currentUserID, final String userToFollowID);
	public boolean canUnFollow( final String currentUserID, final String userToUnFollowID);
	public boolean processUnFollowing( final UserDetails currentUser, final UserDetails userToUnFollow);
	public boolean processFollowing(UserDetails currentUser,
			UserDetails userToUnFollow);
	public boolean isUserAReader( final UserDetails authenticatedUser, final UserDetails userToTest);
}
