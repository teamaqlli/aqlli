package com.aqlli.main.shared.util;

import java.util.Collection;

import com.aqlli.main.client.model.UserDetails;
import com.aqlli.main.shared.exceptions.UserNotEnabledException;
import com.aqlli.main.shared.exceptions.UserNotFoundException;
public interface UserProviderInterface {
	public UserDetails loadUser( String userId) throws UserNotFoundException, UserNotEnabledException;
	public Collection<UserDetails> findByName(String username); 
}