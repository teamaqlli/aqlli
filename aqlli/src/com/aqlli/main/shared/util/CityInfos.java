package com.aqlli.main.shared.util;

public class CityInfos{
    
    private String cityName;

    private int count;
    
    private String latitude,  longitude,  country;
    
    public String getLatitude() {
		return this.latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return this.longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	private String error;
    
    
    public CityInfos(){}

    public String getCityName(){
    	return cityName;
    }

    public int getCount(){
    	return count;
    }

    public void setCityName( String cityName){
    	this.cityName = cityName;
    }
    
    public void setCount( int count){
    	this.count = count;
    }
    
    public String getError(){
    	return error;
    }

    public void setError( String error){
    	this.error = error;
    }
}