package com.aqlli.main.shared.util;

import javax.persistence.Transient;

import com.aqlli.main.client.model.AqlliPost;
import com.googlecode.objectify.Key;

public class KeyGetter {
	
	@Transient
	public Key<AqlliPost> getAqlliPostKey( AqlliPost post) {
	   return Key.create( AqlliPost.class, post.getIdent());
	}

}
