package com.aqlli.main.shared.util;


public interface UserPrincipaleInterface {
  String getRole();
  String getUserId();
}
