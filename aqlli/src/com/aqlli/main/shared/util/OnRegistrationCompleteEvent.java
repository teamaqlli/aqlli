package com.aqlli.main.shared.util;

import java.util.Locale;

import org.springframework.context.ApplicationEvent;

import com.aqlli.main.client.model.UserDetails;

@SuppressWarnings("serial")
public class OnRegistrationCompleteEvent extends ApplicationEvent{
	  private final String appUrl;
	  private final UserDetails user;
	  private Locale locale;
	  
	  
	public OnRegistrationCompleteEvent( UserDetails user, Locale locale, String appUrl){
	    super(user);
		this.user = user;
		this.appUrl = appUrl;
		this.locale = locale;
	  }

	public String getAppUrl() {
		return appUrl;
	}

	public UserDetails getUser() {
		return user;
	}
	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	 }