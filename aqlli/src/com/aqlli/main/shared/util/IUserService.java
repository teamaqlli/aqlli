package com.aqlli.main.shared.util;

import java.util.List;

import org.json.JSONException;

import com.aqlli.main.client.model.UserDetails;
import com.aqlli.main.client.model.VerificationToken;
import com.aqlli.main.shared.exceptions.EmailExistsException;
import com.aqlli.main.shared.exceptions.UserNotFoundException;


public interface IUserService {
	UserDetails registerNewUserAccount(UserDetails accountDto)     
      throws EmailExistsException;
	
	UserDetails getUser( String verificationToken);
	
	void createVerificationToken( UserDetails user, String token);
	
	void createInvitationToken( UserDetails user, String token);
	
	VerificationToken getVerificationToken( String verificationToken);
	
	VerificationToken getInvitationToken( String invToken);
	
	
	void SaveRegisteredUser( UserDetails user);
	
	void deleteUser( UserDetails user);
	
	UserDetails getUserPrincipal( String email);
	
	int setLang( UserDetails userDto, String jsonLang) throws JSONException;
	
	UserDetails followUser( UserDetails connectedUser, UserDetails userToFollow) throws UserNotFoundException;
	
	UserDetails unfollowUser ( UserDetails connectedUser, UserDetails userToUnFollow) throws UserNotFoundException;
	
	boolean isAlreadyFollowed( UserDetails connectedUser, UserDetails userToTest);
	
	int getNumberOfUser();
	
	Long incrementPageNumberOfViews( UserDetails u);
	
	Long getPageNumberOfViews( UserDetails u);
	
	void updateBirthDate(final String day, final String month, final String year, final UserDetails user);
	
	void updateName(final String firstName, final String lastName, final UserDetails user);
	
	void updateSocialBtns(final String twitterId, final String fbId, final String gpId, final String webs, final UserDetails user);
	
	void updateDescriptionInfos( final String description, final UserDetails user);
	
	void updateOccupInfos( final String poste, final String company, final String hometown, final UserDetails user);
	
	void updateHForInfos( final String hFor, final UserDetails user);
	
	void savePageVisite( UserDetails user, UserDetails visitedUser);
	
	List<CityInfos> readersCitiesInfos( UserDetails user);

	void updateNotifsSettings(String type, boolean value,
			UserDetails user);
}
