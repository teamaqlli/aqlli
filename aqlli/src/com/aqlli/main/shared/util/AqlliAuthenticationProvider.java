package com.aqlli.main.shared.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.aqlli.main.client.model.UserDetails;
import com.aqlli.main.repositories.UserRepository;
import com.aqlli.main.shared.security.AqlliPasswordEncoderGenerator;

/**
 * The "AuthenticationProvider" components perform the authentication, i.e.,
 * they manage the checking of user's identity.
*/
public class AqlliAuthenticationProvider implements AuthenticationProvider {
	private boolean isAuth;
	private UserRepository repository = new UserRepository();
	private UserDetails userInDataStore = null;
	private String userInDataStorePwd;
	private AqlliPasswordEncoderGenerator passEncoder = new AqlliPasswordEncoderGenerator();
	
	// ---------------------------------- PUBLIC METHODS
	@Override
	public Authentication authenticate(Authentication authentication)
			throws AuthenticationException {
		// Login
		// String login= authentication.getName();
		String login = (String) authentication.getPrincipal();
		// Password
		String password = (String) authentication.getCredentials();
		// Additional details of current (not yet authenticated) user
		UserDetails myUser = null;
        if (password == null || login == null || login.trim().length() == 0
				|| password.trim().length() == 0) {
			throw new AuthenticationServiceException("Your login/password are empty!!!");
		}
		isAuth = false;
		try{
			userInDataStore = repository.loadUser( login);
		}catch( Exception e){
			e.printStackTrace();
			return null;
		}
		try {
			userInDataStorePwd = userInDataStore.getPassword();
			isAuth = passEncoder.isPasswordMatching(password, userInDataStorePwd);
			List<GrantedAuthority> AUTHORITIES = new ArrayList<GrantedAuthority>();
			if ( isAuth) {
				// Technical role : FOR AUTHENTICATION ONLY
				AUTHORITIES.add( new SimpleGrantedAuthority( "ROLE_USER"));

			} else {
				throw new AuthenticationServiceException(
						"Your login attempt was not successful.");
			}
			return new AqlliAuthenticationToken(authentication.getName(),
					authentication.getCredentials(), AUTHORITIES, myUser);

		} catch (AuthenticationServiceException e ) {
			throw e;
		
		}catch (Throwable e) {
			throw new AuthenticationServiceException(
					"An error/exception occurs during the authentication. Please, try again.",
					e);
			} 
	}

	@Override
	public boolean supports(Class<? extends Object> authentication) {
		return UsernamePasswordAuthenticationToken.class
				.isAssignableFrom(authentication)
				&& authentication
						.equals(UsernamePasswordAuthenticationToken.class);
	}
}