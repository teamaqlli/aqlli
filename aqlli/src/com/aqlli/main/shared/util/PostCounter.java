package com.aqlli.main.shared.util;

import static com.aqlli.main.service.AqlliOfyService.ofy;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

import com.aqlli.main.client.model.UnikIdSeq;
import com.googlecode.objectify.Key;
/**
 * @author Arix OWAYE / twitter : @arixowaye
 * This class returns a sequence we use for Ids
 * Sequence value is persisted for each user
 */
public class PostCounter {
	private static AtomicInteger sequence = new AtomicInteger(0);
	private static final ReentrantLock lock = new ReentrantLock( true);
	
	private static List<UnikIdSeq> l = null;
	private static UnikIdSeq newU = null;
	/**
	 * @param user_id
	 * @return a unique Id
	 * checks whether there is already a value in the datastore in case of id == 0
	 */
	public static int getNextValue( String user_id){
		int dataStoreSeq = -1;
		if( sequence.get() == 0){
			l = ofy().load().type( UnikIdSeq.class).filter("userUniqueId", user_id).order("-lastId").limit(1).list();
			if( l != null){
				if ( l.size() > 0) dataStoreSeq = l.get(0).getLastId();
				if ( dataStoreSeq > sequence.get()) sequence.set( dataStoreSeq);
			}	
		}
		int newId = sequence.incrementAndGet();
        persistCounter(user_id, newId);
        return newId;
	}
	public static int getValue(){
		return sequence.get();
	}
	/**
	 * @param user_id unique user ID
	 * @param lastId last used Post Id 
	 */
	private static void persistCounter( String user_id, int lastId){
		lock.lock();
		try{
			List<Key<UnikIdSeq>> keys = ofy().load().type( UnikIdSeq.class).filter("userUniqueId", user_id).keys().list();
			ofy().delete().keys(keys).now();
			//save the new one
			newU = new UnikIdSeq();
			newU.setLastId(lastId);
			newU.setUserUniqueId(user_id);
			ofy().save().entities( newU).now();
		}catch( Exception e ){
			e.printStackTrace();
		}finally{
			lock.unlock();
		}
	}
}
