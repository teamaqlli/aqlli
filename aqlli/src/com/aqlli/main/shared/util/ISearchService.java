package com.aqlli.main.shared.util;

import java.util.List;

import com.aqlli.main.client.model.UserDetails;

public interface ISearchService {
  public List<UserDetails> searchUsersbyQuery(final int limit, final String query, final String propertyToLookUp);
}
