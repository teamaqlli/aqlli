package com.aqlli.main.shared.util;

import com.aqlli.main.client.model.UserDetails;
import com.aqlli.main.client.model.VerificationToken;

public interface VerificationTokenRepoInterface {
  VerificationToken findByToken( String token);
  VerificationToken findByUser( UserDetails user);
  void deleteToken( VerificationToken token);
  VerificationToken findInviteToUseByToken( String token);
	
}
