package com.aqlli.main.shared.util;

public class Consts {
	/*aqlli social network links*/
	public final static String fb 		=   "https://www.facebook.com/aqlliFr";
	public final static String tw 		=   "https://twitter.com/aqlli_fr";
	public final static String gp 		=   "https://plus.google.com/106910419300047477169/about";
	public final static String inst 	= "";
	public final static String linkedIn = "";
	public final static String yout 	= "https://www.youtube.com/channel/UCDa1IgV2LhLDABhv0nKKx1Q";
	/* sendgrid conf*/	
	public final static String sendgridApiKey    		= "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";
	public final static String sendgridApiKeyId  		= "K7o1ZY_BSO-APBmKy9Q60Q";
	
	/*recover pass consts*/
	public final static String typeTokenRecoverPassword = "recoverPassToken";
	
	/*registration consts*/
	public final static String typeTokenRegistration 	= "registrationToken";
	
	/*invitation consts*/
	public final static String typeTokenInvitation 		= "invitationToken";
	
	/*api settings type*/
	public final static String APIUPDSETTINGSGEN 		= "gen";
	public final static String APIUPDSETTINGSSOC 		= "soc";
	public final static String APIUPDSETTINGSDESC 		= "desc";
	public final static String APIUPDSETTINGSHFO 		= "hfo";
	public final static String APIUPDSETTINGSOCCU 		= "occu";
	public final static String APIUPDSETTINGSNOTIFS 	= "notifs";
	
    
}
