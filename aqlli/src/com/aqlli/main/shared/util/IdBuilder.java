package com.aqlli.main.shared.util;

import java.util.Random;
import java.util.UUID;
/*dependency -- gwt-user.jar*/
import com.google.gwt.i18n.server.KeyGenerator;
import com.google.gwt.i18n.server.Message;

/**
 * @author arix OWAYE
 * twitter: arixowaye
 */
public class IdBuilder implements KeyGenerator{

	private IdBuilder(){
		super();
	}
	/**
	 * @return an unique string used as an ID
	 */
	public static String generateId(){
		UUID id = UUID.randomUUID();
		return id.toString();
	}
	
	public static String generateShortId(){ //returns a random value between 1000 and 100000
		Random r = new Random( System.currentTimeMillis());
		int res = 1000 + r.nextInt( 100000);
		return String.valueOf( res);
		
	}
	
	public static void main( String[] args){
		System.out.println( IdBuilder.generateId());
		System.out.println( IdBuilder.generateId());
		System.out.println( IdBuilder.generateId());
		System.out.println( IdBuilder.generateId());
		System.out.println( IdBuilder.generateId());
	}
	
	

	@Override
	public String generateKey(Message msg) {
		return generateId();
	}
}
