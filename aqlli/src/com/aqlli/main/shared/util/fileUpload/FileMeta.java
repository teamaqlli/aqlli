package com.aqlli.main.shared.util.fileUpload;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class FileMeta {
	private String name;
	private long size;
	private String url;
	private String delete_url;
	private String delete_type;
	private String thumbnail_url;

	public FileMeta(String filename, long size, String url, String urlPreview) {
		this.name = filename;
		this.size = size;
		this.url = url;
		this.delete_url = url;
		this.delete_type = "DELETE";
		this.thumbnail_url = urlPreview;
	}

	public FileMeta() {
	}
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getSize() {
		return this.size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDelete_url() {
		return this.delete_url;
	}

	public void setDelete_url(String delete_url) {
		this.delete_url = delete_url;
	}

	public String getDelete_type() {
		return this.delete_type;
	}

	public void setDelete_type(String delete_type) {
		this.delete_type = delete_type;
	}

	public String getThumbnail_url() {
		return this.thumbnail_url;
	}

	public void setThumbnail_url(String thumbnail_url) {
		this.thumbnail_url = thumbnail_url;
	}

}