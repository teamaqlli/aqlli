package com.aqlli.main.shared.util.fileUpload;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Entity {
	private List<FileMeta> files;

	public Entity(List<FileMeta> files) {
		this.setFiles(files);
	}

	public Entity() {
	}

	/**
	 * @return the files
	 */
	public List<FileMeta> getFiles() {
		return files;
	}

	/**
	 * @param files
	 *            the files to set
	 */
	public void setFiles(List<FileMeta> files) {
		this.files = files;
	}
}