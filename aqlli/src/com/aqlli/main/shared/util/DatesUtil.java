package com.aqlli.main.shared.util;

import java.util.Calendar;
import java.util.Locale;

public class DatesUtil {
  private final static Calendar calendar = Calendar.getInstance( Locale.FRANCE);

  public static int getHour(){
	  return calendar.get( Calendar.HOUR_OF_DAY);
  }
  public static int getMinutes(){
	  return  calendar.get( Calendar.MINUTE);
  }
  public static int getSeconds(){
	  return calendar.get( Calendar.SECOND);
  }
  public static int getYear(){
	  return calendar.get( Calendar.YEAR);
  }
  public static int getDayOfMonth(){
	  return calendar.get( Calendar.DAY_OF_MONTH);
  }
  public static int getMonth(){
	  return calendar.get( Calendar.MONTH);
  }
  private static void setTime( int addMillis){
	  calendar.setTimeInMillis( calendar.getTimeInMillis() + addMillis);
  }
  
  public static String getCronJobDate(){
	  setTime( 60*1000);// we add a minute
	  return "0 " + getMinutes() + " " + getHour() + " " + getDayOfMonth() + " "  +getMonth() + " " + getYear();  
  }
}
