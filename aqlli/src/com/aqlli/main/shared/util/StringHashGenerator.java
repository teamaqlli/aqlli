package com.aqlli.main.shared.util;

import java.util.Date;

public class StringHashGenerator {
	private static final String ALPHA = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	  private static final String ALPHANUMERIC = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
	  private static final String NUMERIC = "1234567890";
		
	  
	  public static enum Mode{
		  ALPHA, ALPHANUMERIC, NUMERIC
	  }
	  
	  private static String stringHashGenerator( int string_length, Mode mode){
			final StringBuffer buffer = new StringBuffer();
			String chars = "";
			
			switch ( mode) {
			case ALPHA:
				chars = StringHashGenerator.ALPHA;
				break;
			case ALPHANUMERIC:
				chars = StringHashGenerator.ALPHANUMERIC;
				break;
			case NUMERIC:
				chars = StringHashGenerator.NUMERIC;
				break;
			default:
				break;
			}
			
			final int charsLength = chars.length();
			
			for( int i = 0; i < string_length; i++){
				double index = Math.random() * charsLength;
				buffer.append( chars.charAt((int)  index));
			}
			return buffer.insert(0, timeStampFormatter(0)).append( timeStampFormatter(1)).toString();
			
		  }
	  //we want it to be as unique as possible so implement this method to append its result to our random string
	  private static String timeStampFormatter( int idx){//O or 1 -> 0 start; 1 -> end
		  String res = "";
		  if( idx == 0)
		    res =  new java.text.SimpleDateFormat( "ss").format(new Date()).replace(":", "");
		  if( idx == 1)
		    res =  new java.text.SimpleDateFormat( "h:mm").format(new Date()).replace(":", "");
		  return res;
	  }
	  public static String generate( int length){
		  return stringHashGenerator( length, Mode.ALPHANUMERIC);
	  }
}
