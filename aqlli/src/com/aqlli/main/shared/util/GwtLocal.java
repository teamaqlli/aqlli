package com.aqlli.main.shared.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GwtLocal {
private String[] langs = {"English", "French"};
private List<String> locales = new ArrayList<String>( Arrays.asList( langs));
public List<String> getLocales(){
	return locales;
}

}
