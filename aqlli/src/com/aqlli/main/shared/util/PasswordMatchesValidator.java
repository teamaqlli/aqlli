package com.aqlli.main.shared.util;

import com.aqlli.main.shared.exceptions.MalformedPasswordException;

/**
 * @author Arix OWAYE / twitter : @arixowaye
 *
 */
public class PasswordMatchesValidator {   
    private final static String REGEX_PASS_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{6,}$";
    /*
     * ^                 # start-of-string
	(?=.*[0-9])       # a digit must occur at least once
	(?=.*[a-z])       # a lower case letter must occur at least once
	(?=.*[A-Z])       # an upper case letter must occur at least once
	(?=.*[@#$%^&+=])  # a special character must occur at least once
	(?=\S+$)          # no whitespace allowed in the entire string
	.{6,}             # anything, at least eight places though
	$                 # end-of-string
     */
	public boolean isValid(String password) throws MalformedPasswordException{   
      return password.matches(REGEX_PASS_PATTERN);
    }     
}
