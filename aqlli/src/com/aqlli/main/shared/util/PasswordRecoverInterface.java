/**
 * 
 */
package com.aqlli.main.shared.util;

import com.aqlli.main.client.model.UserDetails;
import com.aqlli.main.client.model.VerificationToken;
import com.aqlli.main.shared.exceptions.UserNotEnabledException;
import com.aqlli.main.shared.exceptions.UserNotFoundException;

/**
 * @author Arix OWAYE / twitter : @arixowaye
 *
 */
public interface PasswordRecoverInterface {
  public boolean resetPassword( String token, String password, UserDetails user);
  public boolean resetPassword( String newPass, UserDetails user);
  public VerificationToken lostPasswordChange( String email) throws UserNotFoundException, UserNotEnabledException;
}
