package com.aqlli.main.shared.util;

import java.util.HashSet;
import java.util.Set;

import com.aqlli.main.client.model.AqlliPost;
import com.aqlli.main.client.model.UserDetails;
import com.aqlli.main.service.UserService;
import com.aqlli.main.shared.exceptions.UserNotFoundException;

public class AqlliUserStats {
	
	UserService userService = new UserService();
	private static final String K = "K";
	private static final String M = "M";
	
	public String getFormattedNumberOfUser(){
		int nbUser = new UserService().getNumberOfUser();
		if (( nbUser) <= 1000) return  String.valueOf( nbUser);
		if ((( nbUser / 1000) > 0) && ( ( nbUser / 1000000) < 0)){
			int newNbUser =   nbUser /  1000;
			return String.valueOf( newNbUser) + K;
		}
		if( ( nbUser / 1000000) > 0){
			int newNbUser =   nbUser /  1000000;
			return String.valueOf( newNbUser) + M;
		}
		return String.valueOf( nbUser);
	}
	
   public String getFormattedPostsNumberOfLikes( AqlliPost post ){
	   Integer nbOfLikes = post.getNumberOfLikes();
	   if (( nbOfLikes) <= 1000) return  String.valueOf( nbOfLikes);
	   if ((( nbOfLikes / 1000) > 0) && ( ( nbOfLikes / 1000000) < 0)){
			Integer newNbUser =   (nbOfLikes /  1000);
			return String.valueOf( newNbUser) + K;
	   }
	   if(( nbOfLikes / 1000000) > 0){
		   	Integer newNbUser =   nbOfLikes /  1000000;
	   		return String.valueOf( newNbUser) + M;
	   }
	   return String.valueOf( nbOfLikes);
   }
   
   public String getFormattedNumberOfFollowers( UserDetails u){
	   int nbFollowers = u.getFollowers().size();
	   if (( nbFollowers) <= 1000) return  String.valueOf( nbFollowers);
	   if ((( nbFollowers / 1000) > 0) && ( ( nbFollowers / 1000000) < 0)){
			int newNbUser =   (nbFollowers /  1000);
			return String.valueOf( newNbUser) + K;
	   }
	   if(( nbFollowers / 1000000) > 0){
		   	int newNbUser =   nbFollowers /  1000000;
	   		return String.valueOf( newNbUser) + M;
	   }
	   return String.valueOf( nbFollowers);
   }
   public String getFormattedNumberOfFollowing( UserDetails u){
	   int nbFollowing = u.getFollowing().size();
	   if (( nbFollowing) <= 1000) return  String.valueOf( nbFollowing);
	   if ((( nbFollowing / 1000) > 0) && ( ( nbFollowing / 1000000) < 0)){
			int newNbUser =   (nbFollowing /  1000);
			return String.valueOf( newNbUser) + K;
	   }
	   if(( nbFollowing / 1000000) > 0){
		   	int newNbUser =   nbFollowing /  1000000;
	   		return String.valueOf( newNbUser) + M;
	   }
	   return String.valueOf( nbFollowing);
   }
   public String getFormattedNumberOfPageViews( UserDetails u){
	   Long nbOfV = userService.getPageNumberOfViews( u);
	   if (( nbOfV) <= 1000) return  String.valueOf( nbOfV);
	   if ((( nbOfV / 1000) > 0) && ( ( nbOfV / 1000000) < 0)){
		  Long newNbUser =   (nbOfV /  1000);
		  return String.valueOf( newNbUser) + K;
	   }
	   if(( nbOfV / 1000000) > 0){
	      Long newNbUser =   nbOfV /  1000000;
	   	  return String.valueOf( newNbUser) + M;
	   }
	   return String.valueOf( nbOfV);
   }
   
   public String getReadersNumberOfUniqueCities( UserDetails u){
	   Set<String> list = new HashSet<>();
	   for( String userId : u.getFollowers()){
		   try {
			UserDetails us = userService.getUserByUniqueID( userId);
			if( !list.contains(us.getLocation())){
				list.add( us.getLocation());
			}
		} catch (UserNotFoundException ex) {
			ex.printStackTrace();
		}
	   }
	   return String.valueOf( list.size());
   }
}
