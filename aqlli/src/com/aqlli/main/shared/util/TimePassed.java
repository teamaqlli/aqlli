package com.aqlli.main.shared.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class TimePassed {
    Map<String, Long> intervals;
    private final static String JUSTNOW    = "<spring:message code='18n.just.now' text='Just now'>"; 
    private final static String SECONDAGO  = "<spring:message code='18n.second.ago' text='second ago'>"; 
    private final static String SECONDSAGO = "<spring:message code='18n.seconds.ago' text='seconds ago'>"; 
    private final static String MINUTEAGO  = "<spring:message code='18n.minute.ago' text='minute ago'>"; 
    private final static String MINUTESAGO = "<spring:message code='18n.minutes.ago' text='minutes ago'>"; 
    private final static String HOURAGO    = "<spring:message code='18n.hour.ago' text='hour ago'>"; 
    private final static String HOURSAGO   = "<spring:message code='18n.hours.ago' text='hours ago'>"; 
    private final static String DAYAGO     = "<spring:message code='18n.day.ago' text='day ago'>"; 
    private final static String DAYSAGO    = "<spring:message code='18n.days.ago' text='days ago'>"; 
    private final static String WEEKAGO    = "<spring:message code='18n.week.ago' text='week ago'>"; 
    private final static String WEEKSAGO   = "<spring:message code='18n.weeks.ago' text='weeks ago'>"; 
    private final static String MONTHAGO   = "<spring:message code='18n.month.ago' text='month ago'>"; 
    private final static String MONTHSAGO  = "<spring:message code='18n.months.ago' text='months ago'>"; 
    private final static String YEARAGO    = "<spring:message code='18n.year.ago' text='year ago'>"; 
    private final static String YEARSAGO   = "<spring:message code='18n.years.ago' text='years ago'>"; 
    
    public TimePassed(){
    	intervals = new HashMap<String, Long>();
    	intervals.put("year", (long) 31556926);
    	intervals.put("month", (long) 2629744);
    	intervals.put("week", (long) 604800);
    	intervals.put("day", (long) 86400);
    	intervals.put("hour", (long) 3600);
    	intervals.put("minute", (long) 60);
    }
	
	public String timePassed( String date){
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    	long datePostSec;
    	long currentTime;
    	long diff;
    	try {
			Date datePost = simpleDateFormat.parse( date);
			datePostSec = datePost.getTime();
			currentTime = System.currentTimeMillis();
			diff = currentTime - datePostSec;
			//now we just find the difference
		    if (diff == 0)
		    {
		        return JUSTNOW;
		    }    

		    if (diff < intervals.get("minute"))
		    {
		        return diff == 1 ? ( diff + " " + SECONDAGO) : diff + " " + SECONDSAGO;
		    }        

		    if (diff >= intervals.get("minute") && diff < intervals.get("hour"))
		    {
		        diff = (long) Math.floor( diff / intervals.get("minute"));
		        return diff == 1 ? diff + " " + MINUTEAGO :  diff + " " + MINUTESAGO;
		    }        

		    if ( diff >= intervals.get("hour") && diff < intervals.get("day"))
		    {
		        diff = (long) Math.floor( diff / intervals.get("hour"));
		        return diff == 1 ? diff + " " + HOURAGO : diff + " " + HOURSAGO;
		    }    

		    if ( diff >= intervals.get("day") &&  diff <  intervals.get("week"))
		    {
		        diff = (long) Math.floor( diff / intervals.get("day"));
		        return diff == 1 ? diff + " " + DAYAGO : diff + DAYSAGO;
		    }    

		    if ( diff >= intervals.get("week") && diff < intervals.get("month"))
		    {
		        diff = (long) Math.floor( diff / intervals.get("week"));
		        return diff == 1 ? diff + " " + WEEKAGO : diff + " " + WEEKSAGO;
		    }    

		    if ( diff >= intervals.get("month") && diff < intervals.get("year"))
		    {
		        diff = (long) Math.floor( diff / intervals.get("month"));
		        return diff == 1 ? diff + " " + MONTHAGO : diff + " " + MONTHSAGO;
		    }    

		    if ( diff >= intervals.get("year"))
		    {
		        diff = (long) Math.floor( diff / intervals.get("year"));
		        return diff == 1 ? diff + " " + YEARAGO : diff + " " + YEARSAGO;
		    }
		} catch (ParseException ex) {
			ex.printStackTrace();
		}
    	return "";
	}
	public static void main( String[] args){
		TimePassed t = new TimePassed( );
		String d = "11/07/2015 10:30:02";
		String res = t.timePassed( d);
		System.out.println( res);
		
	}
}
