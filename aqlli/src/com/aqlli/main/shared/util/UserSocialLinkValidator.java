package com.aqlli.main.shared.util;

import org.apache.commons.validator.UrlValidator;

public class UserSocialLinkValidator {
  
  private final String[] schemes = {"http", "https"}; //allowed schemes
  private final String fb1 		 = "www.facebook.com/";
  private final String fb2 		 = "facebook.com/";
  private final String tw1 		 = "www.twitter.com/";
  private final String tw2 		 = "twitter.com/";
  private final String gp 		 = "plus.google.com/";
  private UrlValidator urlValidator;
  
  public UserSocialLinkValidator(){
	  urlValidator = new UrlValidator(schemes);
  }
  
  public String getFbLinkWithValideId( String fbLink){
	  fbLink = urlPrevalidate( fbLink);
	  if( !fbLink.contains(fb1) || !fbLink.contains(fb2)) return "";
	  if( urlValidator.isValid(fbLink)) return fbLink;
	  return "";
  }
  
  public String getTwLinkWithValideId( String twLink){
	  twLink = urlPrevalidate( twLink);
	  if( !twLink.contains(tw1) || !twLink.contains(tw2)) return "";
	  if( urlValidator.isValid(twLink)) return twLink;
	  return "";
  }
  
  public String getGpLinkWithValideId( String gpLink){
	  gpLink = urlPrevalidate( gpLink);
	  if( !gpLink.contains(gp)) return "";
	  if( urlValidator.isValid(gpLink)) return gpLink;
	  return "";
  }
  
  public String getTwitterWithValideId( String id){
	  if ( id.isEmpty()) return "";
	  if ( !id.startsWith("@")) return "@" + id;
	  return id;
  }
  
  private String urlPrevalidate( String url){
	String http = "http://";
	String https  = "https://";
	if ( url.startsWith(http) || url.startsWith(https)){
	 return url;
	}else{
	 return https + url;//default scheme is https
    }
  }
}
