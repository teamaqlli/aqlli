package com.aqlli.main.shared.util;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import com.aqlli.main.client.model.UserDetails;

/**
 * <p>
 * 		Utility class using SPRING to retrieve the properties and roles of a authenticated user.
 * </p>
 * 
 * @author Huseyin OZVEREN
 * 
 */
public class MyRoleUtil {

	// --------------------------------------------------------------------------------------------------- FONCTIONS PUBLIQUES

	/**
	 * return true if at least one role match
	 */
	public boolean loggedUserHasRole(String ... roles) {
		SecurityContext context = SecurityContextHolder.getContext(); 
		//GrantedAuthority[] authorities = context.getAuthentication().getAuthorities();
		@SuppressWarnings("unchecked")
		Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>) context.getAuthentication().getAuthorities();
		if (authorities != null && authorities.size()>0) {
			for (GrantedAuthority authority : authorities) {
				for (String role : roles) {
					if (authority.getAuthority().matches(role)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	
	/**
	 * Return the username/login of the user
	 */
	public String getLoggedUserName() {
		SecurityContext context = SecurityContextHolder.getContext();
		Authentication authentication = context.getAuthentication();
		
		String userName = null;
		if(authentication.getPrincipal() instanceof org.springframework.security.core.userdetails.User){
			org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User) authentication.getPrincipal();
			userName = user.getUsername();
		}else{
			userName = (String) authentication.getPrincipal();
			authentication.getName();
			authentication.getPrincipal();
			authentication.getCredentials();
			authentication.isAuthenticated();
			authentication.getDetails();
		}
		return userName;
	}

	/**
	 * Return the list of user roles
	 */
	@SuppressWarnings("unchecked")
	public Collection<String> getLoggedUserRolesNames() {
		SecurityContext context = SecurityContextHolder.getContext();
		//GrantedAuthority[] authorities = context.getAuthentication().getAuthorities();
		Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>) context.getAuthentication().getAuthorities();
		Collection<String> roles = new ArrayList<String>();
		if (authorities != null && authorities.size()>0) {
			for (GrantedAuthority authority : authorities) {
				roles.add(authority.getAuthority());
			}
		}
		return roles;
	}


	/**
	 * Return the informations/details relatives to the authenticated user
	 * @return
	 */
	public UserDetails getCurrentMyUser(){
		SecurityContext context = SecurityContextHolder.getContext();
		Authentication authentication = context.getAuthentication();
		UserDetails user = null;
		if(authentication instanceof AqlliAuthenticationToken){
			user = ((AqlliAuthenticationToken)authentication).getMyUser();
		}
		
		return user;
	}
}