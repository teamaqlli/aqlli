package com.aqlli.main.shared.util;

import com.aqlli.main.client.model.UserDetails;
import com.aqlli.main.client.model.VerificationToken;

public interface UserPasswordServiceInterface {
	void SavePasswordRecoverToken( VerificationToken token);
	void deleteRecoverPassToken( VerificationToken token);
	VerificationToken getRecoverPassToken( UserDetails user);
}
