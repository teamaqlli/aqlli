package com.aqlli.main.shared.util;

import java.util.ArrayList;
import java.util.List;

import com.aqlli.main.client.model.AqNotification;
import com.aqlli.main.client.model.AqlliPost;
import com.aqlli.main.client.model.ReportABug;
import com.aqlli.main.client.model.UserDetails;
import com.aqlli.main.service.AqlliOfyService;

public interface IWallService {
	public AqlliPost addPost( UserDetails userDto, String data);
	public boolean deletePost( Long postId);
	public List<AqlliPost> getNextPosts( UserDetails user, String lastPId, int i);
	public List<AqlliPost> initWall( UserDetails user, int i);
	public AqlliPost findPostByUniqueId( Long postId);
	public void savePost( AqlliPost post);
	public void saveBugReport(ReportABug rep);
	public void saveNotification(AqNotification notif);
	public void deleteNotification(AqNotification notif);
	public AqNotification findNotifById(Long id);
	public List<AqNotification> getNotifications(String receiverId, Long lastId, int limit);
}
