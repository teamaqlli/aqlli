/**
 * 
 */
package com.aqlli.main.shared.util;

/**
 * @author Arix OWAYE / twitter : @arixowaye
 *
 */
public interface IInviteFriend {
	public void singleInvitation( final String user_email, final String friends_email);
}
