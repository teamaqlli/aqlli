package com.aqlli.main.shared.util;

import java.util.HashMap;
import java.util.Map;
/**
 * @author Arix OWAYE / twitter : @arixowaye
 * First version: 19-05-2015
 */
public class SupportedLocales {
  //list des langues supportées
  private final Map< String, String> locales = new HashMap< String, String>();
  //liste des code locales avec en correspondance les langues supportées
  private final Map< Integer, String> languages = new HashMap< Integer, String>();
  //init à la construction
  public SupportedLocales(){
	//languages
	languages.put(0, "English");
	languages.put(1, "Uzbek");
	languages.put(2, "Portuguese");
	languages.put(3, "French");
	languages.put(4, "Spanish");
	languages.put(5, "German");
	languages.put(6, "Simplified Chinese");
	//locales en rapport avec les langues
    locales.put("en", languages.get(0));
    locales.put("uz", languages.get(1));
    locales.put("pt", languages.get(2));
    locales.put("fr", languages.get(3));
    locales.put("es", languages.get(4));
    locales.put("de", languages.get(5));
    locales.put("zh", languages.get(6));
  }
  /**
   * @param locale le code locale de la langue à tester
   * @return si le code est supporté par l'application
   */
  public boolean isSupported( String locale){
	  if ( locale.trim() == ""){
		  return false;
	  }
	  if (!( locale instanceof String)){
		  return false;
	  }
	  if( locales.containsKey(locale)){
		  String lang = locales.get(locale);
		  if ( languages.containsValue( lang)){
			  return true;
		  }
	  }
	  return false;
  }
}
