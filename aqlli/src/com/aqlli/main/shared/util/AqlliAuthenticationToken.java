package com.aqlli.main.shared.util;

import java.util.Collection;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import com.aqlli.main.client.model.UserDetails;

public class AqlliAuthenticationToken extends UsernamePasswordAuthenticationToken{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// ----------------------------------- PRIVATE ATTRIBUTES
	private UserDetails myUser = null;
	// ----------------------------------- CONSTRUCTOR
	public AqlliAuthenticationToken(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities, UserDetails myUser){
		super(principal, credentials, authorities);
	    this.myUser = myUser;
	}
	    // ----------------------------------- GET/SET TERS
	public UserDetails getMyUser() {
        return myUser;
	}
	public void setMyUser(UserDetails myUser) {
        this.myUser = myUser;
	}
}
