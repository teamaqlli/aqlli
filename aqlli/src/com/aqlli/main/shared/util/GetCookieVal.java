package com.aqlli.main.shared.util;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

public class GetCookieVal {

	public String getCookie( String cookieName, HttpServletRequest req){
		Cookie[] cookies = req.getCookies();
		String cookieVal = "";
		for ( int i = 0; i < cookies.length; i++){
			if( cookies[i].getName().equalsIgnoreCase(cookieName)){
				cookieVal = cookies[i].getValue();
			}
		}
		return cookieVal;
	}
}
