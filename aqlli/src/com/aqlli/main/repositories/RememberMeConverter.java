package com.aqlli.main.repositories;

import java.util.HashSet;
import java.util.Set;

import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.GenericConverter;
import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;

public class RememberMeConverter implements GenericConverter, org.springframework.core.convert.ConversionService {


	@Override
	public Set<ConvertiblePair> getConvertibleTypes() {
		Set<ConvertiblePair> convertibles = new HashSet<ConvertiblePair>();
		convertibles.add( new ConvertiblePair(RememberMeToken.class, PersistentRememberMeToken.class));
		convertibles.add( new ConvertiblePair(PersistentRememberMeToken.class, RememberMeToken.class));
		return convertibles;
	}

	@Override
	public Object convert(Object source, TypeDescriptor sourceType,
			TypeDescriptor targetType) {
		if (sourceType.getType().equals(RememberMeToken.class)) {
            // RememberMeToken to PersistentRememberMeToken

            RememberMeToken from = (RememberMeToken) source;
            return new PersistentRememberMeToken(from.getUsername(), from.getSeries(), from.getTokenValue(), from.getDate());

        } else {
            // PersistentRememberMeToken to RememberMeToken
            PersistentRememberMeToken from = (PersistentRememberMeToken) source;
            return new RememberMeToken(from.getUsername(), from.getSeries(), from.getTokenValue(), from.getDate());
        }
    }


	@Override
	public boolean canConvert(Class<?> sourceType, Class<?> targetType) {
		if ((sourceType.getClass().equals(RememberMeToken.class)) && (sourceType.getClass().equals(PersistentRememberMeToken.class))){
			return true;
		}
		if ((sourceType.getClass().equals(PersistentRememberMeToken.class)) && (sourceType.getClass().equals(RememberMeToken.class))){
			return true;
		}
		return false;
	}


	@Override
	public boolean canConvert(TypeDescriptor sourceType,
			TypeDescriptor targetType) {
		if (sourceType.getType().equals(RememberMeToken.class) && sourceType.getType().equals(PersistentRememberMeToken.class)){
		  return true;	
		}
		if (sourceType.getType().equals(PersistentRememberMeToken.class) && sourceType.getType().equals(RememberMeToken.class)){
		  return true;	
		}
		return false;
	}


	@Override
	public <T> T convert(Object source, Class<T> targetType) {
		if (source.getClass().equals(RememberMeToken.class)) {
            // RememberMeToken to PersistentRememberMeToken
            RememberMeToken from = (RememberMeToken) source;
            return (T) new PersistentRememberMeToken(from.getUsername(), from.getSeries(), from.getTokenValue(), from.getDate());

        } else {
            // PersistentRememberMeToken to RememberMeToken
            PersistentRememberMeToken from = (PersistentRememberMeToken) source;
            return (T) new RememberMeToken(from.getUsername(), from.getSeries(), from.getTokenValue(), from.getDate());
        }
	}

}
