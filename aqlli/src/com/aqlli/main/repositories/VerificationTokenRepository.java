package com.aqlli.main.repositories;

import com.aqlli.main.client.model.UserDetails;
import com.aqlli.main.client.model.VerificationToken;
import com.aqlli.main.service.AqlliOfyService;
import com.aqlli.main.shared.util.Consts;
import com.aqlli.main.shared.util.VerificationTokenRepoInterface;

public class VerificationTokenRepository implements
		VerificationTokenRepoInterface {


	@Override
	public VerificationToken findByToken(String token) { //any token
		VerificationToken vToken = null;
		try{
			vToken = AqlliOfyService.ofy().load().type( VerificationToken.class).filter("token ==", token).first().now();
		}catch ( Exception e){
			e.printStackTrace();
		}
		return vToken;
	}


	@Override
	public VerificationToken findByUser(UserDetails user) { //registration token
		VerificationToken vToken = null;
		try{
			vToken = AqlliOfyService.ofy().load().type( VerificationToken.class).filter("user ==", user).filter("type ==", Consts.typeTokenRegistration).first().now();
		}catch ( Exception e){
			e.printStackTrace();
		}
		return vToken;
	}


	@Override
	public void deleteToken(VerificationToken token) {
		if ( token != null){
			AqlliOfyService.ofy().delete().entity(token).now();
		}
		
	}

	@Override
	public VerificationToken findInviteToUseByToken(String token) {
		VerificationToken vToken = null;
		try{
			vToken = AqlliOfyService.ofy().load().type( VerificationToken.class).filter("token ==", token).filter("type==", Consts.typeTokenInvitation).first().now();
		}catch ( Exception e){
			e.printStackTrace();
		}
		return vToken;
	}
}
