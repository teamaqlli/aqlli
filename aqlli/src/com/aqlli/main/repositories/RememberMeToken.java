package com.aqlli.main.repositories;

import java.util.Date;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class RememberMeToken {

	@Id
	private String series;
	@Index
	private String username;
	private String tokenValue;
	private Date date;
	
	private String key;
	private String cookieName;
	private String tokenLength;
	private String parameter;
	
	
	public RememberMeToken() {
		key = "resterConnecterAqll1PrivateKey";
		cookieName = "remember-me";
		tokenLength = "32";
		parameter = "_spring_security_remember_me";
	}
	
	public RememberMeToken( String username, String series, String tokenValue, Date date){
		this.username = username;
		this.series = series;
		this.tokenValue = tokenValue;
		this.date = date;
	}
	
	public String getSeries() {
		return series;
	}
	public void setSeries(String series) {
		this.series = series;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getTokenValue() {
		return tokenValue;
	}
	public void setTokenValue(String tokenValue) {
		this.tokenValue = tokenValue;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the parameter
	 */
	public String getParameter() {
		return parameter;
	}

	/**
	 * @param parameter the parameter to set
	 */
	public void setParameter(String parameter) {
		this.parameter = parameter;
	}
	public String getTokenLength() {
		return this.tokenLength;
	}

	public void setTokenLength(String tokenLength) {
		this.tokenLength = tokenLength;
	}

	public String getCookieName() {
		return this.cookieName;
	}

	public void setCookieName(String cookieName) {
		this.cookieName = cookieName;
	}

	public String getKey() {
		return this.key;
	}

	public void setKey(String key) {
		this.key = key;
	}

}
