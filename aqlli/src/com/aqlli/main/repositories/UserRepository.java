package com.aqlli.main.repositories;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.aqlli.main.client.model.UserDetails;
import com.aqlli.main.service.AqlliOfyService;
import com.aqlli.main.shared.exceptions.UserNotEnabledException;
import com.aqlli.main.shared.exceptions.UserNotFoundException;
import com.aqlli.main.shared.util.UserProviderInterface;
import com.google.appengine.api.datastore.KeyFactory;

public class UserRepository implements UserProviderInterface{
	
	//find a user by Id which is an email address
	@Override
	public UserDetails loadUser(String userId) throws UserNotFoundException, UserNotEnabledException{
		UserDetails user = AqlliOfyService.ofy().load().type(UserDetails.class).id( userId).now();
		if( user == null){
			throw new UserNotFoundException(this, "No user found with Id :" + userId);
		}
		if( user != null && !user.isEnabled())
		  throw new UserNotEnabledException( this, "User found and not enabled yet :" + userId);
		return user;
	}
	public boolean isUserExists( String email){
		UserDetails user = null;
		try{
			user = AqlliOfyService.ofy().load().type(UserDetails.class).id( email).now();
		}catch( Exception e){
			e.printStackTrace();
		}
		return (user != null)?true:false;
	}
	public boolean objectExists(String id, Class<?> objectClass) {
		  return AqlliOfyService.ofy().load().filterKey(KeyFactory.createKeyString(objectClass.toString(), id)).keys().first() != null;
	}
	
	//find a user by email
	@Override
	public Collection<UserDetails> findByName( String name){
		List<UserDetails> users = AqlliOfyService.ofy().load().type(UserDetails.class).list();
		List<UserDetails> result = new ArrayList<UserDetails>();
		for( UserDetails user : users){
			if( user.getLastName().equals( name))
				result.add( user);
		}
		return result;
	}
	
	//delete this user
	public void delete( String email){
		String mail = email.toLowerCase().trim();
		AqlliOfyService.ofy().delete().type(UserDetails.class).id( mail).now();
	}
    //update the datastore with this user
	public UserDetails save( UserDetails userDetails){
		AqlliOfyService.ofy().save().entity( userDetails).now();
		return userDetails;
	}
	
	//all users from the datastore
	public Collection<UserDetails> getAll(){
		return AqlliOfyService.ofy().load().type(UserDetails.class).list();
	}
}
