package com.aqlli.main.repositories;

import java.util.Date;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import com.aqlli.main.service.AqlliOfyService;

public class RememberMePersistentTokenRepository implements PersistentTokenRepository {
    private RememberMeConverter converter;
    
    public RememberMePersistentTokenRepository(){
    	converter = new RememberMeConverter();
    }


    @Override
	public void createNewToken(PersistentRememberMeToken token) {
    	RememberMeToken current = AqlliOfyService.ofy().load().type(RememberMeToken.class).id(token.getSeries()).now();
        if (current != null) {
            throw new DataIntegrityViolationException("Series Id '" + token.getSeries() + "' already exists!");
        }

        RememberMeToken ofyToken = converter.convert(token, RememberMeToken.class);
        AqlliOfyService.ofy().save().entity(ofyToken);
    }


    @Override
	public void updateToken(String series, String tokenValue, Date lastUsed) {
        PersistentRememberMeToken token = getTokenForSeries(series);
        RememberMeToken ofyToken = new RememberMeToken(token.getUsername(), series, tokenValue, new Date());
        AqlliOfyService.ofy().save().entity(ofyToken);
    }


    @Override
	public PersistentRememberMeToken getTokenForSeries(String seriesId) {
        RememberMeToken rememberMeToken = AqlliOfyService.ofy().load().type(RememberMeToken.class).id(seriesId).now();
        if (rememberMeToken != null) {
            return converter.convert(rememberMeToken, PersistentRememberMeToken.class);
        } else {
            return null;
        }
    }


    @Override
	public void removeUserTokens(String username) {
    	AqlliOfyService.ofy().delete().keys(
    			AqlliOfyService.ofy().load().type(RememberMeToken.class).filter("username", username).keys().list()
        );
    }
}