<%@ page contentType="text/html; charset=UTF-8"%>
<%@page session="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<html>
<head>
<meta charset="utf-8">
<title><spring:message code="i18n.title.errorpage"
		text="Aqlli Error page" /> - Aqlli</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="<spring:message code="i18n.meta.errorpage" text="page not found error 404 Aqlli"/>">
<meta name="author" content="Arix .O">
<link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"
	media="all" rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="http://static.aqlli.com/scripts/css/leftSideMenu.css" media="all"
	rel="stylesheet">
<link rel="stylesheet" type="text/css" media="all"
	href="http://static.aqlli.com/scripts/css/bootstrap.css">
	<script src="http://static.aqlli.com/scripts/js/classie.js"></script>
<script data-main="/aqlli/js/app" src="/aqlli/js/require.js"></script>
<link href="http://static.aqlli.com/scripts/css/styles.css" media="all"
	rel="stylesheet">
	<style>
	.errorpage{
    content: "";
    background: url(http://static.aqlli.com/pictures/dog-sad_1672.jpg) no-repeat; 
    -webkit-background-size: cover; /* pour anciens Chrome et Safari */
    background-size: cover; /* version standardisée */
    z-index: -1;
    width:100%;
    
  }
	@media (max-width: 1200px) {
    .errorpage{
      content: "";
      background: url(http://static.aqlli.com/pictures/dog-sad_640.jpg) no-repeat; 
      -webkit-background-size: cover; /* pour anciens Chrome et Safari */
      background-size: cover; /* version standardisée */
      z-index: -1;
      width:100%;
    
  }
  }
  @media ( max-width :768px) {
  
  .errorpage{
    content: "";
    background: url(http://static.aqlli.com/pictures/dog-sad_425.jpg) no-repeat; 
    -webkit-background-size: cover; /* pour anciens Chrome et Safari */
    background-size: cover; /* version standardisée */
    z-index: -1;
    width:100%;
    
  }
  
}
#footer{
background:rgba(255, 255, 255, 0.8);
}
#msgContainer{
margin-top:60px;
}
#msgContainer > div.col-md-3{
background:rgba(250, 250, 250, 0.5);
margin-right:3px;
margin-left:3px;
}
#goBackBtn{
display:block;
margin-bottom:8px;
}
ol.list-inline li {
	width: 100%;
	margin-bottom: 5px;
}
ol.list-inline li a.btn {
	width: 100%;
}
#oops{
background:rgba(119, 136, 153, 0.7);
color:white;
}
.sponsored{
margin-top:8px;
margin-bottom:8px;
}
	 	
	</style>
</head>

<body class="errorpage">
	<!-- left navbar -->
	<jsp:include page="/WEB-INF/pages/leftSideMenu.jsp" />
	<!-- Books Carousel for Home page-->
	<!-- top navbar -->
	<%@ include file="/WEB-INF/pages/topNavBar.jsp"%>
	<div id="wrapper">
		<div class="section" id="maincontentholder">
			<div class="col-md-12" id="msgContainer">
              
             <div class="col-md-8 alert" id="oops">
                <spring:message code="i18n.errorpage.something.wrong" text="Oops! We can't find the page you're looking for"/>
              
             </div>   
             <div class="col-md-3 alert text-center">
                <span id="goBackBtn" ><spring:message code="i18n.nav.go.back"
										text="Go back" />&nbsp;<button class="btn btn-default" onclick="goBack()"><i class="fa fa-arrow-left"></i></button></span>
             	<div>
						<ol class="list-inline">
							<li><a href="${pageContext.servletContext.contextPath}/login" class="btn btn-sm"><spring:message code="i18n.signin" text="Login"/></a></li>
							<li><a href="${pageContext.servletContext.contextPath}/user/registration" class="btn btn-sm"><spring:message code="i18n.register" text="Register"/></a></li>
						</ol>
					</div>
             	<div class="container text-center">
								<span><spring:message code="i18n.follow.us"
										text="Follow us" /></span>
								
								<div class="row text-center" id="socialContainer">
									<div class="col-xs-6 text-center">
										<a href="#"><i class="fa fa-3x fa-fw fa-twitter"></i></a>
									</div>
									<div class="col-xs-6 text-center">
										<a href="#"><i class="fa fa-3x fa-fw fa-facebook"></i></a>
									</div>
								</div>
							</div>
             </div>
             <div class="sponsored"></div>
            </div>

		</div>
		<%@ include file="/WEB-INF/pages/footer.jsp"%>
	</div>
</body>
</html>