/*only pure js functions here*/
function checkPassImg(){
      //Store the password field objects into variables ...
      var pass1 = document.getElementById('pass1');
      var pass2 = document.getElementById('pass2');
      //Store the Confimation Message Object ...
      var message = document.getElementById('confirmMessageImg');
      //Set the colors we will be using ...
      var goodColor = "#66cc66"; //green
      var badColor = "#ff6666";  //red
      //Compare the values in the password field 
      //to the confirmation field
      if(pass1.value == pass2.value){
        //The passwords match. 
        //Set the color to the good color and inform
        //the user that they have entered the correct password 
        pass2.style.backgroundColor = goodColor;
        message.style.color = goodColor;
        message.innerHTML = '<i class="fa fa-check-circle fa-lg"></i>';
      }else{
        //The passwords do not match.
        //Set the color to the bad color and
        //notify the user.
        pass2.style.backgroundColor = badColor;
        message.style.color = badColor;
        message.innerHTML = '<i class="fa fa-times-circle fa-lg"></i>';
      }
    }
