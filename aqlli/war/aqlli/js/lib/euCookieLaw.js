var cookieCompliance = "cookiesCompliance";
var cookieComplianceValue = 'on';                     // Value of cookie
 
function displayCookieMsg(){
    var cc = document.getElementById( 'cookiesCompliance');
    if ( cc){
      document.getElementsByTagName('body')[0].className+=' cookiebanner'; 
      var div = document.createElement('div');
      div.setAttribute('id','cookie-law');
      div.setAttribute('class','well cc navbar-fixed-bottom');
      div.innerHTML = '<p>Our website uses cookies. By continuing we assume your permission to deploy cookies, as detailed in our <a class="fa" href="/legal/cookies" rel="nofollow" title="Cookies Policy - Aqlli">cookies policy</a>. <a class="close-cookie-banner fa" href="javascript:void(0);" onclick="closeCookieMsg();"><span>&nbsp; Close &nbsp;<i class="fa fa-times-circle-o"></i></span></a></p>';    
      cc.appendChild(div);
      var actualcc = store(window.cookieCompliance);
      if ( !actualcc){
        store( window.cookieCompliance, window.cookieComplianceValue);
      }
    }
}

$(document).ready(function(){
  if ( store(window.cookieCompliance) != 'on'){
    displayCookieMsg(); 
  }
});

function closeCookieMsg(){
	var element = document.getElementById('cookie-law');
	element.parentNode.removeChild(element);
}