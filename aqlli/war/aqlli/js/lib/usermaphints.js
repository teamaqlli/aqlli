var map, ui, group;
var id   = 'zpXpvSWUoxkZoK0q8wnY';
var code = 'GpNnr92zCasA_tLUssefDg';
var behavior = null;

/**
 * var myjson = {"cities": [
  {"lat": "10.2",
  "lng": "50.3",
  "count": "5"},
  {"lat": "50.2",
  "lng": "50.3",
  "count": "5"},
  {"lat": "100.2",
  "lng": "50.3",
  "count": "5"},
  {"lat": "0.2",
  "lng": "50.3",
  "count": "5"}
  ]
};
 * 
 */

$(document).ready(function(){
	var mapContainer = document.getElementById( "mapfollowers");
	var mapIndex     = document.getElementById( "mapbx");
	
	if( mapContainer){
		//disable div view if there is no city
		if( $( "#mapfollowers").attr( "data-map-show") == "true"){
			if( $( "#mapfollowers").hasClass( "gradient-pattern")){
				$( "#mapfollowers").removeClass( "gradient-pattern");
			}
			$("#mapfollowers span").hide();
		}else{
			if( !$( "#mapfollowers").hasClass( "gradient-pattern")){
				$( "#mapfollowers").addClass( "gradient-pattern");	
			}
			$( "#mapfollowers span").show();
			return false;
		}
		var userCityLat = $( "#mapfollowers").attr( "data-map-lat");
		var userCityLng = $( "#mapfollowers").attr( "data-map-lng");
		
	    if ( userCityLat && userCityLng)
	    	updateLocationValues( userCityLat, userCityLng);  
	}else if( mapIndex){
		aqLocation();
	}

});

function init( userCityLat, userCityLong){

	var userName 	= $( "#mapfollowers").attr( "data-map-usrname");
	var uuid		= $( "#mapfollowers").attr( "data-map-usrid");
	if ( !uuid) return false;
	var platform = new H.service.Platform({
		app_id   : id,
		app_code : code,
    	useCIT: true
	});
	
	//Obtain the default map types from the platform object:
	var defaultLayers = platform.createDefaultLayers();
	
	// Create a map inside the map container DOM node
	try{
		if( !map){
			map = new H.Map( document.getElementById( "mapfollowers"),
	            defaultLayers.normal.map,{
	            zoom: 7,
	            center: { lat: userCityLat, lng: userCityLong}
			});
			
			//init group:
			group = new H.map.Group();
			map.addObject( group);
			behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));
		}
		
	}catch (e) {}
	
    //MapEvents enables the event system
	//Behavior implements default interactions for pan/zoom (also on mobile touch environments)
	
	function switchMapLanguage(map, platform, lang){
          var mapTileService = platform.getMapTileService({
          type:'base'
        }),
        
        languageMapLayer = mapTileService.createTileLayer(
            'maptile',
            'normal.day',
            256,
            'png8',
            {lg: lang}
        );
        map.setBaseLayer( languageMapLayer);
        
        //Create default UI:
	ui = H.ui.UI.createDefault(map, defaultLayers);
	
        //Remove map settings as unnecessary
	ui.removeControl( 'mapsettings');
        ui.removeControl( 'scalebar');
        
        }
	
	function addInfoBubble(map){
		//add tap event listener to the group
		group.addEventListener( 'tap', function(evt){
			//event target is the marker itself, group is a parent event target
			//for all objects that it contains
			map.setCenter( evt.target.getPosition());
			var bubble = new H.ui.InfoBubble( evt.target.getPosition(), {
			  content : evt.target.getData()
			});
			//show info bubble
			ui.addBubble( bubble);
		}, false);
                //add logged user
                if( userName)
                    addMarkerToGroup( group, userCityLat, userCityLong,
				'<div>'+ userName + '</div>'); 
                       
                //ajax get cities, coordinate and number time a city appears
                //iterate through it
                if( true){
                  $.ajax({
                    url: '/api/map/citiesinfos?uuid=' + uuid,
                    dataType: 'json',
                    type: 'GET',
                    beforeSend: function( xhr){
                      xhr.setRequestHeader("Accept", "application/json");
                      xhr.setRequestHeader("Content-Type", "application/json");
                    },
                    success: function( myjson, textStatus, jqXHR){
                      $.each( myjson , function(index, obj){
                        console.log( myjson);
                    	var count = obj.count;
                        var lat = obj.latitude;
                        var lng = obj.longitude;
                        if( lat && count && lng)
                        	updateCitiesMarkers( lat, lng, count);
                     });
                    },
                    error: function( jqXHR, textStatus, errorThrown){
                    }
                  });   
               }
	}
	
	try{
		addInfoBubble(map);
        switchMapLanguage(map, platform, getFormattedMapLang());
	}catch( Exception){}
	
}

function aqLocation(){

	var bxlat     = $("#mapbx").attr("data-latitude");
	var bxlng     	 = $("#mapbx").attr("data-longitude");
	if( !bxlat) bxlat = 44.83980376613671;
	if( !bxlng) bxlng = -0.580902099609375;
		
	var platform = new H.service.Platform({
		app_id   : id,
		app_code : code,
    	useCIT: true
	});
	//Obtain the default map types from the platform object:
	var defaultLayers = platform.createDefaultLayers();
	// Create a map inside the map container DOM node
	map = new H.Map( document.getElementById("mapbx"),
            defaultLayers.normal.map,{
            zoom: 5,
            center: { lat: bxlat, lng: bxlng }
	});
	
        //init group:
	group = new H.map.Group();
	map.addObject( group);
	
        //MapEvents enables the event system
	//Behavior implements default interactions for pan/zoom (also on mobile touch environments)
	var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));
	
	function switchMapLanguage(map, platform, lang){
          var mapTileService = platform.getMapTileService({
          type:'base'
        }),
        
        languageMapLayer = mapTileService.createTileLayer(
            'maptile',
            'normal.day',
            256,
            'png8',
            {lg: lang}
        );
        map.setBaseLayer( languageMapLayer);
        
        //Create default UI:
	ui = H.ui.UI.createDefault(map, defaultLayers);
	
        //Remove map settings as unnecessary
	ui.removeControl( 'mapsettings');
        ui.removeControl( 'scalebar');
        
        }
	
	function addInfoBubble(map){
		//add tap event listener to the group
		group.addEventListener( 'tap', function(evt){
			map.setCenter( evt.target.getPosition());
			//event target is the marker itself, group is a parent event target
			//for all objects that it contains
			var bubble = new H.ui.InfoBubble( evt.target.getPosition(), {
			  content : evt.target.getData()
			});
			//show info bubble
			ui.addBubble( bubble);
		}, false);
        //add logged user
        addMarkerToGroup( group, bxlat, bxlng,
				'<div>Aqlli</div>'); 
	}
	//Now use the map as required...
	addInfoBubble( map);
    switchMapLanguage( map, platform, getFormattedMapLang());
}

$(window).resize(function(){
    var mapContainer = document.getElementById( "mapfollowers") || document.getElementById( "mapbx");
    if( mapContainer && map){
      map.getViewPort().resize();
}

});

function getFormattedMapLang(){
	  var langage = (navigator.language || navigator.browserLanguage || navigator.systemLanguage);
	  var defLang = "ENG";
	  if( langage.toLowerCase().indexOf("zh-tw") > -1){
	    return "CHT";
	  }
	  if( langage.toLowerCase().indexOf("zh") > -1){
	    return "CHI";
	  }
	  
	  if( langage.toLowerCase().startsWith("fr")){
	    return "FRE";
	  }
	  /*if( langage.toLowerCase().startsWith("de")){
		    return "GER";
	  }
	  if( langage.toLowerCase().startsWith("nl")){
	    return "DUT";
	  }
	  if( langage.toLowerCase().startsWith("ar")){
	    return "ARA";
	  }
	  if( langage.toLowerCase().startsWith("el")){
	    return "GRE";
	  }
	  if( langage.toLowerCase().startsWith("he")){
	    return "HEB";
	  }
	  if( langage.toLowerCase().startsWith("hi")){
	    return "HIN";
	  }
	  if( langage.toLowerCase().startsWith("id")){
	    return "IND";
	  }
	  if( langage.toLowerCase().startsWith("fa")){
	    return "PER";//persan
	  }
	  if( langage.toLowerCase().startsWith("pl")){
	    return "POL";
	  }
	  if( langage.toLowerCase().startsWith("pt")){
	    return "POR";
	  }
	  if( langage.toLowerCase().startsWith("ru")){
	    return "RUS";
	  }
	  if( langage.toLowerCase().startsWith("es")){
	    return "SPA";
	  }
	  if( langage.toLowerCase().startsWith("th")){
	    return "THA";
	  }
	  if( langage.toLowerCase().startsWith("uk")){
	    return "UKR";
	  }
	  if( langage.toLowerCase().startsWith("vi")){
	    return "VIE";
	  }
	  if( langage.toLowerCase().startsWith("ur")){
	    return "URD";
	  }*/
	  return defLang;
	}
	function updateLocationValues( lat, lng){
		init( lat, lng);
	}
	function updateCitiesMarkers( lat, lng, cityCount){
		addMarkerToGroup( group, lat, lng,
				"<div ><i class='fa fa-users map'></i>" + cityCount + "</div>"
                        );
	}
	function addMarkerToGroup(group, latitude, longitude, html){
		  var marker = new H.map.Marker({lat:latitude, lng:longitude});
		  marker.draggable = false;
		  marker.setData(html);
		  group.addObject(marker);
	}