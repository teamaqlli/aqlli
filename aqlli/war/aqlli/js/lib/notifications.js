/*Created by Arix OWAYE on 10/04/16, inpired from binpress.com*/
var res = {
  title: "Arix now reads you",
  body: "",
  lang: "EN",
  iconUrl: ""
};
var notification = null;
var hidden, visibilityChange, windowHasFocus = true, perm;

/*when the user leaves the window*/
if (typeof document.hidden !== 'undefined') {
  // Opera 12.10, Firefox >=18, Chrome >=31, IE11
  hidden = 'hidden';
  visibilityChangeEvent = 'visibilitychange';
} else if (typeof document.mozHidden !== 'undefined') {
  // Older firefox
  hidden = 'mozHidden';
  visibilityChangeEvent = 'mozvisibilitychange';
} else if (typeof document.msHidden !== 'undefined') {
  // IE10
  hidden = 'msHidden';
  visibilityChangeEvent = 'msvisibilitychange';
} else if (typeof document.webkitHidden !== 'undefined') {
  // Chrome <31 and Android browser (4.4+ !)
  hidden = 'webkitHidden';
  visibilityChangeEvent = 'webkitvisibilitychange';
}

function init() {
	  var id = $("body").attr("data-user-id");
	  var u = '/api/notify?receiverId=' + id + "&lastNotifId=" + 0;
	  if ( id) {
		window.setInterval(function() {
	      $.ajax({
	        url: u,
	        dataType: 'json',
	        type: 'GET',
	        success: function(data, status) {
	          if ( !windowHasFocus && isDesktopNotifsActivated() == "on") {
	        	  htmlNotify( res, status);
	        	  console.log( "html");
	          } else {
	            aqlliNotify( res, status);
	            console.log( "aqlli");
	          }
	        }
	      });
	    }, 5000); //poll every 5 secs
	}
}


function visibleChangeHandler() {
  try{
	  if (document[hidden] && perm == "granted") {
		    windowHasFocus = false;
	  } else {
		    windowHasFocus = true;
	  }
  }catch (e) {}
}

$(document).ready(function() {
  notification = window.Notification || window.mozNotification || window.webkitNotification;
  /*listening to page visibility*/
  if ( windowHasFocus) {
    init();
  }
  if ( isDesktopNotifsActivated() == "on" && !('undefined' === typeof notification))
    notification.requestPermission(function(permission) {
      if (permission == "default" || permission == "denied") {
    	  store( "notifDesktop", "off");
    	  isDesktopNotif = false;
      }
      perm = permission;
    });
  document.addEventListener('visibilitychange', visibleChangeHandler, false);
});

function htmlNotify(data, status) {
  if (document['hidden']) {
    if (notification && 'undefined' === typeof notification) {
      messageCount = 0;
      if ('' != originalTitle)
        document.title = originalTitle;
      originalTitle = '';
    } else {
      if (false == Notify(data.title, data.body, data.lang, data.iconUrl)) {
        //fallback signaling which updates the tab title
        if ('' == originalTitle)
          originalTitle = document.title;
        messageCount++;
        document.title = '(' + messageCount + ') ' + originalTitle;
      } else {
        //Notification was shown
      }
    }
  }
}

function aqlliNotify(data, status) {
}

function Notify(titleText, bodyText, lng, iconUrl) {
  if (notification && 'undefined' === typeof notification)
    return false; //Not supported
  var noty = new notification(
    titleText, {
      body: bodyText,
      dir: 'ltr', // or auto, rtl
      lang: lng, //lang used within the notification.
      tag: 'notificationPopup', //An element ID to get/set the content
      icon: iconUrl //The URL of an image to be used as an icon
    }
  );
  noty.onclick = function() {
    
  };
  noty.onerror = function() {
    
  };
  noty.onshow = function() {
    
  };
  noty.onclose = function() {
    
  };
  return true;
}

function isDesktopNotifsActivated() {
  return store( "notifDesktop");
}

