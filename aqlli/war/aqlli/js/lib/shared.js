// Global Contact form
    $(document).ready(function( ){
        var form     = $('#main-contact-form');
        var csrftoken    = $("meta[name='_csrf']").attr("content");
          var csrfheader   = $("meta[name='_csrf_header']").attr("content");

        form.submit(function(event){
          event.preventDefault();
          var dta = {};
        $("#main-contact-form").serializeArray().map(function(x){dta[x.name] = x.value;}); 
          var endpoint     = $(this).attr('action');
          var namef        = $.trim( $("#main-contact-form div input#name").val());
              var subjectf     = $.trim( $("#main-contact-form div input#subject").val());
              var mailf        = $.trim( $("#main-contact-form div input#email").val());
              var mesgf        = $.trim( $("#message").val());

          if( namef == ""){
              if( !$("#main-contact-form div input#name").parent().hasClass( "has-error"))
                $("#main-contact-form div input#name").parent().addClass( "has-error");
            }else{
              if( $("#main-contact-form div input#name").parent().hasClass( "has-error"))
                $("#main-contact-form div input#name").parent().removeClass( "has-error");
            }
            if( subjectf == ""){
              if( !$("#main-contact-form div input#subject").parent().hasClass( "has-error"))
                $("#main-contact-form div input#subject").parent().addClass( "has-error");
            }else{
              if( $("#main-contact-form div input#subject").parent().hasClass( "has-error"))
                $("#main-contact-form div input#subject").parent().removeClass( "has-error");
            }
            if( mailf == ""){
              if( !$("#main-contact-form div input#email").parent().hasClass( "has-error"))
                $("#main-contact-form div input#email").parent().addClass( "has-error");
            }else{
              if( $("#main-contact-form div input#email").parent().hasClass( "has-error"))
                $("#main-contact-form div input#email").parent().removeClass( "has-error");
            }
            if( mesgf == ""){
              if( !$("#message").parent().hasClass( "has-error"))
                $("#message").parent().addClass( "has-error");
            }else{
              if( $("#message").parent().hasClass( "has-error"))
                $("#message").parent().removeClass( "has-error");
            }
          $.ajax({
              type : "POST",
              data : JSON.stringify( dta),
              url: endpoint,
              processData:false,
            dataType : "json",
              beforeSend: function(xhr){
              xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
                xhr.setRequestHeader(csrfheader, csrftoken);
              }
          }).done(function(data){
              //form_status.html('').delay(3000).fadeOut();
              $('#main-contact-form')[0].reset();
          });
        });
    });
    //limiting message length
    $(document).ready(function() {

    $('textarea[maxlength]#message').keyup(function(){
    	//get the limit from maxlength attribute
    	var limit = parseInt($(this).attr('maxlength'));
    	//get the current text inside the textarea
    	var text = $(this).val();
    	//count the number of characters in the text
    	var chars = text.length;
    	//check if there are more characters then allowed
    	if(chars > limit){
    		//and if there are use substr to get the text before the limit
    		var new_text = text.substr(0, limit);
    		//and change the current text with the new text
    		$(this).val(new_text);
    	}
    });
/*picture upload */   
    /*global window, $ */
    $(function () {
        'use strict';
        var csrftoken = $("meta[name='_csrf']").attr("content");
        var csrfheader = $("meta[name='_csrf_header']").attr("content");
        //Change this to the location of your server-side upload handler:
        var url = window.location.hostname + ":8888/aq/file",
            uploadButton = $('<button id="vld"/>')
                .addClass('btn btn-primary')
                .prop('disabled', false)
                .text('Valider')
                .on('click', function () {
                	var $this = $(this),
                        data = $this.data();
                        data.submit().success(function () {
                          $this.remove();
                        });
                });
        /*$('#fileupload').fileupload('option', {
            // Enable image resizing, except for Android and Opera,
            // which actually support image resizing, but fail to
            // send Blob objects via XHR requests:
            disableImageResize: /Android(?!.*Chrome)|Opera/
                .test(window.navigator.userAgent),
            maxFileSize: 999000,
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
    });*/
        $('#fileupload').fileupload({
        	xhrFields: {withCredentials: true},
            url: url,
            autoUpload: false,
            paramName: 'files[]',
            autoUpload: false,
            dataType : 'json',
            submit: function (ev, data) {
                var $this = $(this);
                var endpoint = '/rest/file/url?' + new Date().getTime() + "?callback=?";
                var f = fileExt( data.files[0].name), xhr = $.ajaxSettings.xhr();
                debugger;
                if( f == "jpeg" || f == "jpg" ||  f == "png" ){
                  /*$.getJSON(endpoint, function( result){
                	  data.url 	 = result.url;
                      data.jqXHR = xhr;
                      $this.fileupload( 'send', data);
                  });*/
                	$.ajax({
                	url: endpoint,
                    dataType: 'jsonp',
                    type: 'GET',
                    processData:false,
                    beforeSend: function( xhr){
                      xhr.setRequestHeader( "Accept", "application/json");
                      xhr.setRequestHeader( "Content-Type", "application/json");
                      xhr.setRequestHeader( csrfheader, csrftoken);
                      xhr.setRequestHeader( "Access-Control-Allow-Origin", "*");
                      xhr.setRequestHeader( 'X-Requested-With', 'XMLHttpRequest');
                      xhr.setRequestHeader( "X-Ajax-call", "true");
                      xhr.setRequestHeader( "X-Content-Type-Options", "nosniff");
                      xhr.setRequestHeader( "X-Frame-Options", "DENY");
                      xhr.setRequestHeader( "Access-Control-Allow-Methods", "GET, POST, OPTIONS");
                      xhr.setRequestHeader( "Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                      xhr.setRequestHeader( "Access-Control-Allow-Headers", "X-Custom-Header");
                      xhr.setRequestHeader( "X-XSS-Protection", "1; mode=block");
                      xhr.setRequestHeader( "Expires", "0");
                    },
                    success: function( result, textStatus, jqXHR){
                      data.url 	 = result.url;
                      data.jqXHR = jqXHR;
                      $this.fileupload( 'send', data).error(function (jqXHR, textStatus, errorThrown) { console.log( errorThrown)});
                      console.error(  'success', url, jqXHR.getAllResponseHeaders());
                    },
                    error: function( jqXHR, textStatus, errorThrown){
                    	console.error(endpoint, jqXHR.status, jqXHR.statusText);
                    	console.error('fail', endpoint, jqXHR.getAllResponseHeaders());
                    }
                  });
                }
            }
        }).on('fileuploadadd', function (e, data) {
          $("#files").html("");
          data.context = $('<div/>').appendTo('#files');
            $.each(data.files, function (index, file) {
                var node = $('<p/>')
                        .append($('<span/>').text(file.name));
                if (!index) {
                    node
                        .append('<br>')
                        .append(uploadButton.clone(true).data(data));
                }
                node.appendTo(data.context);
                prevPic( data);
            });
        }).on('fileuploadprogressall', function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        }).on('fileuploaddone', function (e, data) {
        debugger;
        $.each(data.result.files, function (index, file) {
            if (file.url) {
                var link = $('<a>')
                    .attr('target', '_blank')
                    .prop('href', file.url);
                $(data.context.children()[index])
                    .wrap(link);
            } else if (file.error) {
                var error = $('<span class="text-danger"/>').text(file.error);
                $(data.context.children()[index])
                    .append('<br>')
                    .append(error);
            }
        });
    }).on('fileuploadfail', function (e, data) {
            
            $.each(data.files, function (index) {
                var error = $('<span class="text-danger"/>').text('File upload failed.');
                $(data.context.children()[index])
                    .append('<br>')
                    .append(error);
            });
        });
    });
    
/**/   
});
    // Enable iframe cross-domain access via redirect option:
    /*$('#fileupload').fileupload(
        'option',
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/result.html?%s'
        )
    );*/
//dropdown in landscape should not overflow screen height
$( window ).resize(function() {
    $(".navbar-collapse").css({ maxHeight: $(window).height() - $(".navbar-header").height() + "px" });
});

function prevPic( img){
  var isIE = (navigator.appName=="Microsoft Internet Explorer");  
    
    var index = 0,
    file = img.files[index],
    node = $(img.context.children()[index]);
    var path = file.name;  
    var ext  = fileExt( path);
    resetPicProgress();
          if(ext == "jpeg" || ext == "jpg" ||  ext == "png" )  
          {       
            document.getElementById("vld").disabled = false;
            if(isIE) {  
              
              node
                .prepend( "<img class='img-responsive' src='" + path + "'/>");  
            }else{  
              if (file) 
              {  
                var reader = new FileReader();  
                reader.onload = function (e) {
                  
                  node
                .prepend( "<img class='img-responsive' src='" + e.target.result + "'/>");
                }
                  reader.readAsDataURL(file);  
              }  
            }  
        }else{  
      document.getElementById("vld").disabled = true;
        $("#files").html("");

    } 
    return false; 
}

function fileExt( fullPath){
  if (fullPath) {
    return fullPath.substring(fullPath.lastIndexOf('.') + 1).toLowerCase();
  }
}
function resetPicProgress(){
  $('#progress .progress-bar').css(
                'width',
                0 + '%'
            );
}
      
$("#updatePicMod").on('hidden.bs.modal', function(){
  $("#files").html("");
  resetPicProgress();
});