var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
		showLeftPush = document.getElementById( 'showLeftPush' ),
		body = document.body;

if( showLeftPush !== null){
  showLeftPush.onclick = function(e) {
    e.preventDefault();
    classie.toggle( this, 'active' );
    classie.toggle( body, 'cbp-spmenu-push-toright' );
    classie.toggle( menuLeft, 'cbp-spmenu-open' );
    disableOther( 'showLeftPush' );
  };
}


function disableOther( button ) {
	if( button !== 'showLeftPush' ) {
		classie.toggle( showLeftPush, 'disabled' );
	}
}

/* postsholder inverter  */
$('#switch').click(function(e){
    e.preventDefault();
    $(this).toggleClass('fa-toggle-off fa-toggle-on');
    var postsinverter = store( 'pinv');
    var  publicationContainer = $('#postsholderrow');
    var inverted = false;
    if ( $('#switch').hasClass( 'fa-toggle-on')){
      inverted = true;
    }else
        if ($('#switch').hasClass( 'fa-toggle-off')){
        inverted = false;  
    }
    if (inverted){
      if( postsinverter == null){
        store( 'pinv', 'on');
      }else if ( postsinverter == 'off'){
        store( 'pinv', 'on');
      }
    }else{
        if( postsinverter == null){
        store( 'pinv', 'off');
      }else if ( postsinverter == 'on'){
        store( 'pinv', 'off');
      } 
    }
    $('#postsholderrow .col-sm-3').toggleClass('well');
    publicationContainer.toggleClass('readability');
});

/* checking user settings for inverter when page loads*/
$(document).ready( function(e){
  var postsinverter = store( 'pinv');
  if (postsinverter == 'on'){
    $('#switch').addClass( 'fa-toggle-on');
    if( $('#switch').hasClass( 'fa-toggle-off')){
      $('#switch').removeClass( 'fa-toggle-off');
    }
    if( $('#postsholderrow .col-sm-3').hasClass('well')){
      $('#postsholderrow .col-sm-3').removeClass('well');
    }
    if( !$('#postsholderrow').hasClass('readability')){
      $('#postsholderrow').addClass('readability');
    }
  }else if ( postsinverter == 'off'){
    $('#switch').addClass( 'fa-toggle-off');
    if( $('#switch').hasClass( 'fa-toggle-on')){
      $('#switch').removeClass( 'fa-toggle-on');
    }
    if( !$('#postsholderrow .col-sm-3').hasClass('well')){
      $('#postsholderrow .col-sm-3').addClass('well');
    }
    if( $('#postsholderrow').hasClass('readability')){
      $('#postsholderrow').removeClass('readability');
    }
  }
});


//post style chooser -- dependance: simpleStorage
$(".bgpubli").click(function(){
  var spanEl = $(this).find( "> span.bg");
  var newBg = spanEl.attr('class').replace('bg ', '');
  store( 'bgpubli', newBg);
  var publis = $('.timeline-content').attr('class');
  var tmp = $.trim( publis.replace('panel timeline-content', ''));
  //mise à jour de la vue
  $('.timeline-content').addClass( newBg);
  $('.timeline-content').removeClass( tmp);
});
/*logout btn*/
$("#logOut").on("click", function(e){
  e.preventDefault();
  document.getElementById("logoutForm").submit();
});