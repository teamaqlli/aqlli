$(document).ready(function() {
  var loc = encodeURIComponent(document.location.href);
  $("#sfb").click(function() {
    window.open('https://www.facebook.com/sharer/sharer.php?u=' + loc, '_blank');
  });
  $("#stw").click(function() {
    window.open('https://twitter.com/share?url=' + loc, '_blank');
  });
  $("#sgp").click(function() {
    window.open('https://plus.google.com/share?url=' + loc, '_blank');
  });
});