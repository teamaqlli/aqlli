var postsmorebtn = "<div class='dropdown'><i class='fa fa-ellipsis-h fa-lg pull-right postsmorebtn dropdown-toggle' data-toggle='dropdown'></i><ul class='dropdown-menu pull-right'><li class='hide_'>Hide from everyone</li><li class='del'>Delete</li><li class='rep'>Report</li></ul></div>";
var isLocationValide = false;    
/*adding class to body*/
$(function(){
  $(document).ready(function(){
	$(document.body).addClass('cbp-spmenu-push'); //left menu push
  });
});

/* Disable / enable share form button*/
$("#status_title").keyup(function(){
      var btnshare = $("#btn-share");
      var statmsg = $("#status_message");
      if (( $.trim( $(this).val()) != '') || ( $.trim( statmsg.val()) != '')){
         btnshare.prop('disabled', false);
      }
      else
      	{
         btnshare.prop('disabled', true);
        }  
      });
      $("#status_message").keyup(function(){
       var btnshare = $("#btn-share");
       var stattitle = $("#status_message");
       if ((  $.trim( $(this).val()) != '') || ( $.trim( stattitle.val()) != '')){
       btnshare.prop('disabled', false);
       }else{
       btnshare.prop('disabled', true);
       } 
     });
      
/* Profile pic */
function valupdpic(){
  var picUri = $("#updpic");
  if ( picUri.text()){
    console.log( picUri.text());
  }
  return false;
}

/*activation du plugin d'uploading de photos de profile*/
/*$(function (){
  $('uploader').fileupload({submit: function ( e, data){
    var $this = $(this);
    debugger;
    $.getJSON( '/file/url?' + new Date().getTime(), function (result){
      data.url = result.url;
      $this.fileupload('send', data);
    });
    return false;
  }
  });
});*/


//upload profile picture and cover
/*$(document).ready(function(){
    if (Modernizr.touch) {
        // show the close overlay button
        $(".close-overlay").removeClass("hidden");
        // handle the adding of hover class when clicked
        $(".img").click(function(e){
            if (!$(this).hasClass("hover")) {
                $(this).addClass("hover");
            }
        });
        // handle the closing of the overlay
        $(".close-overlay").click(function(e){
            e.preventDefault();
            e.stopPropagation();
            if ($(this).closest(".img").hasClass("hover")) {
                $(this).closest(".img").removeClass("hover");
            }
        });
    } else {
        // handle the mouseenter functionality
        $(".img").mouseenter(function(){
            $(this).addClass("hover");
        })
        // handle the mouseleave functionality
        .mouseleave(function(){
            $(this).removeClass("hover");
        });
    }
});*/

//search bar
    $(document).on('ready', function() {
      $('#searchresult').hide();
    });
    $("#search-now").on("click", function(e){
      e.preventDefault();
    });
    $(function() {

      $(".search-box").keyup(function() {
        var searchkw   = $(this).val();
        var srurl  = '/search?query=' + searchkw;
        var output = "";
        var url    = "";
        var avatar = "";
        var name   = "";
        var work   = "";
        var city   = "";
        var id     = "";
        var separator = "";
        if( !$.trim(searchkw)) $("#searchresult").html("");
          
          $.ajax({
            url: srurl,
            dataType: 'json',
            type: 'GET',
            processData:false,
            beforeSend: function( xhr){
              xhr.setRequestHeader("Accept", "application/json");
              xhr.setRequestHeader("Content-Type", "application/json");
            },
            success: function( datausers, textStatus, jqXHR){
              var data = $.parseJSON(JSON.stringify(datausers.users) || '[]');
              $.each(data, function(key, val){
              if( data[key].id){
                id = data[key].id;
                url = "/users/" + id;
              }else{
                url = "/";
              }
              if( data[key].avatar) avatar = data[key].avatar;
              if( data[key].name) name = data[key].name;
              if( data[key].work) work = data[key].work;
              if( data[key].city){
                city      = data[key].city;
                separator = ' | ';
                var c = $.parseJSON(city || '[]').name;
              } 
              output += addUsrCard( url, avatar, name, work, c, separator);
            });
              $("#searchresult").html( output);  
            },
              error: function( jqXHR, textStatus, errorThrown){
              }
            });
                
        });

      jQuery(document).on(
          "click",
          function(e) {
            var $clicked = $(e.target);
            if (!$clicked.hasClass("search-box")) {
              jQuery("#searchresult").fadeOut();
              if ($('#searchresult').hasClass("well")) {
                jQuery("#searchresult").toggleClass('well');
              }
            }
            if ($clicked.hasClass("search-box")
                && !$('#searchresult').hasClass("well")) {
              jQuery("#searchresult").toggleClass('well');
            }
          });
      $('.search-box').click(function() {
        jQuery("#searchresult").fadeIn();

      });

    });

//formating live search results
function addUsrCard( usrUrl, userPicUrl, name, work, city, separator){
    var searchTemplate = window.AqTemplate.searchElement;
    var selt = {
      work : work,
      separator: separator,
      city: city,
      userPicUrl: userPicUrl,
      usrUrl: usrUrl
    }
	return Mustache.to_html( searchTemplate, selt);
}

//navigator go back
function goBack() {
	window.history.back();
}

//update wall
$.fn.showContainer = function(){
  $(this).removeClass('hide');
}

$.fn.hideContainer = function(){
  $(this).addClass('hide');
}

/*Password lost*/
  $(document).ready(function(){
    $("#btnlostPass").on("click", function(e){
      e.preventDefault();
      var postLoaderMsg   = $("#passLostMsg");
      var lostPassInput   = $("#lostPassInput").val();
      
      if ( !isEmailValid(lostPassInput)){
          if( !$( "#lostPass div.col-md-12 div.form-group").hasClass("has-error")){
            $("#lostPass div.col-md-12 div.form-group").toggleClass("has-error");
            $("#lostPass div.col-md-12 div.form-group").removeClass("has-success");
          }
      }else{
        if( !$( "#lostPass div.col-md-12 div.form-group").hasClass("has-success")){
            $("#lostPass div.col-md-12 div.form-group").toggleClass("has-success");
          }
      }

      if( lostPassInput){
        if($( "#lostPass div.col-md-12 div.form-group").hasClass("has-error") && isEmailValid(lostPassInput)){
          $("#lostPass div.col-md-12 div.form-group").removeClass("has-error");
        }
      }else{
          if( !$( "#lostPass div.col-md-12 div.form-group").hasClass("has-error")){
            $("#lostPass div.col-md-12 div.form-group").toggleClass("has-error");
            $("#lostPass div.col-md-12 div.form-group").removeClass("has-success");
          } 
      }
      var jsonData     = {"email" : lostPassInput};
      var endpoint     = '/login/password.lost';
      var csrftoken    = $("meta[name='_csrf']").attr("content");
      var csrfheader   = $("meta[name='_csrf_header']").attr("content");
      if ( lostPassInput && isEmailValid(lostPassInput)){
        $.ajax({
          url: endpoint,
          dataType: 'json',
          type: 'POST',
          data: JSON.stringify( jsonData),
          processData:false,
          beforeSend: function( xhr){
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.setRequestHeader(csrfheader, csrftoken);
          },
          success: function( data, textStatus, jqXHR){
            postLoaderMsg.html("<i class='fa fa-check text-success'></i>");
          },
          error: function( jqXHR, textStatus, errorThrown){

          }
        });
      }
    });
  });

  $(document).ready(function(e){
    
    //change connected user password
  $("#savePassBtn").on("click", function(e){
      e.preventDefault();
      var p1 = $("#np1").val();
      var p2 = $("#np2").val();
      var span = $("~ span", this);
      var jsonData = {"p1" : p1, "p2" : p2};
      var endpoint     = '/api/login/password.reset';
      var csrftoken    = $("meta[name='_csrf']").attr("content");
      var csrfheader   = $("meta[name='_csrf_header']").attr("content");
      if(( p1 == '') || ( p2 == '')){
        $('#updatePass div.input-group').addClass('has-error');
      }else{
        if( $('#updatePass div.input-group').hasClass('has-error')){
          $('#updatePass div.input-group').removeClass('has-error');
        }
      }  
      if( p1 && p2){
          $.ajax({
          url: endpoint,
          dataType: 'json',
          type: 'POST',
          data: JSON.stringify( jsonData),
          processData:false,
          beforeSend: function( xhr){
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.setRequestHeader(csrfheader, csrftoken);
          },
          success: function( data, textStatus, jqXHR){
            var res = JSON.stringify( data);
            if (  res.toLowerCase().indexOf( 'passreseterror') < 0 ){
              if( $('#updatePass div:first-child').hasClass('has-error')){
                  $('#updatePass div:first-child').removeClass('has-error');
              }
              span.html('&nbsp;&nbsp;<i class="fa fa-check fa-lg"></i>').delay(3000).fadeOut();
            }else{
                if( !$('#updatePass div:first-child').hasClass('has-error')){
                  $('#updatePass div:first-child').addClass('has-error');
                }
            }
          },
          error: function( jqXHR, textStatus, errorThrown){

          }
        });
        }
  });
  //change anonymous pass - form
  $("#cpwdfrm").submit(function(e){
    e.preventDefault();
    var np = $("#newpassword").val();
    var rnp = $("#repeatnewpassword").val();
    var boolContinue = true;
    if( ($.trim( np) == '') || ( $.trim( rnp) == '') || !( np == rnp)){
      $("#cpwdfrm div.form-group").addClass( "has-error");
      boolContinue = false;  
    }
    if( !boolContinue) return false;
    var formObj = $(this).serializeObject();
    var formURL = $(this).attr('action');
    var token   = $("meta[name='_csrf']").attr("content");
    var header  = $("meta[name='_csrf_header']").attr("content");
    
    $.ajax({
      url: formURL,
      type: 'POST',
      data: JSON.stringify( formObj),
      mimeType: "application/json",
      contentType:false,
      cache:false,
      processData:false,
      beforeSend: function( xhr){
        xhr.setRequestHeader("Accept", "application/json");
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.setRequestHeader(header, token);
      },
      success: function( data, textStatus, jqXHR){
        $("#cpwdfrm")[0].reset();
        if ( $("#cpwdfrm div.form-group").hasClass( "has-error")){
          $("#cpwdfrm div.form-group").removeClass( "has-error");
        }
        $("#cpwdfrm div.form-group").addClass( "has-success");
        $("#cpnr").html('&nbsp;&nbsp;<i class="fa fa-check fa-lg"></i>').delay(3000).fadeOut();
        setTimeout( function(){
          window.location.replace("/login");
        }, 5000);
      },
      error: function( jqXHR, textStatus, errorThrown){
      }
    });
  });
  

  });
  
/*Invite a friend */
  $(document).ready(function(){
    //invite friends to use the app
    $("#btnfrndOk").on("click", function(e){
      e.preventDefault();
      var inviteAfriendInput  = $("#tellafrndInput").val();
      if ( !isEmailValid(inviteAfriendInput)){
          if( !$("#tellafrndInput").parent().hasClass("has-error")){
            $("#tellafrndInput").parent().addClass("has-error");
          }
      }else{
        if( $("#tellafrndInput").parent().hasClass("has-error")){
          $("#tellafrndInput").parent().removeClass("has-error");
        }
      }
      var jsonData = {"email" : inviteAfriendInput};
      var endpoint     = '/user/invite';
      var csrftoken    = $("meta[name='_csrf']").attr("content");
      var csrfheader   = $("meta[name='_csrf_header']").attr("content");
      if ( inviteAfriendInput && isEmailValid(inviteAfriendInput)){
        $.ajax({
          url: endpoint,
          dataType: 'json',
          type: 'POST',
          data: JSON.stringify( jsonData),
          processData:false,
          beforeSend: function( xhr){
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.setRequestHeader(csrfheader, csrftoken);
          },
          success: function( data, textStatus, jqXHR){
            $("span#success").html('<i class="fa fa-check fa-lg"></i>').delay(3000).fadeOut();
            $("#tellafrndInput").val("");
          },
          error: function( jqXHR, textStatus, errorThrown){

          }
        });
      }
    });

    //Settings infos update
    $("#sgensave").on("click", function(){
      var lastName = $("#setNm").val();
      var firstName = $("#setPr").val();
      var day   = $("#bday select#birthdayHolder option:selected").text();
      var month = $("#bday select#birthmonthHolder option:selected").val();
      var year  = $("#bday select#birthYearHolder option:selected").text();
      var jsonData = {"lastName" : lastName, "firstName" : firstName, "dateOfBirth":{ "day": day, "month" : month, "year": year}};
      var token    = $("meta[name='_csrf']").attr("content");
      var header   = $("meta[name='_csrf_header']").attr("content");
      var endpoint = '/api/settings?field=gen';
      if( lastName && firstName){
        $.ajax({
          url: endpoint,
          type: 'POST',
          dataType: 'json',
          data: JSON.stringify(jsonData),
          mimeType: "application/json",
          cache:false,
          processData: false,
          beforeSend: function( xhr){
            xhr.setRequestHeader(header, token);
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
          },
          success: function( data, textStatus, jqXHR){
            window.location.reload();
          }
          
        });
      }
    });

    //Settings infos update
    $("#occupupd").on("click", function(){
      var job 	   = $("#setJb").val();
      var company  = $("#setComp").val();
      var citydata = $("#setHt").parent().attr("data-result");
      var jsonData = {"job" : job, "company" : company, "city" : citydata};
      var token    = $("meta[name='_csrf']").attr("content");
      var header   = $("meta[name='_csrf_header']").attr("content");
      var endpoint = '/api/settings?field=occu';
      if( isLocationValide){
    	  if( $("#setHt").parent().hasClass( "has-error"))
    		  $("#setHt").parent().removeClass("has-error");
          $.ajax({
            url: endpoint,
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify(jsonData),
            mimeType: "application/json",
            cache:false,
            processData: false,
            beforeSend: function( xhr){
              xhr.setRequestHeader(header, token);
              xhr.setRequestHeader("Accept", "application/json");
              xhr.setRequestHeader("Content-Type", "application/json");
            },
              success: function( data, textStatus, jqXHR){
            }  
          });
      }else{
    	  if( !$("#setHt").parent().hasClass( "has-error"))
    		  $("#setHt").parent().addClass("has-error");
      }
    });

    //user desc updates
    $("#descupd").on("click", function(){
      var description = $("#setUd").val();
      var jsonData = {"desc" : description};
      var token    = $("meta[name='_csrf']").attr("content");
      var header   = $("meta[name='_csrf_header']").attr("content");
      var endpoint = '/api/settings?field=desc';
        $.ajax({
          url: endpoint,
          type: 'POST',
          dataType: 'json',
          data: JSON.stringify(jsonData),
          mimeType: "application/json",
          cache:false,
          processData: false,
          beforeSend: function( xhr){
            xhr.setRequestHeader(header, token);
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
          },
          success: function( data, textStatus, jqXHR){
          }
          
        });
    });
    
    //closes upd userinfos on modal closing
    $('#updateInfsMod').on('hidden.bs.modal', function(){
      window.location.reload();
    });

    //social infos update
    $("#socialupd").on("click", function(){
      var twId   = $("#setTw").val();
      var fbId   = $("#setFb").val();
      var gpId   = $("#setGp").val();
      var webs   = $("#setWs").val();
      
      var jsonData = {"twitterId" : twId, "facebookId" : fbId, "googlepId" : gpId, "website" : webs};
      var token    = $("meta[name='_csrf']").attr("content");
      var header   = $("meta[name='_csrf_header']").attr("content");
      var endpoint = '/api/settings?field=soc';
       if( isURL( webs) || !webs){
        $.ajax({
          url: endpoint,
          type: 'POST',
          dataType: 'json',
          data: JSON.stringify(jsonData),
          mimeType: "application/json",
          cache:false,
          processData: false,
          beforeSend: function( xhr){
            xhr.setRequestHeader(header, token);
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
            if( $("#setWs").parent().hasClass("has-error")) $("#setWs").parent().removeClass( "has-error");
          },
          success: function( data, textStatus, jqXHR){
          }
          
        });
      }else{
        $("#setWs").parent().addClass( "has-error");
      }
    });
  });
/*Post on wall*/
  $(document).ready(function(){
    $("#timeline-post-frm").submit(function(e){
      e.preventDefault();
      var id = "/users/" + $('input#uuid').val();
      var id2 = id + "#";
      var pathname = $(location).attr('pathname');
      $("#btnShareContainer input#created").val( $.now());
      var formObj = $(this).serializeObject();
      var formURL = '/api/updatewall';
      var token = $("meta[name='_csrf']").attr("content");
      var header = $("meta[name='_csrf_header']").attr("content");
      if ( (id == pathname) || (id2 == pathname)){
        $.ajax({
          url: formURL,
          type: 'POST',
          data: JSON.stringify( formObj),
          mimeType: "application/json",
          contentType:false,
          cache:false,
          processData:false,
          beforeSend: function( xhr){
            xhr.setRequestHeader(header, token);
          },
          success: function( data, textStatus, jqXHR){
            $("#timeline-post-frm")[0].reset();
            $("#uwall ul.list-unstyled").prepend( addNewPostToView(data));
          },
          error: function( jqXHR, textStatus, errorThrown){

          }
        });
      }
    });

    $('#btn-share').click(function( e) {
      e.preventDefault();
      var id = $('input#uuid').val();
      var shareType = $('#shareType').val();
      var status_title = $('textarea#status_title').val();
      var message_content = $('textarea#status_message').val();
      $("#timeline-post-frm").submit();
      $('#btn-share').unbind();
    });
    $('body').delegate( '#shareType', 'click', function( e){
      e.preventDefault();
      $('.typeOfVideo').hideContainer( 'hide');
      $('.typeOfImage').hideContainer('hide');
      $('.shareType').val( $(this).attr('class'));
      if( $(this).attr( 'class').toLowerCase().indexOf("video") >= 0){
        $('.typeOfVideo').showContainer('hide');
        $('.typeOfImage').hideContainer('hide');
      }
      if($(this).attr('class').toLowerCase().indexOf("photo") >= 0) {
      $('.typeOfVideo').hideContainer('hide');
      $('.typeOfImage').showContainer('hide');
      }
      return false;
    });
    $(window).scroll(function(){
      var id = "/users/" + $('input#uuid').val();
      var id2 = id + "#";
      var pathname = $(location).attr('pathname');
      if ( (id == pathname) || (id2 == pathname)){
        if  ($(window).scrollTop() == $(document).height() - $(window).height()){
          loadData();
        }
      }      
    });
  });

  function loadData() {
    //#uwall ul li:first-child
    $('div#lastPostsLoader').html('<i class="fa fa-spinner fa-pulse fa-2x"></i>');
    var id = $("#uwall ul li:last-child").attr("id");
    var url = $(this).attr("href");
    if ( id){
      var lnk = '/api/updatewall?lastID=' + id + "&uuid=" + window.location.pathname.split( '/' ).pop();
      $.ajax({
        type : 'GET',
        url  : lnk,
        dataType : 'json',
        success  : function(publication) {
          if ( publication) {
            addFormatedData( publication);
          }
        }
      });
    }
      $('div#lastPostsLoader').empty();
    }
    function addFormatedData(  data) {
      //format json data in html and add it to the view
      $( "#lastPostsLoader").html( "<i class='fa fa-spinner fa-spin fa-lg'></i>");
       var res = "";
       if ( $.isArray( data)){
        $.each( data, function(k, v){
          if ( $.type( v) == "object"){
            var tostr = JSON.stringify( v);
            var parsed = $.parseJSON( "[" + tostr + "]");
            $.each( parsed, function(key, value){
              res += addNewPostToView( value);          
            });        
          }
        });
      }else{
         var jsonobj = $.parseJSON( '[' + data + ']');
         res = addNewPostToView( jsonobj); 
      }
      $("#uwall ul.list-unstyled").append( res);
      $( "#lastPostsLoader").html("");
      return false;
    }
    function validateVideoUrl(){
      var url        = $('#videoUrl').val();
      var regYoutube = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
      var regVimeo   = /^.*(vimeo\.com\/)((channels\/[A-z]+\/)|(groups\/[A-z]+\/videos\/))?([0-9]+)/;
      if(regYoutube.test(url) || regVimeo.test(url)) {
        return true;
      }else{
        return false;
      }
    }


//initialisation de l'user wall au demarrage
$(document).ready( function(){
  var id = "/users/" + $('input#uuid').val();
  var id2 = id + "#";
  var pathname = $(location).attr('pathname');
  var uwall = $("#wall");
  if ( uwall && ((id == pathname) || (id2 == pathname))){
    var lnk   = "/api/initwall?uuid=" + id.split('/').pop();
    $.ajax({
      url  : lnk,
      type : "GET",
      dataType: "json",
      beforeSend: function( xhr){
        xhr.setRequestHeader("Accept", "application/json");
        xhr.setRequestHeader("Content-Type", "application/json");
      },
      success: function ( json){
        $( "#initL").html( "<i class='fa fa-spinner fa-spin fa-lg'></i>");
        $("#uwall ul.list-unstyled").html( addData( json));
        $("#initL").html("");
      }
    });
  }
});

function addData( liste){
  if ( $("#uwall")){
    if( liste.toString() == ""){
      return "";
    }
    var u = "";
    $.each( liste, function(key, val){
      var jsonobj = $.parseJSON( '[' + val + ']');
      //if is array --> to json
      $.each( jsonobj, function( key, value){
      if ( value.shareType == "status"){
        u += "<li class='post-list' id='" +  value.postId + "' data-post='" + value.ident + "'>";
        u += "<div class='timeline-row'>";
        u += "<div class='timeline-time'>";
        u += time_passed ( value.created / 1000) +  postsmorebtn + "</div>";
        u += "<div class='timeline-icon'><div class='bg-thumb'>";
        u += "<a href='/users/" + value.user_id  + "'> <img class='img-circle' alt='' src='" + value.userProfilePic + "'></a></div></div>";
        u += "<div class='panel timeline-content " + getPubliStyleClass() +  "'><div class='panel-body'>";
        u += "<span class='ptitle'>" + value.title + "</span>";
        u += "<p>" + value.message.value + "</p>";
        u += "</div>";
        u += postInteract( value.numberOfLikes);
        u += "</div></div></li>";
      }
      if ( value.shareType == "achron"){
        u += "<li id='" + value.postId + "' data-post='" + value.ident + "'>";
        u += "<div class='timeline-row'>";
        u += "<div class='timeline-time'>";
        u += time_passed ( value.created / 1000) + postsmorebtn + "</div>";
        u += "<div class='timeline-icon'><div class='bd-primary'><i class='fa fa-pencil'></i></div></div>";
        u += "<div class='panel timeline-content " + getPubliStyleClass() +  "'><div class='panel-body'>";
        u += "<span class='ptitle'>" + value.message.value + "</span>";
        u += "<p></p>";
        u += "</div>";
        u += postInteract( value.numberOfLikes);
        u += "</div></div></li>";
      }
      if ( value.shareType == "photo"){
        u += "<li id='" + value.postId + "' data-post='" + value.ident + "'>";
        u += "<div class='timeline-row'>";
        u += "div class='timeline-time'>";
        u += time_passed ( value.created / 1000) + postsmorebtn + "</div>";
        u += "<div class='timeline-icon'><div class='bd-primary'><i class='fa fa-pencil'></i></div></div>";
        u += "<div class='panel timeline-content " + getPubliStyleClass() +  "'><div class='panel-body'>";
        u += "<span class='ptitle'>" + value.title + "</span>";
        u += "<img class='img-responsive' src='" + value.imageUrl + "'>";
        u += "<p>" + value.message.value + "</p>";
        u += "</div>";
        u += postInteract( value.numberOfLikes);
        u += "</div></div></li>";
      }
      if ( value.shareType == "video"){
        var media = videoMedia( value.videoUrl);
        
        if ( media.embedurl){
          u += "<li id='" + value.postId + "' data-post='" + value.ident + "'>";
          u += "<div class='timeline-row'><div class='timeline-time'>";
          u += time_passed ( value.created / 1000) + postsmorebtn + "</div>";
          u += "<div class='timeline-icon'><div class='bg-thumb'>";
          u += "<a href='/users/" + value.user_id  + "'> <img class='img-circle' alt='' src='" + value.userProfilePic + "'></a>";
          u += "</div></div><div class='panel timeline-content " + getPubliStyleClass() +  "'><div class='panel-body'>";
          u += "<span class='ptitle'>" + value.title + "</span>";
          u += "<div class='video-container'>";
          u += "<iframe allowfullscreen='' frameborder='0' mozallowfullscreen='' src='" + media.embedurl + "?enablejsapi=0&origin=http://www.aqlli.com' webkitallowfullscreen='' scrolling='no'></iframe></div>";
          u += "<p>" + value.message.value + "</p>";
          u += "</div>";
          u += postInteract( value.numberOfLikes);
          u += "</div></div></li>";
        }else{
            u += "<li class='post-list' id='" +  value.postId + "' data-post='" + value.ident + "'>";
            u += "<div class='timeline-row'><div class='timeline-time'>";
            u += time_passed ( value.created / 1000) + postsmorebtn + "</div>";
            u += "<div class='timeline-icon'><div class='bg-thumb'>";
            u += "<a href='/users/" + value.user_id  + "'> <img class='img-circle' alt='' src='" + value.userProfilePic + "'></a></div></div>";
            u += "<div class='panel timeline-content " + getPubliStyleClass() +  "'><div class='panel-body'>";
            u += "<span class='ptitle'>" + value.title + "</span>";
            u += "<p>" + value.message.value + "</p>";
            u += "</div>";
            u += postInteract( value.numberOfLikes);
            u += "</div></div></li>";
        }
      }
      });
    });
    
    return u;
  }
  return "";
}
function addNewPostToView (value){
    var u ="";
    if ( value.shareType == "status"){
        u += "<li class='post-list' id='" +  value.postId + "' data-post='" + value.ident + "'>";
        u += "<div class='timeline-row'>";
        u += "<div class='timeline-time'>";
        u += time_passed ( value.created / 1000) + postsmorebtn + "</div>";
        u += "<div class='timeline-icon'><div class='bg-thumb'>";
        u += "<a href='/users/" + value.user_id  + "'> <img class='img-circle' alt='' src='" + value.userProfilePic + "'></a></div></div>";
        u += "<div class='panel timeline-content " + getPubliStyleClass() +  "'><div class='panel-body'>";
        u += "<span class='ptitle'>" + value.title + "</span>";
        u += "<p>" + value.message.value + "</p>";
        u += "</div>";
        u += postInteract( value.numberOfLikes);
        u += "</div></div></li>";
      }
      if ( value.shareType == "achron"){
        u += "<li id='" + value.postId + "' data-post='" + value.ident + "'>";
        u += "<div class='timeline-row'>";
        u += "<div class='timeline-time'>";
        u += time_passed ( value.created / 1000) + postsmorebtn + "</div>";
        u += "<div class='timeline-icon'><div class='bd-primary'><i class='fa fa-pencil'></i></div></div>";
        u += "<div class='panel timeline-content " + getPubliStyleClass() +  "'><div class='panel-body'>";
        u += "<span class='ptitle'>" + value.message.value + "</span>";
        u += "<p></p>";
        u += "</div>";
        u += postInteract( value.numberOfLikes);
        u += "</div></div></li>";
      }
      if ( value.shareType == "photo"){
        u += "<li id='" + value.postId + "' data-post='" + value.ident + "'>";
        u += "<div class='timeline-row'>";
        u += "div class='timeline-time'>";
        u += time_passed ( value.created / 1000) + postsmorebtn + "</div>";
        u += "<div class='timeline-icon'><div class='bd-primary'><i class='fa fa-pencil'></i></div></div>";
        u += "<div class='panel timeline-content " + getPubliStyleClass() +  "'><div class='panel-body'>";
        u += "<span class='ptitle'>" + value.title + "</span>";
        u += "<img class='img-responsive' src='" + value.imageUrl + "'>";
        u += "<p>" + value.message.value + "</p>";
        u += "</div>";
        u += postInteract( value.numberOfLikes);
        u += "</div></div></li>";
      }
      if ( value.shareType == "video"){
        var media = videoMedia( value.videoUrl);
        
        if ( media.embedurl){
          u += "<li id='" + value.postId + "' data-post='" + value.ident + "'>";
          u += "<div class='timeline-row'><div class='timeline-time'>";
          u += time_passed ( value.created / 1000) + postsmorebtn + "</div>";
          u += "<div class='timeline-icon'><div class='bg-thumb'>";
          u += "<a href='/users/" + value.user_id  + "'> <img class='img-circle' alt='' src='" + value.userProfilePic + "'></a>";
          u += "</div></div><div class='panel timeline-content " + getPubliStyleClass() +  "'><div class='panel-body'>";
          u += "<span class='ptitle'>" + value.title + "</span>";
          u += "<div class='video-container'>";
          u += "<iframe allowfullscreen='' frameborder='0' mozallowfullscreen='' src='" + media.embedurl + "?enablejsapi=0&origin=http://www.aqlli.com' webkitallowfullscreen='' scrolling='no'></iframe></div>";
          u += "<p>" + value.message.value + "</p>";
          u += "</div>";
          u += postInteract( value.numberOfLikes);
          u += "</div></div></li>";
        }else{
            u += "<li class='post-list' id='" +  value.postId + "' data-post='" + value.ident + "'>";
            u += "<div class='timeline-row'><div class='timeline-time'>";
            u += time_passed ( value.created / 1000) + postsmorebtn + "</div>";
            u += "<div class='timeline-icon'><div class='bg-thumb'>";
            u += "<a href='/users/" + value.user_id  + "'> <img class='img-circle' alt='' src='" + value.userProfilePic + "'></a></div></div>";
            u += "<div class='panel timeline-content " + getPubliStyleClass() +  "'><div class='panel-body'>";
            u += "<span class='ptitle'>" + value.title + "</span>";
            u += "<p>" + value.message.value + "</p>";
            u += "</div>";
            u += postInteract( value.numberOfLikes);
            u += "</div></div></li>";
        }
      }
      return u;
    }

//see if passwords are equals when registering
$(document).ready( function(){
      
      $('#pass1').keyup(function() {
        var BadsizePass = "<br/><spring:message code='i18n.message.badSizePass'" + "text='The password must be at least 6 characters long'" + "/>";
        var Strong = '<i class="fa fa-lock fa-lg"></i>&nbsp;<i class="fa fa-smile-o fa-lg"></i>';
        var Medium = '<i class="fa fa-smile-o fa-lg"></i>';
        var Weak = '<i class="fa fa-unlock fa-lg text-danger"></i>&nbsp;<i class="fa fa-lg fa-frown-o text-danger"></i>';
        var strongRegex = new RegExp("^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$", "g");
        var mediumRegex = new RegExp("^(?=.{7,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", "g");
        var enoughRegex = new RegExp("(?=.{6,}).*", "g");
        
        if (false == enoughRegex.test($(this).val())) {
             $('#pwdlength').html(BadsizePass);
        }else{
          $('#pwdlength').html('');
        }
        if (strongRegex.test($(this).val())) {
             $('#pwdstrength').className = 'ok';
             $('#pwdstrength').html(Strong);
        } else if (mediumRegex.test($(this).val())) {
             $('#pwdstrength').className = 'alert';
             $('#pwdstrength').html(Medium);
        } else {
             $('#pwdstrength').className = 'error';
             $('#pwdstrength').html(Weak);
        }
        });
     });
    //follow user button
    $(".flcontainer").on("click", function(e){
        e.preventDefault();
        var data_name      = $(this).attr("data-name");
        var data_user_id   = $(this).attr("data-user-id");
        var data_user_name = $(this).attr("data-user-name");
        var data_follow_status = $(this).attr("data-follow-status");
        var csrftoken    = $("meta[name='_csrf']").attr("content");
        var csrfheader   = $("meta[name='_csrf_header']").attr("content");
        var endpoint = "/api/follow";
        var jsonData = {"data_name":data_name, "data_user_id":data_user_id, "data_user_name":data_user_name, "data_follow_status":data_follow_status};
        var div = $(this);
        if( data_name && data_user_id && data_user_name){
          $.ajax({
          url: endpoint,
          dataType: 'json',
          type: 'POST',
          data: JSON.stringify( jsonData),
          processData:false,
          beforeSend: function( xhr){
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.setRequestHeader(csrfheader, csrftoken);
          },
          success: function( data, textStatus, jqXHR){
            followOrUnfollow( div);
          },
          error: function( jqXHR, textStatus, errorThrown){

          }
        });

        }
    });

function followOrUnfollow( $div){
  if( $div.attr( "data-follow-status") == "following"){
    $div.removeClass( "following");
    $div.addClass( "follow");
    $div.find( 'i').html( $div.attr( "data-follow-tag"));
    $div.find( 'i').removeClass( "fa-user-times");
    $div.find( 'i').addClass( "fa-user-plus");
    $div.find( 'i').attr('title', $div.attr( "data-follow-tag"));
    $div.attr( "data-follow-status", "follow");
    return false;
  } 
  if( $div.attr("data-follow-status") == "follow"){
    $div.addClass("following");
    $div.find('i').html( $div.attr( "data-unfollow-tag"));
   
    $div.find('i').removeClass("fa-user-plus");
    $div.find('i').addClass("fa-user-times");
    $div.removeClass("follow");
    $div.find( 'i').attr('title', $div.attr( "data-unfollow-tag"));
    $div.attr("data-follow-status", "following");
    return false;
  }
  return false;
}
function postInteract( numberOfLikes){
  var res = "";
  if( numberOfLikes){
    res =  "<div class='post-interct'><i class='fa fa-lg fa-heart faa-pulse animated faa-slow'></i><span class='lks' >" + numberOfLikes + "</span></div>";
  }else{
    res = "<div class='post-interct'><i class='fa fa-lg fa-heart faa-pulse animated faa-slow'></i><span class='lks'></span></div>";
  }
  return res;
}

    //follow user button
    $(document).on("click", ".post-interct .fa-heart", function(e){
        e.preventDefault();
        var data_post_id   = $(this).parent().parent().parent().parent().attr("data-post");
        var likesCountContainer = $(this).siblings("span.lks");
        var csrftoken      = $("meta[name='_csrf']").attr("content");
        var csrfheader     = $("meta[name='_csrf_header']").attr("content");
        var endpoint       = "/api/post?type=like&postId=" + data_post_id;
        var spn = $(this);
        if( data_post_id){
          $.ajax({
          url: endpoint,
          dataType: 'json',
          type: 'POST',
          processData:false,
          beforeSend: function( xhr){
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.setRequestHeader(csrfheader, csrftoken);
          },
          success: function( data, textStatus, jqXHR){
            debugger;
            likesCountContainer.text( data.numberOfLikes);
            if( !spn.hasClass("lk")){
              spn.addClass("lk");
            }
            //todo send like notif here
          },
          error: function( jqXHR, textStatus, errorThrown){

          }
        });

        }
    });

//Positionnement de la bare de boutons et son animation
/*var startProductBarPos=-1;
    var bar = document.getElementById('btnsm');
    function findPosY(obj) {
        var curtop = 0;
        if ( bar){
          if (typeof (obj.offsetParent) != 'undefined' && obj.offsetParent) {
            while (obj.offsetParent) {
                curtop += obj.offsetTop;
                obj = obj.offsetParent;
            }
            curtop += obj.offsetTop;
        }
        else if (obj.y)
            curtop += obj.y;
        return curtop;
        }
    }

  window.onscroll=function(){
      var enabled = false;
      if ( bar && enabled){
        var top = $('#btnsm').offset().top;

        if(startProductBarPos < 0) startProductBarPos = findPosY(bar);

        if( pageYOffset>startProductBarPos){
          $(".links-menu-holder div.center-block ul.pull-right").css("margin-right", "20px");
          bar.style.marginTop = "-10px";
          bar.style.marginRight = "100px";
          $('#btnsm').css('position','');
          top = $('#btnsm').offset().top;
          $('#btnsm').css('position','absolute');
          //$('#btnsm').css('top', Math.max( startProductBarPos, $(document).scrollTop()));
          $('#btnsm').stop().animate({
            "top": Math.max( startProductBarPos, $(document).scrollTop())
          }, "slow");
          bar.style.zIndex = "800";
        }else{
          $('#btnsm').css('position','');
          $(".links-menu-holder div.center-block ul.pull-right").css("margin-right", "-50px");
          bar.style.marginTop = "0px";
          bar.style.zIndex = "800";
        }
      }   
  };*/
  /* Login Help page*/
  $(function() {
    var noPass = false;
    var noAccount = false;
    $("#lostPass").hide();
    $("a#lostPassM").click(function() {
      if (noPass == false) {
        $("#noAccount").slideToggle();
        $("#lostPass").slideToggle();
        noPass = true;
        noAccount = true;
      }
      return false;
    });
    $("a#noAccountM").click(function() {
      if (noAccount == true) {
        $("#noAccount").slideToggle();
        $("#lostPass").slideToggle();
        noAccount = false;
        noPass = false;
      }
      return false;
    });
  });

  /*Delete account*/
  $(document).ready(function(){
    $('#uuidd').click(function() {
      var id = $('input#uuid').val();
      if ( id){
        var target = '/user/delete';
        var json = {"uid" : id};
        var token = $("meta[name='_csrf']").attr("content");
        var header = $("meta[name='_csrf_header']").attr("content");
        $.ajax({
          url: target,
          data: JSON.stringify(json),
          type: "POST",
          beforeSend: function( xhr){
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.setRequestHeader(header, token);
          },
          success: function(){
            window.location.reload();
          }
        });
      }
    });
  });
  $.fn.serializeObject = function() {
    var o = {};
    $(this).find('input[type="hidden"], input[type="text"], input[type="password"], input[type="checkbox"]:checked, input[type="radio"]:checked, select, textarea').each(function() {
        if ($(this).attr('type') == 'hidden') { //if checkbox is checked do not take the hidden field
            var $parent = $(this).parent();
            var $chb = $parent.find('input[type="checkbox"][name="' + this.name.replace(/\[/g, '\[').replace(/\]/g, '\]') + '"]');
            if ($chb != null) {
                if ($chb.prop('checked')) return;
            }
        }
        if (this.name === null || this.name === undefined || this.name === '')
            return;
        var elemValue = null;
        if ($(this).is('select'))
            elemValue = $(this).find('option:selected').val();
        else elemValue = this.value;
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(elemValue || '');
        } else {
            o[this.name] = elemValue || '';
        }
    });
    return o;
}
function videoMedia(pastedData) {
var success = false;
var media   = {};

if ( vimeoID( pastedData)) {
    var vimeo_id = vimeoID( pastedData);
    media.type  = "vimeo";
    media.id    = vimeo_id;
    media.embedurl = "https://player.vimeo.com/video/" + vimeo_id + "?title=0&amp;byline=0&amp;portrait=0";
    success = true;
}else if ( getDailyMotionId(pastedData)) {
    var dailymotion_id = getDailyMotionId(pastedData);
    if( dailymotion_id){
      if ( dailymotion_id.toLowerCase().indexOf ("video/") >= 0){  dailymotion_id = dailymotion_id.replace( "video/", "");}
      media.type  = "dailymotion";
      media.id    = dailymotion_id;
      media.embedurl = "http://www.dailymotion.com/embed/video/" + dailymotion_id;
      success = true;
    }
}else if ( vineId( pastedData)){
   var vine_id = vineId( pastedData);
   media.type = "vine";
   media.id = vine_id;
   media.embedurl = "https://vine.co/v/" + vine_id + "/embed/simple";
   success = true;
  
}else if (pastedData.match('(http|https)://(www.)?youtube|youtu\.be')) {
    if (pastedData.match('embed')) { youtube_id = pastedData.split(/embed\//)[1].split('"')[0]; }
    else { youtube_id = pastedData.split(/v\/|v=|youtu\.be\//)[1].split(/[?&]/)[0]; }
    media.type  = "youtube";
    media.id    = youtube_id;
    media.embedurl = "http://www.youtube.com/embed/" + youtube_id;
    success = true;
}

if (success) { return media; }
else { media.embedurl = pastedData; media.type = "unknown"; return media; }
return false;
}
function getDailyMotionId(url) {
    var m = url.match(/^.+dailymotion.com\/((video|hub|embed)\/([^_]+))?[^#]*(#video=([^_&]+))?/);
    return m ? m[5] || m[3] : null;
}
vimeo_Reg = /https?:\/\/(?:www\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|)(\d+)(?:$|\/|\?)/;
vine_Reg = /((http:\/\/(vine\.co\/v\/.*|www\.vine\.co\/v\/.*))|(https:\/\/(vine\.co\/v\/.*|www\.vine\.co\/v\/.*))|((\/\/vine\.co\/v\/.*|www\.vine\.co\/v\/.*)))/i;

function vimeoID(url) {
  var match = url.match(vimeo_Reg);
  if (match){
    return match[3];
  }else{
    return false;
  }
}
function vineId( url){
    var match = url.match( vine_Reg);
    if( match){
      var res = url.split(".co/v/").pop();  
      if ( res.toLowerCase().indexOf( "/") > -1){
        var res2 = res.split("/");
        return res2[0];
      }else{
        return res;
      }
    }else{
      return false;
    }
  }
function time_passed (ttamp){
    var timestamp      =  ttamp;
    var current_time   = $.now()/1000; //in seconds
    var diff           = Math.floor( current_time - timestamp);
    var result = "";
    //year, month, week, day, hour and minute in seconds
    var intervals      =  {
        'year' : 31556926, 'month' : 2629744, 'week' : 604800, 'day' : 86400, 'hour' : 3600, 'minute': 60
    };
    
    if (diff == 0)
    {
        result = 'just now';
        return result;
    }    

    if (diff < 60)
    {
        result = diff == 1 ? diff   +  ' second ago' : diff  +  ' seconds ago';
        return result;
    }        

    if (diff >= 60 && diff < intervals['hour'])
    {
        diff = Math.floor(diff/intervals['minute']);
        result = ( diff == 1) ? diff   +  ' minute ago' : diff   +  ' minutes ago';
        return result;
    }        

    if (diff >= intervals['hour'] && diff < intervals['day'])
    {
        diff = Math.floor(diff/intervals['hour']);
        result = ( diff == 1) ? diff   +  ' hour ago' : diff   +  ' hours ago';
        return result;
    }    

    if (diff >= intervals['day'] && diff < intervals['week'])
    {
        diff = Math.floor(diff/intervals['day']);
        result = (diff == 1) ? diff   +  ' day ago' : diff   +  ' days ago';
        return result;
    }    

    if (diff >= intervals['week'] && diff < intervals['month'])
    {
        diff = Math.floor(diff/intervals['week']);
        result = (diff == 1)  ?diff   +  ' week ago' : diff   +  ' weeks ago';
        return result;
    }    

    if (diff >= intervals['month'] && diff < intervals['year'])
    {
        diff = Math.floor(diff/intervals['month']);
        result = (diff == 1) ? diff  +  ' month ago' : diff   +  ' months ago';
        return result;
    }    

    if (diff >= intervals['year'])
    {
        diff = Math.floor(diff/intervals['year']);
        result = (diff == 1)  ? diff   +  ' year ago' : diff  +  ' years ago';
        return result;
    }
    return false;
} 
//selection du style des publication
function getPubliStyleClass(){
  var el = store( 'bgpubli');
  if ( el){
    return el;
  }else{
    return 'bgw';//default white
  }
}
//client side email validation
function isEmailValid ( value ) {
  var regExp = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i;
  if ( 'string' !== typeof value ){
    return false;
  }
  if ( !regExp.test( value ) ){
    return false;
  }
  return true;
}
window.isMobileOrTablet = function() {
  var check = false;
  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
  return check;
}
//client side url validation
function isURL(str) {
  var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
  '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|'+ // domain name
  '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
  '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
  '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
  '(\\#[-a-z\\d_]*)?$','i'); // fragment locator

  return pattern.test(str);
}

//emoji picker
$(document).ready(function(e) {
      //visibility of the emoji picker button
      var allowedLoc = $(location).attr('href');
      if( !(allowedLoc.indexOf( "/users/") >= 0)){
        return false;
      }
      //For now, we want to disable custom emojipicker for safari because of bugs
      var isSafari = navigator.vendor.indexOf("Apple")==0 && /\sSafari\//.test(navigator.userAgent);
      var pickerbtn = $(".pickEmoji.pull-left");
      if ( pickerbtn && !window.isMobileOrTablet() && !isSafari){
        $(".pickEmoji.pull-left").css("visibility", "visible");
      }else{
        $(".pickEmoji.pull-left").css("visibility", "hidden");
      }
      $('.emojipickercontent').emojiPicker({
        width: '300px',
        height: '200px',
        button:false
      });
      var focusOnType = "";
        
      $('#status_title').on('focus', function(){
        focusOnType = "title";
      });
      $('#status_message').on('focus', function(){
        focusOnType = "message";
      });
      $('.pickEmoji').click(function(e) {
        e.preventDefault();
        var scrollTop     = $(window).scrollTop(),
          elementOffset = $('#btnShareContainer').offset().top,
          bottom      = elementOffset - $('#btnShareContainer').outerHeight( true);

        if( focusOnType == "title"){
          $('#status_title').emojiPicker('toggle');
        }
        if( focusOnType=="message") {
          $('#status_message').emojiPicker('toggle');
        }
        $("div.emojiPicker").css({ top: bottom + "px" });
      });
      $( window ).resize(function() {
        $('.emojiPicker').css('display', 'none');
      });
    });
//report a bug modal
$(document).ready( function(){
  $("#reportbugv").click(function(e){
    e.preventDefault();
    var descLength = $("#inputReportBugDesc").attr("maxlength");
    var t          = $("#inputTitleReportBug").val();
    var desc       = $("#inputReportBugDesc").val();
    if( !t){
      if( !$("#inputTitleReportBug").parent().hasClass("has-error")){
          $("#inputTitleReportBug").parent().addClass("has-error");
      }
      if( $("#inputTitleReportBug").parent().hasClass("has-success")){
        $("#inputTitleReportBug").parent().removeClass( "has-success");
      }
    }else{
      if( $("#inputTitleReportBug").parent().hasClass("has-error")){
          $("#inputTitleReportBug").parent().removeClass( "has-error");
      }
      if( !$("#inputTitleReportBug").parent().hasClass("has-success")){
        $("#inputTitleReportBug").parent().addClass( "has-success");
      }
    }

    if( !desc){
      if( !$("#inputReportBugDesc").parent().hasClass("has-error")){
          $("#inputReportBugDesc").parent().addClass("has-error");
      }
      if( $("#inputReportBugDesc").parent().hasClass("has-success")){
        $("#inputReportBugDesc").parent().removeClass( "has-success");
      }
    }else{
      if( $("#inputReportBugDesc").parent().hasClass("has-error")){
          $("#inputReportBugDesc").parent().removeClass( "has-error");
      }
      if( !$("#inputReportBugDesc").parent().hasClass("has-success")){
        $("#inputReportBugDesc").parent().addClass( "has-success");
      }
    }

    if( (descLength > 0 && descLength < 401) && t && desc){
        var token     = $("meta[name='_csrf']").attr("content");
        var header    = $("meta[name='_csrf_header']").attr("content");
        var urlOrigin = window.location.href;
        var target = "/api/reportbug";
        var json = {"title":t, "description" : desc, "urlOrigin": urlOrigin, "target" : target};
        $.ajax({
          url: target,
          data: JSON.stringify(json),
          type: "POST",
          beforeSend: function( xhr){
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.setRequestHeader(header, token);
          },
          success: function(){
            $("#reportbugv").newt("span").removeClass("hide");
          }
        });
    }
  });
});

$(document).ready( function(){
  //validation button is enabled only when a user selects a city
  /* user cities fetcher*/
  var loc = document.getElementById("occupupd");
  if( loc){
	  loc.disabled = true;
	  TeleportAutocomplete.init('.user_city').on('change', function(value) {
		  $(".user_city").parent().attr("data-result", JSON.stringify(value, null, 2));
		  isLocationValide = true;
	  });
  }
  $(".user_city").keyup( function(){
	  isLocationValide = false;
  });
});

function appendScript(pathToScript) {
    var head = document.getElementsByTagName("head")[0];
    var js = document.createElement("script");
    js.type = "text/javascript";
    js.src = pathToScript;
    head.appendChild(js);
}