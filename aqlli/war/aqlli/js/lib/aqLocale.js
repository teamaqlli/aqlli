function createCookie(name,value, reload) {
    var date = new Date();
    date.setTime( date.getTime()+( 18600*24*60*60*1000));
    var expires = "; expires="+date.toGMTString();
    document.cookie = name+"="+value+expires+"; path=/";
    if ( reload == true){
      window.location.reload();
    }
    return true;
}

function getCookie( cname){
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for( i = 0; i < ca.length; i++){
    var c = ca[i];
    while( c.charAt(0) == ' ') c = c.substring(1);
    if( c.indexOf( name) == 0)
       return c.substring(name.length, c.length);
  }
  return "";
}
function delCookie(cname) {
  document.cookie = cname +'=; expires=Thu, 01-Jan-70 00:00:01 GMT;';
}
window.onload = function(e){ 
  var localeCookie = "GWT_LOCALE";
  var defaultLang = getCookie( localeCookie);
  if (( defaultLang == "default") || ( !defaultLang)){
    if( navigator.browserLanguage)
      var language = navigator.browserLanguage;
    else
    var language = navigator.language;
    if ( language.indexOf( 'fr') > -1) createCookie(localeCookie, 'fr', false);
    else if ( language.indexOf( 'en') > -1) createCookie(localeCookie, 'en', false);
    else if ( language.indexOf( 'uz') > -1) createCookie(localeCookie, 'uz', false);
    else if ( language.indexOf( 'de') > -1) createCookie(localeCookie, 'de', false);
    else if ( language.indexOf( 'es') > -1) setLocaleCookie(localeCookie, 'es', false);
    else if ( ( language.indexOf( 'zh') > -1) || ( language.indexOf( 'cn') > -1) ) createCookie (localeCookie, 'zh', false);
    else if ( language.indexOf( 'pt')) createCookie(localeCookie, 'pt', false);
    else
     createCookie(localeCookie, 'en', false);  
    }  
  }
  function clfs( newLang) {//sets cookie and reloads
      var localeCookie = "GWT_LOCALE";
        delCookie( localeCookie);
        createCookie( localeCookie, newLang, true);
      }
      function changeLangFormSubmit( newLang) {

        document.getElementById('langToSet').value = newLang;
        document.getElementById("changeLangForm").submit();
      }
   function aclfs( ln){
     var u = document.getElementById("changeLangForm").action;
     var json = {"defaultLang" : ln};
     var token = $("meta[name='_csrf']").attr("content");
     var header = $("meta[name='_csrf_header']").attr("content");
     var lang = ln;
     if ( lang){
       var localeCookie = "GWT_LOCALE";
       createCookie( localeCookie, lang, false);
      $.ajax({
        url: u,
        data: JSON.stringify(json),
        type: "POST",
        beforeSend: function( xhr){
          xhr.setRequestHeader("Accept", "application/json");
          xhr.setRequestHeader("Content-Type", "application/json");
          xhr.setRequestHeader(header, token);
        },
        success: function(){
          window.location.reload();
        }
      });
     }

   }