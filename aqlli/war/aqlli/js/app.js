//Warning : Dans le code html, importer classie js avant require js
require.config({
	catchError: {
		define: true
	},
	waitSeconds: 0,
  //Paths to scripts
  'paths': {
	'notifications' : '/aqlli/js/lib/notifications',
	'templates' : '/aqlli/js/lib/templates',
	'jquery.ui.widget' : '/aqlli/js/upd/vendor/jquery.ui.widget',
	'jquery.iframe-transport' : '/aqlli/js/upd/jquery.iframe-transport',
	'jquery.fileupload' : '/aqlli/js/upd/jquery.fileupload',
	'jquery.fileupload-process' : '/aqlli/js/upd/jquery.fileupload-process',
	'jquery.fileupload-image' : '/aqlli/js/upd/jquery.fileupload-image',
	'jquery.fileupload-validate' : '/aqlli/js/upd/jquery.fileupload-validate',
	'app'            : '/aqlli/js/app',
	'shared'         : '/aqlli/js/lib/shared',
	'sharer'         : '/aqlli/js/lib/sharer',
	'mapsjs-core'	 : '//js.api.here.com/v3/3.0/mapsjs-core',
	'mapsjs-service' : '//js.api.here.com/v3/3.0/mapsjs-service',
	'mapsjsEvents'   : '//js.api.here.com/v3/3.0/mapsjs-mapevents',
    'mapsjsUi' 		 : '//js.api.here.com/v3/3.0/mapsjs-ui',
	'usermaphints'   : '/aqlli/js/lib/usermaphints',
	'jquery'		 : [
    	'http://static.aqlli.com/scripts/js/jquery',
    	'/aqlli/js/lib/jquery'
    ],
    'html5shiv'     : '//html5shiv.googlecode.com/svn/trunk/html5.js',
    'euCookieLaw'   : [
        '/aqlli/js/lib/euCookieLaw'
    ],
    'leftSideMenu'	: [
    	'/aqlli/js/lib/leftSideMenu'
    ],
	'aqlli.auth' 	: [
		'/aqlli/js/lib/myAuth'
	],
	'SimpleStore'   :[
	    'http://static.aqlli.com/scripts/js/SimpleStore',
	    '/aqlli/js/lib/SimpleStore'
	],
	'bootstrap' 	: [
		'http://static.aqlli.com/scripts/js/bootstrap',
		'/aqlli/js/lib/bootstrap'
	],
	'bootstrap-rating' :[
		'http://static.aqlli.com/scripts/js/bootstrap-rating.min',
		'/aqlli/js/lib/bootstrap-rating.min'
	],
	'mainfunctions' : [
		'/aqlli/js/lib/mainfunctions'
	],
	'modernizr' 	: [
		'http://static.aqlli.com/scripts/js/modernizr.min',
		'/aqlli/js/lib/modernizr.min'
	],
	'aqLocale'  	: [
		'/aqlli/js/lib/aqLocale'
	],
	'jquery.emojipicker' : [
	    'http://static.aqlli.com/scripts/js/jquery.emojipicker',
	    '/aqlli/js/lib/jquery.emojipicker'
	],
	'jquery.emojipicker.tw' : [
	    'http://static.aqlli.com/scripts/js/jquery.emojipicker.tw',
        '/aqlli/js/lib/jquery.emojipicker.tw'
	],
	'facebook'		: '//connect.facebook.net/en_US/sdk',
	'facebookLike'  : [
		'http://static.aqlli.com/scripts/js/facebookLike',
		'/aqlli/js/lib/facebookLike'
	],
	'twitter' 		: '//platform.twitter.com/widgets',
	'googlePlus' 	: '//apis.google.com/js/plusone',
	'Mustache' : '/aqlli/js/lib/mustache'
  },
  'baseUrl'	: '/aqlli/js/lib',
  //loading with dependency
  'shim' : {
	'jquery' : {
	  exports : '$'
	},
	'templates' : {
		exports: 'templates'
	},
	'notifications': {
		deps:['jquery', 'SimpleStore'],
		exports: 'notifications'
	},
	'html5shiv':{
		deps:['jquery'],
        exports: 'html5shiv' 
    },
	'shared' : {
		deps:['jquery', 'jquery.fileupload'],
		exports: 'shared'
	},
	'jquery.ui.widget' : {deps:['jquery']},
	'jquery.iframe-transport' : {deps:['jquery']},
	'jquery.fileupload' : {deps:['jquery']},
	'jquery.fileupload-process' : {deps:['jquery']},
	'jquery.fileupload-image' : {deps:['jquery']},
	'jquery.fileupload-validate' : {deps:['jquery']},
	'mapsjs-core':{
	  exports : 'mapsjs-core' 
	},
	'mapsjs-service' : {
		deps:['mapsjs-core'],
	},
	'mapsjsUi': {
        deps: ['mapsjs-service', 'mapsjs-core']
      },
	'modernizr':{
	  deps: ['jquery'],
	  exports : 'modernizr'
	},
	'SimpleStore':{
		exports: 'SimpleStore'
	},
	'bootstrap':{
	  deps: ['jquery'],
	  exports : 'bootstrap'
	},
	'leftSideMenu':{
	  deps:['jquery', 'classie', 'SimpleStore'],
	  exports: 'leftSideMenu'
	},
	'facebook':{
	  exports: 'FB'
	},
	'euCookieLaw' : {
		deps:['jquery', 'SimpleStore'],
		exports: 'euCookieLaw'
	},
	'bootstrap-rating' :{
		deps:['bootstrap'],
		exports : 'bootstrap-rating'
	},
	'usermaphints' : {
		deps: ['mapsjs-core', 'mapsjs-service', 'SimpleStore', 'mapsjsUi'],
		exports: 'usermaphints'
	},
    'mapsjsEvents': {
      deps: ['mapsjs-core']
    },
    'jquery.emojipicker' : {
      deps: ['jquery'],
      exports: 'emojis'
    },
    'jquery.emojipicker.tw' : {
      deps: ['jquery', 'jquery.emojipicker']
    },
	'mainfunctions':{
		  deps:['jquery', 'modernizr', 'SimpleStore', 'jquery.emojipicker', 'jquery.emojipicker.tw', 'teleport-autocomplete', 'templates', 'Mustache'],
		  exports : 'mainfunctions'
	},
	'sharer' : {
		deps: ['jquery']
	}
  }
  });

require(["jquery", "mainfunctions", "app/main", "aqLocale", "aqlli.auth", "modernizr", "bootstrap", "leftSideMenu", "euCookieLaw", "bootstrap-rating", "SimpleStore", 'shared', 'sharer', 'notifications', 'templates', 'Mustache'], function($){
    if( document.location.href.indexOf("/users")){
      require(["facebook", "twitter", "googlePlus", "facebookLike", 'jquery.emojipicker', 'jquery.emojipicker.tw', "jquery.ui.widget", "jquery.iframe-transport", "jquery.fileupload", "jquery.fileupload-process",
     	"jquery.fileupload-image", "jquery.fileupload-validate"]);
    }
    if( document.location.href.indexOf("/home")){
      require(["usermaphints", "mapsjs-service", 'mapsjsEvents', 'mapsjsUi']);
    }
    //ie8 and below specific scripts
    if( $('html.lt-ie9').size()){
    	require(["html5shiv"]);
    }
}, function(err){
    //process error
    if (err.requireType === 'timeout') {
        
    }else {
        
    } 
});