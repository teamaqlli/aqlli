<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<sec:authorize var="isloggedIn" access="isAuthenticated()" />
<%-- Aqlli... What else? ;) --%>
<%-- Redirection to the login page handler --%>
<c:choose>
    <c:when test="${isloggedIn}">
    	<c:redirect url="/home"/>
    </c:when>
    <c:otherwise>
    </c:otherwise>
</c:choose>
