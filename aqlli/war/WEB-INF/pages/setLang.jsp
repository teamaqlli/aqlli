<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page import="com.aqlli.main.client.model.UserDetails"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
  //récupère la valeur du cookie de localité
  String GWT_LOCALE = "GWT_LOCALE";
  Cookie cookie = null;
  Cookie[] cookies = null;
  String defaultLang = "";
  cookies = request.getCookies();
  if (cookies != null) {
	for (int i = 0; i < cookies.length; i++) {
	  cookie = cookies[i];
	  if (cookie.getName().compareTo( GWT_LOCALE) == 0) {
		defaultLang = cookie.getValue();
	  }
	}
  }
  //le cookie n'existe pas?
  if ( defaultLang == ""){
	  cookie = new Cookie( GWT_LOCALE, "default");
	  cookie.setComment( "User language cookie");
	  java.util.Date now = new java.util.Date();
	  cookie.setMaxAge( 1000 * 60 * 60 * 24 * 18600);//Dans 23 ans
	  response.addCookie( cookie);
  }
%>
<% 
  if (request.isUserInRole("USER_ROLE")){
	 UserDetails user = (UserDetails)request.getAttribute("user");
	 if ( user.getDefaultLang().equalsIgnoreCase("default") && (defaultLang != "")){
	   user.setDefaultLang( defaultLang);
	 }
	 if ( user.getDefaultLang() != "" && user.getDefaultLang() != "default"){
		  cookie = new Cookie( GWT_LOCALE, user.getDefaultLang());
		  cookie.setComment( "User language cookie");
		  java.util.Date now = new java.util.Date();
		  cookie.setMaxAge( 1000 * 60 * 60 * 24 * 18600);//Dans 23 ans
		  response.addCookie( cookie);
	  }
  }
%>
<div class="btn-group btn-group-xs dropup" id="langPickerHolder">
    <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><i class="fa fa-language fa-lg" style="color:white;"></i>&nbsp;<spring:message code="i18n.language" text="Language"/>&nbsp;<span class="caret"></span> </a>
	<ul class="dropdown-menu langmod" role="menu">
		<li class=""><a onclick="clfs( 'en');"><spring:message code="i18n.english" text="English"/></a></li>
		<li><a onclick="clfs( 'fr');"><spring:message code="i18n.french" text="french"/></a></li>
		<li><a onclick="clfs( 'zh');"><spring:message code="i18n.simplified.chinese" text="Chinese(Simplified)"/></a></li>
	</ul>
</div>