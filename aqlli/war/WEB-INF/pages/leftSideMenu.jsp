<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<sec:authorize var="loggedIn" access="isAuthenticated()" />
<c:if test="${loggedIn}">
	<%@ include file="logout.jsp"%>
	<div class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left list-group"
		id="cbp-spmenu-s1">
		<a href="/settings" class="list-group-item"><i
			class="fa fa-wrench fa-fw">&nbsp;</i>
		<spring:message code="i18n.settings" text="Settings" /></a>
		<a
			id="logOut" class="list-group-item"><i
			class="fa fa-sign-out fa-fw">&nbsp;</i>
		<spring:message code="i18n.log.out" text="Log out" /></a>
		<a
			class="list-group-item"><i class="fa fa-toggle-off fa-fw"
			id="switch"></i>&nbsp;<spring:message code="i18n.reading.light"
				text="Reading Light" /></a>
		<ul class="list-unstyled list-group-item">
			<li class="text-center"><span id="readblty"><spring:message
						code="i18n.readability" text="Readability" /></span></li>
			<li>
				<ul class="list-inline text-center" id="bgs">

					<li class="bgpubli"><span class="bg bgg">Aqlli</span> <span
						class="nm"><spring:message code="i18n.color.grey"
								text="Grey" /></span></li>
					<li class="bgpubli"><span class="bg bgb">Aqlli</span> <span
						class="nm"><spring:message code="i18n.color.black"
								text="Black" /></span></li>
					<li class="bgpubli"><span class="bg bgp">Aqlli</span> <span
						class="nm"><spring:message code="i18n.color.paper"
								text="Paper" /></span></li>
					<li class="bgpubli"><span class="bg bgw">Aqlli</span> <span
						class="nm"><spring:message code="i18n.color.white"
								text="White" /></span></li>
				</ul>
			</li>
		</ul>
		<div class="toggler" id="showLeftPush">
			<span class="fa fa-cog">&nbsp;</span>
		</div>
	</div>
</c:if>