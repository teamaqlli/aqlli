<!Doctype HTML>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page session="false"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="http://static.aqlli.com/scripts/js/myAuth.js"
	type="text/javascript"></script>
<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
<script>
$(window).load( function(){
$('#pass1img').keyup(function(e) {
     var strongRegex = new RegExp("^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$", "g");
     var mediumRegex = new RegExp("^(?=.{7,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", "g");
     var enoughRegex = new RegExp("(?=.{6,}).*", "g");
     if (false == enoughRegex.test($(this).val())) {
             $('#pwdstrength').html('BadsizePass');
     } else if (strongRegex.test($(this).val())) {
             $('#pwdstrength').className = 'ok';
             $('#pwdstrength').html('Strong');
     } else if (mediumRegex.test($(this).val())) {
             $('#pwdstrength').className = 'alert';
             $('#pwdstrength').html('Medium');
     } else {
             $('#pwdstrength').className = 'error';
             $('#pwdstrength').html('Weak');
     }
     return true;
});

});
</script>
<title>register</title>
</head>
<body onload='document.registerForm.firstName.focus();'>
<noscript>
	var noScriptDiv       = window.getElementById("NoScript");
	var clickHereRefresh  = '<a href="javascript:location.reload();"><img src="http://www.deflamenco.com/files/img/reload.png" alt="reload"/></a>';
	noScriptDiv.innerHTML = "You need to activate Javascript in order to register in Aqlli. After activation, refresh the page or click " + clickHereRefresh;
	var showSendButton = "showButton";
</noscript>
<div id="NoScript">
</div>

	<c:if test="${param.submitted}">
		<!-- champs vides -->
		<c:if test="${empty param.firstName}" var="noFirstName" />
		<c:if test="${empty param.email}" var="noEmail" />
		<c:if test="${empty param.lastName}" var="noLastName" />
		<c:if test="${empty param.matchingPassword}" var="noMatchingPassword" />
		<c:if test="${empty param.password}" var="noPassword" />
		<c:if test="${(empty param.birthDay)}"
			var="dateEmpty" />
		<c:if test="${empty param.useTerms}" var="noCheckUseTerm" />
	</c:if>
    
	<H1>Register</H1>
	<form:form modelAttribute="user" method="POST" enctype="utf8"
		name="registerForm">
		<input type="hidden" name="submitted" value="true" />
		<br>
		<table>
			<tr>
				<td><label>FirstName (*)</label></td>
				<td>
					<form:input path="firstName" name="firstName" />
						<c:if test="${noFirstName}">
							<small><font color="red"> Note: you must enter a name </font></small>
						</c:if>
				</td>
			</tr>
			<tr>
				<td><label>LastName (*)</label></td>
				<td><form:input path="lastName" name="lastName"/> <c:if
						test="${noLastName}">
						<small><font color="red"> Note: you must enter a
								LastName </font></small>
					</c:if></td>
			</tr>
			<tr>
				<td><label>Email (*)</label></td>
				<td><form:input path="email" name="email"/> <c:if
						test="${noEmail}">
						<small><font color="red"> Note: you must enter a
								mail address </font></small>
					</c:if></td>
			</tr>
			<tr>
				<td><label for="pass">Password (*)</label></td>
				<td><form:input name="pass" id="pass1img" path="password"
						type="password" /> <c:if test="${noPassword}">
						<small><font color="red"> Note: you must enter a a
								password </font></small>
					</c:if>
				<br/>
			    <span>Password Strength: </span>
				<span id="pwdstrength"></span>
				</td>
				
			</tr>
			<tr>
				<td><label for="matchingPassword">Confirm pass (*)</label></td>
				<td><form:input name="matchingPassword" id="pass2img"
						path="matchingPassword" type="password"
						onkeyup="checkPassImg(); return false;" /> <c:if
						test="${noMatchingPassword}">
						<small><font color="red"> Note: you must confirm
								the password </font></small>
					</c:if> <span id="confirmMessageImg" class="confirmMessage"></span>
				<br/>
				<c:if
						test="${passNotEq}">
						<small><font color="red"> Passwords must match</font></small>
				</c:if>	
				</td>
			</tr>
			<tr>
				<td><label for="birthDay">Birthday (*)</label></td>
				<td><form:select name="birthDay" id="birthdayInput"
						path="birthDay">
						<option value="" selected>&nbsp;</option>
						<option value="01">1</option>
						<option value="02">2</option>
						<option value="03">3</option>
						<option value="04">4</option>
						<option value="05">5</option>
						<option value="06">6</option>
						<option value="07">7</option>
						<option value="08">8</option>
						<option value="09">9</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
						<option value="13">13</option>
						<option value="14">14</option>
						<option value="15">15</option>
						<option value="16">16</option>
						<option value="17">17</option>
						<option value="18">18</option>
						<option value="19">19</option>
						<option value="20">20</option>
						<option value="21">21</option>
						<option value="22">22</option>
						<option value="23">23</option>
						<option value="24">24</option>
						<option value="25">25</option>
						<option value="26">26</option>
						<option value="27">28</option>
						<option value="28">28</option>
						<option value="29">29</option>
						<option value="30">30</option>
						<option value="31">31</option>
					</form:select>
				</td>
				<td><form:select name="birthMonth" id="BirthMonthHolder"
						path="birthMonth">
						<option value="01" selected>January</option>
						<option value="02">February</option>
						<option value="03">March</option>
						<option value="04">April</option>
						<option value="05">May</option>
						<option value="06">June</option>
						<option value="07">July</option>
						<option value="08">August</option>
						<option value="09">September</option>
						<option value="10">October</option>
						<option value="11">November</option>
						<option value="12">December</option>
					</form:select></td>
				<td><form:select name="birthYear" id="birthYearInput"
						path="birthYear">
						<option value="2002">2002</option>
						<option value="2001">2001</option>
						<option value="2000">2000</option>
						<option value="1999">1999</option>
						<option value="1998">1998</option>
						<option value="1997">1997</option>
						<option value="1996">1996</option>
						<option value="1995">1995</option>
						<option value="1994">1994</option>
						<option value="1993">1993</option>
						<option value="1992">1992</option>
						<option value="1991">1991</option>
						<option value="1990" selected>1990</option>
						<option value="1989">1989</option>
						<option value="1988">1988</option>
						<option value="1987">1987</option>
						<option value="1986">1986</option>
						<option value="1985">1985</option>
						<option value="1984">1984</option>
						<option value="1983">1983</option>
						<option value="1982">1982</option>
						<option value="1981">1981</option>
						<option value="1980">1980</option>
						<option value="1979">1979</option>
						<option value="1978">1978</option>
						<option value="1977">1977</option>
						<option value="1976">1976</option>
						<option value="1975">1975</option>
						<option value="1974">1974</option>
						<option value="1973">1973</option>
						<option value="1972">1972</option>
						<option value="1971">1971</option>
						<option value="1970">1970</option>
						<option value="1969">1969</option>
						<option value="1968">1968</option>
						<option value="1967">1967</option>
						<option value="1966">1966</option>
						<option value="1965">1965</option>
						<option value="1964">1964</option>
						<option value="1963">1963</option>
						<option value="1962">1962</option>
						<option value="1961">1961</option>
						<option value="1960">1960</option>
						<option value="1959">1959</option>
						<option value="1958">1958</option>
						<option value="1957">1957</option>
						<option value="1956">1956</option>
						<option value="1955">1955</option>
						<option value="1954">1954</option>
						<option value="1953">1953</option>
						<option value="1952">1952</option>
						<option value="1951">1951</option>
						<option value="1950">1950</option>
						<option value="1949">1949</option>
						<option value="1948">1948</option>
						<option value="1947">1947</option>
						<option value="1946">1946</option>
						<option value="1945">1945</option>
						<option value="1944">1944</option>
						<option value="1943">1943</option>
						<option value="1942">1942</option>
						<option value="1941">1941</option>
						<option value="1940">1940</option>
						<option value="1939">1939</option>
						<option value="1938">1938</option>
						<option value="1937">1937</option>
						<option value="1936">1936</option>
						<option value="1935">1935</option>
						<option value="1934">1934</option>
						<option value="1933">1933</option>
						<option value="1932">1932</option>
						<option value="1931">1931</option>
						<option value="1930">1930</option>
						<option value="1929">1929</option>
						<option value="1928">1928</option>
						<option value="1927">1927</option>
						<option value="1926">1926</option>
						<option value="1925">1925</option>
						<option value="1924">1924</option>
						<option value="1923">1923</option>
						<option value="1922">1922</option>
						<option value="1921">1921</option>
						<option value="1920">1920</option>
						<option value="1919">1919</option>
						<option value="1918">1918</option>
						<option value="1917">1917</option>
						<option value="1916">1916</option>
						<option value="1915">1915</option>
						<option value="1914">1914</option>
						<option value="1913">1913</option>
						<option value="1912">1912</option>
						<option value="1911">1911</option>
						<option value="1910">1910</option>
						<option value="1909">1909</option>
						<option value="1908">1908</option>
						<option value="1907">1907</option>
						<option value="1906">1906</option>
						<option value="1905">1905</option>
						<option value="1904">1904</option>
						<option value="1903">1903</option>
						<option value="1902">1902</option>
						<option value="1901">1901</option>
						<option value="1900">1900</option>
					</form:select>
					<br/>
					<c:if test="${dateEmpty}">
						<small><font color="red">The date of birth can't be empty </font></small>
					</c:if>
					</td>
			</tr>
			<tr>
				<td><form:checkbox name="useTerms" id="useTermsCBox"
						path="useTerms" value="agree" /> <c:if
						test="${noCheckUseTerm}">
						<small><font color="red"> You need to accept the
								terms to be able to use surf the site </font></small>
					</c:if></td>
			</tr>
		</table>
		<c:if test="${showSendButton eq null}">
			<button type="submit" >Submit</button>
		</c:if>
	</form:form>
	<br>
	<div id="AccountExists">
		<a href="<c:url value="/aqlli/login" />"> I already have an
			account</a>
	</div>
	<div id="SelectLocaleContainer">
		<span>Select your language</span>
		<select id="LocaleHolder">
			<option value="empty" selected>&nbsp;</option>
			<option value="FR">French</option>
			<option value="EN">English</option>
		</select>
	</div>
</body>
</html>