<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page session="false"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<title>register</title>
</head>
<body>
    <H1>
        Register
    </H1>
    <form:form modelAttribute="user" method="POST" enctype="utf8">
        <br>
        <tr>
       <td><label>FirstName
            </label>
        </td>
       <td><form:input path="firstName" value=""/></td>
       <fmt:message key="faq"></fmt:message>
   </tr>
   <tr>
       <td><label>LastName
            </label>
        </td>
       <td><form:input path="lastName" value="" /></td>
       <!--<form:errors path="errorastName" element="div"/>-->
   </tr>
   <tr>
       <td><label>Email
            </label>
        </td>
       <td><form:input path="email" value="" /></td>
       <!--<form:errors path="email" element="div" />-->
   </tr>
   <tr>
       <td><label>Password
            </label>
        </td>
       <td><form:input path="password" value="" type="password" /></td>
       <!--<form:errors path="password" element="div" />-->
   </tr>
   <tr>
       <td><label>Confirm pass
            </label>
        </td>
       <td><form:input path="matchingPassword" value="" type="password" /></td>
       <!--<form:errors element="div" />-->
   </tr>
        <button type="submit">Submit
        </button>
    </form:form>
    <br>
    <a href="<c:url value="/aqlli/login" />">
        Login
    </a>
</body>
</html>