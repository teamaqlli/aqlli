<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ page session="true"%>
<%@ include file="/WEB-INF/pages/include/htmlLangTag.jsp"%>
<sec:authorize var="isloggedIn" access="isAuthenticated()" />
<html lang="${lng}">
<head>
<meta charset="utf-8">
<title><spring:message code="i18n.register"
		text="Sign Up" /> | Aqlli</title>
		<link rel="apple-touch-icon" href="http://static.aqlli.com/icons/touch-icon-iphone.png">
<link rel="apple-touch-icon" sizes="76x76" href="http://static.aqlli.com/icons/touch-icon-ipad.png">
<link rel="apple-touch-icon" sizes="120x120" href="http://static.aqlli.com/icons/touch-icon-iphone-retina.png">
<link rel="apple-touch-icon" sizes="152x152" href="http://static.aqlli.com/icons/touch-icon-ipad-retina.png">
<link href="http://static.aqlli.com/scripts/css/footer.css" media="all"
	rel="stylesheet">
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta content="follow,index" name="robots">
<meta name="robots" content="NOODP">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="description"
	content="<spring:message code="i18n.meta.register"
		text="register aqlli new account" />">
<meta name="author" content="Arix OWAYE">
<link rel="publisher" href="//plus.google.com/b/106910419300047477169"/>
<link rel="icon" sizes="any" mask href="http://static.aqlli.com/icons/favicon.svg">
<link rel="canonical" href="${requestScope['javax.servlet.forward.request_uri']}">
<link rel="shortcut icon" href="http://static.aqlli.com/icons/favicon.ico"
	type="image/x-icon" />
<link href="http://netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css"
	rel="stylesheet">
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"
        media="all" rel="stylesheet">
<link href="http://static.aqlli.com/scripts/css/styles.css" media="all"
	rel="stylesheet">
	<script src="http://static.aqlli.com/scripts/js/classie.js"></script>
<script data-main="/aqlli/js/app" src="/aqlli/js/require.js"></script>
<style>
#register-box {
	width: 60%;
	padding: 20px;
	margin: 30px auto;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
}
body{
background-color: #F5F5F5;
}
#bottombtns{
margin-bottom: 15px;
}
#register-box div.text-center span{
font-size: 25px;
margin-top:750px;
}
#register-box div.text-center span i{
color:#a94442;
margin-right: 5px;
}
</style>
</head>

<body onload='document.registerForm.firstName.focus();'>
<div id="fb-root"></div>
<%@ include file="/WEB-INF/pages/include/noScript.jsp"%>
	<% String badMail          = (String)request.getAttribute("emailRejected");
       pageContext.setAttribute("badMail", badMail);
       String passNotEq        = (String)request.getAttribute("passNotEq");
       pageContext.setAttribute("passNotEq", passNotEq);
    %>
	<c:if test="${param.submitted}">
		<!-- champs vides -->
		<c:if test="${empty param.firstName}" var="noFirstName" />
		<c:if test="${empty param.email}" var="noEmail" />
		<c:if test="${empty param.lastName}" var="noLastName" />
		<c:if test="${empty param.matchingPassword}" var="noMatchingPassword" />
		<c:if test="${empty param.password}" var="noPassword" />
		<c:if test="${(empty param.birthDay)}" var="dateEmpty" />
		<c:if test="${empty param.useTerms}" var="noCheckUseTerm" />
	</c:if>
	<div id="wrapper">
	<%@ include file="topNavBar.jsp"%>
	<div class="container reg" id="maincontentholder">
		<div class="row">
			<div class="col-md-12">
				<div id="register-box">
				  <c:if test="${isInviteTokenValid eq 'invalid'}">
				    <div class="text-center">
				      <span><i class="fa fa-exclamation-circle"></i><spring:message code="i18n.register.no.invite" text="You need an invitation to register."/></span>
				    </div>
				  </c:if>
				  <c:if test="${isInviteTokenValid eq 'valid'}">
				  	<spring:message code="i18n.message.field.required" text="This field is required" var="requiredField"/>
					<spring:message code="i18n.message.term.use.invalid" text="You need to accept the terms in order to use Aqlli services" var="termsOfUse"/>
					<spring:message code="i18n.message.invalid.field" text="This field is invalid" var="invalidField"/>
					<h3 class="text-center">
						<spring:message code="i18n.message.createYourAccount" text="Create your Aqlli account"/>
					</h3>
					<form:form modelAttribute="user" method="POST" enctype="UTF-8"
						name="registerForm">
						<input type="hidden" name="submitted" value="true">
						<form:input type="hidden" path="location" name="location" value='<%=request.getHeader("X-AppEngine-City") %>'/>
						<div class="form-group">
							<label class="control-label" for="firstName">
							<spring:message code="i18n.firstName" text="First name"/>&nbsp;*</label>
							<form:input class="form-control" name="firstName"
								path="firstName" id="firstName" type="text"
								oninvalid="this.setCustomValidity('${requiredField}')" />
							<c:if test="${noFirstName}">
								<small class="text-danger"><spring:message code="i18n.message.field.required" text="This field is required"/></small>
							</c:if>
						</div>
						<div class="form-group">
							<label class="control-label" for="lastName">
							<spring:message code="i18n.lastName" text="Last name"/>&nbsp;*</label>
							<form:input class="form-control" name="lastName" id="lastName"
								path="lastName" type="text"
								oninvalid="this.setCustomValidity('${requiredField}')" />
							<c:if test="${noLastName}">
								<small class="text-danger"><spring:message code="i18n.message.field.required" text="This field is required"/></small>
							</c:if>
						</div>
						<div class="form-group" draggable="false">
							<label class="control-label" for="email">
							<spring:message code="i18n.email" text="E-mail"/>&nbsp;*</label>
							<form:input class="form-control" id="email" name="email"
								type="email" path="email"
								oninvalid="this.setCustomValidity('${invalidField}')" />
							<c:if test="${noEmail}">
								<small class="text-danger"><spring:message code="i18n.message.field.required" text="This field is required"/></small>
							    <br/>
							</c:if>
							<c:if test="${(badMail == 'invalidMail')}">
								<small class="text-danger">
								  <spring:message code="i18n.message.invalid.email" text="This field is invalid"/>
								</small>
							</c:if>
						</div>
						<div class="form-group">
							<label class="control-label" for="pass">
							<spring:message code="i18n.password" text="Password"/>&nbsp;*</label>
							<form:input class="form-control" id="pass1" type="password"
								name="pass" path="password"/>
							<c:if test="${noPassword}">
								<small class="text-danger"><spring:message code="i18n.message.field.required" text="This field is required"/></small>
							</c:if>
							<div class="regErr" id="pwdErr">
								<span id="pwdstrength" class="text-danger"></span>
								<span id="pwdlength" class="text-danger"></span>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label" for="matchingPassword">
							<spring:message code="i18n.confirmPass" text="Confirm password"/>&nbsp;*</label>
							<form:input class="form-control" type="password"
								name="matchingPassword" id="pass2" path="matchingPassword"
								oninvalid="this.setCustomValidity('${requiredField}')"
								onkeyup="checkPassImg(); return false;" />
							<span id="confirmMessageImg" class="confirmMessage"></span>
							<c:if test="${noMatchingPassword}">
								<small class="text-danger">
								  <spring:message code="i18n.message.confirmPass" text="You must confirm password"/>
								  <br/>
								</small>
							</c:if>
							<c:if test="${(passNotEq == 'passwordsMustMatch')}">
								<small class="text-danger">
								  <spring:message code="i18n.message.notEq.passwords" text="Entered passwords are different"/>
								</small>
							</c:if>
						</div>
						<div class="form-group">
							<label class="control-label" for="birthDay">
							<spring:message code="i18n.birthday" text="Date of birth"/>&nbsp;*</label>
							<a href="${pageContext.servletContext.contextPath}/user/registration/help"><i class="fa fa-question-circle fa-lg pull-right"></i></a>
							<form:select name="birthDay" path="birthDay" id="birthdayHolder" class="form-control">
								<option value="" selected>&nbsp;</option>
								<option value="01">1</option>
								<option value="02">2</option>
								<option value="03">3</option>
								<option value="04">4</option>
								<option value="05">5</option>
								<option value="06">6</option>
								<option value="07">7</option>
								<option value="08">8</option>
								<option value="09">9</option>
								<option value="10">10</option>
								<option value="11">11</option>
								<option value="12">12</option>
								<option value="13">13</option>
								<option value="14">14</option>
								<option value="15">15</option>
								<option value="16">16</option>
								<option value="17">17</option>
								<option value="18">18</option>
								<option value="19">19</option>
								<option value="20">20</option>
								<option value="21">21</option>
								<option value="22">22</option>
								<option value="23">23</option>
								<option value="24">24</option>
								<option value="25">25</option>
								<option value="26">26</option>
								<option value="27">27</option>
								<option value="28">28</option>
								<option value="29">29</option>
								<option value="30">30</option>
								<option value="31">31</option>
							</form:select>
							<form:select name="birthMonth" path="birthMonth"
								id="birthmonthHolder" class="form-control">
								<option value="01" selected>
									<spring:message code="i18n.january" text="January"/>
								</option>
								<option value="02">
									<spring:message code="i18n.february" text="February"/>
								</option>
								<option value="03">
									<spring:message code="i18n.march" text="March"/>
								</option>
								<option value="04">
									<spring:message code="i18n.april" text="April"/>
								</option>
								<option value="05">
									<spring:message code="i18n.may" text="May"/>
								</option>
								<option value="06">
									<spring:message code="i18n.june" text="June"/>
								</option>
								<option value="07">
									<spring:message code="i18n.july" text="July"/>
								</option>
								<option value="08">
									<spring:message code="i18n.august" text="Auguste"/>
								</option>
								<option value="09">
									<spring:message code="i18n.september" text="September"/>
								</option>
								<option value="10">
									<spring:message code="i18n.october" text="October"/>
								</option>
								<option value="11">
									<spring:message code="i18n.november" text="November"/>
								</option>
								<option value="12">
									<spring:message code="i18n.december" text="December"/>
								</option>
							</form:select>
							<form:select name="birthYear" path="birthYear"
								id="birthYearInput" class="form-control">
								<option value="2002">2002</option>
								<option value="2001">2001</option>
								<option value="2000">2000</option>
								<option value="1999">1999</option>
								<option value="1998">1998</option>
								<option value="1997">1997</option>
								<option value="1996">1996</option>
								<option value="1995">1995</option>
								<option value="1994">1994</option>
								<option value="1993">1993</option>
								<option value="1992">1992</option>
								<option value="1991">1991</option>
								<option value="1990" selected>1990</option>
								<option value="1989">1989</option>
								<option value="1988">1988</option>
								<option value="1987">1987</option>
								<option value="1986">1986</option>
								<option value="1985">1985</option>
								<option value="1984">1984</option>
								<option value="1983">1983</option>
								<option value="1982">1982</option>
								<option value="1981">1981</option>
								<option value="1980">1980</option>
								<option value="1979">1979</option>
								<option value="1978">1978</option>
								<option value="1977">1977</option>
								<option value="1976">1976</option>
								<option value="1975">1975</option>
								<option value="1974">1974</option>
								<option value="1973">1973</option>
								<option value="1972">1972</option>
								<option value="1971">1971</option>
								<option value="1970">1970</option>
								<option value="1969">1969</option>
								<option value="1968">1968</option>
								<option value="1967">1967</option>
								<option value="1966">1966</option>
								<option value="1965">1965</option>
								<option value="1964">1964</option>
								<option value="1963">1963</option>
								<option value="1962">1962</option>
								<option value="1961">1961</option>
								<option value="1960">1960</option>
								<option value="1959">1959</option>
								<option value="1958">1958</option>
								<option value="1957">1957</option>
								<option value="1956">1956</option>
								<option value="1955">1955</option>
								<option value="1954">1954</option>
								<option value="1953">1953</option>
								<option value="1952">1952</option>
								<option value="1951">1951</option>
								<option value="1950">1950</option>
								<option value="1949">1949</option>
								<option value="1948">1948</option>
								<option value="1947">1947</option>
								<option value="1946">1946</option>
								<option value="1945">1945</option>
								<option value="1944">1944</option>
								<option value="1943">1943</option>
								<option value="1942">1942</option>
								<option value="1941">1941</option>
								<option value="1940">1940</option>
								<option value="1939">1939</option>
								<option value="1938">1938</option>
								<option value="1937">1937</option>
								<option value="1936">1936</option>
								<option value="1935">1935</option>
								<option value="1934">1934</option>
								<option value="1933">1933</option>
								<option value="1932">1932</option>
								<option value="1931">1931</option>
								<option value="1930">1930</option>
								<option value="1929">1929</option>
								<option value="1928">1928</option>
								<option value="1927">1927</option>
								<option value="1926">1926</option>
								<option value="1925">1925</option>
								<option value="1924">1924</option>
								<option value="1923">1923</option>
								<option value="1922">1922</option>
								<option value="1921">1921</option>
								<option value="1920">1920</option>
								<option value="1919">1919</option>
								<option value="1918">1918</option>
								<option value="1917">1917</option>
								<option value="1916">1916</option>
								<option value="1915">1915</option>
								<option value="1914">1914</option>
								<option value="1913">1913</option>
								<option value="1912">1912</option>
								<option value="1911">1911</option>
								<option value="1910">1910</option>
								<option value="1909">1909</option>
								<option value="1908">1908</option>
								<option value="1907">1907</option>
								<option value="1906">1906</option>
								<option value="1905">1905</option>
								<option value="1904">1904</option>
								<option value="1903">1903</option>
								<option value="1902">1902</option>
								<option value="1901">1901</option>
								<option value="1900">1900</option>
							</form:select>
							<span>
								<c:if test="${dateEmpty}">
									<small class="text-danger">
									  <spring:message code="i18n.message.invalid.field" text="This field is invalid"/>
									</small>
								</c:if>
							</span>
						</div>
						<div class="form-group" draggable="false">
							<label class="control-label" for="gender">
							<spring:message code="i18n.gender" text="Gender"/></label>
							<form:select class="form-control" id="genderRegistration" name="gender"
							 path="gender">
							    <option value="M"><spring:message code="i18n.male" text="Male"/></option>
								<option value="F" selected><spring:message code="i18n.female" text="Female"/></option>
								<option value="O"><spring:message code="i18n.noGender" text="Other"/></option>	
							</form:select>
						</div>
						<div class="checkbox">
							<label id="useTermsHolder"> <form:checkbox
									id="userTermsCBox" path="useTerms" name="useTerms"
									oninvalid="this.setCustomValidity('${termsOfUse}')"
									value="agree" />
									<spring:message code="i18n.terms.of.use" text="I accept the terms of use" arguments='${pageContext.servletContext.contextPath}/legal/terms,${pageContext.servletContext.contextPath}/legal/privacy,${pageContext.servletContext.contextPath}/legal/cookies' htmlEscape="false"/>
									 
							</label> <span><br> <c:if test="${noCheckUseTerm}">
									<small class="text-danger">
									  <spring:message code="i18n.message.term.use.invalid" text="You need to accept the terms in order to use Aqlli services"/>
									</small>
								</c:if> </span>
						</div>
						<div id="bottombtns">
						<button type="submit" class="btn btn-primary">
							<spring:message code="i18n.submit" text="Submit"/>
						</button>
						<span class="pull-right">
						  <a
							href="${pageContext.servletContext.contextPath}/login">
							<spring:message code="i18n.i.have.an.account" text="I have an account"/>
						  </a>
						</span>
						</div>
					</form:form>
				  </c:if>	
				</div>
			</div>
		</div>
	</div>
  </div>
  <%@ include file="footer.jsp"%>  
</body>
</html>