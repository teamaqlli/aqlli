<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="com.aqlli.main.client.model.UserDetails"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<sec:authorize var="isloggedIn" access="isAuthenticated()" />
<%@ include file="/WEB-INF/pages/include/htmlLangTag.jsp"%>
<html lang="${lng}">
<head>
<meta charset="utf-8">
<title>${userFromUrl.fullName}</title>
<link rel="apple-touch-icon"
	href="http://static.aqlli.com/icons/touch-icon-iphone.png">
<link rel="apple-touch-icon" sizes="76x76"
	href="http://static.aqlli.com/icons/touch-icon-ipad.png">
<link rel="apple-touch-icon" sizes="120x120"
	href="http://static.aqlli.com/icons/touch-icon-iphone-retina.png">
<link rel="apple-touch-icon" sizes="152x152"
	href="http://static.aqlli.com/icons/touch-icon-ipad-retina.png">
<link href="http://static.aqlli.com/scripts/css/footer.css" media="all"
	rel="stylesheet">
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta content="follow,index" name="robots">
<meta name="robots" content="NOODP">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="description"
	content="<spring:message code='i18n.meta.perso.content' text='Aqlli is a social media that helps people to keep up with followers and to share content with them.'/>">
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />

<!-- fb -->
<meta property="og:title" content="${userFromUrl.fullName} | Aqlli" />
<meta property="og:type" content="website" />
<meta property="og:image" content="${userFromUrl.defaultUserAvatarUrl}" />
<meta property="og:url"
	content="${requestScope['javax.servlet.forward.request_uri']}" />
<meta property="og:description"
	content="<spring:message code='i18n.meta.perso.content' text='Aqlli is a social media that helps people to keep up with followers and to share content with them.'/>" />
<meta property="fb:admins" content="100005426680123" />
<meta property="og:site_name" content="Aqlli" />
<!-- tw -->
<meta name="twitter:card" content="summary">
<meta name="twitter:url"
	content="${requestScope['javax.servlet.forward.request_uri']}" />
<meta name="twitter:title" content="${userFromUrl.fullName} | Aqlli" />
<meta name="twitter:description"
	content="<spring:message code='i18n.meta.perso.content' text='Aqlli is a social media that helps people to keep up with followers and to share content with them.'/>" />
<meta name="twitter:image" content="${userFromUrl.defaultUserAvatarUrl}" />
<meta name="twitter:site" content="@aqlli_fr" />
<meta name="twitter:creator" content="Arix OWAYE" />
<!-- g+ -->
<meta itemprop="name" content="${userFromUrl.fullName} | Aqlli" />
<meta itemprop="description"
	content="<spring:message code='i18n.meta.perso.content' text='Aqlli is a social media that helps people to keep up with followers and to share content with them.'/>" />
<meta itemprop="audience" content="Web users" />
<meta itemprop="image" content="${userFromUrl.defaultUserAvatarUrl}" />
<meta itemprop="alternativeHeadline" content="Social Media | Aqlli" />
<link rel="publisher" href="//plus.google.com/b/106910419300047477169" />
<link rel="icon" sizes="any" mask
	href="http://static.aqlli.com/icons/favicon.svg">
<link rel="canonical"
	href="${requestScope['javax.servlet.forward.request_uri']}">
<link rel="shortcut icon"
	href="http://static.aqlli.com/icons/favicon.ico" type="image/x-icon" />
<link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"
	media="all" rel="stylesheet">
<link href="http://static.aqlli.com/scripts/css/teleport-autocomplete.min.css" />
<link rel="stylesheet" type="text/css"
	href="http://static.aqlli.com/scripts/css/font-awesome-animation.min.css"
	media="all">
<link rel="stylesheet" type="text/css" media="all"
	href="http://static.aqlli.com/scripts/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="http://static.aqlli.com/scripts/css/leftSideMenu.css" media="all">
<link rel="stylesheet" type="text/css"
	href="http://static.aqlli.com/scripts/css/jquery.emojipicker.css"
	media="all">
<link rel="stylesheet" type="text/css"
	href="http://static.aqlli.com/scripts/css/jquery.emojipicker.tw.css"
	media="all">
<link href="http://static.aqlli.com/scripts/css/styles.css" media="all"
	rel="stylesheet">
<script src="http://static.aqlli.com/scripts/js/classie.js"></script>
<script src="/aqlli/js/lib/teleport-autocomplete.js"></script>
<script data-main="/aqlli/js/app" src="/aqlli/js/require.js"></script>

<style>
.input-file {
	position: relative;
}
.input-file .input-group-addon {
	border: 0px;
	padding: 0px;
}
.input-file .input-group-addon .btn {
	border-radius: 0 4px 4px 0
}
.input-file .input-group-addon input {
	cursor: pointer;
	position: absolute;
	width: 72px;
	z-index: 2;
	top: 0;
	right: 0;
	filter: alpha(opacity =         0);
	-ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
	opacity: 0;
	background-color: transparent;
	color: transparent;
}
#uploader {
	margin-top: 2px;
	margin-bottom: 2px;
}
#uhp {
	margin-left: 60px;
	margin-right: 60px;
}
#btnsm div ul {
	margin-top: 15px;
}
.modalbody {
	padding: 20px;
}

.gen .input-group span.input-group-addon {
	min-width: 100px;
}

.gen .input-group,.gen .uinfos {
	margin-bottom: 6px;
}

div input[type=submit]#submitPic {
	margin-top: 2px;
}

#user-advert-holder>span {
	position: relative;
	width: 175px;
	max-width: 175px;
	font-size: 110%;
	background-color: rgba(0, 0, 0, 0.06);
	font-weight: 600;
}

#user-advert-holder>span i {
	margin-right: 2px;
}

.dropdown-menu.more {
	margin-left: -110px;
	margin-top: 9px ! important;
}

.dropdown-menu.more li {
	width: 100%;
}

.dropdown-menu.more li a {
	text-align: left ! important;
}

.usrmoreshare a span {
	margin-right: 5px;
}

#sfb i,h-u-fb span i {
	color: #3a5795;
}

#stw i,#h-u-tw span i {
	color: #55acee;
}

#sgp i,h-u-gp span i {
	color: #d62d20;
}

#sfb i:hover {
	color: #085083;
}

#stw i:hover {
	color: #085083;
}

#sgp i:hover {
	color: #085083;
}
#usrFlStatus{
margin-bottom:10px;
}
input[type="file"]{
  max-width: 100%;
}
span.fa-stack.fa-2x:hover{
  color:red;
  -webkit-transform: scale(1.3,1.3);
    -moz-transform: scale(1.3,1.3);
  -ms-transform: scale(1.3,1.3);
    -o-transform: scale(1.3,1.3);
  transform: scale(1.3,1.3);
  -webkit-transition: -webkit-transform .7s;
    -moz-transition: -moz-transform .7s;
    -ms-transition: -ms-transform .7s;
    -o-transition: -o-transform .7s;
    transition: transform .7s;
}
div#files div p img{
    max-height: 80px;
    max-width: 80px;
    margin-bottom: 10px;
    margin-top: 5px;
    -webkit-box-shadow: -3px 5px 21px -2px rgba(0,0,0,0.75);
    -moz-box-shadow: -3px 5px 21px -2px rgba(0,0,0,0.75);
    box-shadow: -3px 5px 21px -2px rgba(0,0,0,0.75);
}
.tp-autocomplete ul{
  list-style-type: none;
}
.tp-autocomplete ul li:hover{
  background-color: rgba(0, 0, 0, 0.05);
}
</style>
</head>
<body style="overflow-x: hidden; overflow-y: auto;">
	<c:if test="${not isloggedIn}">
		<c:set var="anonymousid" value="anonymous"></c:set>
		<c:set var="classLeftLayout" value="col-sm-9"/>
	</c:if>
	<c:if test="${isloggedIn}">
		<c:set var="anonymousid" value="userinfsholder"></c:set>
		<c:set var="classLeftLayout" value="col-sm-3"/>
	</c:if>
	<c:if test="${(empty userFromUrl.facebookFollowME)}"
		var="noFacebookFollowME" />
	<c:if test="${(empty userFromUrl.twitterFollowMe)}"
		var="noTwitterFollowMe" />
	<c:if test="${(empty userFromUrl.googlePlusFollowMe)}"
		var="noGoogleFollowMe" />
	<c:if test="${user.uniqueID eq userFromUrl.uniqueID}"
		var="isCurrUserTheAuthUser" scope="request" />
	<c:if test="${(empty userFromUrl.website)}" var="noWebsite" />
	<c:if test="${(empty userFromUrl.location)}" var="noLocation" />
	<c:if test="${(empty userFromUrl.jobPosition)}" var="noJob" />
	<c:if test="${(empty userFromUrl.company)}" var="noCompany" />
	<c:choose>
	  <c:when test="${followStatus eq 'follow'}">
	    <c:set scope="session" var="fstatusclass" value="follow"/>
	    <c:set scope="session" var="followstatusicon" value="fa-user-plus"/>
	    <spring:message code="i18n.follow.user" var="titlefollowstatus" text="Follow"/>
	  </c:when>
	  <c:when test="${followStatus eq 'following'}">
	    <c:set scope="session" var="fstatusclass" value="following"/>
	    <c:set scope="session" var="followstatusicon" value="fa-user-times"/>
	    <spring:message code='i18n.unFollow.user' text='Remove' var="titlefollowstatus"/>
	  </c:when>
	</c:choose>
	<c:if test="${isUserAReader}">
	  <spring:message code="i18n.is.user.a.reader" text="Reads you" var="isareaderstatus"/>
	</c:if>
	<div id="fb-root"></div>
	<%@ include file="/WEB-INF/pages/include/noScript.jsp"%>
	<div id="wrapper">
		<div class="container-fluid" id="maincontentholder">
			<%@ include file="topNavBar.jsp"%>
			<%@ include file="leftSideMenu.jsp"%>
			<jsp:include page="/WEB-INF/pages/include/publicProfileHeader.jsp" />
			<jsp:include page="/WEB-INF/pages/include/linksmenuholder.jsp" />
			<div class="row-fluid">
				<jsp:include page="/WEB-INF/pages/include/updateProfilePic.jsp" />
				<jsp:include page="/WEB-INF/pages/include/updateProfileInfos.jsp" />
				<div class="row" id="postsholderrow">
					<div class="${classLeftLayout} well">
						<div class="w">
							<c:if test="${isCurrUserTheAuthUser}">
								<div class="text-center following" data-log="true" id="usrFlStatus"><i class="btn fa btn-primary fa-user-plus fa-lg disabled" title='<spring:message code="i18n.follow.user" text="Follow"/>'>&nbsp;<span><spring:message code="i18n.follow.user" text="Follow"/></span></i></div>
							</c:if>
							<c:if test="${not isloggedIn}">
								<div class="text-center following" data-log="false" id="usrFlStatus"><i class="btn fa btn-primary fa-user-plus fa-lg disabled" title='<spring:message code="i18n.follow.user" text="Follow"/>'>&nbsp;<span><spring:message code="i18n.follow.user" text="Follow"/></span></i></div>
							</c:if>
							<c:if test="${(not isCurrUserTheAuthUser) and ( isloggedIn)}">
								<div class="flcontainer text-center ${fstatusclass}" data-log="true" data-name="${data_name}" data-user-id="${data_user_id}" data-user-name="${data_user_name}"  data-follow-status="${fstatusclass}" id="usrFlStatus" data-follow-tag="<spring:message code="i18n.follow.user" text="Follow"/>" data-unfollow-tag="<spring:message  code='i18n.unFollow.user' text='Remove'/>">
								  <i class="btn btn-primary fa <c:out value="${followstatusicon}"/> fa-lg" title="<c:out value="${titlefollowstatus}"/>">&nbsp;<span><c:out value="${titlefollowstatus}"/></span></i>
								  <span class="isreader">${isareaderstatus}</span>
								</div>
							</c:if>
							<section id="${anonymousid}">
								<c:if test="${not noTwitterFollowMe}">
									<div id="h-u-tw" class="text-center">
										<span class="h-u-icon inb" title="Twitter Follow"><i
											class="fa fa-twitter fa-lg"></i></span> <span class="butn"> <%@ include
												file="twitterFollowBtn.jsp"%>
										</span>
									</div>
								</c:if>
								<c:if test="${not noFacebookFollowME}">
									<div id="h-u-fb" class="text-center">
										<span class="h-u-icon inb" title="Facebook Follow"><i
											class="fa fa-facebook-square fa-lg"></i></span> <span class="butn">
											<%@ include file="facebookFollowBtn.jsp"%>
										</span>
									</div>
								</c:if>
								<c:if test="${not noGoogleFollowMe}">
									<div id="h-u-gp" class="text-center">
										<span class="h-u-icon inb" title="Google Follow"><i
											class="fa fa-google-plus-square fa-lg"></i></span> <span
											class="butn"> <%@ include
												file="googlePlusFollowBtn.jsp"%>
										</span>
									</div>
								</c:if>
								<c:if test="${not noLocation }">
									<div class="usergenfields text-center" id="h-u-hometown">
										<span class="h-u-icon inb"><i
											class="fa fa-building-o fa-lg"></i><span><spring:message
													code="i18n.town" text="City" /></span></span>
											<span class="butn usergenfields">${currentlocation}</span>
									</div>
								</c:if>
								<c:if test="${not noWebsite}">
									<div class="usergenfields text-center" id="h-u-wbs">
										<span class="h-u-icon inb" title="Website"><i
											class="fa fa-globe fa-lg"></i><span><spring:message
													code="i18n.user.update.infos.website" text="Website" /></span></span> <span
											class="butn usergenfields"> <a
											href="${userFromUrl.website}" target="_blank"
											title="${userFromUrl.fullName} <spring:message
													code='i18n.user.update.infos.website' text='Website' />">${userFromUrl.website}</a>
										</span>
									</div>
								</c:if>
								<c:if test="${not noJob}">
									<div class="usergenfields text-center" id="h-u-w">
										<span class="h-u-icon inb"><i
											class="fa fa-briefcase fa-lg"></i><span><spring:message
													code="i18n.work" text="Work" /></span></span><span
											class="butn usergenfields" id="jobposition">${userFromUrl.jobPosition}</span>
									</div>
								</c:if>
								<c:if test="${not noCompany}">
									<div class="usergenfields text-center" id="h-u-w">
										<span class="h-u-icon inb"><i
											class="fa fa-briefcase fa-lg"></i><span><spring:message
													code="i18n.company" text="company" /></span></span><span
											class="butn usergenfields" id="jobCompany">${userFromUrl.company}</span>
									</div>
								</c:if>
								<c:if test="${true}">
									<div class="usergenfields text-center" id="h-u-following">
										<span class="h-u-icon inb"><i class="fa fa-users fa-lg"></i><span><spring:message
													code="i18n.following" text="Following" /></span></span><span
											class="butn usergenfields" id="following">${nbFollowing}</span>
									</div>
								</c:if>
								<c:if test="${true}">
									<div class="usergenfields text-center" id="h-u-followers">
										<span class="h-u-icon inb"><i class="fa fa-users fa-lg"></i><span><spring:message
													code="i18n.followers" text="Followers" /></span></span> <span
											class="butn usergenfields" id="followers">${nbFollowers}</span>
									</div>
								</c:if>
								<c:if test="${(not empty userSettings.userDescription)}">
									<div id="h-u-w" class="userDescription text-center">
										<span class="h-u-icon inb"><i
											class="fa fa-sticky-note fa-lg"></i><span><spring:message
													code="i18n.description" text="Description" /></span></span><span
											class="break" id="userDescription"><span id="usdesc">${userSettings.userDescription}</span></span>
									</div>
								</c:if>
							</section>
						</div>
					</div>
					<c:if test="${isloggedIn}">
						<div class="col-sm-6">
							<c:import url="/WEB-INF/pages/include/postUpdateForm.jsp" />
							<div id="wallposts">
								<div id="initL" class="text-center"></div>
								<div class="timeline" id="uwall">
									<ul class="list-unstyled">
									</ul>
								</div>
								<div id="lastPostsLoader" class="text-center"></div>
							</div>
						</div>
					</c:if>
					<div class="col-sm-3 well break" id="persoads">
							<div class="text-center">
								<span><spring:message code="i18n.sponsored"
										text="Sponsored" /> &nbsp;<i class="fa fa-bullhorn"></i></span>
								<div id="ad1">Publicité 1</div>
								<div id="ad2">Publicité 2</div>
							</div>
					</div>
				</div>

			</div>

		</div>
	</div>
	<div id="reportBugMod" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"><spring:message code="i18n.report.bug" text="Report bug"/></h4>
          </div>
          <div class="modal-body">
            <form class="form-horizontal" role="form">
              <div class="form-group">
                <div class="col-sm-2">
                  <label for="inputTitleReportBug" class="control-label"><spring:message code="i18n.title" text="Title"/></label>
                </div>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="inputTitleReportBug" placeholder='<spring:message code="i18n.title" text="Title"/>'>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-2">
                  <label for="inputReportBugDesc" class="control-label"><spring:message code="i18n.description" text="Description"/></label>
                </div>
                <div class="col-sm-10">
                  <textarea type="text" class="form-control" id="inputReportBugDesc" placeholder='<spring:message code="i18n.description" text="Description"/>'
                  style="resize: vertical; min-height:47px;" maxlength="400"></textarea>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <span style="color: grey;"><spring:message code="i18n.report.bug.msg" text="Please use this form to report a bug only. Other purpose will not be treated."/><br/></span>
                  <button id="reportbugv" class="btn btn-primary"><spring:message code="i18n.send" text="Send"/></button>
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <a class="btn btn-primary" data-dismiss="modal"><spring:message code="i18n.close" text="Close"/></a>
          </div>
        </div>
      </div>
    </div>
	<jsp:include page="footer.jsp" />
</body>
</html>