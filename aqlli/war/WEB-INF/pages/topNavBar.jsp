<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<sec:authorize var="isloggedIn" access="isAuthenticated()" />
<c:choose>
    <c:when test="${isloggedIn}">
    	<c:set var="homeUrl"
		value="/home" />
		<c:set var="usrname" value="${user.fullName}"/>
    </c:when>
    <c:otherwise>
        <c:set var="homeUrl"
		value="/" />
		<c:set var="usrname" value="Profile"/>
    </c:otherwise>
</c:choose>
<div class="navbar navbar-fixed-top navbar-inverse">
	<div class="container-fluid top">
		<div class="navbar-header">
		      <button type="button" class="navbar-toggle clollapsed"
					data-target=".navbar-collapse" data-toggle="collapse"
					aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
		    <a class="navbar-brand" href="${homeUrl}"><img style="width:40px;"
				src="http://static.aqlli.com/icons/drawing_white_on_blue_rounded_60_60.png"
				id="aqlli-logo-nav" alt="Aqlli"
				><span id="navbar-brand-text"><spring:message
					code="i18n.aqlli" text="Aqlli"/></span></a>
			<form class="navbar-form pull-left" role="search">
            <div class="input-group">
               <input type="text" class="form-control search-box" placeholder="<spring:message code="i18n.search" text="Search"/>">
               <div class="input-group-btn">
                  <button id="search-now" type="submit" class="btn btn-primary" style="height:34px;"><span style="color:white;" class="fa fa-search"></span></button>
               </div>
               <input type="hidden"
							name="${_csrf.parameterName}" value="${_csrf.token}" />
			   
            </div>
         </form>
		</div>
		
		<c:if test="${isloggedIn}">
		  <div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="nav navbar-nav navbar-right">
					<li class="profileNav-pic" draggable="false">
					<a
						title="${usrname}"
						href="/users/${user.uniqueID}"
						id="profilepic"><img src="${user.defaultUserAvatarUrl}"
							class="img-circle profile-icon ">
							<button type="button" class="btn-link btn-xs navbar-link"
								draggable="false">
							</button> </a>
					</li>
					<li class="active"><a
						href="/home" title="<spring:message code='i18n.home'/>"><i
							class="fa fa-home"></i>&nbsp;<spring:message code="i18n.home" text="Home"/></a></li>
					<li>
							<a id="aqnotifications" title="<spring:message code='i18n.notifications' text='Notifications'/>"><i class="fa fa-bell"></i>&nbsp<spring:message code="i18n.notifications" text="Notifications"/></a>		
  							<span class="badge badge-notify hidden red">0</span>
						
					</li>
					<li><a href="/invite" title="<spring:message
								code='i18n.invite' text='Invite'/>"> <i class="fa fa-envelope"></i>&nbsp;<spring:message
								code="i18n.invite" text="Invite"/></a></li>
				</ul>
			</div>	
		</c:if>
		<c:if test="${usrname eq 'Profile'}">
		  <div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="nav navbar-nav navbar-right">
					<li class="active"><a
						href="/login"><i class="fa fa-sign-in"></i>&nbsp;<spring:message code="i18n.signin" text="Login"/></a></li>
					<li><a href="/user/registration"><i class="fa fa-pencil-square-o"></i>&nbsp;<spring:message
								code="i18n.sign.up" text="Sign Up"/></a></li>
				</ul>
			</div>	
		</c:if>
		</div>
		<div id="notificationContainer" class="push-right">
	<div id="notificationTitle"><spring:message code='i18n.notifications' text='Notifications'/></div>
	<div id="notificationsBody" class="notifications">
		<div class="empty text-center"><i class="fa fa-bell-slash fa-5x" aria-hidden="true"></i></div>
	</div>
	<div id="notificationFooter"><a href="#" class="btn btn-primary"><spring:message code='i18n.info.see.more' text="See more"/></a></div>
</div>
</div>

<div id="searchresult"></div>