<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page session="true"%>
<%@ include file="/WEB-INF/pages/include/htmlLangTag.jsp"%>
<html lang="${lng}">
<head>
<meta charset="utf-8">
<title><spring:message code="i18n.settings" text="Settings" /> | Aqlli</title>
<link rel="apple-touch-icon" href="http://static.aqlli.com/icons/touch-icon-iphone.png">
<link rel="apple-touch-icon" sizes="76x76" href="http://static.aqlli.com/icons/touch-icon-ipad.png">
<link rel="apple-touch-icon" sizes="120x120" href="http://static.aqlli.com/icons/touch-icon-iphone-retina.png">
<link rel="apple-touch-icon" sizes="152x152" href="http://static.aqlli.com/icons/touch-icon-ipad-retina.png">
<link href="http://static.aqlli.com/scripts/css/footer.css" media="all" rel="stylesheet">
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta content="nofollow,noindex" name="robots">
<meta name="robots" content="NOODP">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="author" content="Arix Owaye">

<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
<link rel="publisher" href="//plus.google.com/b/106910419300047477169" />
<link rel="icon" sizes="any" mask
	href="http://static.aqlli.com/icons/favicon.svg">
<link rel="canonical"
	href="${requestScope['javax.servlet.forward.request_uri']}">
<link rel="shortcut icon"
	href="http://static.aqlli.com/icons/favicon.ico" type="image/x-icon" />
<link rel="stylesheet" type="text/css" media="all"
	href="http://static.aqlli.com/scripts/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="http://static.aqlli.com/scripts/css/leftSideMenu.css" media="all">
<link href="http://static.aqlli.com/scripts/css/styles.css" media="all"
	rel="stylesheet">
<link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"
	media="all" rel="stylesheet">
<script src="http://static.aqlli.com/scripts/js/classie.js"></script>
<script data-main="/aqlli/js/app" src="/aqlli/js/require.js"></script>
<style>
.setupHolder {
	margin-top: 10px;
}

.setupHolder div {
	margin-bottom: 10px;
}

#uPass {
	font-size: 25px;
}

#updatePass {
	display: none;
}

#btnValHolder button.btn {
	margin-right: 5px;
}

#bday select.form-control {
	float: left;
	width: 30%;
	margin-right: 2px;
}

#bday {
	width: 100%;
	margin-left: -1px;
}
#langPickerHolder a i{
color:white;
}
.hf{
 min-width:80px;
}

</style>
</head>
<body>
		<div id="fb-root"></div>
	<%@ include file="/WEB-INF/pages/include/noScript.jsp"%>
	<div id="wrapper">
		<%@ include file="topNavBar.jsp"%>
		<%@ include file="leftSideMenu.jsp"%>
		<div class="section" id="maincontentholder">
			<div class="container">
				<div class="row">
					<div class="col-md-12 setupHolder">
						<div class="gen col-md-12 well">
							<h4 class="text-center"><spring:message code="i18n.general" text="General"/></h4>
							<hr>
							<div class="input-group">
								<span class="input-group-addon hf"><spring:message code="i18n.lastName" text="Lastname"/></span> <input type="text"
									name="field_name" class="form-control"
									placeholder="<c:out value='${user.lastName}'/>" id="setNm" value="<c:out value='${user.lastName}'/>">
							</div>
							<div class="input-group">
								<span class="input-group-addon hf"><spring:message code="i18n.firstName" text="Firstname"/></span> <input type="text"
									name="field_name" class="form-control"
									placeholder="<c:out value='${user.firstName}' />" id="setPr" value="<c:out value='${user.firstName}' />">
							</div>
							<div class="input-group">
								<span class="input-group-addon hf"><spring:message code="i18n.primary.email" text="Primary"/></span> <span
									name="field_name" class="form-control-static" id="setEm">&nbsp;<c:out value='${user.email}' /></span>
							</div>
							<div class="input-group">
								<span class="input-group-addon">ID</span> <span
									name="field_name" class="form-control-static" id="setID">&nbsp;<c:out value='${user.uniqueID}' /></span>
							</div>
							<div class="input-group">
								<span class="i"><spring:message code="i18n.birthday" text="Date of birth"/></span>
								<div id="bday">
									<%@include file="/WEB-INF/pages/include/dateOfBirthSelect.jsp"%>
								</div>

							</div>
							<div class="input-group">
								<button id="sgensave" class="btn btn-primary btn-xs"><spring:message code="i18n.save" text="Save"/></button>
							</div>
						</div>

						<div class="gen col-md-12 well">
							<h4 class="text-center"><spring:message  text="Security" code="i18n.security"/></h4>
							<hr>
							<div class="input-group">
								<span class="input-group-addon hf"><spring:message code="i18n.password" text="Mot de passe"/></span> <input
									type="text" name="field_name" class="form-control"
									placeholder="***********************" id="uPass" size="100" disabled="">
								<span class="input-group-addon" id="showUpdatePass"
									onclick="showUpdPassForm();"> <a><i
										class="fa fa-wrench" style="color: red;"></i></a>
								</span>
							</div>
							<div id="updatePass">
								<div class="input-group">
									<span class="input-group-addon hf"><spring:message code="i18n.old.pass" text="Old password"/> &nbsp;</span> <input
										type="password" name="uPass1" class="form-control"
										id="np1">
								</div>
								<div class="input-group">
									<span class="input-group-addon hf"><spring:message code="i18n.new.pass" text="New Password"/></span> <input
										type="password" class="form-control" id="np2"
										name="uPass2">
								</div>
								<div class="input-group" id="btnValHolder">
									<button class="btn btn-primary btn-xs" id="savePassBtn"><spring:message code="i18n.save" text="Save"/></button>
									<button class="btn btn-primary btn-xs" id="btnPassCancel"
										onclick="cancelUpdPassForm();"><spring:message code="i18n.cancel" text="Cancel"/></button>
									<span></span>
								</div>
							</div>
						</div>
						<div class="gen col-md-12 well">
							<h4 class="text-center"><spring:message code="i18n.privacy" text="Privacy"/></h4>
							<hr>
							<div class="input-group">
								<span><spring:message code="i18n.privacy.who.can.see" text="Who can see my profile
									and post"/>:</span> <span name="field_name" class="form-control"
									id="seestuff"><spring:message code="i18n.privacy.seestuff" text="Authenticated users"/></span>
							</div>
							<div class="input-group">
								<span><spring:message code="i18n.privacy.who.can.see.msg" text="For now only Aqlli users can interact with you. We are working to refine the confidentiality of your personal page and allow you to share in a secure way all your posts with non Aqlli users."/></span>
							</div>
						</div>
						<div class="gen col-md-12 well">
							<h4 class="text-center"><spring:message code="i18n.account" text="Account"/></h4>
							<hr>
							<div class="input-group">
								<span class="input-group-addon hf"><spring:message code="i18n.account.type" text="Type"/> &nbsp;&nbsp;</span> <input
									type="text" name="field_name" class="form-control" id="accT"
									disabled="" placeholder="${accountType}">
							</div>
							<hr>
							<ul class="list-unstyled">
								<li id="deflL"><spring:message code="i18n.default.lang" text="Default language:"/></li>
								<li><%@include
										file="/WEB-INF/pages/include/setLangAuthenticated.jsp"%>
								</li>
							</ul>
							<hr>
							<div class="input-group">
								<button class="btn btn-xs btn-primary" data-toggle="modal"
									data-target="#modalDelAcc"><spring:message code="i18n.suppress.account" text="Delete Account"/></button>
									<span></span>
							</div>
						</div>
						<!--  -->
						<div class="col-md-12 well">
							<h4 class="text-center"><spring:message code="i18n.notifications" text="Notifications"/></h4>
							<hr>
							<div class="input-group">
								<span class="input-group-addon hf"><a id="notifDesktop" class="list-group-item"><i class="fa-lg fa fa-toggle-off fa-fw"
			id="switch"></i>&nbsp;<spring:message code="i18n.notifications.desktop" text="Desktop notifications"/></a></span>
							</div>
							<div class="input-group">
								<span class="input-group-addon hf"><a id="notifsEmails" class="list-group-item"><i class="fa-lg fa fa-toggle-on fa-fw"
			id="switch"></i>&nbsp;<spring:message code="i18n.notifications.emails" text="Emails"/></a></span>
							</div>
						</div>
						<!--  -->
					</div>
					<div class="fade modal text-left" id="modalDelAcc">
							<div class="modal-dialog modal-lg">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal"
											aria-hidden="true">×</button>
										<h4 class="modal-title"><spring:message code="i18n.account.del.title"
											text="Delete account | Thank you for sweet moments" htmlEscape="false"/> &nbsp; <i
												class="fa fa-heart-o"></i>
										</h4>
									</div>
									<div class="modal-body">
									  <spring:message code="i18n.account.del.msg"
											text="Do you want to permanently delete your account? We do not keep your data, consider backing up first." htmlEscape="false"/>
									</div>
									<div class="modal-footer">
										<a class="btn btn-primary" data-dismiss="modal"><spring:message code="i18n.no" text="No"/></a> <input
											type="hidden" value="<c:out value='${user.uniqueID}'/>" id="uuid" /> <a
											class="btn btn-primary" id="uuidd"><spring:message code="i18n.yes" text="Yes"/></a>
									</div>
								</div>
							</div>
						</div>
				</div>
			</div>
		</div>
		<%@ include file="footer.jsp"%>
	</div>
	<script>
	function initBday() {
		var nday = document.getElementById('birthdayHolder');
		var nyear = document.getElementById('birthYearHolder');
		var nmonth = document.getElementById("birthmonthHolder");
		var opts = nmonth.options;
		if (nday) {
			nday.value   = ${user.birthDay};
		}
		if (nyear) {
			nyear.value  = ${user.birthYear};
		}
		for( var opt, j = 0; opt = opts[j]; j++){
			if( j == parseInt("${user.birthMonth}")){
			  nmonth.selectedIndex = j - 1;
			  break;
			}
		}
		return false;
	}
	// Create a new anonymous function, to use as a wrapper
	(function(){
		initBday();
	// Close off the anonymous function and execute it
	})();
	//settings page - hide show password update form
	function showUpdPassForm() {
		var myDiv = document.getElementById("updatePass");
		myDiv.style.display = "block";
	}
	function cancelUpdPassForm() {
		document.getElementById("updatePass").style.display = "none";
	}
	
	</script>
</body>
</html>