<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page session="true"%>
<html>
<head>
<meta charset="utf-8">
<title><fmt:message key='home' /> - Aqlli</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="<fmt:message key='metaHomeContent'/>">
<meta name="author" content="Arix .O">
<link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"
	media="all" rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="http://static.aqlli.com/scripts/css/leftSideMenu.css" media="all"
	rel="stylesheet">
<link rel="stylesheet" type="text/css" media="all"
	href="http://static.aqlli.com/scripts/css/bootstrap.css">
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.1/modernizr.min.js"></script>
<link href="http://static.aqlli.com/scripts/css/styles.css" media="all"
	rel="stylesheet">
</head>

<body style="overflow-x: hidden; overflow-y: auto;"
	class="cbp-spmenu-push">
	<!-- left navbar -->
	<%@ include file="leftSideMenu.jsp"%>
	<!-- Books Carousel for Home page-->
	<!-- top navbar -->
	<%@ include file="topNavBar.jsp"%>
	<div id="wrapper">
		<div class="section" id="maincontentholder">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="jumbotron">
							<h1>
								<fmt:message key="MesgHomeGoingOnAqlli" />
							</h1>
							<p>
								<fmt:message key="DeleteAccount" />
							</p>
							<form:form style="width:50%" modelAttribute="user"
								action="${pageContext.request.contextPath}/aqlli/home"
								method="POST">
								<div class="form-group">
									<form:label class="control-label" for="exampleInputEmail1"
										path="email">Email address</form:label>
									<form:input class="form-control" id="exampleInputEmail1"
										placeholder="Enter email" type="email" name="email"
										path="email" />
								</div>
								<form:button type="submit" class="btn btn-default">
									<fmt:message key="submit" />
								</form:button>
							</form:form>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4 well">Col 1</div>
					<div class="col-md-4 well">Content</div>
					<div class="col-md-4 well">Col3</div>
				</div>
			</div>

		</div>
		<%@ include
							file="footer.jsp"%>
	</div>
	<!-- imports script; mettre jquery avant bootstrap -->
	<script src="http://static.aqlli.com/scripts/js/jquery.js"></script>
	<script src="http://static.aqlli.com/scripts/js/bootstrap.js"></script>
	<script src="http://static.aqlli.com/scripts/js/main.js"></script>
	<script src="http://static.aqlli.com/scripts/js/classie.js"></script>
	<script src="http://static.aqlli.com/scripts/js/leftSideMenu.js"></script>
</body>

</html>