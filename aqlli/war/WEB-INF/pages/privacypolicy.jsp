<%@ page contentType="text/html; charset=UTF-8"%>
<%@page session="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ include file="/WEB-INF/pages/include/htmlLangTag.jsp"%>
<!DOCTYPE html>
<html lang="${lng}">
<head>
<meta charset="utf-8">
<title><spring:message code="i18n.privacy.policy"
		text="Privacy policy" /> | Aqlli</title>
<link rel="apple-touch-icon"
	href="http://static.aqlli.com/icons/touch-icon-iphone.png">
<link rel="apple-touch-icon" sizes="76x76"
	href="http://static.aqlli.com/icons/touch-icon-ipad.png">
<link rel="apple-touch-icon" sizes="120x120"
	href="http://static.aqlli.com/icons/touch-icon-iphone-retina.png">
<link rel="apple-touch-icon" sizes="152x152"
	href="http://static.aqlli.com/icons/touch-icon-ipad-retina.png">
<link href="http://static.aqlli.com/scripts/css/footer.css" media="all"
	rel="stylesheet">
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta content="follow,index" name="robots">
<meta name="robots" content="NOODP">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="description"
	content="<spring:message code="i18n.privacy.policy"
		text="Privacy policy" />">
<meta name="author" content="Arix OWAYE">
<link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"
	media="all" rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="http://static.aqlli.com/scripts/css/leftSideMenu.css" media="all"
	rel="stylesheet">
<link rel="stylesheet" type="text/css" media="all"
	href="http://static.aqlli.com/scripts/css/bootstrap.css">
<script src="http://static.aqlli.com/scripts/js/classie.js"></script>
<script data-main="/aqlli/js/app" src="/aqlli/js/require.js"></script>
<link href="http://static.aqlli.com/scripts/css/styles.css" media="all"
	rel="stylesheet">
<style>

#msgContainer>div.col-md-3 {
	margin-right: 3px;
	margin-left: 3px;
}

#goBackBtn {
	display: block;
	margin-bottom: 8px;
}

ol.list-inline li {
	width: 100%;
	margin-bottom: 5px;
}

ol.list-inline li a.btn {
	width: 100%;
}

</style>
</head>

<body>
	<%@ include file="/WEB-INF/pages/include/noScript.jsp"%>
	<%@ include file="/WEB-INF/pages/topNavBar.jsp"%>
	<%@ include file="/WEB-INF/pages/leftSideMenu.jsp"%>
	<div id="wrapper">
		<div class="section" id="maincontentholder">
			<div class="row">
				<div class="col-md-12" id="msgContainer">
					<div class="col-md-8 alert">
					<div class="well">
					  						<h4><spring:message code="i18n.privacy.policy"
		text="Privacy policy" /></h4>
						<p>This Privacy Policy governs the manner in which Aqlli
							collects, uses, maintains and discloses information collected
							from users (each, a "User") of the Aqlli.com website ("Site").
							This privacy policy applies to the Site and all products and
							services offered by Aqlli.</p>
						<h6>Personal identification information</h6>
						<p>We may collect personal identification information from
							Users in a variety of ways, including, but not limited to, when
							Users visit our site, register on the site and in connection with
							other activities, services, features or resources we make
							available on our Site. Users may be asked for, as appropriate,
							name, email address, date of birth, gender,</p>
						<p>Users may, however, visit public pages of our Site
							anonymously.</p>
						<p>We will collect personal identification information from
							Users only if they voluntarily submit such information to us.
							Users can always refuse to supply personally identification
							information, except that it may prevent them from engaging in
							certain Site related activities.</p>
						<h6>Non-personal identification information</h6>
						<p>We may collect non-personal identification information
							about Users whenever they interact with our Site. Non-personal
							identification information may include the browser name, the type
							of computer and technical information about Users means of
							connection to our Site, such as the operating system and the
							Internet service providers utilized and other similar
							information.</p>
						<h6>Web browser cookies</h6>
						<p>Our Site may use "cookies" to enhance User experience.
							User's web browser places cookies on their hard drive for
							record-keeping purposes and sometimes to track information about
							them. User may choose to set their web browser to refuse cookies,
							or to alert you when cookies are being sent. If they do so, note
							that some parts of the Site may not function properly.</p>
						<p>
							See more in our cookies policy page: <a
								href="${pageContext.servletContext.contextPath}/legal/cookies">Cookies
								policy</a>
						</p>
						<h6>Aggregated Statistics</h6>
						<p>Aqlli may collect statistics about the behavior of visitors
							to its websites. Aqlli may display this information publicly or
							provide it to others. However, Aqlli does not disclose
							personally-identifying information other than as described below.</p>
						<h6>Protection of Certain Personally-Identifying Information</h6>
						<p>Aqlli discloses potentially personally-identifying and
							personally-identifying information only to those of its
							employees, contractors and affiliated organizations that (i) need
							to know that information in order to process it on Aqlli’s behalf
							or to provide services available at Aqlli’s websites, and (ii)
							that have agreed not to disclose it to others. Some of those
							employees, contractors and affiliated organizations may be
							located outside of your home country; by using Aqlli’s websites,
							you consent to the transfer of such information to them.</p>
						<p>Aqlli will not rent or sell potentially
							personally-identifying and personally-identifying information to
							anyone. Other than to its employees, contractors and affiliated
							organizations, as described above, Aqlli discloses potentially
							personally-identifying and personally-identifying information
							only in response to a subpoena, court order or other governmental
							request, or when Aqlli believes in good faith that disclosure is
							reasonably necessary to protect the property or rights of Aqlli,
							third parties or the public at large.</p>
						<p>If you are a registered user of an Aqlli website and have
							supplied your email address, Aqlli may occasionally send you an
							email to tell you about new features, solicit your feedback, or
							just keep you up to date with what’s going on with Aqlli and our
							products. We primarily use our website to communicate this type
							of information, so we expect to keep this type of email to a
							minimum. If you send us a request (for example via a support
							email or via one of our feedback mechanisms), we reserve the
							right to publish it in order to help us clarify or respond to
							your request or to help us support other users. Aqlli takes all
							measures reasonably necessary to protect against the unauthorized
							access, use, alteration or destruction of potentially
							personally-identifying and personally-identifying information.</p>
						<h6>How we use collected information</h6>
						<p>Aqlli collects and uses Users personal information for the
							following purposes:</p>
						<ul>
							<li><i>To improve customer service</i><br>Your
								information helps us to more effectively respond to your
								customer service requests and support needs.</li>
							<li><i>To personalize user experience</i><br>We may use
								information in the aggregate to understand how our Users as a
								group use the services and resources provided on our Site.</li>
							<li><i>To improve our Site</i><br>We continually strive
								to improve our website offerings based on the information and
								feedback we receive from you.</li>
							<li><i>To administer a content, promotion, survey or
									other Site feature</i><br>To send Users information they
								agreed to receive about topics we think will be of interest to
								them.</li>
							<li><em>To send periodic emails</em><br />
							<br />The email address Users provide for order processing, will
								only be used to send them information and updates pertaining to
								their order. It may also be used to respond to their inquiries,
								and/or other requests or questions. If User decides to opt-in to
								our mailing list, they will receive emails that may include
								company news, updates, related product or service information,
								etc. If at any time the User would like to unsubscribe from
								receiving future emails, we include detailed unsubscribe
								instructions at the bottom of each email or User may contact us
								via our Site.</li>
						</ul>
						<h6>How we protect your information</h6>
						<p>We adopt appropriate data collection, storage and
							processing practices and security measures to protect against
							unauthorized access, alteration, disclosure or destruction of
							your personal information, username, password, transaction
							information and data stored on our Site.</p>
						<p>
							<br /> <br />Sensitive and private data exchange between the
							Site and its Users happens over a SSL secured communication
							channel and is encrypted and protected with digital
							signatures.&nbsp;<br />
							<br /> <br />
						<h6>Third party websites</h6>
						<p>Users may find advertising or other content on our Site
							that link to the sites and services of our partners, suppliers,
							advertisers, sponsors, licensors and other third parties. We do
							not control the content or links that appear on these sites and
							are not responsible for the practices employed by websites linked
							to or from our Site. In addition, these sites or services,
							including their content and links, may be constantly changing.
							These sites and services may have their own privacy policies and
							customer service policies. Browsing and interaction on any other
							website, including websites which have a link to our Site, is
							subject to that website's own terms and policies.</p>
						<h6>Advertising</h6>
						<p>Ads appearing on our site may be delivered to Users by
							advertising partners, who may set cookies. These cookies allow
							the ad server to recognize your computer each time they send you
							an online advertisement to compile non personal identification
							information about you or others who use your computer. This
							information allows ad networks to, among other things, deliver
							targeted advertisements that they believe will be of most
							interest to you. This privacy policy does not cover the use of
							cookies by any advertisers.</p>
						<h6>
							Google Adsense</b>
						</h6>
						<p>
							Some of the ads may be served by Google. Google's use of the DART
							cookie enables it to serve ads to Users based on their visit to
							our Site and other sites on the Internet. DART uses "non
							personally identifiable information" and does NOT track personal
							information about you, such as your name, email address, physical
							address, etc. You may opt out of the use of the DART cookie by
							visiting the Google ad and content network privacy policy at <a
								href="http://www.google.com/privacy_ads.html">http://www.google.com/privacy_ads.html</a>
						</p>
						<h6>Compliance with children's online privacy protection act</h6>
						<p>
							Protecting the privacy of the very young is especially important.
							For that reason, we never collect or maintain information at our
							Site from those we actually know are under 13, and no part of our
							website is structured to attract anyone under 13.<br>
							<br>
						</p>
						<h6>Business Transfert</h6>
						<p>If Aqlli, or substantially all of its assets, were
							acquired, or in the unlikely event that Aqlli goes out of
							business or enters bankruptcy, user information would be one of
							the assets that is transferred or acquired by third party. You
							acknoledge that such transfers may occur, and that any acquirer
							of Aqlli may continue to use your personal information as set
							forth in this policy.</p>

						<h6>Changes to this privacy policy</h6>
						<p>Aqlli has the discretion to update this privacy policy at
							any time. When we do, revise the updated date at the bottom of
							this page,. We encourage Users to frequently check this page for
							any changes to stay informed about how we are helping to protect
							the personal information we collect. You acknowledge and agree
							that it is your responsibility to review this privacy policy
							periodically and become aware of modifications.</p>
						<h6>Your acceptance of these terms</h6>
						<p>
							By using this Site, you signify your acceptance of this policy
							and <a
								href="${pageContext.servletContext.contextPath}/legal/terms">terms
								of service</a>. If you do not agree to this policy, please do not
							use our Site. Your continued use of the Site following the
							posting of changes to this policy will be deemed your acceptance
							of those changes.
						</p>
						<h6>Contacting us</h6>
						<p>
							If you have any questions about this Privacy Policy, the
							practices of this site, or your dealings with this site, please
							<a title="contact@aqlli.com" href="mailto:contact@aqlli.com"> contact us</a>
						</p>
					  
					</div>

					</div>
					<div class="col-md-3 alert text-center">
					  <div class="well">
					    						<span id="goBackBtn"><spring:message
								code="i18n.nav.go.back" text="Go back" />&nbsp;
							<button class="btn btn-primary" onclick="goBack()">
								<i class="fa fa-arrow-left"></i>
							</button></span>
						<div>
							<ol class="list-inline">
								<li><a
									href="/login"
									class="btn btn-sm btn-primary"><spring:message code="i18n.signin"
											text="Login" /></a></li>
								<li><a
									href="/user/registration"
									class="btn btn-sm btn-primary"><spring:message code="i18n.register"
											text="Register" /></a></li>
							</ol>
						</div>
						<div class="container text-center">
							<%@ include file="followus.jsp"%>
						</div>
					  </div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<%@ include file="/WEB-INF/pages/footer.jsp"%>
</body>
</html>