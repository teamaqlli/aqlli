<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page session="true"%>
<%@ include file="/WEB-INF/pages/include/htmlLangTag.jsp"%>
<!DOCTYPE html>
<html lang="${lng}">
<head>
<title><spring:message code="i18n.title.registration.status"
		text="Registration status" /> Aqlli</title>
		<link rel="apple-touch-icon" href="http://static.aqlli.com/icons/touch-icon-iphone.png">
<link rel="apple-touch-icon" sizes="76x76" href="http://static.aqlli.com/icons/touch-icon-ipad.png">
<link rel="apple-touch-icon" sizes="120x120" href="http://static.aqlli.com/icons/touch-icon-iphone-retina.png">
<link rel="apple-touch-icon" sizes="152x152" href="http://static.aqlli.com/icons/touch-icon-ipad-retina.png">
<link href="http://static.aqlli.com/scripts/css/footer.css" media="all"
	rel="stylesheet">
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta name="robots" content="NOODP">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta NAME="ROBOTS" CONTENT="NOINDEX,NOFOLLOW, NOARCHIVE">
<meta NAME="GOOGLEBOT" CONTENT="NOARCHIVE">
<META NAME="COPYRIGHT" CONTENT="&copy; 2015 Aqlli">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<meta name="description"
	content="<spring:message code="i18n.title.registration.status"
		text="Registration status" />">
<meta name="author" content="Arix OWAYE">
<link rel="publisher" href="//plus.google.com/b/106910419300047477169"/>
<link rel="icon" sizes="any" mask href="http://static.aqlli.com/icons/favicon.svg">
<link rel="canonical" href="${requestScope['javax.servlet.forward.request_uri']}">
<link rel="shortcut icon" href="http://static.aqlli.com/icons/favicon.ico"
	type="image/x-icon" />
<link
	href="http://netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css"
	rel="stylesheet">
<link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"
	media="all" rel="stylesheet">
<link href="http://static.aqlli.com/scripts/css/styles.css" media="all"
	rel="stylesheet">
	<script src="http://static.aqlli.com/scripts/js/classie.js"></script>
<script data-main="/aqlli/js/app" src="/aqlli/js/require.js"></script>
<style>
.aqBtn {
	margin-bottom: 10px;
}

.aqBtn .btn {
	width: 50%;
}

.main {
	background-color: #f5f5f5;
	padding-top: 20px;
	padding-bottom: 10px;
	border-radius: 10px;
}

#registrationInfo {
	margin-bottom: 25px;
}

.section#maincontentholder {
	margin-top: 60px;
}
</style>
<script>
	window.addEventListener(
					"DOMContentLoaded",
					function() {
						var ntf = document.getElementById("newTokenForm");
						var nrl = document.getElementById("newRegLink");
						if( nrl){
							nrl.addEventListener(
									"click",
									function() {
										event.preventDefault();
										var jqxhr = $.post("${pageContext.servletContext.contextPath}/user/confirm/reset", $("#newTokenForm").serialize());
										jqxhr.done( function(){
											$('#registrationInfo').html('');
											$('#registrationInfo').html("<spring:message code='i18n.registration.newToken.sent' arguments='${mail}' htmlEscape='false' text='We have sent you another verification link. Please check your email and click the button activate in the message we just.'/>");
										});
									
									});
						}
					});
</script>
</head>
<body>
	<%
		String regSuccess = (String) request
				.getAttribute("registrationSuccess");
		String confRegNullToken = (String) request
				.getAttribute("nullToken");
		String confRegUnknownToken = (String) request
				.getAttribute("unknownToken");
		String confTokenExpired = (String) request
				.getAttribute("confirmTokenExpired");
		pageContext.setAttribute("registrationSuccess", regSuccess);
		pageContext.setAttribute("nullToken", confRegNullToken);
		pageContext.setAttribute("unknownToken", confRegUnknownToken);
		pageContext.setAttribute("confirmTokenExpired", confTokenExpired);
	%>
	<c:set var="defaultNullToken"
		value="Invalid link. You can register by clicking &lt;a href='${pageContext.servletContext.contextPath}/aqlli/user/registration' &gt;here&lt;/a&gt;" />
	<c:set var="defaultTokenExpired"
		value="Your verification link expired. Click &lt;a href='#'&gt; here &lt;i class='fa fa-share'&gt;&lt;/i&gt;&lt;/a&gt; to send another link in your inbox" />
	<c:set var="defaultSuccessReg"
		value="You successfully registered. A validation link has been sent your your inbox" />
	<c:set var="defaultaccountValid"
		value="Account already validated. You can sign in and share thoughts with aqlli members." />
	<div id="wrapper">
	    <%@ include file="/WEB-INF/pages/include/noScript.jsp"%>
		<%@ include file="topNavBar.jsp"%>
		<div class="section" id="maincontentholder">
			<c:if test="${(confirmTokenExpired == 'expired')}">
			  <form:form id="newTokenForm" name="newTokenForm"
				modelAttribute="userForMail"
				action="${pageContext.servletContext.contextPath}/user/confirm/reset"
				method="POST">
				<form:input class="form-control" id="um" type="hidden" name="email"
					path="email" value="${mail}"/>
			  </form:form>
			</c:if>
			<div class="container main">
				<div class="row text-center">
					<div class="col-md-12" id="registrationInfo">
						<c:if test="${(nullToken == 'invalid')}">
							<spring:message code="i18n.registration.nullTken"
								arguments="${pageContext.servletContext.contextPath}/user/registration"
								htmlEscape="false" text="${defaultNullToken}" />
						</c:if>
						<c:if test="${(confirmTokenExpired == 'expired')}">
							<spring:message code="i18n.registration.tokenExpired"
								text="${defaultTokenExpired}" htmlEscape="false" />
						</c:if>
						<c:if test="${(registrationSuccess == 'registrationSuccess')}">
							<spring:message code="i18n.registration.regSuccess"
								arguments="${userForMail.username}" htmlEscape="false"
								text="${defaultSuccessReg}" />
						</c:if>
						<c:if test="${(unknownToken == 'unknown')}">
							<spring:message code="i18n.registration.accountValid"
								text="${defaultaccountValid}" htmlEscape="false"/>
						</c:if>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 text-center aqBtn">
						<a class="btn btn-primary"
							href="${pageContext.servletContext.contextPath}/login">
							<spring:message code="i18n.signin" text="Sign in" />
						</a>
					</div>
					<div class="col-md-6 text-center aqBtn">
						<a class="btn btn-primary"
							href="${pageContext.servletContext.contextPath}/user/registration">
							<spring:message code='i18n.register' text="Sign up" />
						</a>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<hr>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="section">
							<div class="container text-center">
								<%@ include file="followus.jsp"%>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<%@ include file="footer.jsp"%>
</body>
</html>