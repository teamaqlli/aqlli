<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="" id="timeline-post-update" draggable="false">
	<div class="timeline-panel">
		<form method="post" role="form"
			enctype="multipart/form-data" class="share-box" id="timeline-post-frm">
			<div class="timeline-body">
				<div class="share-form" draggable="false">
					<ul class="post-types img-responsive">
						<li class="post-type"><span class="status"
							id="shareTypePubli">
								<button type="button" id="shareType" class="btn-link status" draggable="false">
									<i class="fa fa-pencil-square-o">&nbsp;</i> <span class="mt"><spring:message
											code="i18n.publication" /></span>
								</button>
						</span></li>
						<li class="post-type"><button id="shareType"
								type="button" class="btn-link photo" draggable="false">
								<i class="fa fa-camera">&nbsp;</i><span class="mt"> <spring:message
										code="i18n.photo" text="Photo" />
								</span>
							</button></li>
						<li class="post-type"><button id="shareType"
								type="button" class="btn-link video" draggable="false">
								<i class="fa fa-video-camera">&nbsp;</i><span class="mt">
									<spring:message code="i18n.video" text="Video" />
								</span>
							</button></li>
						<li class="post-type">
							<button id="shareType" type="button" class="btn-link chron"
								draggable="false">
								<i class="fa fa-book">&nbsp;</i> <span class="mt"><spring:message
										code="i18n.chronicle" /></span>
							</button>
						</li>
					</ul>
					<div class="share">
						<div class="arrow"></div>
						<div draggable="false">
							<textarea name="title" cols="40" rows="10" id="status_title"
								class="form-control message"
								style="height: 30px; overflow: hidden; resize: none;"
								placeholder="<spring:message code="i18n.caption" text="Caption" />"></textarea>
							<textarea name="message" cols="40" rows="10" id="status_message"
								class="form-control message"
								style="height: 62px; overflow: hidden; resize: vertical;"
								placeholder="<spring:message code="i18n.share.something" text="Share something"/>"></textarea>
						</div>
						<div class="typeOfImage hide">

							<input type="file" accept=".png, .jpg, .jpeg" class="form-control" name="image" id="image" />
						</div>
						<div class="typeOfVideo hide">
							<input type="text" class="form-control"
								placeholder="<spring:message code='i18n.ytorvim' text='Youtube / Vimeo' />"
								id="videoUrl" name="videoUrl">
						</div>
					</div>
					<div class="col-xs-12" id="btnShareContainer">
						<input type="submit" name="submit"
							value="<spring:message code='i18n.submit' text='Submit' />"
							id="btn-share" class="btn btn-xs pull-right"> <input
							type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" />
						<input type="hidden" name="id" value="${user.uniqueID}" id="uuid"/>
						<input type="hidden" name="shareType" class="shareType" value="status"/>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>