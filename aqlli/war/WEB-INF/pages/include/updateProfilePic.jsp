<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<sec:authorize var="isloggedIn" access="isAuthenticated()" />
<c:if test="${isloggedIn}">
      <div class="fade modal" id="updatePicMod">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
              <h4 class="modal-title">Change your profile picture</h4>
            </div>
            <div class="modal-body">
              <div id="uhp">
                <div style="display:block;">
                      <div id="thumbUPics" class="text-center">
                        <span style="display:block;">Your existing pics</span>
                        <span class="fa-stack fa-2x">
                          <i class="fa fa-picture-o fa-stack-2x"></i>
                          <i class="fa fa-search fa-stack-1x" style="color:blue;"></i>
                        </span>
                      </div>
                </div>
              </div>
              <div class="uploader">
                <div id="fileupload">
                  <form id="usr-pic-upd-form" method="post" enctype="multipart/form-data">
                    <div class="fileupload-buttonbar">
                     <span class="btn btn-success fileinput-button">
				        <i class="fa fa-plus"></i>
				        <span>Add files...</span>
				        <!-- The file input field used as target for the file upload widget -->
				        <input type="file" name="files[]">
				    </span>
                    </div>
                  </form>
                  <div class="fileupload-content">
        
                    <!-- The global progress bar -->
				    <div id="progress" class="progress">
				        <div class="progress-bar progress-bar-success"></div>
				    </div>
				    <!-- The container for the uploaded files -->
				    <div id="files" class="files"></div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
      </div>
</c:if>