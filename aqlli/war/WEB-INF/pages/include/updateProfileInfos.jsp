<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<sec:authorize var="isloggedIn" access="isAuthenticated()" />
<c:set var="twId" value="${user.twitterFollowMe}"/>
<c:if test="${empty user.twitterFollowMe}">
  <c:set var="twId" value="@arixowaye"/>
</c:if>
<c:set var="fbId" value="${user.facebookFollowME}"/>
<c:if test="${empty user.facebookFollowME}">
  <c:set var="fbId" value="o.arix"/>
</c:if>
<c:set var="gpId" value='${user.googlePlusFollowMe}'/>
<c:if test="${empty user.googlePlusFollowMe}">
  <c:set var="gpId" value="114769259917982809421"/>
</c:if>
<c:set var="wst" value='${user.website}'/>
<c:if test="${empty user.website}">
  <c:set var="wst" value="www.aqlli.com"/>
</c:if>

<c:choose>
  <c:when test="${empty cookie}">
  </c:when>
<c:otherwise>
<c:forEach var="cookieVal" items="${cookie}">
  <c:if test="${cookieVal.key eq 'GWT_LOCALE'}">
    <c:set var="lng" value="<c:out value='${cookieVal.value.value}'/>"/>
  </c:if>
  <c:if test="${not lng}">
    <c:set var="lng" value="en"/>
  </c:if>
</c:forEach>
</c:otherwise>
</c:choose>
<c:if test="${isloggedIn}">
<div class="modal fade" id="updateInfsMod">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">�</button>
				<h4 class="modal-title"><spring:message code="i18n.user.update.infos" text="Update Infos" /></h4>
			</div>
			<div class="modal-body">
				<div class="setupHolder">
					<div class="gen well">
						<h4 class="text-center"><spring:message code="i18n.user.update.infos.social.buttons" text="Social buttons" /></h4>
						<hr>
						<div class="input-group">
							<span class="input-group-addon">Twitter ID</span> <input type="text"
								name="user_tw_fl" class="form-control" placeholder="<c:out value='${twId}'/>"
								id="setTw" value="<c:out value='${twId}'/>" tabindex="1">
						</div>
						<div class="input-group">
							<span class="input-group-addon">Facebook ID</span> <input
								type="text"  name="user_fb_fl" class="form-control"
								placeholder="<c:out value='${fbId}'/>" id="setFb" value="<c:out value='${fbId}'/>" tabindex="2">
						</div>
						<div class="input-group">
							<span class="input-group-addon">Google+ ID</span> <input
								type="text" name="user_gp_fl" class="form-control"
								placeholder="<c:out value='${gpId}'/>" id="setGp" value="<c:out value='${gpId}'/>" tabindex="3">
						</div>
						<div class="input-group">
							<span class="input-group-addon"><spring:message code="i18n.user.update.infos.website" text="Website" /></span> <input type="text"
								name="user_Website" class="form-control" placeholder="<c:out value='${wst}'/>"
								id="setWs" value="<c:out value='${wst}'/>" tabindex="4">
						</div>
						<div class="input-group">
							<button id="socialupd" class="btn btn-primary btn-xs"><spring:message code="i18n.save" text="Save" />
</button>

							<span style="display: none;">&nbsp;&nbsp;<i
								class="fa fa-check fa-lg"></i></span>
						</div>
					</div>
					<div class="gen well">
						<h4 class="text-center"><spring:message code="i18n.description" text="Description" /></h4>
						<hr>
						<div class="uinfos">
							<input
								type="text" name="user_description" class="form-control" id="setUd" tabindex="5">
						</div>
						<div class="input-group">
							<button id="descupd" class="btn btn-primary btn-xs"><spring:message code="i18n.save" text="Save" />
</button>
							<span style="display: none;">&nbsp;&nbsp;<i
								class="fa fa-check fa-lg"></i></span>
						</div>
					</div>
					<div class="gen well">
						<h4 class="text-center"><spring:message code="i18n.user.update.infos.occupation" text="Occupation" /></h4>
						<hr>
						<div class="input-group">
							<span class="input-group-addon"><spring:message code="i18n.job.position" text="Job position"/></span> <input
								type="text" name="user_job_position" class="form-control"
								placeholder="<c:out value='${user.jobPosition}'/>" id="setJb" value="<c:out value='${user.jobPosition}'/>" tabindex="6">
						</div>
						<div class="input-group">
							<span class="input-group-addon"><spring:message code="i18n.company" text="Company"/></span> <input type="text"
								name="user_company" class="form-control" autocomplete="off" placeholder="<c:out value='${user.company}'/>"
								id="setComp" value="<c:out value='${user.company}'/>" tabindex="7">
						</div>
						<div class="input-group" style="width:100%">
							<span class="input-group-addo"><spring:message code="i18n.city.from" text="HomeTown"/></span> <input type="text"
								name="user_city" class="user_city form-control" placeholder="<c:out value='${currentlocation}'/>"
								id="setHt" value="<c:out value='${currentlocation}'/>" tabindex="8">
						</div>
						<div class="input-group">
							<button id="occupupd" class="btn btn-primary btn-xs"><spring:message code="i18n.save" text="Save" />
</button>
							<span style="display: none;">&nbsp;&nbsp;<i class="fa fa-check fa-lg"></i></span>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<a class="btn btn-primary" data-dismiss="modal"><spring:message code="i18n.close" text="Close"/></a>
			</div>
		</div>
	</div>
</div>
</c:if>