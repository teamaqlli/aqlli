<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div id="timeline-post-update" draggable="false">
	<div class="timeline-panel">
		<form method="post" role="form"
			enctype="application/json" class="share-box" id="timeline-post-frm">
			<div class="timeline-body">
				<div class="share-form" draggable="false">
					<ul class="post-types img-responsive">
						<li class="post-type"><span class="status"
							id="shareTypePubli">
								<button type="button" id="shareType" class="btn-link status" draggable="false">
									<i class="fa fa-pencil-square"></i>&nbsp; <span class="mt"><spring:message
											code="i18n.user.post" text="Post"/></span>
								</button>
						</span></li>
						<li class="post-type"><button id="shareType"
								type="button" class="btn-link photo" draggable="false">
								<i class="fa fa-camera">&nbsp;</i><span class="mt"> <spring:message
										code="i18n.photo" text="Photo" />
								</span>
							</button></li>
						<li class="post-type"><button id="shareType"
								type="button" class="btn-link video" draggable="false">
								<i class="fa fa-video-camera">&nbsp;</i><span class="mt">
									<spring:message code="i18n.video" text="Video" />
								</span>
							</button></li>
					</ul>
					<div class="share">
						<div class="arrow"></div>
						   <input type="text" name="title" id="status_title"
								class="form-control message emojipickercontent"
								style="height: 30px; overflow: hidden; resize: none;"
								placeholder="<spring:message code="i18n.caption" text="Caption" />">
							<textarea name="message" cols="40" rows="10" id="status_message"
								class="form-control message emojipickercontent"
								style="height: 62px; overflow: hidden; resize: vertical;"
								placeholder="<spring:message code="i18n.share.something" text="Share something"/>"></textarea>
						<div class="typeOfVideo hide">
							<input type="text" class="form-control"
								placeholder="<spring:message code='i18n.ytorvim' text='Youtube / Vimeo' />"
								id="videoUrl" name="videoUrl">
						</div>
					</div>
					<div class="col-xs-12" id="btnShareContainer">
						<span class="pickEmoji pull-left" style="visibility:hidden;"><i class="fa fa-smile-o fa-lg"></i></span>
						<input type="submit" name="submit"
							value="<spring:message code='i18n.submit' text='Submit' />"
							id="btn-share" class="btn btn-xs pull-right btn-primary" disabled> <input
							type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" />
						<input type="hidden" name="id" value="${userFromUrl.uniqueID}" id="uuid"/>
						<input type="hidden" name="shareType" class="shareType" value="status"/>
						<input type="hidden" name="created" id="created" value=""/>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>