<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page import="com.aqlli.main.client.model.UserDetails"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
  //récupère la valeur du cookie de localité
  String local = "GWT_LOCALE";
  Cookie c = null;
  Cookie[] cs = null;
  String ln = "";
  cs = request.getCookies();
  if (cs != null) {
	for (int i = 0; i < cs.length; i++) {
	  c = cs[i];
	  if (c.getName().compareTo( local) == 0) {
		ln = c.getValue();
	  }
	}
  }
  //le cookie n'existe pas?
  if ( ln == ""){
	  c = new Cookie( local, "default");
	  c.setComment( "User language cookie");
	  c.setMaxAge( 1000 * 60 * 60 * 24 * 18600);//Dans 23 ans
	  response.addCookie( c);
  }
%>
<% 
  if (request.isUserInRole("USER_ROLE")){
	 UserDetails user = (UserDetails)request.getAttribute("user");
	 if ( user.getDefaultLang().equalsIgnoreCase("default") && (ln != "")){
	   user.setDefaultLang( ln);
	 }
	 if ( user.getDefaultLang() != "" && user.getDefaultLang() != "default"){
		  c = new Cookie( local, user.getDefaultLang());
		  c.setComment( "User language cookie");
		  c.setMaxAge( 1000 * 60 * 60 * 24 * 18600);//Dans 23 ans
		  response.addCookie( c);
	  }
  }
%>

<form action="${pageContext.request.contextPath}/api/update.lang" method="post" id="changeLangForm">
  <input type="hidden" name="lang" value="" id="langToSet"/>
  <input type="hidden" 
    name="${_csrf.parameterName}"
	value="${_csrf.token}" />
</form>

<div class="btn-group btn-group-xs dropup" id="langPickerHolder">
    <a class="btn dropdown-toggle btn-primary" data-toggle="dropdown"><i class="fa fa-language fa-lg"></i>&nbsp;<spring:message code="i18n.language" text="Language"/>&nbsp;<span class="caret"></span> </a>
	<ul class="dropdown-menu" role="menu">
		<li><a onclick="aclfs( 'en');"><spring:message code="i18n.english" text="English"/></a></li>
		<li><a onclick="aclfs( 'fr');"><spring:message code="i18n.french" text="french"/></a></li>
		<li><a onclick="aclfs( 'zh');"><spring:message code="i18n.simplified.chinese" text="Chinese(Simplified)"/></a></li>
	</ul>
</div>