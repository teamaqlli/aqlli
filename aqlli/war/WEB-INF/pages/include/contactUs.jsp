<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="contact-form" id="contact-form">
	<h3><spring:message code="i18n.contact.info" text="Contact info"/></h3>
	<address>
		<strong>Aqlli</strong>
		<br>
		<span><a href="mailto:contact@aqlli.com">contact@aqlli.com</a></span>
	</address>

	<form enctype="UTF-8" id="main-contact-form" name="contact-form" method="post"
		action="/contact/message">
		<div class="form-group">
			<input type="text" name="name" id="name" class="form-control"
				placeholder='<spring:message code="i18n.name" text="Name"/>'/>
		</div>
		<div class="form-group">
			<input type="email" name="email" id="email" class="form-control"
				placeholder="<spring:message code="i18n.email" text="Email"/>"/>
				
		</div>
		<div class="form-group">
			<input type="text" name="subject" id="subject" class="form-control"
				placeholder="<spring:message code="i18n.subject" text="Subject"/>"/>
		</div>
		<div class="form-group">
			<textarea name="message" class="form-control" rows="8"
				placeholder="<spring:message code="i18n.message" text="Message"/>" maxlength="2147483647" draggable="false" id="message" style="resize:vertical"></textarea>
		</div>
		<button type="submit" class="btn btn-primary"><spring:message code="i18n.send" text="Send"/></button>
	</form>
</div>