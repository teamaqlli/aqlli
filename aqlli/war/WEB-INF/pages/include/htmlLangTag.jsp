<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:choose>
  <c:when test="${empty cookie}">
  </c:when>
<c:otherwise>
<c:forEach var="cookieVal" items="${cookie}">
  <c:if test="${cookieVal.key eq 'GWT_LOCALE'}">
    <c:set var="lng" value="${cookieVal.value.value}"/>
  </c:if>
  <c:if test="${not lng}">
    <c:set var="lng" value="en"/>
  </c:if>
</c:forEach>
</c:otherwise>
</c:choose>