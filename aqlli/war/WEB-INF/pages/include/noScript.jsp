<noscript>
<div id="js-disabled-warning">
    <div class="message">
        <p><spring:message code="i18n.noscript.msg" text="You're using this site with javascript disabled. Javascript is essential for using this site. Please enable it!"  htmlEscape="false"/></p>
        <p><spring:message code="i18n.noscript.activate.help" text="You can get some help by visiting the website: www.browsehappy.com" arguments='http://browsehappy.com/, http://browsehappy.com/' htmlEscape="false"/></p>
        <span><a class="pull-right" href="javascript:location.reload();"><i class="fa fa-refresh"></i>&nbsp;<spring:message code="i18n.noscript.refresh" text="Refresh" htmlEscape="false"/></a></span>
     </div>
     <script>
     (function() {
    var alert_modal = document.getElementById('js-disabled-warning');
    alert_modal.parentNode.removeChild(alert_modal);
})();
     </script>
</div>
</noscript>