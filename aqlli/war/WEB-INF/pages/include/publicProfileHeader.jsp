<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<sec:authorize var="isloggedIn" access="isAuthenticated()" />
<c:choose>
	<c:when test="${not isloggedIn}">
	  <div class="row effect" id="header-holder">
			<div class="timeline-header img">
				<div id="user-advert-holder">
					<img class="img-responsive profile-cover"
						src="${userFromUrl.defaultUserCoverUrl}" id="user-book-ad"
						style="width: 100%; height: 100%;" draggable="false" /> <span
						class="center-block usergenfields text-center">${userFromUrl.fullName}
					</span>
				</div>
			</div>
			<div class="profile-pic">
				<img src="${userFromUrl.defaultUserAvatarUrl}"
					class="center-block img-responsive img-rounded" id="user-avatar" />
			</div>
		</div>
	</c:when>
	<c:otherwise>
	   <div class="row effect" id="header-holder">
			<div class="timeline-header img">
				<div id="user-advert-holder">
					<img class="img-responsive profile-cover"
						src="${userFromUrl.defaultUserCoverUrl}" id="user-book-ad"
						style="width: 100%; height: 100%;" draggable="false" /> <span
						class="center-block usergenfields text-center"><c:if
							test="${pageContext.request.userPrincipal.name != null}">${userFromUrl.fullName}</c:if>
					</span>
				</div>
			</div>
			<div class="profile-pic">
				<img src="${userFromUrl.defaultUserAvatarUrl}"
					class="center-block img-responsive img-rounded" id="user-avatar" />
			</div>
		</div>
		
	</c:otherwise>
</c:choose>


