<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<% // get User Agent header from request
String usrAgent = request.getHeader("User-Agent").toLowerCase();
if (usrAgent == null) usrAgent = "";
// platforms
boolean is_mac68k = ((usrAgent.indexOf("68k") > -1) || (usrAgent.indexOf("68000") > -1));
boolean is_macppc = ((usrAgent.indexOf("ppc") > -1) || (usrAgent.indexOf("powerpc") > -1));
//is mac?
boolean is_mac = ((usrAgent.indexOf("mac") > -1) || is_mac68k || is_macppc);
// is microsoft?
boolean is_msie = (usrAgent.indexOf("msie") > -1);

boolean is_unix = usrAgent.toLowerCase().indexOf("x11") >= 0;

boolean is_android = usrAgent.toLowerCase().indexOf("android") >= 0;

boolean is_iphone = usrAgent.toLowerCase().indexOf("iphone") >= 0;

boolean is_unknown = !is_mac && !is_msie && !is_unix && !is_android;

double version = 0.0;
// get MSIE version
if (is_msie) {
  try {
    String tempStr = usrAgent.substring(usrAgent.indexOf("msie"),usrAgent.length());
    version = Double.parseDouble(tempStr.substring(4,tempStr.indexOf(";")));
  } catch (NumberFormatException nfe) {
       version = 0.0;
  }
       catch (StringIndexOutOfBoundsException siobe) {
     version = 0.0;
  }
}
//checking if JS is supported
boolean is_msie4up  = (is_msie && (version >= 4.00));
Boolean useJS = new Boolean(!is_mac && is_msie4up);
request.setAttribute("useJS", useJS);
out.println("");
%>