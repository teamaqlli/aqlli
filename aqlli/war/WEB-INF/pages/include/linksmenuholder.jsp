<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<sec:authorize var="isloggedIn" access="isAuthenticated()" />
<div class="links-menu-holder" id="btnsm">
				<div class="center-block">
					<ul class="pull-left">
					<c:if test="${isCurrUserTheAuthUser}">
						<li><button type="button" class="btn-link" draggable="false"
								data-toggle="modal" data-target="#updatePicMod">
								<i class="fa fa-camera"></i>&nbsp; <span class="mt"><spring:message
										code="i18n.update.profile.picture" text="Profile picture" /></span>
							</button></li>
						<li>
							<button type="button" class="btn-link" draggable="false"
								data-toggle="modal" data-target="#updateInfsMod">
								<i class="fa fa-info"></i> &nbsp; <span class="mt"><spring:message
										code="i18n.update.profile.info" text="Information" /></span>
							</button>
						</li>
					</c:if>
					</ul>
					<ul class="pull-right">
						<c:if test="${isloggedIn}">
							<li><button type="button" class="btn-link message" draggable="false">
									<i class="fa fa-comment"></i>&nbsp;<span class="mt"><spring:message
										code="i18n.message" text="Message" /></span>
								</button>
							</li>
						</c:if>
						<li>
							<div class="dropdown">
								<button aria-haspopup="true" aria-expanded="true" data-toggle="dropdown" type="button" class="btn-link dropdown-toggle" id="usrmorebtn" draggable="false">
								<i class="fa fa-plus-square"></i>&nbsp;<span class="mt">
									<spring:message code="i18n.more" text="More" />
								</span>
							</button>
								<ul class="dropdown-menu more" aria-labelledby="usrmorebtn">
									<c:if test="${isloggedIn}">
										<li><a><spring:message code="i18n.report.bug" text="Report a bug"/>&nbsp;<button draggable="false"
								data-toggle="modal" data-target="#reportBugMod"><i class="fa fa-bug fa-lg"></i></button></a></li>
									</c:if>
									<li class="usrmoreshare"><a><spring:message code="i18n.share.page" text="Share page"/>&nbsp;<span id="stw"><i class="fa fa-twitter fa-lg"></i></span><span id="sgp"><i class="fa fa-google-plus fa-lg"></i></span><span id="sfb"><i class="fa fa-facebook fa-lg"></i></span></a></li>
								</ul>
							</div>
						</li>
					</ul>
				</div>
			</div>