<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<sec:authorize var="loggedIn" access="isAuthenticated()" />
<c:if test="${not loggedIn}">
	<div id="cookiesCompliance" style="margin-bottom: -50px;"
		class="text-align navbar-fixed-bottom"></div>
</c:if>
<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="col-sm-6 flinks">
				<ul class="list-unstyled">
					<li>Aqlli &reg; 2016</li>
					<li><a href="/legal"><spring:message code="i18n.legal" text="Legal"/></a></li>
					<li><a href="/pages"><spring:message code="i18n.pages.center" text="Pages center"/></a></li>
					<li><a href="${pageContext.servletContext.contextPath}/pages/careers"><spring:message code="i18n.career" text="Career"/></a></li>
				</ul>
			</div>
			<div class="col-sm-6">
				<ul class="social-icons">
					<c:if test="${not loggedIn}">
						<li><%@include file="/WEB-INF/pages/setLang.jsp"%></li>
					</c:if>
					<li class="fsocial"><a href="https://www.facebook.com/aqlliFr" target="_blank" title="Facebook"><i
							class="fa fa-facebook"></i></a></li>
					<li class="fsocial"><a href="https://twitter.com/aqlli_fr" target="_blank" title="Twitter"><i
							class="fa fa-twitter"></i></a></li>
					<li class="fsocial"><a
						href="https://plus.google.com/106910419300047477169/about" target="_blank" title="Google+"><i
							class="fa fa-google-plus"></i></a></li>
				</ul>
			</div>
		</div>
	</div>
</footer>