<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page session="true"%>
<%@ include file="/WEB-INF/pages/include/htmlLangTag.jsp"%>
<html lang="${lng}">
<head>
<meta charset="utf-8">
<title><spring:message code="i18n.thank.you.page"
		text="thanks!"/> | Aqlli</title>
<link rel="apple-touch-icon"
	href="http://static.aqlli.com/icons/touch-icon-iphone.png">
<link rel="apple-touch-icon" sizes="76x76"
	href="http://static.aqlli.com/icons/touch-icon-ipad.png">
<link rel="apple-touch-icon" sizes="120x120"
	href="http://static.aqlli.com/icons/touch-icon-iphone-retina.png">
<link rel="apple-touch-icon" sizes="152x152"
	href="http://static.aqlli.com/icons/touch-icon-ipad-retina.png">
<link href="http://static.aqlli.com/scripts/css/footer.css" media="all"
	rel="stylesheet">
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta content="follow,index" name="robots">
<meta name="robots" content="NOODP">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="description"
	content="<spring:message code="i18n.meta.thanks" text="Thanks page for people and companies that have help Aqlli out :heart:"/>">
<!-- fb -->
<meta property="og:title" content="Your are heros | Aqlli" />
<meta property="og:type" content="website" />
<meta property="og:image" content="http://static.aqlli.com/icons/touch-icon-ipad-retina.png" />
<meta property="og:url"
	content="${requestScope['javax.servlet.forward.request_uri']}" />
<meta property="og:description"
	content="<spring:message code='i18n.thanks.page.msg' text='We have all started somewhere. From
									scratch, nobody can advance alone. So this page is dedicated to
									all the people and companies that have helped Aqlli s build.' />" />
<meta property="fb:admins" content="100005426680123" />
<meta property="og:site_name" content="Aqlli" />
<!-- tw -->
<meta name="twitter:card" content="summary">
<meta name="twitter:url"
	content="${requestScope['javax.servlet.forward.request_uri']}" />
<meta name="twitter:title" content="You are heros | Aqlli" />
<meta name="twitter:description"
	content="<spring:message code='i18n.thanks.page.msg' text='We have all started somewhere. From
									scratch, nobody can advance alone. So this page is dedicated to
									all the people and companies that have helped Aqlli s build.' />" />
<meta name="twitter:image" content="http://static.aqlli.com/icons/touch-icon-ipad-retina.png" />
<meta name="twitter:site" content="@aqlli_fr" />
<meta name="twitter:creator" content="Arix OWAYE" />
<!-- g+ -->
<meta itemprop="name" content="<spring:message code='i18n.thank.you.page'
		text='thanks!'/> | Aqlli" />
<meta itemprop="description"
	content="<spring:message code='i18n.thanks.page.msg' text='We have all started somewhere. From
									scratch, nobody can advance alone. So this page is dedicated to
									all the people and companies that have helped Aqlli s build.' />" />
<meta itemprop="audience" content="Web users" />
<meta itemprop="image" content="http://static.aqlli.com/icons/touch-icon-ipad-retina.png" />
<meta itemprop="alternativeHeadline" content="You are heros | Aqlli" />
<link rel="publisher" href="//plus.google.com/b/106910419300047477169" />
<link rel="icon" sizes="any" mask
	href="http://static.aqlli.com/icons/favicon.svg">
<link rel="canonical"
	href="${requestScope['javax.servlet.forward.request_uri']}">
<link rel="shortcut icon"
	href="http://static.aqlli.com/icons/favicon.ico" type="image/x-icon" />
<link rel="stylesheet" type="text/css" media="all"
	href="http://static.aqlli.com/scripts/css/bootstrap.css">
<link rel="stylesheet" type="text/css"
	href="http://static.aqlli.com/scripts/css/leftSideMenu.css" media="all"
	rel="stylesheet">
<link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"
	media="all" rel="stylesheet">
<script src="http://static.aqlli.com/scripts/js/classie.js"></script>
<script data-main="/aqlli/js/app" src="/aqlli/js/require.js"></script>
<link href="http://static.aqlli.com/scripts/css/styles.css" media="all"
	rel="stylesheet">
<style>
ol.list-inline.text-center li a.btn {
	width: 100%;
	margin-bottom: 5px;
}

ol.list-inline.text-center li {
	width: 100%;
}

#wrapper {
	margin-top: 60px;
}

#thnku,#thnku i {
	color: white;
}
</style>
</head>

<body>
	<div id="fb-root"></div>
	<%@ include file="/WEB-INF/pages/include/noScript.jsp"%>
	<%@ include file="topNavBar.jsp"%>
	<%@ include file="/WEB-INF/pages/leftSideMenu.jsp"%>
	<div id="wrapper">
		<div id="maincontentholder">
			<div class="row">
				<div class="col-md-9">
					<div class="well">
						<div>
							<p class="text-center">
								<span id="thnktext"><spring:message
										code="i18n.thanks.page.msg"
										text="We have all started somewhere. From
									scratch, nobody can advance alone. So this page is dedicated to
									all the people and companies that have helped Aqlli's build." /></span>
							</p>
						</div>
						<div style="background-color: #778899">
							<h3 class="text-center" id="thnku">
								<spring:message code="i18n.thank.you.page" text="Thank you!" />
								<i class="fa fa-thumbs-o-up"></i>
							</h3>

						</div>
						<div class="text-center">
							<br />
							<h4>
								<i class="fa fa-briefcase">&nbsp;<spring:message
										code="i18n.companies" text="Companies" /></i>
							</h4>
						</div>
						<div>
							<ul class="list-group">
								<li class="list-group-item"><a
									href="http://shapebootstrap.net/"><i
										class="fa fa-external-link-square"></i>&nbsp; Shapebootstrap</a></li>
							</ul>
						</div>
						<div class="text-center">
							<h4>
								<i class="fa fa-users">&nbsp;<spring:message
										code="i18n.people" text="People" /></i>
							</h4>
						</div>
						<div class="row">
							<div class="col-md-2">
								<img
									src="//static.aqlli.com/pictures/thanks/Sijin_Li.jpg"
									class="img-circle img-responsive" draggable="false">
							</div>
							<div class="col-md-4">
								<h3 class="text-left">Sijin Li</h3>
								<p class="text-left">
									<spring:message code="i18n.translator.female" text="Translator" />
								</p>
								<p class="text-left">
									<a href="https://www.facebook.com/sijin.li.94"><i class="fa fa-facebook fa-lg"></i></a>
								</p>
							</div>
							<div class="col-md-2">
								<img src="//static.aqlli.com/pictures/thanks/lobar_uzakova.jpg"
									class="img-circle img-responsive" alt="Lobar Uzakova" draggable="false">
							</div>
							<div class="col-md-4">
								<h3 class="text-left">Lobar Uzakova</h3>
								<p class="text-left">
									<spring:message code="i18n.translation.aq.name" text="Translator"/>
								</p>
								<p class="text-left">
									<a href="https://www.linkedin.com/profile/view?id=379586158"><i class="fa fa-linkedin-square fa-lg"></i></a>
								</p>
								
							</div>
						</div>
						<div class="row">
							<div class="col-md-2">
								<img src="//static.aqlli.com/pictures/thumbs/female-profile-180-180.jpg"
									class="img-circle img-responsive" alt="Juliette PASSEBOIS DUCROS" draggable="false">
							</div>
							<div class="col-md-4">
								<h3 class="text-left">Juliette PASSEBOIS DUCROS</h3>
								<p class="text-left">
									<spring:message code="i18n.thanks.advices" text="Smart advice" />
								</p>
								<p class="text-left">
									<a href="https://www.linkedin.com/profile/view?id=220496257"><i class="fa fa-linkedin-square fa-lg"></i></a>
								</p>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-3 well">
					<ol class="list-inline text-center">
						<li><a href="${pageContext.servletContext.contextPath}/login"
							class="btn btn-sm btn-primary"><spring:message
									code="i18n.signin" text="Login" /></a></li>
						<li><a
							href="${pageContext.servletContext.contextPath}/user/registration"
							class="btn btn-sm btn-primary"><spring:message
									code="i18n.register" text="Register" /></a></li>
					</ol>
					<div class="container text-center">
						<%@ include file="followus.jsp"%>

					</div>
				</div>
			</div>

		</div>

	</div>
	<%@ include file="footer.jsp"%>
</body>
</html>