<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page session="true"%>
<%@ include file="/WEB-INF/pages/include/htmlLangTag.jsp"%>
<!DOCTYPE html>
<html lang="${lng}">
<head>
<meta charset="utf-8">
<title><spring:message code="i18n.title.register.help"
		text="Aqlli registration help page" /></title>
		<link rel="apple-touch-icon" href="http://static.aqlli.com/icons/touch-icon-iphone.png">
<link rel="apple-touch-icon" sizes="76x76" href="http://static.aqlli.com/icons/touch-icon-ipad.png">
<link rel="apple-touch-icon" sizes="120x120" href="http://static.aqlli.com/icons/touch-icon-iphone-retina.png">
<link rel="apple-touch-icon" sizes="152x152" href="http://static.aqlli.com/icons/touch-icon-ipad-retina.png">
<link href="http://static.aqlli.com/scripts/css/footer.css" media="all"
	rel="stylesheet">
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta content="follow,index" name="robots">
<meta name="robots" content="NOODP">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="description"
	content="<spring:message code="i18n.meta.register.help" text="Get help for your registration at Aqlli"/>">
<meta name="author" content="Arix Owaye">
<link rel="publisher" href="//plus.google.com/b/106910419300047477169"/>
<link rel="icon" sizes="any" mask href="http://static.aqlli.com/icons/favicon.svg">
<link rel="canonical" href="${requestScope['javax.servlet.forward.request_uri']}">
<link rel="shortcut icon" href="http://static.aqlli.com/icons/favicon.ico"
	type="image/x-icon" />
<link rel="stylesheet" type="text/css" media="all"
	href="http://static.aqlli.com/scripts/css/bootstrap.css">
<link href="http://static.aqlli.com/scripts/css/styles.css" media="all" rel="stylesheet">
<link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"
	media="all" rel="stylesheet">
	<script src="http://static.aqlli.com/scripts/js/classie.js"></script>
<script data-main="/aqlli/js/app" src="/aqlli/js/require.js"></script>
<style>
.col-md-2.well ol.list-inline.text-center li {
	width: 100%;
	margin-bottom: 5px;
}

.col-md-2.well ol.list-inline.text-center li a.btn {
	width: 100%;
}
.section#maincontentholder {
	margin-top: 60px;
}
.col-md-4.links {
	margin-bottom: 25px;
}
.col-md-2.well {
	margin-left: 10px;
}
</style>
</head>
<body>
<div id="fb-root"></div>
	<%@ include file="/WEB-INF/pages/include/noScript.jsp"%>
	<%@ include file="topNavBar.jsp"%>
	<div id="wrapper">
		<div class="section" id="maincontentholder">
			<div class="container">
				<div class="row">
					<div class="col-md-4 links">
						<ul class="nav nav-stacked nav-tabs">
							<li class="active"><a href="#" id="askBDayM"><spring:message code="i18n.registration.help.bday" text="Why do you need my date of birth"/></a></li>
						</ul>
					</div>
					<div class="col-md-6 well">
						<div class="row" id="askBDay"><spring:message code="i18n.registration.help.bday.msg" text="Youth safety is one of our main concerns. Only persons aged 13 or older may register on Aqlli."/></div>
					</div>
					<div class="row">
					<div class="col-md-2 well">
						<ol class="list-inline text-center">
							<li><a href="${pageContext.servletContext.contextPath}/login" class="btn btn-block btn-sm btn-primary"><spring:message code="i18n.signin" text="Login"/></a></li>
							<li><a href="${pageContext.servletContext.contextPath}/user/registration" class="btn btn-block btn-sm btn-primary"><spring:message code="i18n.register" text="Register"/></a></li>
						</ol>
						<div class="container text-center">
								<%@ include file="followus.jsp"%>
							</div>
					</div>
					</div>
					
				</div>
			</div>
		</div>
		
	</div>
	<%@ include file="footer.jsp"%>
</body>
</html>