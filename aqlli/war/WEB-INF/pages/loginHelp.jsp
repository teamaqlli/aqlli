<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page session="true"%>
<%@ include file="/WEB-INF/pages/include/htmlLangTag.jsp"%>
<!DOCTYPE html>
<html lang="${lng}">
<meta charset="utf-8">
<title><spring:message code="i18n.title.login.help" text="Aqlli login help page" /></title>
<link rel="apple-touch-icon" href="http://static.aqlli.com/icons/touch-icon-iphone.png">
<link rel="apple-touch-icon" sizes="76x76" href="http://static.aqlli.com/icons/touch-icon-ipad.png">
<link rel="apple-touch-icon" sizes="120x120" href="http://static.aqlli.com/icons/touch-icon-iphone-retina.png">
<link rel="apple-touch-icon" sizes="152x152" href="http://static.aqlli.com/icons/touch-icon-ipad-retina.png">
<link href="http://static.aqlli.com/scripts/css/footer.css" media="all"
	rel="stylesheet">
<meta name="_csrf" content="${_csrf.token}"/>
<meta name="_csrf_header" content="${_csrf.headerName}"/>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta content="follow,index" name="robots">
<meta name="robots" content="NOODP">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="description" content="<spring:message code="i18n.meta.login.help" text="Login help page for Aqlli"/>">
<meta name="author" content="Arix OWAYE">
<link rel="publisher" href="//plus.google.com/b/106910419300047477169"/>
<link rel="icon" sizes="any" mask href="http://static.aqlli.com/icons/favicon.svg">
<link rel="canonical" href="${requestScope['javax.servlet.forward.request_uri']}">
<link rel="shortcut icon" href="http://static.aqlli.com/icons/favicon.ico"
	type="image/x-icon" />
<link href="http://static.aqlli.com/scripts/css/footer.css" media="all"
	rel="stylesheet">
<link rel="stylesheet" type="text/css" media="all" href="http://static.aqlli.com/scripts/css/bootstrap.css">
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" media="all" rel="stylesheet">
	<script src="http://static.aqlli.com/scripts/js/classie.js"></script>
<script data-main="/aqlli/js/app" src="/aqlli/js/require.js"></script>
<link href="http://static.aqlli.com/scripts/css/styles.css" media="all" rel="stylesheet">
<style>
.col-md-2.well ol.list-inline.text-center li {
	width: 100%;
	margin-bottom: 5px;
}

.col-md-2.well ol.list-inline.text-center li a.btn {
	width: 100%;
}

.col-md-2.well {
	margin-left: 10px;
}

.col-md-4.links {
	margin-bottom: 25px;
}
.section#maincontentholder{
  margin-top:60px;
}
#lostPass{
  display: none;
}
</style>
</head>
<body>
	<div id="fb-root"></div>
	<%@ include file="/WEB-INF/pages/include/noScript.jsp"%>
	<%@ include file="topNavBar.jsp"%>
    <div id="wrapper">
    <div class="section" id="maincontentholder">
		<div class="container">
			<div class="row">
				<div class="col-md-4 links">
					<ul class="nav nav-stacked nav-tabs">
						<li class="active"><a href="#" id="noAccountM"><spring:message code="i18n.login.help.mail.lost" text="I do not
								have an account"/></a></li>
						<li><a href="#" id="lostPassM"><spring:message code="i18n.login.help.notMember" text="I have lost my password"/></a></li>
					</ul>
				</div>
				<div class="col-md-6 well">
					<div class="row" id="noAccount">
					   <spring:message code="i18n.login.help.notMember.msg" arguments='${pageContext.servletContext.contextPath}/user/registration' htmlEscape='false' text="If your not yet a member of Aqlli you can create an account by clicking the register button on the right"/>
					</div>
					<div class="row" id="lostPass">
						<span>
						  <spring:message code="i18n.login.help.mail.lost.msg" text="Enter your email address. We will send you a password reset link."/>
						</span>
						<div class="col-md-12">
						  <div class="form-group">
							<label class="control-label" for="lostPassInput"><spring:message code="i18n.email" text="Email"/></label> <input class="form-control" id="lostPassInput"
										placeholder="<spring:message code="i18n.email" text="Email"/>" type="email">
						  </div>
						  <button type="submit" class="btn btn-primary" id="btnlostPass"><spring:message code="i18n.submit" text="Submit"/></button>
						  <span id="passLostMsg"></span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-2 well">
						<ol class="list-inline text-center">
							<li><a href="${pageContext.servletContext.contextPath}/login" class="btn btn-sm btn-primary"><spring:message code="i18n.signin" text="Login"/></a></li>
							<li><a href="${pageContext.servletContext.contextPath}/user/registration" class="btn btn-sm btn-primary"><spring:message code="i18n.register" text="Register"/></a></li>
						</ol>
						<div class="container text-center">
								<%@ include file="followus.jsp"%>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	</div>
	<%@ include file="footer.jsp"%>
</body>
</html>