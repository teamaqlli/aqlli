<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page session="true"%>
<%@ include file="/WEB-INF/pages/include/htmlLangTag.jsp"%>
<!DOCTYPE html>
<html lang="${lng}">
<meta charset="utf-8">
<title><spring:message code="i18n.change.password" text="Password update" /> | Aqlli</title>
<link rel="apple-touch-icon"
	href="http://static.aqlli.com/icons/touch-icon-iphone.png">
<link rel="apple-touch-icon" sizes="76x76"
	href="http://static.aqlli.com/icons/touch-icon-ipad.png">
<link rel="apple-touch-icon" sizes="120x120"
	href="http://static.aqlli.com/icons/touch-icon-iphone-retina.png">
<link rel="apple-touch-icon" sizes="152x152"
	href="http://static.aqlli.com/icons/touch-icon-ipad-retina.png">
<link href="http://static.aqlli.com/scripts/css/footer.css" media="all"
	rel="stylesheet">
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta content="nofollow,noindex" name="robots">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="description"
	content="Securely change your password">
<meta name="author" content="Arix OWAYE">
<link rel="icon" sizes="any" mask
	href="http://static.aqlli.com/icons/favicon.svg">
<link rel="canonical"
	href="${requestScope['javax.servlet.forward.request_uri']}">
<link rel="shortcut icon"
	href="http://static.aqlli.com/icons/favicon.ico" type="image/x-icon" />
<link rel="stylesheet" type="text/css" media="all"
	href="http://static.aqlli.com/scripts/css/bootstrap.css">
<link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"
	media="all" rel="stylesheet">
<link href="http://static.aqlli.com/scripts/css/styles.css" media="all"
	rel="stylesheet">
<script src="http://static.aqlli.com/scripts/js/classie.js"></script>
<script data-main="/aqlli/js/app" src="/aqlli/js/require.js"></script>

<style>
.col-md-2.well ol.list-inline.text-center li {
	width: 100%;
	margin-bottom: 5px;
}

.col-md-2.well ol.list-inline.text-center li a.btn {
	width: 100%;
}

.col-md-2.well {
	margin-left: 10px;
}

.col-md-4.links {
	margin-bottom: 25px;
}

.section#maincontentholder {
	margin-top: 60px;
}

#lostPass {
	display: none;
}
</style>
</head>
<body>
	<div id="fb-root"></div>
	<%@ include file="/WEB-INF/pages/include/noScript.jsp"%>
	<div id="wrapper">
		<%@ include file="topNavBar.jsp"%>
		<div class="section" id="maincontentholder">
			<div class="container">
				<div class="row">
					<div class="col-md-10 well">
						<c:if test="${not ( nullToken eq 'tokenIsNull')}">
							<form method="POST" class="form-horizontal" id="cpwdfrm"
								action="/login/password.change">
								<fieldset>
									<!-- Form Name -->
									<legend>
										<spring:message code="i18n.change.password"
											text="Password update" />
									</legend>
									<!-- Password input-->
									<div class="form-group">
										<label class="col-md-4 control-label" for="newpassword"><spring:message
												code="i18n.new.pass" text="New
											password" /></label>
										<div class="col-md-5">
											<input id="newpassword" name="newpassword" type="password"
												placeholder="Password" class="form-control input-md"
												required>
										</div>
									</div>

									<!-- Password input-->
									<div class="form-group">
										<label class="col-md-4 control-label" for="repeatnewpassword"><spring:message
												code="i18n.confirmPass" text="Confirm password" /></label>
										<div class="col-md-5">
											<input id="repeatnewpassword" name="repeatnewpassword"
												type="password" placeholder="" class="form-control input-md"
												required>
										</div>
									</div>
									<!-- Button -->
									<div class="form-group">
										<label class="col-md-4 control-label" for="changepassnow"></label>
										<div class="col-md-4">
											<button id="changepassnow" name="changepassnow"
												class="btn btn-primary">
												<spring:message code="i18n.save" text="Save" />
												<span id="cpnr"></span>
											</button>
										</div>
									</div>
								</fieldset>
								<input type="hidden" name="${_csrf.parameterName}"
									value="${_csrf.token}" />
									<input type="hidden" id="tkn" name="token"
									value="${token}" />
									<input type="hidden" id="eml" name="email" value="${mail}" />
							</form>
						</c:if>
						<c:if test="${nullToken eq 'tokenIsNull'}">
cc
						</c:if>
					</div>
					<div class="row">
						<div class="col-md-2 well">
							<ol class="list-inline text-center">
								<li><a
									href="${pageContext.servletContext.contextPath}/login"
									class="btn btn-sm btn-primary"><spring:message
											code="i18n.signin" text="Login" /></a></li>
								<li><a
									href="${pageContext.servletContext.contextPath}/user/registration"
									class="btn btn-sm btn-primary"><spring:message
											code="i18n.register" text="Register" /></a></li>
							</ol>
							<div class="container text-center">
								<%@ include file="followus.jsp"%>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<%@ include file="footer.jsp"%>
</body>
</html>