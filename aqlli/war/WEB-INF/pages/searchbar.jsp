<div style="background-color: white; height: 75px; margin-top: 50px;">
			<ul class="col-md-12">
				<li class="search">
					<form class="search-container" action="/users">
						<input id="search-box" type="text" class="search-box"
							name="searchbox-input"
							placeholder="<spring:message code="i18n.search" text="Search"/>" />
						<label for="search-box"><span
							class="fa fa-search search-icon"></span></label><input type="submit"
							id="search-submit" /> <input type="hidden"
							name="${_csrf.parameterName}" value="${_csrf.token}" />
					</form>
					<div id="searchresult" style="position:relative; z-index:9999; margin-top: 10px; margin-left: 6px;"></div>
				</li>
				
			</ul>
		</div>