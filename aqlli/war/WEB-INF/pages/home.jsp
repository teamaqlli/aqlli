<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page session="true"%>
<%@ include file="/WEB-INF/pages/include/htmlLangTag.jsp"%>
<html lang="${lng}">
<head>
<meta charset="utf-8">
<title>${user.fullName} | <spring:message code="i18n.home"
		text="home" /></title>
<link rel="apple-touch-icon"
	href="http://static.aqlli.com/icons/touch-icon-iphone.png">
<link rel="apple-touch-icon" sizes="76x76"
	href="http://static.aqlli.com/icons/touch-icon-ipad.png">
<link rel="apple-touch-icon" sizes="120x120"
	href="http://static.aqlli.com/icons/touch-icon-iphone-retina.png">
<link rel="apple-touch-icon" sizes="152x152"
	href="http://static.aqlli.com/icons/touch-icon-ipad-retina.png">
<link href="http://static.aqlli.com/scripts/css/footer.css" media="all"
	rel="stylesheet">
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta NAME="ROBOTS" CONTENT="NOINDEX,NOFOLLOW, NOARCHIVE">
<meta NAME="GOOGLEBOT" CONTENT="NOARCHIVE">
<META NAME="COPYRIGHT" CONTENT="&copy; 2015 Aqlli">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<meta name="robots" content="NOODP">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="description" content="">
<link rel="publisher" href="//plus.google.com/b/106910419300047477169" />
<link rel="icon" sizes="any" mask
	href="http://static.aqlli.com/icons/favicon.svg">
<link rel="canonical"
	href="${requestScope['javax.servlet.forward.request_uri']}">
<link rel="shortcut icon"
	href="http://static.aqlli.com/icons/favicon.ico" type="image/x-icon" />
<link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"
	media="all" rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="http://static.aqlli.com/scripts/css/leftSideMenu.css" media="all"
	rel="stylesheet">
<link rel="stylesheet" type="text/css" media="all"
	href="http://static.aqlli.com/scripts/css/bootstrap.css">
<script src="http://static.aqlli.com/scripts/js/classie.js"></script>
<script data-main="/aqlli/js/app" src="/aqlli/js/require.js"></script>
<link href="http://static.aqlli.com/scripts/css/styles.css" media="all"
	rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="http://js.api.here.com/v3/3.0/mapsjs-ui.css" />
<style>
.carousel-page {
	width: 100%;
	height: 200px;
	background-color: #5f666d;
	color: white;
}

.carousel-page img {
	width: 30%;
	margin: 0 auto;
}

#mapfollowers {
	position: absolute;
	height: 100%;
	width: 100%;
	z-index: 1;
	margin-left: -15px;
	-moz-box-shadow: 3px 3px 20px 1px #656565;
	-webkit-box-shadow: 3px 3px 20px 1px #656565;
	-o-box-shadow: 3px 3px 20px 1px #656565;
	box-shadow: 3px 3px 20px 1px #656565;
	filter: progid:DXImageTransform.Microsoft.Shadow(color=#656565,
		Direction=135, Strength=20);
}
.w{
    overflow:hidden;
    margin-bottom:10px;
}
section#homehints div {
    background: white;
    float: left;
    height: 100px;
    margin: 1%;
    width: 30%;
}
section#homehints {
    margin:-1%;
}
.w div i{
margin-right: 5px;
}
.inb{
display:block;
}
.inb span{
display:block;
margin-top:-15px;
}

.gradient-pattern {
  -webkit-box-sizing: content-box;
  -moz-box-sizing: content-box;
  box-sizing: content-box;
  width: 100%;
  height: 100%;
  border: none;
  font: normal normal bold 0.5px/normal Arial, Helvetica, sans-serif;
  color: rgba(255,255,255,1);
  text-align: center;
  -o-text-overflow: clip;
  text-overflow: clip;
  background: -webkit-radial-gradient(circle, rgba(209,207,207,1) 0, rgb(35,29,24) 25%, rgba(0,0,0,0) 25%, rgba(0,0,0,0) 100%), -webkit-radial-gradient(circle, rgb(63,57,50) 25%, rgba(0,0,0,0) 25%), -webkit-radial-gradient(circle, rgb(35,29,24) 25%, rgba(0,0,0,0) 25%), -webkit-radial-gradient(circle, rgb(63,57,50) 25%, rgba(0,0,0,0) 25%), #085083;
  background: radial-gradient(circle, rgba(209,207,207,1) 0, rgb(35,29,24) 25%, rgba(0,0,0,0) 25%, rgba(0,0,0,0) 100%), radial-gradient(circle, rgb(63,57,50) 25%, rgba(0,0,0,0) 25%), radial-gradient(circle, rgb(35,29,24) 25%, rgba(0,0,0,0) 25%), radial-gradient(circle, rgb(63,57,50) 25%, rgba(0,0,0,0) 25%), #085083;
  background-position: 3em 3em, 3em 4em, auto auto, 0 1em;
  -webkit-background-origin: padding-box;
  background-origin: padding-box;
  -webkit-background-clip: border-box;
  background-clip: border-box;
  -webkit-background-size: 4em 4em;
  background-size: 4em 4em;
  overflow: hidden!important;
}
</style>
</head>

<body style="overflow-x: hidden; overflow-y: auto;">
	<div id="fb-root"></div>
	<%@ include file="/WEB-INF/pages/include/noScript.jsp"%>
	<%@ include file="leftSideMenu.jsp"%>
	<%@ include file="topNavBar.jsp"%>
	<div id="wrapper">
		<div class="section" id="maincontentholder">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-12">
						<div class="jumbotron">
							<div class="section">
								<div>
									<div class="row">
										<div class="col-md-6">
											<div class="w ww text-center">
											    <div><i class="fa fa-line-chart fa-lg"></i><span>Overview</span></div>
												<section id="homehints">
													<div><span class="inb usergenfields"><i class="fa fa-users"></i><span><spring:message
													code="i18n.followers" text="Followers" /></span><span>${nbFollowers}</span></span></div>
													<div><span class="inb usergenfields"><i class="fa fa-globe"></i><span><spring:message code="i18n.overview.city.connections" text="City connections"/></span><span>${nbUniqueCities}</span></span></div>
													<div><span class="inb usergenfields"><i class="fa fa-eye"></i><span><spring:message code="i18n.views" text="Views"/></span><span>${nbViews}</span></span></div>
												</section>
											</div>
										</div>
										<div class="col-md-6" style="min-height: 300px;">
											<div id="mapfollowers" data-map-show="${data_map_show}" data-map-country="${data_map_country}" data-map-lat="${data_map_lat}" data-map-lng="${data_map_lng}" data-map-city="${data_map_city}" data-map-usrname="${data_map_usrname}" data-map-usrid="${data_map_usrid}"><span hidden="" style="padding:10px; position: absolute;top: 40%;left: 0;
    right: 0; max-width:100% width:100% color:white; font: normal normal bold 18.5px/normal Arial, Helvetica, sans-serif;">Please add a city to your profile to see your popularity on the map</span>
										</div>
											</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- debut -->
				<div class="row">
					<div class="col-sm-3">
						<div class="well break">
							<span class="text-center"> Besties </span>
							<ul class="list-group">
								<li class="list-group-item">Best 1</li>
								<li class="list-group-item">Best 2</li>
								<li class="list-group-item">Best 3</li>
							</ul>
						</div>
						<div class="well break">
							<span class="text-center"> Recommendations </span>
							<ul class="list-unstyled">
								<li class=" ">Reco 1</li>
								<li class=" ">Reco 2</li>
								<li class=" ">Reco 3</li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6 well">
						<div>Content</div>
						<div class="pull-right ">
							<ul class="list-inline ">
								<li><a
									class="btn btn-primary btn-xs
                                    "><i
										class="fa fa-fw fa-angle-left "></i>Prev</a></li>
								<li><a
									class="btn btn-primary btn-xs
                                    ">Next<i
										class="fa fa-angle-right fa-fw "></i></a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="text-center well break">
							<span><spring:message code="i18n.sponsored"
									text="Sponsored" /> &nbsp;<i class="fa fa-bullhorn"></i></span>
							<div id="ad1">Publicité 1</div>
							<div id="ad2">Publicité 2</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<%@ include file="footer.jsp"%>
	<!-- move HERE Map call here -->
</body>

</html>