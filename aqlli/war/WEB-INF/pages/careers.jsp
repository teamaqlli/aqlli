<%@ page contentType="text/html; charset=UTF-8"%>
<%@page session="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ include file="/WEB-INF/pages/include/htmlLangTag.jsp"%>
<!DOCTYPE html>
<html lang="${lng}">
<head>
<meta charset="utf-8">
<title><spring:message code="i18n.title.career"
		text="Join the company" /></title>
<link rel="apple-touch-icon" href="http://static.aqlli.com/icons/touch-icon-iphone.png">
<link rel="apple-touch-icon" sizes="76x76" href="http://static.aqlli.com/icons/touch-icon-ipad.png">
<link rel="apple-touch-icon" sizes="120x120" href="http://static.aqlli.com/icons/touch-icon-iphone-retina.png">
<link rel="apple-touch-icon" sizes="152x152" href="http://static.aqlli.com/icons/touch-icon-ipad-retina.png">
<link href="http://static.aqlli.com/scripts/css/footer.css" media="all"
	rel="stylesheet">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta content="follow,index" name="robots">
<meta name="robots" content="NOODP">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="description" content="<spring:message code="i18n.meta.career" text="Find the job that suit you at Aqlli and help us bring more innovation and simplicity to our users"/>">
<meta name="author" content="Arix OWAYE">
<!-- fb -->
<meta property="og:title" content="<spring:message code="i18n.title.career"
		text="Join the company" />"/>
<meta property="og:type" content="website"/>
<meta property="og:image" content="http://static.aqlli.com/icons/touch-icon-ipad-retina.png"/>
<meta property="og:url" content="${requestScope['javax.servlet.forward.request_uri']}"/>
<meta property="og:description" content="<spring:message code="i18n.meta.career" text="Find the job that suit you in Aqlli and help us bring more innovation and simplicity to our users"/>"/>
<meta property="fb:admins" content="100005426680123"/>
<meta property="og:site_name" content="Aqlli"/>
<!-- tw -->
<meta name="twitter:card" content="summary">
<meta name="twitter:url" content="${requestScope['javax.servlet.forward.request_uri']}"/>
<meta name="twitter:title" content="<spring:message code="i18n.title.career"
		text="Join the company" />"/>
<meta name="twitter:description" content="<spring:message code="i18n.meta.career" text="Find the job that suit you at Aqlli and help us bring more innovation and simplicity to our users"/>"/>
<meta name="twitter:image" content="http://static.aqlli.com/icons/touch-icon-ipad-retina.png"/>
<meta name="twitter:site" content="@aqlli_fr"/>
<meta name="twitter:creator" content="Arix OWAYE"/>
<!-- g+ -->
<meta itemprop="name" content="<spring:message code="i18n.title.career"
		text="Join the company" />"/>
<meta itemprop="description" content="<spring:message code="i18n.meta.career" text="Find the job that suit you at Aqlli and help us bring more innovation and simplicity to our users"/>"/>
<meta itemprop="audience" content="Web users"/>
<meta itemprop="image" content="http://static.aqlli.com/icons/touch-icon-ipad-retina.png"/>
<meta itemprop="alternativeHeadline" content="Careers | Aqlli"/>
<link rel="publisher" href="//plus.google.com/b/106910419300047477169"/>
<link rel="icon" sizes="any" mask href="http://static.aqlli.com/icons/favicon.svg">
<link rel="canonical" href="${requestScope['javax.servlet.forward.request_uri']}">
<link rel="shortcut icon" href="http://static.aqlli.com/icons/favicon.ico"
	type="image/x-icon" />
<link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"
	media="all" rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="http://static.aqlli.com/scripts/css/leftSideMenu.css" media="all"
	rel="stylesheet">
<link rel="stylesheet" type="text/css" media="all"
	href="http://static.aqlli.com/scripts/css/bootstrap.css">
	<script src="http://static.aqlli.com/scripts/js/classie.js"></script>
<script data-main="/aqlli/js/app" src="/aqlli/js/require.js"></script>
<link href="http://static.aqlli.com/scripts/css/styles.css" media="all"
	rel="stylesheet">
	<style>
	
.msgContainer{
margin-top:60px;
}
#msgContainer > div.col-md-3{
margin-right:3px;
margin-left:3px;
}	
	
#goBackBtn{
display:block;
margin-bottom:8px;
}
ol.list-inline li {
	width: 100%;
	margin-bottom: 5px;
}
ol.list-inline li a.btn {
	width: 100%;
}

.sponsored{
margin-top:8px;
margin-bottom:8px;
}
	 	
	</style>
</head>

<body>
	<div id="fb-root"></div>
	<div id="wrapper">
		<%@ include file="topNavBar.jsp"%>
		<%@ include file="/WEB-INF/pages/include/noScript.jsp"%>
	    <%@ include file="leftSideMenu.jsp"%>
		<div class="section msgContainer" id="maincontentholder">
			<div class="container col-md-9">
				<div class="container well">
					<div>
						<p class="text-center">
								<span><spring:message code="i18n.career.msg" text="There is no position available for now. Check here next time for new opportunities."/></span>
						</p>
					</div>
				</div>
			</div>
				
				<div class="col-md-3 well">
				<ol class="list-inline text-center">
					<li><a
						href="${pageContext.servletContext.contextPath}/login"
						class="btn btn-sm btn-primary"><spring:message code="i18n.signin"
								text="Login" /></a></li>
					<li><a
						href="${pageContext.servletContext.contextPath}/user/registration"
						class="btn btn-sm btn-primary"><spring:message code="i18n.register"
								text="Register" /></a></li>
				</ol>
				<div class="container text-center">
				  <%@ include file="followus.jsp"%>
								
				</div>
			</div>
			
			</div>
			
		</div>
		<%@ include file="footer.jsp"%>
</body>
</html>