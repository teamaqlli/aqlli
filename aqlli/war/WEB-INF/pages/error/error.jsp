<%@ page contentType="text/html; charset=UTF-8"%>
<%@page session="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<html>
<head>
<meta charset="utf-8">
<title><spring:message code="i18n.title.errorpage"
		text="Wrong page" /> | Aqlli</title>
<meta content="nofollow,noindex" name="robots">
<meta name="robots" content="NOODP">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="description" content="<spring:message code="i18n.meta.errorpage" text="Error, something went wrong. We could not find the page you are asking"/>">
<meta name="author" content="Arix OWAYE">
<link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"
	media="all" rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="http://static.aqlli.com/scripts/css/leftSideMenu.css" media="all"
	rel="stylesheet">
<link rel="stylesheet" type="text/css" media="all"
	href="http://static.aqlli.com/scripts/css/bootstrap.css">
	<script src="http://static.aqlli.com/scripts/js/classie.js"></script>
<script data-main="/aqlli/js/app" src="/aqlli/js/require.js"></script>
<link href="http://static.aqlli.com/scripts/css/footer.css" media="all"
	rel="stylesheet">
<link href="http://static.aqlli.com/scripts/css/styles.css" media="all"
	rel="stylesheet">
	<style>
	.errorpage{
    z-index: -1;
    width:100%; 
  }
#msgContainer{
margin-top:100px;
}
#msgContainer > div.col-md-3{
margin-right:3px;
margin-left:3px;
}
#goBackBtn{
display:block;
margin-bottom:8px;
}
#goBackBtn button i{
color:white;
}
ol.list-inline li {
	width: 100%;
	margin-bottom: 5px;
}
ol.list-inline li a.btn {
	width: 100%;
}
#oops{
font-size: 170%;
}
#oops span{
display: block;
margin-top:30px;
margin-bottom:30px;
}
#oops span i{
font-size:1000%;
}
.sponsored{
margin-top:8px;
margin-bottom:8px;
}
	 	
	</style>
</head>

<body class="errorpage">
	<div id="fb-root"></div>
	<%@ include file="/WEB-INF/pages/include/noScript.jsp"%>
	<%@ include file="/WEB-INF/pages/topNavBar.jsp"%>
	<div id="wrapper">	
		<div class="section" id="maincontentholder">
			<%@ include file="/WEB-INF/pages/leftSideMenu.jsp"%>
			<div class="col-md-12" id="msgContainer">
             <div class="col-md-8 text-center" id="oops">
                <spring:message code="i18n.errorpage.something.wrong" text="Oops! We can't find the page you're looking for"/>
                <span><i class="fa fa-frown-o fa-lg"></i></span>
             </div>   
             <div class="col-md-3 alert text-center">
                <span id="goBackBtn" ><spring:message code="i18n.nav.go.back"
										text="Go back" />&nbsp;<button class="btn btn-primary" onclick="goBack()"><i class="fa fa-arrow-left"></i></button></span>
             	<div>
						<ol class="list-inline">
							<li><a href="${pageContext.servletContext.contextPath}/login" class="btn btn-sm btn-primary"><spring:message code="i18n.signin" text="Login"/></a></li>
							<li><a href="${pageContext.servletContext.contextPath}/user/registration" class="btn btn-sm btn-primary"><spring:message code="i18n.register" text="Register"/></a></li>
						</ol>
					</div>
             	<div class="container text-center">
								<span><spring:message code="i18n.follow.us"
										text="Follow us" /></span>
								
								<div class="row text-center" id="socialContainer">
									<div class="col-xs-6 text-center">
										<a href="#"><i class="fa fa-3x fa-fw fa-twitter"></i></a>
									</div>
									<div class="col-xs-6 text-center">
										<a href="#"><i class="fa fa-3x fa-fw fa-facebook"></i></a>
									</div>
								</div>
							</div>
             </div>
             <div class="sponsored"></div>
            </div>

		</div>
		
	</div>
	<%@ include file="/WEB-INF/pages/footer.jsp"%>
</body>
</html>