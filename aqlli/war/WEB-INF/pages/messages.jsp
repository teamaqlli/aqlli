<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page session="true"%>
<%@ include file="/WEB-INF/pages/include/htmlLangTag.jsp"%>
<html lang="${lng}">
<head>
<meta charset="utf-8">
<title><spring:message code="i18n.title.invite" text="Invite" />
	| Aqlli</title>
<link rel="apple-touch-icon"
	href="http://static.aqlli.com/icons/touch-icon-iphone.png">
<link rel="apple-touch-icon" sizes="76x76"
	href="http://static.aqlli.com/icons/touch-icon-ipad.png">
<link rel="apple-touch-icon" sizes="120x120"
	href="http://static.aqlli.com/icons/touch-icon-iphone-retina.png">
<link rel="apple-touch-icon" sizes="152x152"
	href="http://static.aqlli.com/icons/touch-icon-ipad-retina.png">
<link href="http://static.aqlli.com/scripts/css/footer.css" media="all"
	rel="stylesheet">
<meta name="_csrf" content="${_csrf.token}" />
<meta name="_csrf_header" content="${_csrf.headerName}" />

<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta content="follow,index" name="robots">
<meta name="robots" content="NOODP">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="description"
	content="<spring:message code="i18n.meta.invite.desc" text="Invite people to join you at Aqlli the social media that puts you in front!"/>">
<link rel="publisher" href="//plus.google.com/b/106910419300047477169" />
<link rel="icon" sizes="any" mask
	href="http://static.aqlli.com/icons/favicon.svg">
<link rel="canonical"
	href="${requestScope['javax.servlet.forward.request_uri']}">
<link rel="shortcut icon"
	href="http://static.aqlli.com/icons/favicon.ico" type="image/x-icon" />
<link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"
	media="all" rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="http://static.aqlli.com/scripts/css/leftSideMenu.css" media="all"
	rel="stylesheet">
<link rel="stylesheet" type="text/css" media="all"
	href="http://static.aqlli.com/scripts/css/bootstrap.css">
<script src="http://static.aqlli.com/scripts/js/classie.js"></script>
<script data-main="/aqlli/js/app" src="/aqlli/js/require.js"></script>
<link href="http://static.aqlli.com/scripts/css/styles.css" media="all"
	rel="stylesheet">
<style>
#msguserbdy{
  position:fixed;
  bottom:10px;
  rigth:10px;
}
#msguserbdy a i{
  height:100%;
  width:100%;
}
</style>
</head>

<body style="overflow-x: hidden; overflow-y: auto;"
	class="cbp-spmenu-push">
	<div id="fb-root"></div>
	<%@ include file="/WEB-INF/pages/include/noScript.jsp"%>
	<%@ include file="topNavBar.jsp"%>
	<%@ include file="leftSideMenu.jsp"%>
	<div id="wrapper">
		<div class="section" id="maincontentholder">
			<div class="row">
				<div class="col-sm-3">
					<div class="well">vbv</div>
				</div>
				<div class="col-sm-6">
					<div class="well">vbv</div>
				</div>
				<div class="col-sm-3">
					<div class="text-center well break">
						<span><spring:message code="i18n.sponsored"
								text="Sponsored" /> &nbsp;<i class="fa fa-bullhorn"></i></span>
						<div id="ad1">Publicité 1</div>
						<div id="ad2">Publicité 2</div>
					</div>
				</div>
			</div>
		</div>
		<!-- <div id="msguserbdy"><a><i class="fa fa-users fa-lg"></i></a></div>-->
	</div>
	<%@ include file="footer.jsp"%>
</body>
</html>