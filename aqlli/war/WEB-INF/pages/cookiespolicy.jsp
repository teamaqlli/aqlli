<%@ page contentType="text/html; charset=UTF-8"%>
<%@page session="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ include file="/WEB-INF/pages/include/htmlLangTag.jsp"%>
<html lang="${lng}">
<head>
<meta charset="utf-8">
<title><spring:message code="i18n.cookie.use"
		text="Cookies policy" /> | Aqlli</title>
<link rel="apple-touch-icon"
	href="http://static.aqlli.com/icons/touch-icon-iphone.png">
<link rel="apple-touch-icon" sizes="76x76"
	href="http://static.aqlli.com/icons/touch-icon-ipad.png">
<link rel="apple-touch-icon" sizes="120x120"
	href="http://static.aqlli.com/icons/touch-icon-iphone-retina.png">
<link rel="apple-touch-icon" sizes="152x152"
	href="http://static.aqlli.com/icons/touch-icon-ipad-retina.png">
<link href="http://static.aqlli.com/scripts/css/footer.css" media="all"
	rel="stylesheet">
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta content="follow,index" name="robots">
<meta name="robots" content="NOODP">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="description"
	content="<spring:message code="i18n.cookie.use"
		text="Cookies policy" />">
<meta name="author" content="Arix OWAYE">
<link rel="publisher" href="//plus.google.com/b/106910419300047477169" />
<link rel="icon" sizes="any" mask
	href="http://static.aqlli.com/icons/favicon.svg">
<link rel="canonical"
	href="${requestScope['javax.servlet.forward.request_uri']}">
<link rel="shortcut icon"
	href="http://static.aqlli.com/icons/favicon.ico" type="image/x-icon" />
<link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"
	media="all" rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="http://static.aqlli.com/scripts/css/leftSideMenu.css" media="all"
	rel="stylesheet">
<link rel="stylesheet" type="text/css" media="all"
	href="http://static.aqlli.com/scripts/css/bootstrap.css">
<script src="http://static.aqlli.com/scripts/js/classie.js"></script>
<script data-main="/aqlli/js/app" src="/aqlli/js/require.js"></script>
<link href="http://static.aqlli.com/scripts/css/styles.css" media="all"
	rel="stylesheet">
<style>
#msgContainer>div.col-md-3 {
	margin-right: 3px;
	margin-left: 3px;
}

#goBackBtn {
	display: block;
	margin-bottom: 8px;
}

ol.list-inline li {
	width: 100%;
	margin-bottom: 5px;
}

ol.list-inline li a.btn {
	width: 100%;
}

.sponsored {
	margin-top: 8px;
	margin-bottom: 8px;
}
</style>
</head>

<body>
	<div id="fb-root"></div>
	<%@ include file="/WEB-INF/pages/include/noScript.jsp"%>
	<%@ include file="/WEB-INF/pages/topNavBar.jsp"%>
	<%@ include file="/WEB-INF/pages/leftSideMenu.jsp"%>
	<div id="wrapper">
		<div class="row">
			<div class="section" id="maincontentholder">
				<div class="col-md-12" id="msgContainer">
					<div class="col-md-8 alert">
						<div class="well">
							<h4>
								<spring:message code="i18n.cookie.use" text="Cookies policy" />
							</h4>
							<h6>About cookies</h6>

							<p>A cookie is a piece of data that is stored on your device
								(a computer, Smartphone or tablet).
							<p>It does not give access to your computer or any personal
								information about you, however, it can affect your privacy
								depending on your browsing habits.</p>
							<p>
								To learn more about cookies you can visit: &nbsp; <a
									href="https://en.wikipedia.org/wiki/HTTP_cookie"
									target="_blank">Wikipedia - Cookies</a>
							</p>

							<p>In order to use Aqlli websites you must know that it is
								necessary to enable the cookies on your browser.</p>

							<h6>Who use cookies</h6>
							<p>To ensure your privacy Aqlli Websites (including
								Aqlli.com) use cookies to improve user experience and website
								security, to ensure safe transactions, to guarantee the access
								to certain functions (like choosing your prefered language),
								also they help us improve the quality of the website according
								to browsing habits of the users.</p>
							<p>We don't use cookies to track you personally do. Some
								third-party providers like Google for instance may store cookies
								to provide a personalized experience to you on other site.</p>

							<h6>Type of cookies we use</h6>
							<ul>
								<li>
									<p>
										<i>Session Cookies:</i>
									</p>
									<p>We use Them to collect and store data when the user
										accesses Aqlli websites. Data are stored until the user closes
										the browser.</p>
								</li>
								<li><p><i>Advertising cookies:</i>
								</p>
									<p>We use them to manage the advising space and to show you
										relevent ads according to your browsing on Aqlli websites.</p></li>
								<li><p><i>Analytic Cookies:</i>
								</p>
									<p>We use them to monitor and analyze the user's behavior
										(For example to track the number of users per day). It help us
										to provide better services in term of quality. For this
										purpose Aqlli websites use Google Analytics.</p></li>
								<li><p><i>Persistent Cookies:</i>
								</p>
									<p>We use them to persist your session when you access
										Aqlli websites with the "Remember me" functionality.</p>
									<p>It is also used to store your interface settings like
										posts background color for instance.</p></li>
								<li><p><i>Language cookies:</i>
								</p>
									<p>We use Cookies to make you life easier and have the
										websites to be displayed in your prefered language.</p></li>
							</ul>

							<h6>Managing cookies</h6>
							<ul>
								<li>You can view or delete cookies depending on the browser
									you use</li>
								<li>See more here: <a href="http://www.aboutcookies.org/"
									target="_blank">aboutcookies.org</a></li>
							</ul>

						</div>

					</div>
					<div class="col-md-3 alert text-center">
						<div class="well">
							<span id="goBackBtn"><spring:message
									code="i18n.nav.go.back" text="Go back" />&nbsp;
								<button class="btn btn-primary" onclick="goBack()">
									<i class="fa fa-arrow-left"></i>
								</button></span>
							<div>
								<ol class="list-inline">
									<li><a
										href="${pageContext.servletContext.contextPath}/login"
										class="btn btn-sm btn-primary"><spring:message
												code="i18n.signin" text="Login" /></a></li>
									<li><a
										href="${pageContext.servletContext.contextPath}/user/registration"
										class="btn btn-sm btn-primary"><spring:message
												code="i18n.register" text="Register" /></a></li>
								</ol>
							</div>
							<div class="container text-center">
								<%@ include file="followus.jsp"%>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<%@ include file="/WEB-INF/pages/footer.jsp"%>
</body>
</html>