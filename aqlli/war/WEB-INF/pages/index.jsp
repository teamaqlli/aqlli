<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page session="true"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ include file="/WEB-INF/pages/include/htmlLangTag.jsp"%>
<sec:authorize var="isloggedIn" access="isAuthenticated()" />
<c:choose>
	<c:when test="${isloggedIn}">
		<c:redirect url="/home" />
	</c:when>
	<c:otherwise>
	</c:otherwise>
</c:choose>
<!DOCTYPE html>
<html lang="${lng}">
<head>
<meta charset="utf-8">
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta content="follow,index" name="robots">
<meta name="robots" content="NOODP">
<meta name="_csrf" content="${_csrf.token}"/>
<meta name="_csrf_header" content="${_csrf.headerName}"/>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, user-scalable=no">
<link rel="apple-touch-icon"
	href="http://static.aqlli.com/icons/touch-icon-iphone.png">
<link rel="apple-touch-icon" sizes="76x76"
	href="http://static.aqlli.com/icons/touch-icon-ipad.png">
<link rel="apple-touch-icon" sizes="120x120"
	href="http://static.aqlli.com/icons/touch-icon-iphone-retina.png">
<link rel="apple-touch-icon" sizes="152x152"
	href="http://static.aqlli.com/icons/touch-icon-ipad-retina.png">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="apple-mobile-web-app-capable" content="yes">
<!-- fb -->
<meta property="og:title"
	content="<spring:message code='i18n.meta.index.content' text='Welcome to Aqlli | Sign In or register and start Sharing'/>" />
<meta property="og:type" content="article" />
<meta property="og:image"
	content="http://static.aqlli.com/pictures/thumbs/join-aqlli-now.png" />
<meta property="og:url"
	content="${requestScope['javax.servlet.forward.request_uri']}" />
<meta property="og:description"
	content="<spring:message code='i18n.meta.perso.content' text='Aqlli is a social media that help people to keep up with followers and to share content with them.'/>" />
<meta property="fb:admins" content="100005426680123" />
<meta property="og:site_name" content="Aqlli" />
<!-- tw -->
<meta name="twitter:card" content="summary">
<meta name="twitter:url"
	content="${requestScope['javax.servlet.forward.request_uri']}" />
<meta name="twitter:title"
	content="<spring:message code='i18n.meta.index.content' text='Welcome to Aqlli | Sign In or register and start Sharing'/>" />
<meta name="twitter:description"
	content="<spring:message code='i18n.meta.perso.content' text='Aqlli is a social media that help people to keep up with followers and to share content with them.'/>" />
<meta name="twitter:image"
	content="http://static.aqlli.com/pictures/thumbs/join-aqlli-now.png" />
<meta name="twitter:site" content="@aqlli_fr" />
<meta name="twitter:creator" content="@arixowaye" />
<!-- g+ -->
<meta itemprop="name" content="<spring:message code='i18n.meta.index.content' text='Welcome to Aqlli | Sign In or register and start Sharing'/>" />
<meta itemprop="description" content="<spring:message code='i18n.meta.perso.content' text='Aqlli is a social media that help people to keep up with followers and to share content with them.'/>" />
<meta itemprop="audience" content="Web users"/>
<meta itemprop="image"
	content="http://static.aqlli.com/pictures/thumbs/join-aqlli-now.png" />
<meta itemprop="alternativeHeadline" content="Social Media" />
<link rel="publisher" href="//plus.google.com/b/106910419300047477169" />
<link rel="canonical"
	href="${requestScope['javax.servlet.forward.request_uri']}">
<link rel="shortcut icon"
	href="http://static.aqlli.com/icons/favicon.ico" type="image/x-icon" />

<title><spring:message code="i18n.title.index" text="The simplest way to put your profile in front | Aqlli" /></title>
<link href="http://static.aqlli.com/scripts/css/bootstrap.css"
	rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<link href="aqlli/css/animate.min.css" rel="stylesheet">
<link href="aqlli/css/owl.carousel.css" rel="stylesheet">
<link href="aqlli/css/owl.transitions.css" rel="stylesheet">
<link href="aqlli/css/prettyPhoto.css" rel="stylesheet">
<link href="http://static.aqlli.com/scripts/css/footer.css" media="all"
	rel="stylesheet">
<link href="aqlli/css/main.css" rel="stylesheet">
<!--[if lt IE 9]>
    <script src="aqlli/js/jsindex/html5shiv.js"></script>
    <script src="aqlli/js/jsindex/respond.min.js"></script>
    <![endif]-->
<style>
.UlulCountdownContainer div {
	display: inline-block;
	color: #d9534f;
	margin-right: 4px;
	margin-left: 4px;
}

.UlulCountdownContainer div span {
	display: block;
	border-top: 1px solid #085083;
	padding-top: 2px;
	text-align: left;
	font-weight: 100;
}
</style>
</head>
<!--/head-->
<body id="home" class="homepage">
	<%@ include file="/WEB-INF/pages/include/noScript.jsp"%>
	<header id="header">
		<nav id="main-menu" class="navbar navbar-default navbar-fixed-top"
			role="banner">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse"
						data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="/"><img
						src="aqlli/images/logo.png" alt="logo"></a>
				</div>
				<div class="collapse navbar-collapse navbar-right">
					<ul class="nav navbar-nav">
						<li class="scroll active"><a href="#home"><spring:message code="i18n.home"
		text="Home" /></a></li>
						<li class="scroll"><a href="#features"><spring:message code="i18n.features"
		text="Features" /></a></li>
						<li class="scroll"><a href="#about"><spring:message code="i18n.about"
		text="About" /></a></li>
						<li class="scroll"><a href="#meet-team"><spring:message code="i18n.team"
		text="Team" /></a></li>
						<li class="scroll"><a href="#pricing"><spring:message code="i18n.pricing"
		text="Pricing" /></a></li>
						<li class="scroll"><a href="#get-in-touch"><spring:message code="i18n.contact"
		text="Contact" /></a></li>
						<li><a href="/login"><spring:message code="i18n.signin"
		text="Sign In" /></a></li>
					</ul>
				</div>
			</div>
			<!--/.container-->
		</nav>
		<!--/nav-->
	</header>
	<!--/header-->
	<section id="main-slider">
		<div>
			<!-- owl-carousel -->
			<div class="item"
				style="background-image: url(aqlli/images/slider/bg1.jpg);">
				<div class="slider-inner">
					<div class="container">
						<div class="row">
							<div class="col-sm-6">
								<div class="carousel-content">
									<h2>
										<spring:message code="i18n.discover.aqlli.tag" text="Discover an effective social profile"/>
									</h2>
									<p><spring:message code="i18n.discover.aqlli.identity" htmlEscape="false" text="<span>Aqlli</span> is a new social network in beta."/></p>
									<p><spring:message code="i18n.discover.howto.reg" htmlEscape="false" text="You can register for free and start sharing with Aqlli users."/></p>
									<a class="btn btn-primary btn-lg" href="#cta"><spring:message code="i18n.discover.learn.how" htmlEscape="false" text="Learn how"/></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--/.item-->
		</div>
		<!--/.owl-carousel-->
	</section>
	<!--/#main-slider-->

	<section id="cta" class="wow fadeIn">
		<div class="container">
			<div class="row">
				<div class="col-sm-9">
					<h2><spring:message code="i18n.features.setup.free" text="Setup your profile for free"/></h2>
					<p><spring:message code="i18n.date.opening.public" text="Aqlli is not open to public yet"/></p>
				</div>
				<div class="col-sm-3 text-right">
					<a class="btn btn-primary btn-lg" href="#pricing"><spring:message code="i18n.register" text="Sign up"/></a>
				</div>
			</div>
		</div>
	</section>
	<!--/#cta-->
	<section id="features">
		<div class="container">
			<div class="section-header">
				<h2 class="section-title text-center wow fadeInDown">
				<spring:message code="i18n.features.awesome" text="Awesome Features"/></h2>
				<p class="text-center wow fadeInDown">
					&nbsp;
				</p>
			</div>
			<div class="row">
				<div class="col-sm-6 wow fadeInLeft">
					<img class="img-responsive" src="aqlli/images/main-feature.jpg"
						alt="features">
				</div>
				<div class="col-sm-6">
					<div class="media service-box wow fadeInRight">
						<div class="pull-left">
							<i class="fa fa-line-chart"></i>
						</div>
						<div class="media-body">
							<h4 class="media-heading"><spring:message code="i18n.features.audience.moni" text="Simple audience monitoring"/></h4>
							<p><spring:message code="i18n.features.audience.moni.msg" text="Simple as ABC. See if you grow your page with few key points."/></p>
						</div>
					</div>

					<div class="media service-box wow fadeInRight">
						<div class="pull-left">
							<i class="fa fa-map-marker"></i>
						</div>
						<div class="media-body">
							<h4 class="media-heading"><spring:message code="i18n.features.audience.place" text="Know where your audience is"/></h4>
							<spring:message code="i18n.features.audience.place.msg" htmlEscape="false" text="<p>Are you a glabal brand by yourself? Or juste an overly friendly person?</p><p>See on a map how far you shine. Real quick.</p>"/>
							
						</div>
					</div>

					<div class="media service-box wow fadeInRight">
						<div class="pull-left">
							<i class="fa fa-trophy"></i>
						</div>
						<div class="media-body">
							<h4 class="media-heading"><spring:message code="i18n.features.besties" text="Besties"/></h4>
							<spring:message code="i18n.features.besties.msg" htmlEscape="false" text="<p>They support you the most. They are always there to leave a comment, share or a like your content.</p><p>Know them on real time.</p>"/>
						</div>
					</div>

					<div class="media service-box wow fadeInRight">
						<div class="pull-left">
							<i class="fa fa-commenting"></i>
						</div>
						<div class="media-body">
							<h4 class="media-heading"><spring:message code="i18n.features.surf.aqlli" text="Surf Aqlli"/></h4>
							<p><spring:message code="i18n.features.surf.aqlli.msg" text="Post updates and share, message or follow other Aqlli users. Spread the love."/></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="cta2">
		<div class="container">
			<div class="text-center">
				<h2 class="wow fadeInUp" data-wow-duration="300ms"
					data-wow-delay="0ms">
					<spring:message code="i18n.aqlli.upcoming.sn" text="<span>Aqlli</span> IS THE UPCOMING SOCIAL NETWORK" htmlEscape="false"/>
					
				</h2>
				<p class="wow fadeInUp" data-wow-duration="300ms"
					data-wow-delay="100ms">
					<spring:message code="i18n.aqlli.upcoming.sn.calltojoin" text="Join now for free and later you will be able to tell you have been there since day one." htmlEscape="false"/>
				</p>
				<img class="img-responsive wow fadeIn"
					src="aqlli/images/cta2/cta2-img.png" alt="join"
					data-wow-duration="300ms" data-wow-delay="300ms">
			</div>
		</div>
	</section>
	<section id="about">
		<div class="container">
			<div class="section-header">
				<h2 class="section-title text-center wow fadeInDown"><spring:message code="i18n.about.us"
		text="About Us" /></h2>
				<p class="text-center wow fadeInDown">
					<cite> <spring:message code="i18n.about.us.tagline"
		text="You can achieve whatever your mind can imagine." /></cite> - Aqlli
				</p>
			</div>
			<div class="row">
				<div class="col-sm-12 wow fadeInRight">
					<h3 class="column-title">Aqlli</h3>
					<spring:message code="i18n.about.us.tagline.msg" htmlEscape="false"
		text="<p><i>Aqlli</i> has been created by a student. He is actually the unique developer.</p><p>The word Aqlli means cleverness, it comes from Uzbek language.</p><p>The idea came in 2013 when a friend, a student too, said she is writing a lot but she couldn't figure out how to share her work and eventually sell it.</p><p>The ultimate goal of Aqlli is to meet this need.</p>"/>
				</div>
			</div>
		</div>
	</section>
	<!--/#about-->

	<section id="work-process">
      <!-- Empty on purpose -->
	</section>
	<!--/#work-process-->

	<section id="meet-team">
		<div class="container">
			<div class="section-header">
				<h2 class="section-title text-center wow fadeInDown"><spring:message code="i18n.meet.team"
		text="Meet The Team" /></h2>
				<p class="text-center wow fadeInDown">
					<spring:message code="i18n.meet.team.msg"
		text='Aqlli is being developed by a student in his free time. See more at "About" section.' />
				</p>
			</div>

			<div class="row">
				<div class="col-sm-6 col-md-3">
					<div class="team-member wow fadeInUp" data-wow-duration="400ms"
						data-wow-delay="0ms">
						<div class="team-img">
							<img style="width:213px;" class="img-responsive" src="aqlli/images/team/01.jpg" alt="Arix OWAYE">
						</div>
						<div class="team-info">
							<h3>Arix OWAYE</h3>
							<span><spring:message code="i18n.owaye.role"
		text="Creator/Designer/Developer" /></span>
						</div>
						<p><spring:message code="i18n.owaye.msg"
		text="Currently student in MSc MIAGE ( Management and IT)  at University of Bordeaux and also software developer at Orange SA." /></p>
						<ul class="social-icons">
							<li><a href="https://www.facebook.com/profile.php?id=100005426680123"><i class="fa fa-facebook"></i></a></li>
							<li><a href="https://twitter.com/arixowaye"><i class="fa fa-twitter"></i></a></li>
							<li><a href="https://plus.google.com/114769259917982809421"><i class="fa fa-google-plus"></i></a></li>
							<li><a href="https://fr.linkedin.com/in/owaye"><i class="fa fa-linkedin"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="col-sm-6 col-md-3">
					<div class="team-member wow fadeInUp" data-wow-duration="400ms"
						data-wow-delay="100ms">
						<div class="team-img">
							<img class="img-responsive" src="aqlli/images/team/02.jpg" alt="Partner wanted">
						</div>
						<div class="team-info">
							<h3><spring:message code="i18n.partner.wanted"
		text="Partner wanted" /></h3>
							<span><spring:message code="i18n.partnerwanted.role"
		text="UI/UX, mobile developer or Financial Officer" /></span>
						</div>
						<p><spring:message code="i18n.partnerwanted.msg"
		text="I am always keen to working with light minded and creative people like me. Let me know how you can help Aqlli." /></p>
						<ul class="social-icons">
							<li><a href="https://www.facebook.com/aqlliFr"><i class="fa fa-facebook"></i></a></li>
							<li><a href="https://twitter.com/aqlli_fr"><i class="fa fa-twitter"></i></a></li>
							<li><a href="https://plus.google.com/106910419300047477169/about"><i class="fa fa-google-plus"></i></a></li>
						</ul>
					</div>
				</div>
			</div>

			<div class="divider"></div>
		</div>
	</section>
	<!--/#meet-team-->
	<section id="animated-number">
		<div class="container">
			<div class="section-header">
				<h2 class="section-title text-center wow fadeInDown"><spring:message code="i18n.fun.facts"
		text="Fun Facts" /></h2>
				
			</div>

			<div class="row text-center">
				<div class="col-sm-4 col-xs-6">
					<div class="wow fadeInUp" data-wow-duration="400ms"
						data-wow-delay="0ms">
						<div class="animated-number" data-digit="4"
							data-duration="1000"></div>
						<strong><spring:message code="i18n.hint.translators.nat"
		text="4 translators, 4 nationalities" /></strong>
					</div>
				</div>
				<div class="col-sm-4 col-xs-6">
					<div class="wow fadeInUp" data-wow-duration="400ms"
						data-wow-delay="100ms">
						<div class="animated-number" data-digit="${nbUsers}"
							data-duration="1000"></div>
						<strong><spring:message code="i18n.users"
		text="USERS" /></strong>
					</div>
				</div>
				<div class="col-sm-4 col-xs-6">
					<div class="wow fadeInUp" data-wow-duration="400ms"
						data-wow-delay="200ms">
						<div class="animated-number" data-digit="3300"
							data-duration="1000"></div>
						<strong><spring:message code="i18n.aqlli.hometown"
		text="Bordeaux, Aqlli's hometown." /></strong>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--/#animated-number-->

	<section id="pricing">
		<div class="container">
			<div class="section-header">
				<h2 class="section-title text-center wow fadeInDown"><spring:message code="i18n.pricing"
		text="Pricing" /></h2>
				<p class="text-center wow fadeInDown">
				<spring:message code="i18n.pricing.free.package.starter" text="We want you to have the best experience here. Starter package is FREE. Forever!<br>Something more is coming for paid plan. Be sure to follow us or email us to stay up to date." htmlEscape="false"/> 
				</p>
				
			</div>

			<div class="row">
				<div class="col-sm-6 col-md-3">
					<div class="wow zoomIn" data-wow-duration="400ms"
						data-wow-delay="0ms">
						<ul class="pricing">
							<li class="plan-header">
								<div class="price-duration">
									<span class="price"> 0 </span> <span class="duration">
										<spring:message code="i18n.plan.free.forever"
		text="Free. Forever." /></span>
								</div>

								<div class="plan-name"><spring:message code="i18n.starter" text="Starter"/></div>
							</li>
							<li><spring:message code="i18n.starter.message" text="Message your network"/></li>
							<li><spring:message code="i18n.starter.besties" text="See Besties"/></li>
							<li><i
							class="fa fa-facebook fa-lg"></i>&nbsp;<i
							class="fa fa-twitter fa-lg"></i>&nbsp;<i
							class="fa fa-google-plus fa-lg"></i>&nbsp;<spring:message code="i18n.starter.social.btns" text="Follow me buttons"/></li>
							<li><spring:message code="i18n.starter.popularity.map" text="Popularity map"/></li>
							<li><spring:message code="i18n.starter.ads" text="Ads"/></li>
							<li class="plan-purchase"><a class="btn btn-primary"
								href="/user/registration"><spring:message code="i18n.register"
		text="Sign up" /></a></li>
						</ul>
					</div>
				</div>
				<div class="col-sm-6 col-md-3">
					<div class="wow zoomIn" data-wow-duration="400ms"
						data-wow-delay="600ms">
						<ul class="pricing">
							<li class="plan-header">
								<div class="price-duration">
									<span class="price">&nbsp;</span> <span class="duration"><spring:message code="i18n.coming.soon"
		text="Coming soon" /></span>
								</div>

								<div class="plan-name">
									&nbsp;
									<div class=""></div>

								</div>
							</li>
							<li class="plan-purchase"><a class="btn btn-primary"
								href="#pricing"><spring:message code="i18n.coming.soon"
		text="Coming soon" /></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--/#pricing-->
	<section id="get-in-touch">
		<div class="container">
			<div class="section-header">
				<h2 class="section-title text-center wow fadeInDown"><spring:message code="i18n.get.in.touch" text="Get in touch"/></h2>
				<p class="text-center wow fadeInDown">
					<spring:message code="i18n.contact.us.catch.phrase" text="If you have questions or need information, we would be delighted to help you. Just complete the form below and we will answer shortly."/>
				</p>
			</div>
		</div>
	</section>
	<!--/#get-in-touch-->
	<section id="contact">
		<div id="mapbx" style="height: 650px"
			data-latitude="44.83980376613671" data-longitude="-0.580902099609375"></div>
		<div class="container-wrapper">
			<div class="container">
				<div class="row">
					<div class="col-sm-4 col-sm-offset-8">
						<c:import url="/WEB-INF/pages/include/contactUs.jsp" />
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--/#bottom-->
	<%@ include file="footer.jsp"%><!--/#footer-->
	<script src="http://static.aqlli.com/scripts/js/jquery-1.11.2.min.js"></script>
	<script src="http://static.aqlli.com/scripts/js/bootstrap.js"></script>
	<script src="aqlli/js/jsindex/owl.carousel.min.js"></script>
	<script src="https://js.api.here.com/v3/3.0/mapsjs-core.js"></script>
	<script src="//js.api.here.com/v3/3.0/mapsjs-mapevents.js"></script>
	<script src="//js.api.here.com/v3/3.0/mapsjs-ui.js"></script>
	<script src="//js.api.here.com/v3/3.0/mapsjs-service.js"></script>
	<script src="/aqlli/js/lib/usermaphints.js"></script>
	<script src="aqlli/js/jsindex/mousescroll.js"></script>
	<script src="aqlli/js/jsindex/smoothscroll.js"></script>
	<script src="aqlli/js/jsindex/jquery.prettyPhoto.js"></script>
	<script src="aqlli/js/jsindex/jquery.isotope.min.js"></script>
	<script src="aqlli/js/jsindex/jquery.inview.min.js"></script>
	<script src="aqlli/js/jsindex/wow.min.js"></script>
	<script src="aqlli/js/jsindex/main.js"></script>
	<script src="aqlli/js/jsindex/countdown.js"></script>
	<script src="http://static.aqlli.com/scripts/js/shared.js"></script>
	<script src="http://static.aqlli.com/scripts/js/aqLocale.js"></script>
</body>
</html>