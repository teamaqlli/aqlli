<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page session="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ include file="/WEB-INF/pages/include/htmlLangTag.jsp"%>
<!DOCTYPE html>
<html lang="${lng}">
<head>
<meta charset="utf-8">
<title><spring:message code="i18n.legal"
		text="Legal center" /> | Aqlli</title>
<link rel="apple-touch-icon"
	href="http://static.aqlli.com/icons/touch-icon-iphone.png">
<link rel="apple-touch-icon" sizes="76x76"
	href="http://static.aqlli.com/icons/touch-icon-ipad.png">
<link rel="apple-touch-icon" sizes="120x120"
	href="http://static.aqlli.com/icons/touch-icon-iphone-retina.png">
<link rel="apple-touch-icon" sizes="152x152"
	href="http://static.aqlli.com/icons/touch-icon-ipad-retina.png">
<link href="http://static.aqlli.com/scripts/css/footer.css" media="all"
	rel="stylesheet">
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta content="follow,index" name="robots">
<meta name="robots" content="NOODP">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="description"
	content="<spring:message code="i18n.meta.legal.desc" text="Find out about Aqlli's Terms, cookies policy and privacy policy"/>">
<meta name="author" content="Arix OWAYE">
<link href="http://static.aqlli.com/scripts/css/footer.css" media="all"
	rel="stylesheet">
<link rel="publisher" href="//plus.google.com/b/106910419300047477169" />
<link rel="icon" sizes="any" mask
	href="http://static.aqlli.com/icons/favicon.svg">
<link rel="canonical"
	href="${requestScope['javax.servlet.forward.request_uri']}">
<link rel="shortcut icon"
	href="http://static.aqlli.com/icons/favicon.ico" type="image/x-icon" />
<link href="http://static.aqlli.com/scripts/css/footer.css" media="all"
	rel="stylesheet">
<link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"
	media="all" rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="http://static.aqlli.com/scripts/css/leftSideMenu.css" media="all"
	rel="stylesheet">
<link rel="stylesheet" type="text/css" media="all"
	href="http://static.aqlli.com/scripts/css/bootstrap.css">
<script src="http://static.aqlli.com/scripts/js/classie.js"></script>
<script data-main="/aqlli/js/app" src="/aqlli/js/require.js"></script>
<link href="http://static.aqlli.com/scripts/css/styles.css" media="all"
	rel="stylesheet">
<style>

 
</style>
</head>

<body>
	<div id="fb-root"></div>
	<%@ include file="/WEB-INF/pages/include/noScript.jsp"%>
	<%@ include file="/WEB-INF/pages/topNavBar.jsp"%>
	<%@ include file="/WEB-INF/pages/leftSideMenu.jsp"%>
	<div id="wrapper">
		<div id="maincontentholder">
			<div class="row">
			   <div class="col-md-9">
				 <div class="well">
				  <ul class="list-unstyled text-center" id="legalroot">
				    <li><a href="/legal/terms"><span><spring:message code="i18n.terms" text="Terms"/></span></a></li>
				    <li><a href="/legal/cookies"><span><spring:message code="i18n.cookies" text="Cookies"/></span></a></li>
				    <li><a href="/legal/privacy"><span><spring:message code="i18n.privacy" text="Privacy"/></span></a></li>
				  </ul>
				</div>
				</div>
			<div class="col-md-3 well">
					<ol class="list-inline text-center">
						<li><a href="${pageContext.servletContext.contextPath}/login"
							class="btn btn-sm btn-primary"><spring:message
									code="i18n.signin" text="Login" /></a></li>
						<li><a
							href="${pageContext.servletContext.contextPath}/user/registration"
							class="btn btn-sm btn-primary"><spring:message
									code="i18n.register" text="Register" /></a></li>
					</ol>
					<div class="container text-center">
						<%@ include file="followus.jsp"%>

					</div>
				</div>
			</div>
		</div>
	</div>
	<%@ include file="/WEB-INF/pages/footer.jsp"%>
</body>
</html>