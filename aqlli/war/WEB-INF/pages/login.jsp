<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page session="true"%>
<%@ include file="/WEB-INF/pages/include/htmlLangTag.jsp"%>
<!DOCTYPE html>
<html lang="${lng}">
<head>
<meta charset="utf-8">
<title><spring:message code="i18n.signin"
		text="Sign In" /> | Aqlli</title>
		<link rel="apple-touch-icon" href="http://static.aqlli.com/icons/touch-icon-iphone.png">
<link rel="apple-touch-icon" sizes="76x76" href="http://static.aqlli.com/icons/touch-icon-ipad.png">
<link rel="apple-touch-icon" sizes="120x120" href="http://static.aqlli.com/icons/touch-icon-iphone-retina.png">
<link rel="apple-touch-icon" sizes="152x152" href="http://static.aqlli.com/icons/touch-icon-ipad-retina.png">
<link href="http://static.aqlli.com/scripts/css/footer.css" media="all"
	rel="stylesheet">
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta content="follow,index" name="robots">
<meta name="robots" content="NOODP">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="description"
	content="<spring:message code="i18n.meta.index.content" text="Welcome to Aqlli | Sign In or register and start Sharing"/>">
<!-- fb -->
<meta property="og:title" content="<spring:message code="i18n.meta.index.content" text="Welcome to Aqlli | Sign In or register and start Sharing"/>"/>
<meta property="og:type"  content="article"/>
<meta property="og:image" content="http://static.aqlli.com/pictures/thumbs/join-aqlli-now.png"/>
<meta property="og:url"   content="${requestScope['javax.servlet.forward.request_uri']}"/>
<meta property="og:description" content="<spring:message code="i18n.meta.perso.content" text="Aqlli is a social media that help people to keep up with followers and to share content with them."/>"/>
<meta property="fb:admins" content="100005426680123"/>
<meta property="og:site_name" content="Aqlli"/>
<!-- tw -->
<meta name="twitter:card" content="summary">
<meta name="twitter:url" content="${requestScope['javax.servlet.forward.request_uri']}"/>
<meta name="twitter:title" content="<spring:message code="i18n.meta.index.content" text="Welcome to Aqlli | Sign In or register and start Sharing"/>"/>
<meta name="twitter:description" content="<spring:message code="i18n.meta.perso.content" text="Aqlli is a social media that help people to keep up with followers and to share content with them."/>"/>
<meta name="twitter:image" content="http://static.aqlli.com/pictures/thumbs/join-aqlli-now.png"/>
<meta name="twitter:site" content="@aqlli_fr"/>
<meta name="twitter:creator" content="Arix OWAYE"/>
<!-- g+ -->
<meta itemprop="name" content="<spring:message code="i18n.meta.index.content" text="Welcome to Aqlli | Sign In or register and start Sharing"/>"/>
<meta itemprop="description" content="<spring:message code="i18n.meta.perso.content" text="Aqlli is a social media that help people to keep up with followers and to share content with them."/>"/>
<meta itemprop="audience" content="Web users"/>
<meta itemprop="image" content="http://static.aqlli.com/pictures/thumbs/join-aqlli-now.png"/>
<meta itemprop="alternativeHeadline" content="Social Media"/>
<link rel="publisher" href="//plus.google.com/b/106910419300047477169"/>
<link rel="canonical" href="${requestScope['javax.servlet.forward.request_uri']}">
<link rel="shortcut icon" href="http://static.aqlli.com/icons/favicon.ico"
	type="image/x-icon" />
<link href="http://static.aqlli.com/scripts/css/footer.css" media="all"
	rel="stylesheet">
<link rel="stylesheet" type="text/css" media="all"
	href="http://static.aqlli.com/scripts/css/bootstrap.css">
<link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"
	media="all" rel="stylesheet">
<!--[if lt IE 9]>
  <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script data-main="/aqlli/js/app" src="/aqlli/js/require.js"></script>
<link href="http://static.aqlli.com/scripts/css/styles.css" media="all"
	rel="stylesheet">
<style>
.error {
	padding: 15px;
	margin-top: -10%;
	margin-bottom: 20px;
	border: 1px solid transparent;
	border-radius: 4px;
	color: #a94442;
	background-color: #f2dede;
	border-color: #ebccd1;
	width: 60%;
	margin-left: 20%;
	text-align: center;
}

.msg {
	padding: 15px;
	margin-bottom: 20px;
	margin-top: -10%;
	border: 1px solid transparent;
	border-radius: 4px;
	color: #3c763d;
	background-color: #dff0d8;
	border-color: #3c763d;
	width: 60%;
	margin-left: 20%;
	text-align: center;
	
}

#login-box {
	width: 70%;
	padding: 20px;
	margin: 30px auto;
	margin-top: 230px;
	vertical-align: middle;
}

body {
	background-color: #F5F5F5;
}

#aqbeta > img {
  width:150px;
  height:auto;
  margin-left:auto;
  margin-right:auto;
  display: block;
  margin-bottom:-100px;
}
#aqbeta {

}
#maincontentholder{
width:100%;
}

</style>
</head>

<body draggable="false" onload='document.loginForm.inputEmail.focus();'>
	<div id="fb-root"></div>
	<div id="wrapper">
		<%@ include file="topNavBar.jsp"%>
		<div class="container" id="maincontentholder">
		<div class="row">
				<div class="col-md-12">
					<div>
		 <span id="aqbeta"><img src="http://static.aqlli.com/pcitures/aqlli_alpha.png" draggable="false"/></span>
		</div>
					<div id="login-box">
						<c:if test="${not empty error}">
							<div class="error">
								<span class="sr-only"><spring:message code="i18n.error" text="Error:"/></span>
								<span><spring:message code="i18n.${error}" /></span>
							</div>
						</c:if>
						<c:if test="${msg eq 'LoggedOutSuccessfully'}">
							<div class="msg">
								<span class="sr-only"><spring:message code="i18n.message" text="Message:"/></span>
								<span><spring:message code="i18n.${msg}" text="You have been successfully logged out."/></span>
							</div>
						</c:if>
						<c:if test="${msg eq 'pwdupdok'}">
							<div class="msg">
								<span class="sr-only"><spring:message code="i18n.message" text="Message:"/></span>
								<span><spring:message code="i18n.${msg}" text="Password changed. You can now sign In."/></span>
							</div>
						</c:if>
						<form role="form" name='loginForm'
							action="${pageContext.servletContext.contextPath}/j_spring_security_check"
							method='POST'
							class="form-horizontal visible-lg visible-md visible-sm visible-xs">
							<div class="form-group">
								<label class="control-label" for="inputEmail"><spring:message
										code="i18n.email" text="Email" /></label>
								<div class="input-group">
									<span class="input-group-addon">@</span> <input tabindex="0"
										class="form-control" id="inputEmail"
										placeholder="<spring:message code="i18n.email" text="Email"/>"
										type="email" name="username"
										oninvalid="setCustomValidity('<spring:message code="i18n.message.invalid.field" text="This field is invalid" />')"
										oninput="setCustomValidity('')" required/>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label" for="inputPassword"><spring:message
										code="i18n.password" text="Password" /></label> <input tabindex="1" name="password"
									class="form-control" id="inputPassword"
									placeholder="<spring:message code="i18n.password" text="Password"/>"
									type="password"
									oninvalid="setCustomValidity('<spring:message code="i18n.message.empty.field" text="This field can't be empty" />')"
									oninput="setCustomValidity('')" required>
							</div>
							<div class="form-group" draggable="false">
								<div class="col-sm-12">
									<div class="checkbox form-group">
										<label> <input tabindex="2" type="checkbox"
											name="_spring_security_remember_me" /> <spring:message
												code="i18n.remember.me" text="Remember me" /></label> <span
											class="pull-right" id="register-from-login"><a
											href="${pageContext.servletContext.contextPath}/user/registration"><spring:message
													code='i18n.register' text="Sign up" /></a></span>
									</div>
								</div>
								<div class="col-sm-12">
									<span class="help-block form-group"><a tabindex="3" 
										href="${pageContext.servletContext.contextPath}/login/help"><i
											class="fa fa-question-circle"></i>&nbsp; <spring:message
												code='i18n.help' text="Help" /></a></span>
								</div>
								<button tabindex="4" type="submit" class="btn btn-primary">
									<spring:message code="i18n.signin" text="Sign in" />
								</button>
							</div>
							<input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" />
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
 <%@ include file="footer.jsp"%>
</body>
</html>