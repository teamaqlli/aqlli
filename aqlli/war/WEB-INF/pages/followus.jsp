<span><spring:message code="i18n.follow.us" text="Follow us" /></span>
								
								<div class="row text-center" id="socialContainer">
									<div class="col-xs-6 text-center">
										<a href="http://twitter.com/aqlli_fr" target="_blank"><i class="fa fa-3x fa-fw fa-twitter"></i></a>
									</div>
									<div class="col-xs-6 text-center">
										<a href="http://facebook.com/aqlliFr" target="_blank"><i class="fa fa-3x fa-fw fa-facebook"></i></a>
									</div>
								</div>